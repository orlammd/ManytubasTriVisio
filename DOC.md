[SOURCES]
Les sources sont rangés dans les dossiers Chapitre X ou Common.
Les sources de Miraye sont nommées m_chX-n
Les sources de films sont nommées f_chX-n
Les sources dans les TV (pubs, jingle...) sont nommées p_chX-n
n est incrémenté par dessus films|miraye|tv

Le jingle JC est dans Common, et est nommé p_jc


[GROUPES]
Les slides des télés sont nommées plane_horn_i
A chaque début de chapitre des groupes sont créés pour associés contenant (plane_horn_i) et contenu (p_chX-n) dans un slide tvi.
C'est tvi qu'on manipule à la fin.

Les slides de miraye sont groupés de cette manière :
m_iraye
|-> m_layout (cadre)
|-> m_chX-n (films)
C'est m_iraye qu'on manipule à la fin.

Les slides de films sont groupés de cette manière :
f_ilm
|-> f_arabesques
      |-> f_arabesque_1
      |-> f_arabesque_2      
|-> f_chX-n
C'est f_ilm qu'on manipule à la fin.

Quand il y a deux films, un deuxième groupe est fabriqué
f_ilm_2
|-> f_arabesques_2
      |-> f_arabesque_3
      |-> f_arabesque_4      
|-> f_chX-n

Un calque ne peut pas appartenir à deux groupes.

Le groupe lights contient les lumières du haut à gauche et à droite. On peut les baisser en mettant leur alpha à 0.3.

Les slides de trijc sont groupés de cette manière :
trijc
|-> trijc_socle
|-> trijc_head
|-> trijc_tarte
|-> trijc_souffle

t_trijc (tool)
|-> t_trijc_tuba
|-> t_trijc_aimant
|-> t_trijc_compas
|-> t_trijc_aspi
|-> t_trijc_lustre

ot_trijc (other tools)
|-> ot_trijc_taser



[MÉTHODES]
toutes les méthodes commencent par pytaVSL.method
où method =
miraye_in : lance un film de miraye avec le t_trijc_tuba
movie_in : lance un film avec le tuba
trijc_io : fait rentrer ou sortir trijc
trijc_change_tool : change l'outil de trijc
trijc_turn_lights : allume ou éteint la transparence du slide f_ilm
aspi_slide : aspire une slide avec l'aspirateur (nécessite trijc sorti)
shaking_slide : fait vibrer un slide
shaking_tvs : automatisation de shake pour les télés
signs_io : sort ou rentre les panneaux du haut

m_switch_video : passer d'une vidéo à l'autre m_iraye
f_switch_video : idem f_ilm

pytaVSL.f_switch_video('f_ch1-1', 'f_ch1-2'):

m_noisy_switch_video
f_noisy_switch_video : idem avec brouillage au moment de la bascule

pytaVSL.m_noisy_switch_video('m_ch1-2', 'm_ch1-3', 3)

jc_jingle_io : fait rentrer ou sortir le jingle

v_hackboat_io : bateau pirate



*pytaVSL*
- set
pytaVSL.set('slide_name', 'property', value)
exemples :
pytaVSL.set('m_iraye', 'rotate_z', 180)
pytaVSL.set('m_iraye', 'position_x', 0.2)
pytaVSL.set('m_iraye', 'position', 0.2, 0.3, -10)
pytaVSL.set('m_iraye', 'scale', 0.2, 0.3)

pytaVSL.get : récupère une propriété d'un calque

- animate
pytaVSL.animate('slide_name', 'property', init_value, dest_value, duration, 's', easing)
exemples :
pytaVSL.animate('m_iraye', 'rotate_z', 180, 0, 0.2, 's', 'random')
pytaVSL.animate('m_iraye', 'scale', [0.2, 0.3], [1, 1], 1, 's')

pytaVSL.animate('m_iraye', 'scale', [0.2, 0.3], [1, 1], 1, 's', loop=True)

pytaVSL.animate('m_iraye', 'scale', [0.2, 0.3], [1, 1], 1, 's', loop=True, easing='mirror-elastic-inout')

- stop_animate
pytaVSL.stop_animate('slide_name', 'poperty')



[CONDUITE]
Chaque élément de conduite est défini comme une fonction :


@pedalboard_button(X) X = 1-24 si on doit appuyer dessus 100 -> ??? si c'est juste pour tester
def nom_de_la_fonction(self):
  """
  Description
  """
  instructions

*Instructions*
soit on les met à la suite :
pytaVSL.set()
pytaVSL.animate()
...

-> toutes les instructions ont lieu en même temps

soit on les met dans une scène (les noms de scène doivent être unique) :
self.start_scene('sequence/nom_de_la_scene', lambda: [
  pytaVSL.set(),
  pytaVSL.animate(,,, 8, 's'),
  self.wait(8, 's')
  instruction_suivante
])
