from sys import argv

from mentat import Engine, Module

# from .raysession import RaySession
from .openstagecontrol import OpenStageControl
from .pedalboard import PedalBoard
from .transport import Transport
#from .microtonality import MicroTonality
from .postprocess import PostProcess
#from .calfmonosynth import CalfMonoSynth, CalfPitcher
#from .autotune import Autotune
#from .klick import Klick
#from .sooperlooper import SooperLooper
#from .loop192 import Loop192
#from .seq192 import Seq192
from .nonmixer import NonMixer
#from .nonmixers import *
#from .zynaddsubfx import ZynAddSubFx, ZynPart
from .tap192 import Tap192
#from .mk2minilab import Mk2Control, Mk2Keyboard
#from .jmjkeyboard import JmjKeyboard
#from .joystick import Joystick
from .pytaVSL import PytaVSL
from .qlcplus import QlcPlus


"""
Engine
"""
engine = Engine('Mentat', 2001, '/home/jeaneudes/OrageOTournage/ManytubasTriVisio/Mentat', tcp_port=55001, debug='--debug' in argv)
#raysession = RaySession('RaySession', 'osc', 2000)


"""
Controllers
"""
openstagecontrolKeyboardOut = Module('OpenStageControlKeyboardOut', 'midi')
openstagecontrol = OpenStageControl('OpenStageControl', 'osc', 3000)
pedalboard = PedalBoard('PedalBoard', 'osc', 3001)

"""
VJing
"""
pytaVSL = PytaVSL('pytaVSL', 'osc', 4000)

"""
Lights
"""
qlcplus = QlcPlus('qlcplus', 'osc', 7700)

"""
Mixers
"""
inputs = NonMixer('Inputs', 'osc', 10000)
outputs = NonMixer('Outputs', 'osc', 10001)

"""
Samplers
"""
mainSampler = Tap192('MainSampler', 'osc', 11040)

"""
Miscellaneous
"""
transport = Transport('Transport')
postprocess = PostProcess('PostProcess')
