from mentat import Module

class QlcPlus(Module):
    """
    DMX Controller
    """

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)


        self.add_parameter('faces', '/faces', 'i', default=0)
        self.add_parameter('contres', '/contres', 'i', default=0)
        self.add_parameter('bulbes', '/bulbes', 'i', default=0)
        self.add_parameter('d_doah', '/dec/doah', 'i', default=0)
        self.add_parameter('d_je', '/dec/je', 'i', default=0)
        self.add_parameter('d_sal', '/dec/sal', 'i', default=0)
        self.add_parameter('bt_doah', '/bt/doah', 'i', default=0)
        self.add_parameter('bt_je', '/bt/je', 'i', default=0)
        self.add_parameter('bt_sal', '/bt/sal', 'i', default=0)
        self.add_parameter('bt_centrale', '/bt/centrale', 'i', default=0)
        self.add_parameter('decor', '/decor', 'i', default=0)

        self.scenes = {
            'full_stage': {
                'faces': 255,
                'contres': 255,
                'bulbes': 255,
                'd_doah': 0.8 * 255,
                'd_je': 0.8 * 255,
                'd_sal': 0.8 * 255,
                # 'bt_doah': ,
                # 'bt_je': ,
                # 'bt_sal': ,
                # 'bt_centrale': ,
                # 'decor':
            },

            'half_stage': {
                'faces': 127,
                'contres': 255,
                'bulbes': 255,
                'd_doah': 0.8 * 255,
                'd_je': 0.8 * 255,
                'd_sal': 0.8 * 255,
                # 'bt_doah': ,
                # 'bt_je': ,
                # 'bt_sal': ,
                # 'bt_centrale': ,
                # 'decor':
            },

            'miraye': {
                'faces': 0,
                'contres': 5/7 * 255,
                'bulbes': 255,
                'd_doah': 0.8 * 255,
                'd_je': 0.8 * 255,
                'd_sal': 0.8 * 255,
                # 'bt_doah': ,
                # 'bt_je': ,
                # 'bt_sal': ,
                # 'bt_centrale': ,
                # 'decor':
            },

            'film': {
                'faces': 0,
                'contres': 2.5/7 * 255,
                'bulbes': 255,
                'd_doah': 0,
                'd_je': 0,
                'd_sal': 0,
                # 'bt_doah': ,
                # 'bt_je': ,
                # 'bt_sal': ,
                # 'bt_centrale': ,
                # 'decor':
            },

            'blackout': {
                'faces': 0,
                'contres': 0,
                'bulbes': 0,
                'd_doah': 0,
                'd_je': 0,
                'd_sal': 0,
                # 'bt_doah': ,
                # 'bt_je': ,
                # 'bt_sal': ,
                # 'bt_centrale': ,
                # 'decor':
            },

            'botcave': {
                'faces': 0,
                'contres': 0.5 / 0.7 * 255,
                'bulbes': 0,
                'd_doah': 0,
                'd_je': 0,
                'd_sal': 0,
                # 'bt_doah': ,
                # 'bt_je': ,
                # 'bt_sal': ,
                # 'bt_centrale': ,
                # 'decor':
            },
        }

    def set_scene(self, scene, duration=3, decoupes=True):
        # Generaux
        for f in self.scenes[scene]:
            if not f.startswith('d_') or (f.startswith('d_') and decoupes):
                self.animate(f, None, self.scenes[scene][f], duration, 's')

    def set_strobe(self, fixture, on=True, min=0, max=255, duration=0.5, easing='linear-mirror'):
        if on:
            self.animate(fixture, min, max, duration, 's', easing, loop=True)
        else:
            self.stop_animate(fixture)
            self.animate(fixture, None, min, duration, 's')
