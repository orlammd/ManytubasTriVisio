from mentat import Module
from random import random as _rand

class Tap192(Module):
    """
    Sample player
    """

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.kits = []

        self.ready = False
        self.pending_kit = None

        if self.name == 'ProdSampler':
            self.add_parameter('kit', '/kit/select', 's', default='s:Snapshat')
            self.send('/setup/get/kits_list', 'Plagiat')

    def set_kit(self, name):
        """
        Set active kit by name

        **Parameters**

        - `name`: name of kit (as written in Tap192)
        """

        if self.ready:

            if name in self.kits:

                name = 's:' + name
                self.set('kit', name, force_send=True)
                self.logger.info('switched to kit %s' % name)

            else:

                self.logger.error('kit %s not found' % name)

        else:

            self.pending_kit = name
            self.logger.info('not ready yet: set_kit() call deffered')

    def pan_follow(self, obj, position):
        '''
        Suivi panoramiques pour les sons associés à quelques objets
        '''
        pan_coefs = {
            'bato_h': {
                'batohacker_move_arrivee': 0.65,
                'batohacker_punch': 0.65,
                'batohacker_plug': 0.65,
                'batohacker_unplug': 0.65,
                'batohacker_plug_pop': 0.65,
                'batohacker_warp_in': 0.65,
                'batohacker_warp_middle': 0.65,
                'batohacker_warp_out': 0.65,
                'batohacker_debevier': 0.65,
                'batohacker_film_stopped': 1,
                'batohacker_trp_io': 0.65,
                'batohacker_swallow': 0.65,
                'batohacker_swallow_out': 0.65,
                'f_ch4_truck_beep': 0.65,
                'f_ch4_truck_open': 0.65,
                'f_ch4_truck_empty': 0.65,
            },

            'bt': {
                'bascule_ep': 0.65,
                'courrier_ep': 0.65,
                'alarm_ep': 0.65,
                'batohacker_unplug': 0.65,
                'batohacker_plug_pop': 0.65,
                'batohacker_warp_in': 0.65,
                'batohacker_warp_middle': 0.65,
                'batohacker_warp_out': 0.65,
                'batohacker_debevier': 0.65,
                'batohacker_film_stopped': 1,
                'batohacker_trp_io': 0.65,
                'batohacker_swallow': 0.65,
                'batohacker_swallow_out': 0.65,
            }
        }

        if obj in pan_coefs.keys():
            for so in pan_coefs[obj].keys():
                if position < 0.3 and position > -0.3:
                    pan = 0
                else:
                    pan = position/abs(position) * pan_coefs[obj][so]

                self.send('/instrument/set/pan', 's:' + so, pan)



####### MANYTUBAS
    def aspi_slide(self):
        self.send('/instrument/play', 's:aspi', 100)

####### Batohacker
    def batohacker_punch(self, pitch=1):
        self.send('/instrument/play', 's:batohacker_punch', 100, pitch)
        self.start_scene('sequence/bato_h_film_punched', lambda: [
            self.wait(0.3, 's'),
            self.batohacker_film_stopped()
        ])

    def batohacker_plug(self, duration=1):
        self.send('/instrument/play', 's:batohacker_plug', 100, 1 / duration)
        self.start_scene('sequence/bh_plug', lambda: [
            self.wait(duration - 0.1, 's'),
            self.send('/instrument/play', 's:batohacker_plug_pop', 100)
        ])

    def batohacker_unplug(self, duration=0.67):
        self.send('/instrument/play', 's:batohacker_plug_pop', 100)
        self.start_scene('sequence/bh_plug', lambda: [
            self.wait(1 / 6 , 's'),
            self.send('/instrument/play', 's:batohacker_unplug', 100, 1 / duration)
        ])

    def batohacker_film_stopped(self, stopped = False):
        if stopped:
            self.send('/instrument/stop', 's:batohacker_film_stopped')
        else:
            self.send('/instrument/play', 's:batohacker_film_stopped', 100)

    def batohacker_trp_io(self, duration=1):
        self.send('/instrument/play', 's:batohacker_trp_io', 100, 2 / duration)

    def batohacker_debevier(self):
        self.send('/instrument/play', 's:batohacker_debevier', 100)

    def batohacker_move(self, duration=1, goto='arrival'):
        # tt = 0
        # first = True
        # while tt < duration:
        #     if first == True:
        #         self.send('/instrument/play', 's:batohacker_move_short', 100, 1 / duration)
        #         first = False
        #     elif duration - tt < 2 / 3 * duration - 0.02 and duration - tt > 2 / 3 * duration + 0.02:
        #         self.send('/instrument/play', 's:batohacker_move_long', 100, 1 / duration)
        #         self.send('/instrument/play', 's:batohacker_move_arrivee', 100)
        #         self.send('/instrument/stop', 's:batohacker_move_short')
        #     self.start_scene('sequence/bh_move_wait', lambda: [
        #         self.wait(0.01, 's')
        #     ])
        #     tt = tt + 0.01
        if goto=='arrival':
            self.start_scene('sequence/bh_move_arrivee', lambda: [
                self.wait(duration, 's'),
                self.send('/instrument/play', 's:batohacker_move_arrivee', 100)
            ])
        elif goto=='midtime':
            self.start_scene('sequence/bh_move_arrivee', lambda: [
                self.wait(duration / 2, 's'),
                self.send('/instrument/play', 's:batohacker_move_arrivee', 100)
            ])
        else:
            self.send('/instrument/play', 's:batohacker_move_arrivee', 100)


    def batohacker_warp(self, warp_in=True):
        if warp_in:
            self.send('/instrument/play', 's:batohacker_warp_in', 100)
            self.start_scene('sequence/batohacker_warp', lambda: [
                self.wait(14 / 30, 's'),
                self.send('/instrument/play', 's:batohacker_warp_middle', 100)
            ])
        else:
            self.send('/instrument/play', 's:batohacker_warp_out', 100)
            self.send('/instrument/stop', 's:batohacker_warp_middle')

    def batohacker_swallow(self, duration=1, avale=True):
        out = ''
        if not avale:
            out = 'out'
        self.start_scene('sequence/batohacker_swallow_audio', lambda: [
            self.send('/instrument/play', 's:batohacker_swallow' + out, 100),
            self.wait(duration, 's'),
            self.send('/instrument/stop', 's:batohacker_swallow' + out)
        ])

###### EP
    def bascule_ep(self, duration = 1):
        self.send('/instrument/play', 's:bascule_ep', 100, 1 / duration)

    def allume_ep(self, on='alarm'):
        if on == 'courrier':
            self.send('/instrument/play', 's:courrier_ep', 100)
        elif on == 'alarm':
            self.send('/instrument/play', 's:alarm_ep', 100)
        else:
            self.send('/instrument/stop', 's:alarm_ep')

###### Trijc
    def trijc_io(self, direction='in', duration=0.5, easing='linear'):
        def opt_inout(play=True):
            if easing.startswith('elastic'):
                if play:
                    self.send('/instrument/play', 's:trijc_' + direction + '_elastic', 100)
                else:
                    self.send('/instrument/stop', 's:trijc_' + direction + '_elastic')
            elif duration < 1.01:
                if play:
                    self.send('/instrument/play', 's:trijc_' + direction + '_short', 100)
                else:
                    self.send('/instrument/stop', 's:trijc_' + direction + '_short')
            else:
                if play:
                    self.send('/instrument/play', 's:trijc_' + direction + '_short', 100, 1 / duration)
                else:
                    self.send('/instrument/stop', 's:trijc_' + direction + '_short')


        opt_inout()
        self.send('/instrument/play', 's:trijc_io', 100, 1 / duration)
        self.start_scene('sequence/trijc_io_audio', lambda: [
            self.wait(duration, 's'),
            self.send('/instrument/stop', 's:trijc_io'),
            opt_inout(play=False)
        ])

    def trijc_aimant(self, duration, on=True):
        if on:
            self.send('/instrument/play', 's:trijc_aimant', 100, 0.5 / duration)
        else:
            self.send('/instrument/stop', 's:trijc_aimant')

    def trijc_compas(self, duration, on=True):
        if on:
            if duration < 1:
                self.send('/instrument/play', 's:trijc_compas', 100, 0.67 / duration)
            else:
                self.send('/instrument/play', 's:trijc_compas', 100)
                # self.start_scene('sequence/trijc_compas_audio', lambda: [
                #     self.send('/instrument/play', 's:trijc_compas_long', 100),
                #     self.wait(duration, 's'),
                #     self.send('/instrument/stop', 's:trijc_compas_long')
                # ])
        else:
            self.send('/instrument/stop', 's:trijc_compas')
            self.send('/instrument/stop', 's:trijc_compas_long')

    def trijc_turn_lights(self, duration=1):
        self.start_scene('sequence/trijc_turn_lights_audio', lambda: [
            self.send('/instrument/play', 's:trijc_lustre_in', 100),
            self.wait(0.123, 's'),
            self.send('/instrument/play', 's:trijc_lustre_middle', 100),
            self.wait(duration - 0.123 - 0.1163, 's'),
            self.send('/instrument/play', 's:trijc_lustre_out', 100),
            self.send('/instrument/stop', 's:trijc_lustre_middle'),
        ])

###### Wings and TV
    def flying_wings(self, type='tv'):
        if type == 'tv':
            self.send('/instrument/play', 's:tv_sound', 100)
        elif type == 'notv':
            self.send('/instrument/play', 's:wings', 100)
        else:
            self.send('/instrument/stop', 's:tv_sound')
            self.send('/instrument/stop', 's:wings')

    def jc_jingle_io(self, duration):
        self.send('/instrument/play', 's:tv_sound', 100)
        self.start_scene('sequence/jc_jingle_io_audio', lambda: [
            self.wait(duration, 's'),
            self.send('/instrument/stop', 's:tv_sound')
        ])

    def jc_jingle_in(self):
        self.send('/instrument/play', 's:tv_sound', 100)


###### MIRAYE
    def miraye_in(self):
        self.start_scene('sequence/miraye_in_audio', lambda: [
            self.wait(0.3, 's'),
            self.send('/instrument/play', 's:tuba_out', 100),
            self.wait(0.5, 's'),
            self.send('/instrument/play', 's:nagra_start', 100),
            self.wait(2.33, 's'),
            self.send('/instrument/play', 's:nagra_middle', 100),
            self.send('/instrument/stop', 's:nagra_start'),
        ])

    def miraye_out(self):
        self.send('/instrument/play', 's:nagra_stop', 100)
        self.send('/instrument/stop', 's:nagra_middle')


###### FILM
    def movie_in(self):
        self.start_scene('sequence/movie_in_audio', lambda: [
            self.wait(0.3, 's'),
            self.send('/instrument/play', 's:tuba_out', 100),
        ])

    def noisy_switch(self, duration):
        self.start_scene('sequence/actes_jc_audio', lambda:[
            self.wait(duration/4, 's'),
            self.send('/instrument/play', 's:noisy_switch', 100),
            self.wait(duration/2, 's'),
            self.send('/instrument/stop', 's:noisy_switch'),
        ])

####### TITLES
    def display_title(self, on=True):
        pass
        # if on:
        #     self.send('/instrument/play', 's:display_title', 100),
        # else:
        #     self.send('/instrument/stop', 's:display_title'),

####### TRACTOBOTTE
    def tracto_exhaustive(self):
        def gas():
            while True:
                if _rand() > 0.7:
                    self.send('/instrument/play', 's:f_ch5_acceleration_tractobotte', 100)
                self.wait(1, 's')

        self.start_scene('sequence/gas_audio', gas)

    def tracto_ecrase(self, duration_ecrase, duration_montee, duration_mid):
        self.start_scene('sequence/tracto_ecrase_audio', lambda: [
            self.wait(duration_montee + 0.2, 's'),
            self.send('/instrument/play', 's:f_ch5_smashing_tractobotte', 100),
            self.wait(duration_mid, 's'),
            self.send('/instrument/play', 's:f_ch5_smashed_tractobotte', 100),
            self.send('/instrument/stop', 's:f_ch5_smashing_tractobotte'),
        ])

######### SYMPHONIES
    def stop_symphonies(self):
        for sym in [
            'flutes_didj_bottes',
            'flutes_didj_percs_bottes',
            'full',
            'piano_bottes',
            'pianos',
            'soufflants_bottes',
            'soufflants_moinsdidj_bottes',
            'violons_bottes',
            'violons_cb_bottes', 'violons']:
            self.send('/instrument/stop', 's:symphonie_' + sym)



    def route(self, address, args):
        """
        Store kit list sent by Tap192
        """
        if address == '/setup/tap192/kits_list':

            self.kits = args[1:]

            if not self.ready:
                self.ready = True
                if self.pending_kit:
                    self.logger.info('ready now: calling set_kit()')
                    self.set_kit(self.pending_kit)

        return False
