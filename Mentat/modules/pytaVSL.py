from mentat import Module
from random import randint, random as _rand
from os import listdir as _ls
import toml
import time

class Slide(Module):
        """
        PytaVSL Slide
        """

        def __init__(self, *args, **kwargs):

            super().__init__(*args, **kwargs)

            self.ping = False
            self.ready = False
            self.query_done = False

        def query_slide_state(self):
            if self.query_done:
                return
            if not self.ping:
                # ping jusqu'à ce que le slide existe
                self.send('/pyta/slide/%s/ping' % self.name, self.engine.port)
            else:
                # ping ok ? on peut demander les parametres
                self.query_done = True
                self.send('/pyta/slide/%s/get' % self.name, '*', self.engine.port)




class PytaVSL(Module):
    """
    VJing Producer
    """

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.path_to_pyta = '/home/jeaneudes/OrageOTournage/ManytubasTriVisio/PytaVSL'

        self.slides = []

        self.slide_params = ['visible', 'position', 'position_x', 'position_y', 'position_z', 'rotate', 'rotate_x', 'rotate_y', 'rotate_z', 'scale', 'zoom',
            'video_time', 'video_speed', 'video_loop', 'video_end', 'rgbwave', 'noise', 'warp_1', 'warp_2', 'warp_3', 'warp_4', 'fish', 'noise']

        self.send('/pyta/subscribe', 'status', 2001)
        self.send('/pyta/subscribe', 'status', 23456)

        # self.add_event_callback('parameter_changed', self.parameter_changed)

        self.volume_miraye = 0.3
        # Objects JC Manytubas
        ### TODO -> à re-ranger de manière à factoriser trijc, t_trijc et ot_trijc
        self.m_TriJC = ['trijc_socle', 'trijc_tarte', 'trijc_head', 'trijc_souffle']
        self.t_TriJC = ['t_trijc_tuba', 't_trijc_aspi', 't_trijc_aimant', 't_trijc_compas']
        self.ot_TriJC = ['ot_trijc_taser']

        self.TriJC_xinpos = 0
        self.TriJC_xoutoffset = -0.3
        self.t_TriJC_xoffset = -0.415


        self.get_excluded_parameters = [
            'position_x',
            'position_y',
            'position_z',
            'rotate_x',
            'rotate_y',
            'rotate_z',
            'zoom'
        ]

        self.add_parameter('slide_list', address=None, types='s', default='')
        self.add_event_callback('restarting', self.restarting)
        self.add_event_callback('parameter_changed', self.parameter_changed)

        # Mentat restarting
        if self.engine.restarted:
            # reset pyta state but don't make it reload its slides
            # instead, recover slide list in mentat
            self.send('/pyta/slide/*/reset')
            self.load('autosave')
            slide_list = self.get('slide_list').split(',')
            for name in slide_list:
                self.add_submodule(Slide(name, parent=self))

        # Mentat fresh start
        else:
            # remove all slides in pyta
            self.send('/pyta/slide/*/remove')

        self.status = 'ready'

    def restarting(self):
        self.parameters['slide_list'].set(','.join([name for name in self.submodules]))
        self.save('autosave')

    def parameter_changed(self, module, name, value):
        '''
        Pan follow for some moving objects
        '''
        panned_modules = [
            'bato_h',
            'bt',
            'f_ch5-tracto_b'
        ]
        if module.name in panned_modules and name == 'position_x':
            self.engine.modules['MainSampler'].pan_follow(module.name, value)

    def get_miraye_volume(self):
        return self.volume_miraye

    def create_clone(self, src, dest):
        if dest not in self.submodules:
            self.status == 'loading'
            slide = Slide(dest, parent=self)
            self.add_submodule(slide)
            self.send('/pyta/clone', src, dest)
            self.logger.info('Clone ' + dest + ' created from ' + src)


    def create_group(self, group, slides):
        # if len(slides) > 1:
        s = '{'
        i = 0
        while i < len(slides):
            if i > 0:
                s = s + ','
            s = s + slides[i]
            i = i+1
        s = s + '}'
        self.status == 'loading'
        if group not in self.submodules:
            slide = Slide(group, parent=self)
            self.add_submodule(slide)
        else:
            self.submodules[group].reset()
        self.send('/pyta/group', s, group)
        self.logger.info('Group ' + group + ' created with: ' + s)


    def load_slide(self, f):
        """
        Chargement + Suivi d'un nouveau calque
        """

        dir = f.partition('/')[0]
        if dir == 'Common':
            slide_name = f.partition('/')[2].partition('/')[2].partition('.')[0]
        else:
            slide_name = f.partition('/')[2].partition('.')[0]

        slide_name = slide_name.lower()

        if slide_name not in self.submodules:
            self.status == 'loading'
            slide = Slide(slide_name, parent=self)
            self.add_submodule(slide)
            self.send('/pyta/load', f)

    def load_slides_from_dir(self, dir='Common'):
        """
        Chargement des calques
        """

        self.logger.info('load slides from dir: ' + dir)

        if dir == 'Common':
            for d in _ls(self.path_to_pyta + '/' + dir):
                if not d == 'overlay':
                    filelist = _ls(self.path_to_pyta + '/' + dir + '/' + d)
                    for f in filelist:
                        if not f == 'overlay' and not f.endswith('wav'):
                            self.load_slide(dir + "/" + d + "/" + f)
        else:
            filelist = _ls(self.path_to_pyta + '/' + dir)
            for f in filelist:
                if not f == 'overlay' and not f.endswith('wav'):
                    self.load_slide(dir + "/" + f)

    def save_state(self, chapter):
        self.save(chapter + '.overlay', omit_defaults = True)

    def position_overlay(self, overlay='Common', force_send=False):
        """
        Position des éléments de décor
        """
        self.logger.info('Positionning overlay ' + overlay)

        self.load(overlay + '.overlay', force_send=force_send)


########################## TRIJC
    def check_jack_caesar_consistency(self):
        pos = 0
        firstpass = True
        for slide_name in self.m_TriJC:
            if self.get(slide_name, 'position_x') == pos or firstpass == True:
                pos = self.get(slide_name, 'position_x')
                firstpass = False
                return -1
            else:
                self.logger.info('TriJC est dans un état déplorable')
                return 0

    def trijc_io(self, direction='in', tool="tuba", duration=0.5, easing='linear'):

        if self.check_jack_caesar_consistency():
            if direction == 'in':
                end = self.TriJC_xinpos
            elif direction == 'out':
                end = self.TriJC_xoutoffset

            y = self.get('trijc_head', 'position_y')
            t_y = self.get('t_trijc_' + tool, 'position_y')
            t_end = end + self.t_TriJC_xoffset

            self.set('t_trijc_' + tool, 'rotate_z', 0)

            self.start_scene('sequences/triJC_io', lambda: [
                [self.set('trijc*', 'visible', 1), self.set('t_trijc_*', 'visible', 0), self.set('t_trijc_' + tool, 'visible', 1)],
                [self.animate('trijc_*', 'position_x', None, end, duration, 's', easing), self.animate('t_trijc_*', 'position_x', None, t_end, duration, 's', easing)],
                [self.animate('trijc_*', 'position_y', y, y + 0.01, duration/2., 's', 'random'), self.animate('t_trijc_' + tool, 'position_y', t_y, t_y + 0.01, duration/2., 's', 'random')],
                self.wait(duration/2., 's'),
                [self.animate('trijc_*', 'position_y', y + 0.01, y, duration/2., 's', 'random'), self.animate('t_trijc_' + tool, 'position_y', t_y + 0.01, t_y, duration/2., 's', 'random')]
                ]
            )
        else:
            self.logger.info('Aborting TriJC IO animation')

    def trijc_change_tool(self, end_tool):
        """
        Changing the tool used by TriJC
        """
        init_tool = ""
        for slide_name in self.submodules:
            if slide_name.startswith('t_trijc_'):
                if self.get(slide_name, 'visible'):
                    init_tool = slide_name

        self.logger.info(init_tool + ' -> ' + end_tool)
        if end_tool == 'lustre':
            self.set('sub_t_trijc_lustre_potard', 'rotate_z', 0)

        self.start_scene('changing_tool', lambda: [
            self.animate(init_tool, 'rotate_z', None, 90, 0.1, 's'),
            self.wait(0.1, 's'),
            self.animate('t_trijc_' + end_tool, 'rotate_z', 90, 0, 0.2, 's', 'elastic-inout'),
            self.set(init_tool, 'visible', 0),
            self.set('t_trijc_' + end_tool, 'visible', 1),
        ])

    def trijc_turn_lights(self, on='on', duration=1):
        dest = 0 if on == 'off' else 1
        angle = 360 * duration if on == 'off' else -360 * duration
        self.animate('sub_t_trijc_lustre_allume', 'alpha', None, dest, duration)
        self.animate('sub_t_trijc_lustre_potard', 'rotate_z', None, angle, duration)


    def aspi_slide(self, slide_name, warp_1, warp_4, duration):
        """
        Aspire une slide ou plusieurs slides dans l'aspi de trijc
        """
        old_z = self.get(slide_name, 'position_z')

        oscil_d = 2/3 * duration
        away_d = 1/3 * duration
        self.start_scene('sequence/aspi_pub' + slide_name, lambda: [
            self.animate(slide_name, 'rgbwave', None, 0.9, duration, 's', 'exponential-inout'),
            self.animate(slide_name, 'warp_1', None, warp_1, oscil_d, 's', 'elastic-inout'),
            self.animate(slide_name, 'warp_4', None, warp_4, oscil_d, 's', 'elastic-inout'),
            self.wait(0.7 * oscil_d, 's'),
            self.animate(slide_name, 'scale', None, [0.035, 0.035], away_d * 0.9, 's', 'exponential-inout' ),
            self.animate(slide_name, 'position_x', None, -0.33, away_d * 0.95, 's', 'exponential-inout'),
            self.animate(slide_name, 'position_y', None, 0.035, away_d * 0.95, 's', 'exponential-inout'),
            self.animate(slide_name, 'alpha', None, 0.1, away_d * 0.8, 's', 'exponential-out'),
            self.wait(away_d, 's'),
            self.set(slide_name, 'visible', 0),

            self.set(slide_name, 'visible', 0),
            self.submodules[slide_name].reset(),
            self.set(slide_name, 'position_z', old_z),

        ])

########################## TRIJC

########################## SIGNS
    def signs_io(self, direction='in', together=False, duration=1, easing='linear'):
        """
        Remontée et descente des panneaux
        """
        if direction == 'out':
            dest = 0.5
        if direction == 'in':
            dest = 0

        if together:
            self.animate('w_signs*', 'position_y', None, dest, duration, 's', easing)
        else:
            for slide_name in self.submodules:
                if ('w_signs_' in slide_name):
                    sigma = _rand()
                    if sigma > 0.5:
                        easing = 'elastic-inout'
                    self.animate(slide_name, 'position_y', None, dest, (1 + sigma)*duration, 's', easing)


########################## SIGNS

########################## MIRAYE

    def miraye_in(self, filename, duration=1, easing='linear'):
        """
        Having Miraye Leparket starting her storytelling
        """
        '''
            [orig, dest]: x, y, z, zoom, rotate_z
        '''
        orig = {
            "x": -0.38,
            "y": -0.025,
            "z": -10,
            "zo": 0.04,
            "rot": -140
        }
        dest = {
            "x": 0.092,
            "y": 0.016,
            "z": -10,
            "zo": 0.837,
            "rot": -720
        }


##### On peut s'en passer ?
        self.set('m_iraye', 'position', orig['x'], orig['y'], orig['z'])
        self.set('m_iraye', 'scale', orig['zo'], orig['zo'])
        self.set('m_iraye', 'rotate_z', orig['rot'])
##### On peut s'en passer ?


##### A virer
        self.set('m_ch*', 'visible', 0)
        self.set('m_*', 'warp_1', 0, 0)
        self.set('m_*', 'warp_4', 0, 0)
        self.set('m_ch*', 'scale', 0.848, 0.848)
        self.set('m_layout', 'scale', 1, 1)
##### A virer


        climax_y = 0.3
        etape_zoom = 0.4 * dest["zo"]
        move_duration= 3/4 * duration
        zoom_duration = (1 - move_duration) * duration

        self.start_scene('sequences/miraye_in', lambda:[
            self.set(filename, 'audio_volume', 0),
            self.set(filename, 'video_time', 0),
            self.set(filename, 'video_speed', 1, force_send=True),
            self.set(filename, 'visible', 1),
            self.set(filename, 'audio_volume', self.volume_miraye),
            self.set('m_layout', 'visible', 1),
            self.animate('t_trijc_tuba', 'rotate_z', None, -7, 0.4, 's', 'elastic-inout'),
            self.wait(0.2, 's'),
            self.set('m_iraye', 'visible', 1),
            self.animate('m_iraye', 'position_x', None, dest["x"], move_duration, 's', easing),
            self.animate('m_iraye', 'rotate_z', None, dest["rot"], move_duration, 's', easing),
            self.animate('m_iraye', 'scale', None, [etape_zoom, etape_zoom], move_duration, 's', easing),
            self.animate('m_iraye', 'position_y', None, climax_y, move_duration * 1/2, 's', easing),
            self.wait(1/2.*duration, 's'),
            self.animate('m_iraye', 'position_y', None, dest["y"], zoom_duration, 's', easing),
            self.wait(1/4.*duration, 's'),
            self.animate('m_iraye', 'scale', None, [dest["zo"], dest["zo"]], zoom_duration, 's', easing),
            self.animate('t_trijc_tuba', 'rotate_z', None, 0, 0.4, 's', 'random'),
        ])

    def m_noisy_switch_video(self, orig, dest, duration):
        """
        Switching from one video to another in m_layout with a noisy state in between
        """

        w_coef = _rand() * 0.8

        self.start_scene('sequence/' + orig + '_-_' + dest, lambda: [
            self.set(dest, 'rgbwave', w_coef),
            self.set(dest, 'noise', 1.0),
            self.animate(orig, 'noise', None, 1.0, duration / 2, 's'),
            self.animate(orig, 'rgbwave', None, w_coef, duration / 2, 's'),
            self.wait(0.25, 's'),
            self.set(dest, 'audio_volume', 0),
            self.set(dest, 'visible', 1),
            self.set(dest, 'video_time', 0, force_send=True),
            self.set(dest, 'video_speed', 1),
            self.set(dest, 'audio_volume', self.volume_miraye),
            self.set(orig, 'visible', 0),
            self.animate(dest, 'noise', 1.0, 0.0, duration / 2, 's'),
            self.animate(dest, 'rgbwave', None, 0.0, duration / 2, 's'),
            self.set(orig, 'noise', 0),
            self.set(orig, 'rgbwave', 0)
            ]
        )

    def m_switch_video(self, orig, dest):
        """
        Switching from one video to another in m_layout with a noisy state in between
        """

        w_coef = _rand() * 0.8

        self.start_scene('sequence/' + orig + '_-_' + dest, lambda: [
            self.set(dest, 'audio_volume', 0),
            self.set(dest, 'visible', 1),
            self.set(dest, 'video_time', 0, force_send=True),
            self.set(dest, 'video_speed', 1),
            self.set(dest, 'audio_volume', self.volume_miraye),
            self.set(orig, 'visible', 0),
            ]
        )


    def miraye_out(self, duration, easing):
        """
        Having Miraye Leparket stopping her storytelling
        """
        pass

    # def title_scene(self, title, duration):
    #     """
    #     Affiche le titre (scène)
    #     """
    #     segments = title.split(' ')
    #     segments_duration = {}
    #     total_length = len(title)
    #     atom = duration / total_length
    #
    #     # On sépare les mots et on compare leur nombre de lettres
    #     for segment in segments:
    #         segments_duration[segment] = atom * len(segment) / total_length
    #         self.set('titre', 'text', segment)
    #         self.wait(segments_duration[segment], 's')
    #         self.logger.info('titre segment:' + segment)
    #
    #
    #
    def display_title(self, title):
        """
        Affiche le titre
        """
        self.logger.info('Display Title: ' + title)

        # def title_seq():
        #     index = 0
        #     while title + '_' + str(index).zfill(4) in self.submodules:
        #         self.logger.info('Display Title: ' + str(index))
        #         self.set(title, 'sequence_index', index)
        #         index = index + 1
        #         self.wait(1/18, 's')
        #
        # self.start_scene('sequence/title_' + title, title_seq)
        # self.set(title, 'visible', 1)



########################## Miraye

########################## FILM

    def visible(self, f_ch, visible=1, warpzoned=False):
        self.set(f_ch, 'visible', visible)
        if warpzoned and f_ch + '_warpzoned' in self.submodules:
            self.set(f_ch + '_warpzoned', 'audio_volume', 0)
            self.set(f_ch + '_warpzoned', 'visible', visible)


    def video_time(self, f_ch, video_time, warpzoned=False, force_send=True):
        self.set(f_ch, 'video_time', video_time, force_send=force_send)
        if warpzoned and f_ch + '_warpzoned' in self.submodules:
            self.set(f_ch + '_warpzoned', 'video_time', video_time, force_send=force_send)

    def video_speed(self, f_ch, video_speed, warpzoned=False):
        self.set(f_ch, 'video_speed', video_speed)
        if warpzoned and f_ch + '_warpzoned' in self.submodules:
            self.set(f_ch + '_warpzoned', 'audio_volume', 0)
            self.set(f_ch + '_warpzoned', 'video_speed', video_speed)


    def movie_in(self, movie, duration, easing='linear', zoom=0.95, x=0, y=0, z=5, y_arabesque=3.04, init=False, warpzoned=False, extinction=True):
        """
        Having Moving coming to front
        """
        orig = {
            "x": -0.38,
            "y": -0.05,
            "z": 5,
            "zo": 0.3,
            "rot": -140
        }
        dest = {
            "x": x,
            "y": y,
            "z": z,
            "y_arabesque" : y_arabesque,
            "zo": zoom,
            "rot": -720
        }

        if init:
            self.set('f_ilm', 'position', orig['x'], orig['y'], orig['z'])
            self.set('f_ilm', 'scale', orig['zo'], orig['zo'])
            self.set('f_ilm', 'rotate_z', orig['rot'])
            self.set('f_ara*e_[1-2]', 'position_y', 0.005)

        climax_y = 0.3
        etape_zoom = 0.4 * dest["zo"]
        # move_duration= 1/2.5 * duration
        complete_duration = 2.5 * duration
        zoom_duration = (1 - duration) * complete_duration

        def movie_scale():
            self.animate(movie, 'scale', None, [1.0, 1.0], zoom_duration, 's'),
            if warpzoned:
                self.animate(movie + '_warpzoned', 'scale', None, [1.0, 1.0], zoom_duration, 's'),


        def lights_off():
            if extinction:
                self.animate('lights_*', 'alpha', None, 0.3, complete_duration, 's', 'linear'),
                self.animate('bt_main', 'brightness', None, 0.3, complete_duration, 's', 'linear'),
                self.animate('*ep_*', 'brightness', None, 0.3, complete_duration, 's', 'linear'),

        self.start_scene('sequences/movie_in', lambda:[
            # self.set(movie, 'video_time', 0, force_send=True), Pas utile ?
            # self.set(movie, 'video_speed', 1), Pas utile ?

            self.set(movie, 'audio_volume', 0),
            self.visible(movie, warpzoned=warpzoned),

            # remplace :
            # self.set(movie, 'visible', 1),
            # self.set(movie + '_warpzoned', 'visible', 1),


            self.animate('t_trijc_tuba', 'rotate_z', None, -7, 0.4, 's', 'elastic-inout'),
            self.wait(0.2, 's'),
            self.set(movie, 'scale', 1.0, 0),
            self.set('f_arabesques', 'visible', 1),
            self.set('f_ilm', 'visible', 1),


            self.animate('f_ilm', 'position_x', None, dest["x"], duration, 's', easing),
            self.animate('f_ilm', 'rotate_z', None, dest["rot"], duration, 's', easing),
            self.animate('f_ilm', 'scale', None, [dest["zo"], dest["zo"]], duration, 's', easing),
            self.animate('f_ilm', 'position_y', None, climax_y, duration * 1/2, 's', easing),
            self.wait(1/2.*duration, 's'),
            self.animate('f_ilm', 'position_y', None, dest["y"], duration * 1/2, 's', easing),
            self.wait(duration, 's'),

            # self.trijc_change_tool('compas'),

            self.video_time(movie, 0, warpzoned=warpzoned),
            self.set(movie, 'audio_volume', 1),
            # Remplace :
            # self.set(movie, 'video_time', 0),
            # self.set(movie + '_warpzoned', 'video_time', 0),
            # self.set('f_arabesques', 'visible', 1),
            # self.animate('t_trijc_compas', 'rotate_z', None, 45, zoom_duration, 's'),
            movie_scale(),
            self.animate('f_ara*e_1', 'position_y', None, dest["y_arabesque"], zoom_duration, 's'),
            self.animate('f_ara*e_2', 'position_y', None, -dest["y_arabesque"], zoom_duration, 's'),
            self.signs_io('out', together=False, duration=complete_duration),
            self.wait(complete_duration / 2, 's'),
            # self.animate('t_trijc_compas', 'rotate_z', None, 0, 0.1, 's', 'elastic-inout'),
            lights_off(),
            # self.trijc_io('out', 'compas', zoom_duration + 0.5)
            self.trijc_io('out', 'tuba', zoom_duration + 0.5)
        ])

    def f_noisy_switch_video(self, orig, dest, duration, warpzoned = False, f_secu=''):
        """
        Switching from one video to another in f_ilm with a noisy state in between
        """

        w_coef = _rand() * 0.8

        def start_film():
            self.set(dest, 'audio_volume', 0),
            self.visible(dest, 1, warpzoned=warpzoned)
            self.video_time(dest, 0, warpzoned=warpzoned, force_send=True)
            self.video_speed(dest, 1, warpzoned=warpzoned)
            self.set(dest, 'audio_volume', 1),

        def shutdown_film():
            self.visible(orig, 0, warpzoned=warpzoned),
            if not f_secu == '':
                self.visible(f_secu, 0, warpzoned=warpzoned),

        self.start_scene('sequence/' + orig + '_-_' + dest, lambda: [
            self.set(dest, 'rgbwave', w_coef),
            self.set(dest, 'noise', 1.0),
            self.animate(orig, 'noise', None, 1.0, duration / 2, 's'),
            self.animate(orig, 'rgbwave', None, w_coef, duration / 2, 's'),
            self.wait(duration / 2, 's'),
            start_film(),
            shutdown_film(),
            self.animate(dest, 'noise', 1.0, 0.0, duration / 2, 's'),
            self.animate(dest, 'rgbwave', None, 0.0, duration / 2, 's'),
            self.set(orig, 'noise', 0),
            self.set(orig, 'rgbwave', 0)
            ]
        )


    def f_switch_video(self, orig, dest, f_secu='', warpzoned=False):
        """
        Switching from one video to another in f_ilm
        """

        def start_film():
            self.set(dest, 'audio_volume', 0),
            self.visible(dest, 1, warpzoned=warpzoned)
            self.video_time(dest, 0, warpzoned=warpzoned, force_send=True)
            self.video_speed(dest, 1, warpzoned=warpzoned)
            self.set(dest, 'audio_volume', 1),

        def shutdown_film():
            self.visible(orig, 0, warpzoned=warpzoned),
            if not f_secu == '':
                self.visible(f_secu, 0, warpzoned=warpzoned),

        self.start_scene('sequence/' + orig + '_-_' + dest, lambda: [
            # self.set(dest, 'video_time', 0, force_send=True),
            # self.set(dest, 'video_speed', 1),
            # self.set(dest, 'visible', 1),
            start_film(),
            shutdown_film()
            ]
        )


    def movie_out(self, duration, easing):
        """
        Having Movie going back
        """
        pass

    # def movie_split(self, left_slide, left_position=[-0.35, 0.15], left_scale=[0.3, 0.3], right_movie, right_position=[0.1, -0.1], right_scale=[0.6, 0.6], trijc_in_duration=0.2, scale_duration=1, move_duration=1, easing='linear'):
    #     """
    #     Having several movies being split over the screen
    #     """
    #
    #     self.create_group('f_ilm_2', ['f_arabesques_2', right_movie])
    #
    #     climax_y = 0.3
    #     etape_zoom = [0.4 * r_scale for r_scale in right_scale]
    #     # move_duration= 1/2.5 * duration
    #     complete_duration = 2.5 * duration
    #     zoom_duration = (1 - duration) * complete_duration
    #     self.set(right_movie, 'video_time', 0),
    #     self.set(right_movie, 'video_speed', 1),
    #     self.set(right_movie, 'visible', 1),
    #
    #     dest =
    #
    #     self.start_scene('sequence/movie_split', lambda: ([
    #         self.trijc_io('in', 'compas', trijc_in_duration, 'elastic-inout'),
    #         self.wait(trijc_in_duration, 's'),
    #         self.animate('t_trijc_compas', 'rotate_z', None, 10, scale_duration / 2, 's', 'elastic-inout'),
    #         self.animate(left_slide, 'scale', None, [l_scale * 2 for l_scale in left_scale], scale_duration / 2, 's', 'elastic-inout'),
    #         self.wait(scale_duration / 2, 's'),
    #         self.trijc_change_tool('aimant'),
    #         self.animate(left_slide, 'scale', None, left_scale, scale_duration / 2, 's', easing),
    #         self.wait(scale_duration / 2, 's'),
    #         self.animate('t_trijc_aimant', 'rotate_z', None, -45, move_duration, 's'),
    #         self.animate(left_slide, 'position_x', None, left_position[0], move_duration, 's', easing),
    #         self.animate(left_slide, 'position_y', None, left_position[1], move_duration, 's', easing),
    #         self.wait(0.5, 's'),
    #         self.trijc_change_tool('tuba'),
    #
    #
    #
    #         self.animate('t_trijc_tuba', 'rotate_z', None, -7, 0.4, 's', 'elastic-inout'),
    #         self.wait(0.2, 's'),
    #         self.set('f_ilm_2', 'visible', 1),
    #
    #         self.animate('f_ilm', 'position_x', None, dest["x"], duration, 's', easing),
    #         self.animate('f_ilm', 'rotate_z', None, dest["rot"], duration, 's', easing),
    #         self.animate('f_ilm', 'scale', None, [dest["zo"], dest["zo"]], duration, 's', easing),
    #         self.animate('f_ilm', 'position_y', None, climax_y, duration * 1/2, 's', easing),
    #         self.wait(1/2.*duration, 's'),
    #         self.animate('f_ilm', 'position_y', None, dest["y"], duration * 1/2, 's', easing),
    #         self.wait(duration, 's'),
    #
    #         self.trijc_change_tool('compas'),
    #         self.set(movie, 'video_time', 0),
    #         self.animate(movie, 'scale', None, [1.0, 1.0], zoom_duration, 's'),
    #         self.animate('f_arabesque_1', 'position_y', None, dest["y_arabesque"], zoom_duration, 's'),
    #         self.animate('f_arabesque_2', 'position_y', None, -dest["y_arabesque"], zoom_duration, 's'),
    #         self.signs_io('out', together=False, duration=complete_duration),
    #         self.wait(complete_duration / 2, 's'),
    #         self.animate('lights*', 'alpha', None, 0.3, complete_duration, 's', 'linear'),
    #         self.trijc_io('out', 'lustre', zoom_duration + 0.5)
    #     ])

########################## FILM

########################## JINGLES

    def jc_jingle_io(self, origin, duration, easing):
        """
        Having Jack Caesar Jingle dropping in / out
        """
        self.shaking_tv_jc()
        self.flying_wings('wings_jc')

        if origin == 'left':
            self.set('tv_jc', 'position', -1, 0, -12)
        elif origin == 'right':
            self.set('tv_jc', 'position', 1, 0, -12)
        elif origin == 'top':
            self.set('tv_jc', 'position', 0, 1, -12)
        elif origin == 'bottom':
            self.set('tv_jc', 'position', 0, -1, -12)
        else:
            self.logger.info('origine inconnue')

        self.start_scene('jack_caesar_jingle', lambda: [
            self.set('tv_jc', 'video_time', 0),
            self.set('tv_jc', 'visible', 1),
            self.animate('tv_jc', 'position_x', None, 0.09, duration, 's', easing),
            self.animate('tv_jc', 'position_y', None, 0.01, duration / 2, 's', 'random'),
            self.wait(duration / 2, 's'),
            self.animate('tv_jc', 'position_y', None, 0.0, duration  / 2, 's', 'random'),
            self.wait(self.get('p_jc', 'video_end') - duration / 2, 's'),
            self.animate('tv_jc', 'position_x', None, 1, duration, 's', easing),
            self.animate('tv_jc', 'position_y', None, 0.01, duration / 2, 's', 'random'),
            self.wait(duration / 2, 's'),
            self.animate('tv_jc', 'position_y', None, 0.0, duration / 2, 's', 'random'),
            self.wait(duration / 2, 's'),
            self.stop_animate('plane_horn_jc', 'position_x'),
            self.stop_animate('plane_horn_jc', 'position_y'),
            self.stop_animate('p_jc', 'position_x'),
            self.stop_animate('p_jc', 'position_y'),
            self.flying_wings('wings_jc', on=False)
        ])

    def jc_jingle_in(self, origin, duration, easing):
        """
        Having Jack Caesar Jingle dropping in
        """
        self.shaking_tv_jc()
        self.flying_wings('wings_jc')

        if origin == 'left':
            self.set('tv_jc', 'position', -1, 0, -12)
        elif origin == 'right':
            self.set('tv_jc', 'position', 1, 0, -12)
        elif origin == 'top':
            self.set('tv_jc', 'position', 0, 1, -12)
        elif origin == 'bottom':
            self.set('tv_jc', 'position', 0, -1, -12)
        else:
            self.logger.info('origine inconnue')

        self.start_scene('jack_caesar_jingle', lambda: [
            self.set('tv_jc', 'visible', 1),
            self.animate('tv_jc', 'position_x', None, 0.09, duration, 's', easing),
            self.animate('tv_jc', 'position_y', None, 0.01, duration / 2, 's', 'random'),
            self.wait(duration / 2, 's'),
            self.animate('tv_jc', 'position_y', None, 0.0, duration  / 2, 's', 'random'),
        ])


########################## JINGLES

########################## WINGS
    def flying_wings(self, wings, p='',  on=True, duration=4/25):

        def fly():
            index = 0
            while True:
                self.set(wings + '_ailes', 'sequence_index', index)
                self.wait(duration + _rand()/20, 's')
                if index:
                    index = 0
                else:
                    index = index + 1

        if on:
            self.start_scene('sequence/flying_wings_' + wings, fly)
        else:
            self.stop_scene('sequence/flying_wings_' + wings)


########################## WINGS

########################## Tracto
    def tracto_ecrase(self, fch, n=2, y_ref=0, duration=1, duration_montee=1, duration_mid=0.3, success=True):
        init_scale = 0.3/0.8 * self.get('f_ch5-tracto_b', 'scale')[0]
        self.animate('tracto-bottus', 'rotate_z', None, 10, 0.2, 's')
        y_ref = 0 # A remplacer mieux

        n_cock = round(_rand()) + 1
        if success:
            self.set('p_cockpit_success' + str(n_cock), 'visible', 1)
        else:
            self.set('p_cockpit_surpris', 'visible', 1)
        self.start_scene('sequence/tracto_ecrase', lambda: [
            self.animate('tracto-pellus', 'rotate_z', None, 35, duration_montee, 's'),
            self.animate('tracto-bottus', 'position_y', None, 0.12, duration_montee, 's'),
            self.wait(duration_montee, 's'),
            self.set('f_ilm_' + str(n), 'fish', 0.1),
            self.set(fch, 'rgbwave', 0.3),
            self.animate('tracto-pellus', 'rotate_z', None, 10, duration_mid, 's', 'elastic-out'),
            self.animate('tracto-bottus', 'position_y', None, y_ref, duration_mid, 's', 'elastic-out'),
            self.wait(duration_mid, 's'),
            self.animate('f_ilm_' + str(n), 'scale', [init_scale, init_scale], [init_scale, 0], duration, 's', easing='elastic-inout'),
            self.animate('tracto-bottus', 'position_y', None, y_ref - 0.2, duration, 's' ,easing='elastic-inout'),
            self.animate('tracto-pellus', 'rotate_z', 10, -25, duration, 's', easing='elastic-inout'),
            self.wait(duration, 's'),
            self.set('f_ilm_' + str(n), 'fish', 0),
            self.set(fch, 'rgbwave', 0),
            self.wait(duration / 2, 's'),
            self.animate('tracto-pellus', 'rotate_z', None, 10, duration_montee, 's'),
            self.animate('tracto-bottus', 'position_y', None, y_ref, duration_montee, 's'),
            self.set('p_cockpit_success' + str(n_cock), 'visible', 0),
            self.set('p_cockpit_surpris', 'visible', 0)
        ])

    def tracto_tape(self, t_up, t_down, t_rel):
            self.animate('tracto-pellus', 'rotate_z', None, 35, t_up, 's'),
            self.animate('tracto-bottus', 'position_y', None, 0.12, t_up, 's'),
            self.wait(t_up, 's'),
            self.animate('tracto-bottus', 'position_y', None, -0.2, t_down, 's' ,easing='elastic-out'),
            self.animate('tracto-pellus', 'rotate_z', None, -25, t_down, 's', easing='elastic-out'),
            self.wait(t_down, 's'),
            self.animate('tracto-pellus', 'rotate_z', None, 10, t_rel, 's'),
            self.animate('tracto-bottus', 'position_y', None, 0, t_rel, 's'),



    def tracto_kick(self, tv='f_ilm', n=2, duration=0.5, y_ref=0):
        self.start_scene('sequence/tracto_ecrase', lambda: [
            self.animate('tracto-bottus', 'rotate_z', 0, 45, duration, 's', easing='elastic-inout'),
            self.animate('tracto-pellus', 'rotate_z', -25, 10, duration * 0.2 / 0.5, 's', easing='elastic-inout'),
            self.animate('tracto-bottus', 'position_y', y_ref - 0.2, y_ref, duration * 0.2 / 0.5, 's', easing='elastic-inout'),
            self.wait(duration * 0.3 / 0.5, 's'),
            self.animate(tv + str(n), 'offset_x', 0, -0.5, duration * 0.3 / 0.5, 's'),
            self.animate(tv + str(n), 'offset_y', 0, 1, duration * 0.3 / 0.5, 's'),
            self.wait(duration * 0.3 / 0.5 + 0.1, 's'),
            self.set(tv + str(n), 'position_x', -0.5),
            self.set(tv + str(n), 'position_y', 1),
            self.set(tv + str(n), 'offset_x', 0),
            self.set(tv + str(n), 'offset_y', 0),
        ])

    def tracto_kickiteasy(self, duration=0.5, y_ref=0):
        self.start_scene('sequence/tracto_ecrase_vite', lambda: [
            self.animate('tracto-bottus', 'rotate_z', 0, 45, duration, 's', easing='elastic-inout'),
            self.animate('tracto-pellus', 'rotate_z', -25, 10, duration * 0.2 / 0.5, 's', easing='elastic-inout'),
            self.animate('tracto-bottus', 'position_y', y_ref - 0.2, y_ref, duration * 0.2 / 0.5, 's', easing='elastic-inout'),
            self.wait(duration * 0.3 / 0.5, 's'),
            self.animate('tracto-bottus', 'rotate_z', 45, 0, 0.4, 's', easing='elastic-inout'),
            self.animate('tracto-pellus', 'rotate_z', 10, -25, 0.4 * 0.2 / 0.5, 's', easing='elastic-inout'),
            self.animate('tracto-bottus', 'position_y', y_ref, y_ref - 0.2, 0.4 * 0.2 / 0.5, 's', easing='elastic-inout'),

        ])
        #send_osc 4000 /pyta/slide/tracto-bottus/animate rotate_z 0 45 0.5 elastic &&
        #send_osc 4000 /pyta/slide/tracto-pellus/animate rotate_z -25 10 0.2 &&
        #send_osc 4000 /pyta/slide/tracto-bottus/animate position_y -0.2 0 0.2 &&
        #sleep 0.3 &&
        #send_osc 2001 /pyta/slide/f_ilm_2/animate position_x -0.08 -0.5 0.3 &&
        # send_osc 2001 /pyta/slide/f_ilm_2/animate position_y 0.28 1 0.3

    def tracto_defile(self, on=True):

        def avanti():
            while True:
                self.animate('f_ch5-tracto_b', 'offset_x', 0, 0.1, 1, 's', easing='elastic-inout')
                self.wait(_rand()*1.8, 's')
                self.animate('f_ch5-tracto_b', 'offset_x', 0.1, 0, 1, 's', easing='elastic-inout')
                self.wait(_rand()*4, 's')


        self.stop_scene('sequence/tracto_defile')
        if on:
            self.logger.info("défile")
            self.animate('tracto-corps_a', 'sequence_index', 0, 30, 4, 's', 'linear-mirror', loop=True),
            self.animate('tracto-jantus*', 'rotate_z', 0, -1080, 4, 's', 'linear-mirror', loop=True),
            self.animate('back', 'texture_offset', [0, 0], [1, 0], 1.5, 's', 'linear', loop = True) # avant : 10s
            self.animate('back', 'offset_y', None, 0.01, 2, 's', 'random-mirror', loop = True) # avant : 10s
            self.start_scene('sequence/tracto_defile', avanti)
        else:
            self.logger.info("stop")
            self.stop_animate('tracto-corps_a', 'sequence_index'),
            self.stop_animate('tracto-jantus*', 'rotate_z'),
            self.stop_animate('back', 'texture_offset')
            self.animate('back', 'texture_offset', None, [0, 0], 0.1, 's')
        #send_osc 2001 /pyta/slide/f_ch5-tracto_b/animate position_x 0 0.1 10 && send_osc 2001 /pyta/slide/f_ch5-tracto_b/animate offset_x 0 0.1 1 elastic && sleep 1.3 && send_osc 2001 /pyta/slide/f_ch5-tracto_b/animate offset_x 0.1 0 1 elastic
        # send_osc 2001 /pyta/slide/back/animate texture_offset 0 0 1 0 10


    def tracto_exhaustive(self, on=True):
        def gas():
            while True:
                pattern = 3 * _rand()
                d = _rand() + 1.5
                x = _rand()
                # Fumée sort
                self.animate('tracto-explodus_4', 'alpha', 1, 0, 0.2, 's')
                self.set('tracto-explodus_4', 'visible', 0)
                self.wait(0.2, 's')
                if pattern > 2:
                    # Enedys sort
                    self.animate('em_1', 'position_x', 0, 0.08 * x, 0.8, 's', 'elastic-out')
                    self.animate('em_1', 'offset_y', 0, 0.01, 0.8, 's', 'random-mirror')
                    self.wait(1, 's')
                    self.animate('em_1', 'position_x', None, 0, 0.8, 's', 'elastic-out')
                    self.animate('em_1', 'offset_y', 0, 0.01, 0.8, 's', 'random-mirror')
                # elif pattern > 1:
                #     # Langue sort
                #     self.animate('tracto-lingus', 'position_x', None, 0.1 * x, 0.2, 's'),
                #     self.animate('tracto-lingus', 'offset_x', 0, 0.02, 1, 's', 'random-mirror'),
                #     self.animate('tracto-lingus', 'offset_y', 0, 0.02, 1, 's', 'random-mirror'),
                #     self.wait(1, 's'),
                #     self.animate('tracto-lingus', 'position_x', None, -0.05, 0.8, 's', 'elastic-inout'),
                #     self.wait(0.8, 's')
                # Fumée sort
                self.animate('tracto-explodus_4', 'alpha', 0, 1, 0.2, 's')
                self.set('tracto-explodus_4', 'visible', 1)
                self.wait(0.2, 's')
                self.animate('tracto-explodus_4', 'alpha', 0.3, 1, 0.3, 's', 'random-mirror', loop=True)

            self.wait(d, 's')

        # Init EM
        self.set('em_1', 'position_x', 0)
        self.set('em_1', 'position_y', 0.28)
        self.set('em_1', 'rotate_z', -100)
        self.set('em_1', 'visible', 1)
        self.set('em_1', 'scale', 0.2, 0.2)

        # Init fumée
        self.set('tracto-explodus_4', 'position', 0.1, 0.3, -0.28)
        self.set('tracto-explodus_4', 'rotate_z', 0)
        self.set('tracto-explodus_4', 'scale', 0.3, 0.3)

        self.start_scene('sequence/exhaustive_gas', gas)

########################## Tracto

########################## Vanupiés Hacking

    def batohacker_shaking(self, on=True, ld1_max = 10, ld2_max = 10):
        if on:
            ld1 = (_rand() + 1) * ld1_max / 2
            ld2 = (_rand() + 1) * ld2_max / 2
            angle = _rand()*5
            angle_offset = _rand()*2 - 1
            self.animate('bato_h', 'offset_y', None, 0.002, 0.05, 's', easing='random-mirror', loop=True)
            self.animate('bh_head', 'rotate_z', -angle + angle_offset, angle, ld1, 's', easing='linear-mirror', loop=True)
            self.animate('bh_ressort', 'rotate_z', -angle + angle_offset, angle, ld1, 's', easing='linear-mirror', loop=True)
            self.animate('bh_head', 'offset_y', 0, -0.02, ld2, 's', easing='elastic-inout-mirror', loop=True)
            self.animate('bh_ressort', 'offset_y', 0, -0.02, ld2, 's', easing='elastic-inout-mirror', loop=True)
            self.animate('bh_ressort', 'scale', [1.0, 1.0], [1.0, 0.8], ld2, 's', easing='elastic-inout-mirror', loop=True)
        else:
            self.stop_animate('bato_h', 'offset_y')

    def batohacker_hmove(self, x_dest=0.47, proue="jardin", duration=1, sacplein=False):
        """
        Hacking Vanupiés
        """
        coef = 1
        if proue == "jardin":
            coef = 1

        elif proue == "cour":
            coef = -1

        self.set('bato_h', 'scale', coef * abs(self.get('bato_h', 'scale')[0]), self.get('bato_h', 'scale')[1])

        self.start_scene('sequence/batohacker_hmove', lambda: [
            self.animate('bato_h', 'position_x', None, x_dest, duration, 's', easing="elastic-inout"),
            self.animate('t_nobh_sacplein*', 'position_x', None, x_dest + 0.014, duration, 's', easing='elastic-inout'),

            self.animate('bh_jambe_gauche', 'offset_y', 0, 0.04, duration, 's', loop=True, easing='random-mirror'),
            self.animate('bh_jambe_droite', 'offset_y', 0, 0.04, duration, 's', loop=True, easing='random-mirror'),

            self.animate('bh_jambe_gauche', 'offset_x', 0, -0.02, 0.05, 's',  loop=True, easing='random-mirror'),
            self.animate('bh_jambe_droite', 'offset_x', 0, 0.02, 0.05, 's',  loop=True, easing='random-mirror'),
            self.wait(duration/4, 's'),

            self.animate('bh_jambe_gauche', 'offset_x', 0, -0.02, 0.1, 's', loop=True, easing='linear-mirror'),
            self.animate('bh_jambe_droite', 'offset_x', 0, 0.02, 0.1, 's', loop=True, easing='linear-mirror'),
            self.wait(duration/5, 's'),
            self.animate('bato_h', 'offset_y', 0, 0.05, duration/2 - 2*duration/5, 's', easing='linear-mirror'),
            self.animate('t_nobh_sacplein*', 'offset_y', 0, 0.05, duration/2 - 2*duration/5, 's', easing='linear-mirror'),
            self.wait(duration/2 - duration/5, 's'),
            self.animate('bh_jambe_gauche', 'offset_x', 0, -0.02, 0.05, 's', loop=True, easing='random-mirror'),
            self.animate('bh_jambe_droite', 'offset_x', 0, 0.02, 0.05, 's', loop=True, easing='random-mirror'),
            self.wait(duration/4, 's'),
            self.stop_animate('bh_jambe_gauche', 'offset_y'),
            self.stop_animate('bh_jambe_droite', 'offset_y'),
            self.stop_animate('bh_jambe_gauche', 'offset_x'),
            self.stop_animate('bh_jambe_droite', 'offset_x'),
            self.set('bh_jambe_*', 'offset_x', 0),
            self.set('bh_jambe_*', 'offset_y', 0)
        ])

    def batohacker_trp_io(self, direction='out', duration=1):
        d_rallonges = duration / 4
        d_entrailles_1 = duration / 2
        d_entrailles_2 = duration / 4
        self.stop_animate('bh_jambe_gauche', 'offset_x'),
        self.stop_animate('bh_jambe_droite', 'offset_x'),
        self.stop_animate('bh_jambe_gauche', 'offset_y'),
        self.stop_animate('bh_jambe_droite', 'offset_y'),
        self.stop_animate('bh_jambe_gauche', 'rotate_z'),
        self.stop_animate('bh_jambe_droite', 'rotate_z'),


        if direction == 'out':
            self.start_scene('sequence/batohacker_trp_out', lambda: [
                self.animate('bh_boat_bas', 'position_y', None, 0, d_rallonges, 's', easing='elastic-inout'),
                self.animate('bh_jambe_gauche', 'offset_y', None, -0.12, d_rallonges, 's', easing='elastic-inout'),
                self.animate('bh_jambe_droite', 'offset_y', None, -0.1, d_rallonges, 's',easing='elastic-inout'),
                self.animate('bh_jambe_gauche', 'offset_x', None, 0.02, d_rallonges, 's'),
                # self.animate('bh_jambe_gauche', 'position_y', None, self.get('bh_jambe_gauche', 'position-y') - 0.12, d_rallonges, 's', easing='elastic-inout'),
                # self.animate('bh_jambe_droite', 'position_y', None,  self.get('bh_jambe_droite', 'position-x') - 0.1, d_rallonges, 's',easing='elastic-inout'),
                # self.animate('bh_jambe_gauche', 'position_x', None,  self.get('bh_jambe_gauche', 'position-x') + 0.02, d_rallonges, 's'),
                self.animate('bh_jambe_gauche', 'rotate_z', None, 70, d_rallonges, 's'),
                self.animate('bh_jambe_droite', 'rotate_z', None, -100, d_rallonges, 's'),
                self.animate('i_bh_rallonges', 'scale', None, [1.0, 1.0], d_rallonges, 's', easing='elastic-inout'),
                self.animate('i_bh_rallonges', 'position_y', None, 0, d_rallonges, 's', easing='elastic-inout'),
                self.wait(d_rallonges, 's'),
                self.animate('i_bh_entrailles', 'scale', [0.0, 0.0], [0.6, 0.6], d_entrailles_1/3, 's'),
                self.animate('i_bh_entrailles', 'rotate_z', 90, 0, d_entrailles_1, 's'),
                self.animate('i_bh_entrailles', 'position_x', -0.05, 0, d_entrailles_1, 's'),
                self.wait(d_entrailles_1, 's'),
                self.animate('i_bh_entrailles', 'scale', None, [1.0, 1.0], d_entrailles_2, 's', 'elastic-inout'),
                self.wait(d_entrailles_2 - 0.1, 's'),
                # self.set('i_bh_faisceau', 'noise', 0.7),
                # self.set('i_bh_faisceau', 'rgbwave', 0.1),
                # self.animate('i_bh_faisceau', 'scale', None, [0.8, 0.8], 0.4, 's', 'elastic-inout'),
            ])

        else:
            self.start_scene('sequence/batohacker_trp_in', lambda: [
                # self.animate('i_bh_faisceau', 'scale', None, [0.8, 0], 0.2, 's', 'elastic-inout'),

                self.animate('i_bh_entrailles', 'scale', None, [1.0, 1.0], d_entrailles_2, 's', 'elastic-inout'),
                self.wait(d_entrailles_2, 's'),

                # self.set('i_bh_faisceau', 'noise', 0),
                # self.set('i_bh_faisceau', 'rgbwave', 0),


                self.animate('i_bh_entrailles', 'rotate_z', 0, 90, d_entrailles_1, 's'),
                self.animate('i_bh_entrailles', 'position_x', 0, -0.05, d_entrailles_1, 's'),
                self.wait(d_entrailles_1*2/3, 's'),
                self.animate('i_bh_entrailles', 'scale', [0.6, 0.6], [0.0, 0.0], d_entrailles_1/3, 's'),
                self.wait(d_entrailles_1/3, 's'),

                self.animate('bh_boat_bas', 'position_y', None, 0.125, d_rallonges, 's', easing='elastic-inout'),
                self.animate('bh_jambe_gauche', 'offset_y', None, 0, d_rallonges, 's', easing='elastic-inout'),
                self.animate('bh_jambe_droite', 'offset_y', None, 0, d_rallonges, 's',easing='elastic-inout'),
                self.animate('bh_jambe_gauche', 'offset_x', None, 0, d_rallonges, 's'),
                self.animate('bh_jambe_gauche', 'rotate_z', None, 0, d_rallonges, 's'),
                self.animate('bh_jambe_droite', 'rotate_z', None, -35, d_rallonges, 's'),

                self.animate('i_bh_rallonges', 'scale', None, [0, 0], d_rallonges, 's', easing='elastic-inout'),
                self.animate('i_bh_rallonges', 'position_y', None, 0.06, d_rallonges, 's', easing='elastic-inout'),
            ])

    def batohacker_plug(self, on=True, duration=1, allonge=0.6, o_set=0.15, extinction=True):
        if on:
            self.start_scene('sequence/batohacker_plug_on', lambda:[
                self.set('o_bh_coude_droit_1', 'visible', 1),
                self.wait(duration / 2 / 3, 's'),
                self.set('o_bh_coude_droit_2', 'visible', 1),
                self.wait(duration / 2 / 3, 's'),
                self.set('o_bh_droit_1', 'visible', 1),
                self.set('o_bh_embouchure', 'visible', 1),
                self.wait(duration / 2 / 3, 's'),
                self.animate('o_bh_droit_1', 'scale', None, [0.25, allonge], duration/2, 's'),
                self.animate('o_bh_droit_1', 'offset_y', None, - o_set, duration/2, 's'),
                self.animate('o_bh_embouchure', 'offset_y', None, - o_set - 0.06, duration/2, 's'),
                self.wait(duration/2, 's'),
                self.animate('bt_main', 'brightness', None, 1.0, 0.2),
                self.animate('*ep_*', 'brightness', None, 1.0, 0.2),
            ])
        else:
            if extinction:
                self.start_scene('sequence/batohacker_plug_off', lambda:[
                    self.animate('o_bh_droit_1', 'scale', None, [0.25, 0.25], duration/2, 's'),
                    self.animate('o_bh_droit_1', 'offset_y', None, 0, duration/2, 's'),
                    self.animate('o_bh_embouchure', 'offset_y', None, 0, duration/2, 's'),
                    self.wait(duration/8, 's'),
                    self.animate('bt_main', 'brightness', None, 0.3, 1),
                    self.animate('*ep_*', 'brightness', None, 0.3, 1),
                    self.wait(duration * 3/8, 's'),
                    self.set('o_bh_droit_1', 'visible', 0),
                    self.set('o_bh_embouchure', 'visible', 0),
                    self.wait(duration / 2 / 3, 's'),
                    self.set('o_bh_coude_droit_2', 'visible', 0),
                    self.wait(duration / 2 / 3, 's'),
                    self.set('o_bh_coude_droit_1', 'visible', 0),
                ])
            else:
                self.start_scene('sequence/batohacker_plug_off', lambda:[
                    self.animate('o_bh_droit_1', 'scale', None, [0.25, 0.25], duration/2, 's'),
                    self.animate('o_bh_droit_1', 'offset_y', None, 0, duration/2, 's'),
                    self.animate('o_bh_embouchure', 'offset_y', None, 0, duration/2, 's'),
                    self.wait(duration/8, 's'),

                    self.wait(duration * 3/8, 's'),
                    self.set('o_bh_droit_1', 'visible', 0),
                    self.set('o_bh_embouchure', 'visible', 0),
                    self.wait(duration / 2 / 3, 's'),
                    self.set('o_bh_coude_droit_2', 'visible', 0),
                    self.wait(duration / 2 / 3, 's'),
                    self.set('o_bh_coude_droit_1', 'visible', 0),
                ])

    def batohacker_swallow(self, duration, avale=True):
        x = [ -0.02, 0.005, 0.001, 0.063, 0.115, 0.14, 0.14 ]
        y = [ -0.08, -0.32, -0.35, -0.37, -0.46, -0.88, -0.94 ]
        rot = [ 0, 0, 75, 75, 0, 0, 0 ]
        coef_d = [1, 0.5, 0.5, 0.5, 1, 0.5]
        bary = sum(coef_d)

        def on_ze_move(n, d, e='linear'):
            self.animate('o_bh_adam', 'position_x', x[n], x[n+1], d, 's', e)
            self.animate('o_bh_adam', 'position_y', y[n], y[n+1], d, 's', e)
            self.animate('o_bh_adam', 'rotate_z', rot[n], rot[n+1], d, 's', easing='linear')

        def on_ze():
            index = 0
            while index < len(x) - 1:
                on_ze_move(index, coef_d[index] / bary * duration)
                self.wait(coef_d[index] / bary * duration, 's')
                if index == len(x) - 1:
                    self.animate('o_bh_adam', 'alpha', None, 0, 0.2, 's')
                index = index + 1
            self.set('o_bh_adam', 'visible', 0)

        def ze_no_move(n, d, e='linear'):
            self.animate('o_bh_adam', 'position_x', x[n], x[n-1], d, 's', e)
            self.animate('o_bh_adam', 'position_y', y[n], y[n-1], d, 's', e)
            self.animate('o_bh_adam', 'rotate_z', rot[n], rot[n-1], d, 's', easing='linear')

        def ze_no():
            index = len(x) -1
            while index > -1:
                ze_no_move(index, coef_d[index] / bary * duration)
                self.wait(coef_d[index] / bary * duration, 's')
                if index == 0:
                    self.animate('o_bh_adam', 'alpha', None, 0, 0.2, 's')
                index = index - 1
            self.set('o_bh_adam', 'visible', 0)


        if avale:
            self.animate('o_bh_adam', 'alpha', 0.3, 1, 0.4, 's', easing='linear-mirror', loop=True)
            self.set('o_bh_adam', 'position_x', x[0])
            self.set('o_bh_adam', 'position_y', y[0])
            self.set('o_bh_adam', 'rotate_z', rot[0])
            self.set('o_bh_adam', 'scale', 1, 0)
            self.set('o_bh_adam', 'visible', 1)
            self.animate('o_bh_adam', 'scale', [1, 0], [1, 1], 0.3, 's')
            self.animate('i_bh_entrailles', 'offset_y', 0, 0.005, 0.3, 's', 'random-mirror')


            self.start_scene('sequence/batohacker_swallow', on_ze)

        else:
            self.animate('o_bh_adam', 'alpha', 0.3, 1, 0.4, 's', easing='linear-mirror', loop=True)
            self.set('o_bh_adam', 'position_x', x[len(x)-1])
            self.set('o_bh_adam', 'position_y', y[len(x)-1])
            self.set('o_bh_adam', 'rotate_z', rot[len(x)-1])
            self.set('o_bh_adam', 'scale', 1, 0)
            self.set('o_bh_adam', 'visible', 1)
            self.animate('o_bh_adam', 'scale', [1, 0], [1, 1], 0.3, 's')

            self.start_scene('sequence/batohacker_regurgite', ze_no)
            self.start_scene('sequence/batohacker_digestion', lambda: [
                self.wait(duration, 's'),
                self.animate('i_bh_entrailles', 'offset_y', 0, 0.005, 0.3, 's', 'random-mirror')
            ])

    def batohacker_punch(self, f_ch="", x_dest=-0.04):
        self.start_scene('sequence/batohacker_punch', lambda: [
            self.set('t_bh_boxe', 'visible', 1),
            self.animate('t_bh_boxe', 'position_x', None, -0.07, 0.3, 's', 'elastic-out'),
            self.wait(0.2, 's'),
            self.animate('f_ilm', 'position_x', None, x_dest, 0.2, 's', 'elastic-inout'),
            self.set(f_ch, 'color_strobe', 1),
            self.set(f_ch, 'video_speed', 0),
            self.set(f_ch + '_warpzoned', 'video_speed', 0),
            self.wait(0.2, 's'),
            self.set(f_ch, 'color_strobe', 0),
            self.animate('t_bh_boxe', 'position_x', None, 0.036, 0.3, 's'),
            self.wait(0.3, 's'),
            self.set('t_bh_boxe', 'visible', 0),
        ])

    def batohacker_debevier(self):
        self.start_scene('sequence/batohacker_debevier', lambda: [
            self.set('t_bh_debevier*', 'visible', 1),
            self.animate('t_bh_debevier*', 'position_x', None, -0.05, 0.5, 's', 'elastic-out'),
            self.wait(0.4, 's'),
            self.animate('t_bh_debevier*', 'position_x', None, -0.032, 1, 's'),
            self.wait(1, 's'),
            self.set('t_bh_debevier*', 'visible', 0),
        ])


#### Enedys

    def getwhistle_ep(self, on=True, duration = 0.5):
        if on:
            self.set('ep_sifflet_*', 'alpha', 1)
            self.animate('ep_sifflet_*', 'position_x', -5, 0, duration, 's')
            self.set('ep_sifflet_*', 'visible', 1)
        else:
            self.animate('ep_sifflet_*', 'alpha', None, 0, duration, 's')
            self.wait(duration, 's')
            self.set('ep_sifflet_*', 'position_x', -10)
            self.set('ep_sifflet_*', 'visible', 0)


    def allume_ep(self, on='alarm'):
        if on == 'alarm':
            self.set('ep_halo', 'hue', 0)
            self.animate('ep_gyrophare', 'alpha', 0, 1, 0.1, 's')
            self.animate('ep_halo', 'alpha', 0, 1, 0.2, 's')
            self.set('ep_gyrophare', 'visible', 1)
            self.set('ep_halo', 'visible', 1)
            self.wait(0.2, 's')
            self.animate('ep_halo', 'alpha', 1, 0.4, 0.2, 's', 'exponential-mirror', loop=True)
            self.animate('ep_gyrophare', 'alpha', 1, 0.4, 0.2, 's', 'exponential-mirror', loop=True)
            self.animate('bt_warning', 'alpha', 0, 1, 0.2, 's', 'random-mirror', loop=True)
            self.animate('bt_warning_orange', 'alpha', 0, 1, 0.2, 's', 'random-mirror', loop=True)
            self.animate('bt_warning_red', 'alpha', 0, 1, 0.2, 's', 'random-mirror', loop=True)
            self.set('bt_warning*', 'visible', 1)
        elif on == 'courrier':
            # self.set('ep_halo', 'color', 0.761, 0.275, 0.157)
            self.set('ep_halo', 'hue', 0.06)
            self.animate('ep_halo', 'alpha', 0, 1, 0.2, 's')
            self.animate('ep_gyrophare_poste', 'alpha', None, 1, 0.1, 's')
            self.set('ep_gyrophare_poste', 'visible', 1)
            self.set('ep_halo', 'visible', 1)
            self.wait(0.2, 's')
            self.animate('ep_halo', 'alpha', 1, 0.4, 0.2, 's', 'exponential-mirror', loop=True)
            self.animate('ep_gyrophare_poste', 'alpha', 1, 0.4, 0.2, 's', 'exponential-mirror', loop=True)
        else:
            self.animate('ep_gyrophare*', 'alpha', None, 0, 0.5, 's')
            self.animate('ep_halo', 'alpha', None, 0, 0.5, 's')
            self.animate('bt_warning', 'alpha', None, 0, 0.5, 's')
            self.animate('bt_warning_orange', 'alpha', None, 0, 0.5, 's')
            self.animate('bt_warning_red', 'alpha', None, 0, 0.5, 's')
            self.wait(0.5, 's')
            self.set('bt_warning*', 'visible', 0)
            self.set('ep_gyrophare', 'color', 0.5, 0.5, 0.5)
            self.set('ep_halo', 'color', 0.5, 0.5, 0.5)
            self.set('ep_halo', 'hue', 0)
            self.set('ep_gyrophare*', 'visible', 0)
            self.set('ep_halo', 'visible', 0)


    def bascule_ep(self, courrier=True, duration = 1, easing = 'elastic-inout'):
        if courrier:
            self.animate('bt_ep', 'rotate_z', None, 0, duration, 's', easing)
            self.animate('enedys_p', 'rotate_z', None, 0, duration, 's', easing)
        else:
            self.animate('bt_ep', 'rotate_z', None, -90, duration, 's', easing)
            self.animate('enedys_p', 'rotate_z', None, 90, duration, 's', easing)

    def face_ep(self, f, duration = 1):
        faces = ['dodo', 'reveil', 'non', 'sifflet']
        for face in faces:
            if self.get('ep_' + face, 'visible', 1) and not face == f:
                self.animate('ep_' + face, 'scale', None, [1, 0.97], duration, 's', 'random-mirror')
                self.wait(duration, 's')
                self.set('ep_' + f, 'visible', 1),
                if f == 'sifflet':
                    self.set('ep_casque', 'visible', 1)
                    self.set('ep_poste', 'visible', 0)
                else:
                    self.set('ep_casque', 'visible', 0)
                    self.set('ep_poste', 'visible', 1)

                self.set('ep_' + face, 'visible', 0),



########################## Vanupiés Hacking

########################## METHODES GENERIQUES

    def shaking_slide(self, slide_name, property, range, duration = 1, easing = 'linear'):
        """
        To make a slide shake (by now not possible to send wildcard)
        """
        self.stop_animate(slide_name, property)
        center_value = self.get(slide_name, property)
        self.start_scene('sequence/shaking_' + slide_name + '_' + property, lambda: [
            self.animate(slide_name, property, None,  center_value - range / 2, duration / 2, 's', easing),
            self.wait(duration / 2, 's'),
            self.animate(slide_name, property, None, center_value + range / 2, duration, 's', easing + '-mirror', loop=True)
        ])


    def shaking_tvs(self, number, content):
        range_x = (_rand() / 2 + 0.5) * 0.01
        range_y = _rand() * 0.01
        duration = (_rand() / 2 + 0.5) * 10
        self.shaking_slide('plane_horn_' + str(number), 'position_x', range_x, duration)
        self.shaking_slide(content, 'position_x', range_x, duration)
        self.shaking_slide('plane_horn_' + str(number), 'position_y', range_y, duration, 'random')
        self.shaking_slide(content, 'position_y', range_y, duration, 'random')

    def shaking_tv_jc(self):
        range_x = (_rand() / 2 + 0.5) * 0.01
        range_y = _rand() * 0.01
        duration = (_rand() / 2 + 0.5) * 10
        self.shaking_slide('plane_horn_jc', 'position_x', range_x, duration)
        self.shaking_slide('p_jc', 'position_x', range_x, duration)
        self.shaking_slide('plane_horn_jc', 'position_y', range_y, duration, 'random')
        self.shaking_slide('p_jc', 'position_y', range_y, duration, 'random')

    def falldown(self, slide_name, chute, d):
        cur_y_pos = self.get(slide_name, 'position_y')
        self.start_scene('sequence/falldown_' + slide_name, lambda:[
            self.animate(slide_name, 'position_y', None, cur_y_pos - chute, 0.3, 's', 'elastic'),
            self.wait(0.3, 's'),
            self.animate(slide_name, 'position_y', None, cur_y_pos - chute + 0.001, 0.5, 's', 'random'),
            self.wait(0.5, 's'),
            self.animate(slide_name, 'position_y', None, cur_y_pos, d-0.8, 's', 'linear'),
            self.wait(d-0.8, 's'),
            ]
        )



########################## Routes

    def route(self, address, args):

        if address == '/pyta/subscribe/update' and args[0] == 'status':
            """
            if pyta is ready, query parameters for slides that are not ready
            """
            self.status = args[1]

        elif '/pyta/slide' in address and '/get/reply/end' in address:
            """
            if pyta a pyta slide has finished sending its parameters, it is ready
            """
            slide_name = address.split('/')[-4]
            self.submodules[slide_name].ready = True

        elif '/pyta/slide' in address and '/ping/reply' in address:
            """
            if pyta a pyta slide has finished sending its parameters, it is ready
            """
            slide_name = address.split('/')[-3]
            self.submodules[slide_name].ping = True


        elif '/pyta/slide' in address and '/get/reply' in address:
            """
            Handle feedback from slides
            """
            slide_name = address.split('/')[-3]

            if slide_name not in self.submodules:
                """
                Feedback from a new slide: create Slide object and query all parameters
                """
                slide = Slide(slide_name, parent=self)
                self.add_submodule(slide)

            else:
                """
                Feedback from existing slide: create parameter if it doesn't exist
                """
                slide = self.submodules[slide_name]
                property_name, *values = args
                if property_name not in slide.parameters and property_name not in self.get_excluded_parameters:

                    types = 's'
                    for v in values:
                        if type(v) == str:
                            types += 's'
                        else:
                            types += 'f'


                    slide.add_parameter(property_name, '/pyta/slide/%s/set' % slide_name, types=types, static_args=[property_name], default=values[0] if len(values) == 1 else values)
                    if property_name == 'scale':
                        slide.add_meta_parameter('zoom', ['scale'],
                            getter = lambda scale: scale[0],
                            setter = lambda zoom: slide.set('scale', zoom)
                        )

                    if property_name in ['position', 'rotate']:
                        axis = {0: '_x', 1: '_y', 2: '_z'}
                        for index, ax in axis.items():
                            def closure(index, ax):

                                def setter(val):
                                    value = slide.get(property_name)
                                    value[index] = val
                                    slide.set(property_name, *value, preserve_animation = True)

                                slide.add_meta_parameter(property_name + ax, [property_name],
                                    getter = lambda prop: prop[index],
                                    setter = setter
                                )

                            closure(index, ax)


        # Don't route message any further
        return False

    state_excluded_parameters = [
        'position_x',
        'position_y',
        'position_z',
        'rotate_x',
        'rotate_y',
        'rotate_z',
        'zoom',
        'ready'
    ]

    def get_state(self, *args, **kwargs):
        """
        Exclude some parameters from state
        """
        state = super(PytaVSL, self).get_state(*args, **kwargs)
        def filter_function(item):
            exclude = False
            for prop in self.state_excluded_parameters:
                if prop in item:
                    exclude = True
                    break
            return not exclude
        state = list(filter(filter_function, state))
        return state



    def is_ready(self):
        """
        Check if all Slide submodules are ready (meaning all their parameters are created)
        """
        for name in self.submodules:
            if not self.submodules[name].ready:
                return False
        return True

    def sync(self, timeout=5):
        """
        Wait until pyta and mentat are synced
        """
        timeleft = timeout
        step = 0.04
        self.logger.info('waiting for sync')
        while not self.is_ready():
            if self.status == 'ready':
                for name in self.submodules:
                    if not self.submodules[name].ready:
                        self.submodules[name].query_slide_state()

                timeout -= step
            elif self.status == 'loading':
                timeleft = timeout

            self.wait(step, 's')
            if timeleft < 0:
                self.logger.critical('could not sync with pyta (timed out, not ready: %s)' % [slide.name for slide in self.submodules.values() if not slide.ready])
        self.logger.info('sync ok')
