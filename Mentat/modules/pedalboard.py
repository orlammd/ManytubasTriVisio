from mentat import Module

class PedalBoard(Module):
    """
    Pedalboard controller
    """

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.lock = False

        self.route_select = False

        self.route_map = {
            1: 'Snapshat'
        }

    def set_lock(self, lock):
        self.lock = lock

    def route(self, address, args):
        """
        Let /pedaboard/button messages pass, unless we're in route selection mode (toggle with button 12)
        """
        # assign unique button numbers to human performers
        if address == '/pedalBoard/button' and not self.lock:
            self.logger.info('OSC Pedalboard: ')
            self.logger.info(address)
            self.logger.info(args)
            if args[0] == 8:
                self.engine.active_route.call_human_button('orl')
                self.set_lock(True)
            elif args[0] == 2:
                self.engine.active_route.call_human_button('adrien')
                self.set_lock(True)
            # elif args[0] == 5:
            #     self.engine.active_route.call_human_button('nico')
            elif args[0] == 5:
                self.engine.active_route.call_human_button('arriere')
                self.set_lock(True)
            self.start_scene('wait_a_bit', lambda: [
                self.wait(0.2, 's'),
                self.set_lock(False)
            ])

        return False # prevent message from reaching routes


        #
        # if args[0] == 12:
        #
        #     self.route_select = not self.route_select
        #
        #     if self.route_select:
        #         self.logger.info('switched to route selection mode')
        #     else:
        #         self.logger.info('switched normal mode')
        #
        #     return False # bypass further routing
        #
        # elif self.route_select:
        #
        #     if args[0] in self.route_map:
        #         self.engine.set_route(self.route_map[args[0]])
        #     else:
        #         self.logger.info('no route in map for button %i' % args[0])
        #
        #     self.route_select = False
        #
        #     return False # bypass further routing
        #
        # else:
        #     pass
