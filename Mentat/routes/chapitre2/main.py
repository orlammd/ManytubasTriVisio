from ..base import *
from .video import Video
from .light import Light
from time import time as _time
from random import random as _rand

from modules import *

class Chapitre2(Video, Light, RouteBase):

    def activate(self):
        """
        Called when the engine switches to this route.
        """

        super().activate()

        transport.set_tempo(60)
        transport.set_cycle('4/4', pattern="Xxxx")

        # Setups, banks...
        # prodSampler.set_kit(self.name)

        self.start_scene('init_chapitre2', lambda: [
            # Overlay
            self.init_chapitre2(),
            # self.wait(0.1, 's'),

            # Chapitre 2
            self.m_ch2_1()
        ])

    def init_chapitre2(self):
        self.debut = _time()
        self.logger.info('Début @ ' + str(self.debut) + ' secondes')

        pytaVSL.send('/pyta/slide/*ch1*/remove')
        pytaVSL.send('/pyta/slide/*ch1*/unload')

        chapter = 'ch2'

        ### Création des groupes du chapitre
        pytaVSL.create_group('tv1', ['plane_horn_1','p_' + chapter + '-5']) # Pub Paillassons
        pytaVSL.create_group('tv2', ['plane_horn_2', 'p_chaussure', 'plane_bulle_fond_2']) # Chaussure
        pytaVSL.create_group('tv3', ['plane_horn_3', 'p_pied', 'plane_bulle_fond_3']) # Pied

        pytaVSL.create_group('m_iraye', ['m_layout', 'm_' + chapter + '*'])
        pytaVSL.create_group('f_arabesques', ['f_ara*e_1', 'f_ara*e_2'])
        pytaVSL.create_group('f_arabesques_2', ['f_ara*e_3', 'f_ara*e_4'])
        pytaVSL.create_group('f_arabesques_3', ['f_ara*e_5', 'f_ara*_6'])
        pytaVSL.create_group('f_ilm', ['f_arabesques', 'f_' + chapter + '*'])


        pytaVSL.sync()

        # self.wait(0.1, 's'),
        pytaVSL.position_overlay('Chapitre2')




    @pedalboard_button(100)
    def m_ch2_1(self):
        """
        Miraye Intro Chapitre 2
        """
        ### Light
        qlcplus.set_scene('miraye')

        ### VJ
        if pytaVSL.get('trijc', 'position_x') == 0:
            pytaVSL.trijc_io('in', 'lustre', 1, 'elastic'),
            mainSampler.trijc_io('in', 1, 'elastic'), ### AUDIO
        self.start_scene('sequence/miraye_intro_chapitre_2', lambda: [
            ### Lancement du Film
            self.wait(2, 's'),
            pytaVSL.trijc_change_tool('tuba'),
            self.wait(0.3, 's'),
            pytaVSL.animate('lights*', 'alpha', None, 1, 0.6),
            pytaVSL.animate('bt_main', 'brightness', None, 1, 0.6),
            pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.6),
            pytaVSL.signs_io('in', duration=0.5, easing='elastic-out'),
            pytaVSL.miraye_in('m_ch2-1', 1),
            mainSampler.miraye_in(), ### AUDIO

            self.wait(10, 's'),
            self.jingle_jc_1()
            ]
        )

    # @pedalboard_button(5001, who='adrien', after='m_ch2_1')
    def jingle_jc_1(self):
        """
        Jingle intempestif #1
        """
        self.start_scene('sequence/jingle_intempestif_1', lambda: [
            pytaVSL.jc_jingle_io('bottom', 0.5, 'elastic-inout'),
            mainSampler.jc_jingle_io(0.5), ### AUDIO
            self.wait(0.7, 's'),
            pytaVSL.set('m_ch2-1', 'video_speed', 0),
            self.wait(pytaVSL.get('p_jc', 'video_end') - 0.7, 's'),
            self.m_ch2_2()
        ])

    @pedalboard_button(101)
    def m_ch2_2(self):
        """
        Miraye Intro Chapitre 2 (2)
        """
        self.start_scene('sequence/miraye_intro_chapitre_2_2', lambda: [
            pytaVSL.m_switch_video('m_ch2-1', 'm_ch2-2'),
            self.wait(5, 's'),
            self.jingle_jc_2()
        ])

    # @pedalboard_button(5002, who='adrien', after='m_ch2_2')
    def jingle_jc_2(self):
        """
        Jingle intempestif #2 (repoussé à la fin)
        """
        self.start_scene('sequence/jingle_intempestif_2', lambda: [
            pytaVSL.jc_jingle_io('top', 0.5, 'elastic-out'), #### TODO à changer pour pouvoir pousser la fin
            mainSampler.jc_jingle_io(0.5), ### AUDIO
            # pytaVSL.set('m_ch2-2', 'video_speed', 0),
            self.wait(pytaVSL.get('p_jc', 'video_end') - 0.5, 's'),
            pytaVSL.m_noisy_switch_video('m_ch2-2', 'm_ch2-3', 0.5),
            mainSampler.noisy_switch(0.5), ### AUDIO


            self.m_ch2_3()
        ])

    @pedalboard_button(102)
    def m_ch2_3(self):
        """
        Miraye Intro Chapitre 2 (3)
        """
        self.start_scene('sequence/miraye_intro_chapitre_2_3', lambda: [
            self.wait(24, 's'), # TODO affiner le timing
            pytaVSL.display_title("ti_ch2-1"),
            mainSampler.display_title(), ### AUDIO
        ])

        self.start_scene('sequence/m_ch2_3_2_f_ch2_4', lambda: [
            self.wait(pytaVSL.get('m_ch2-3', 'video_end') - 2.55, 's'),
            pytaVSL.trijc_change_tool('aspi'),
            self.wait(0.5, 's'),
            pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, -10, 0.1, 's'),
            self.wait(0.1, 's'),
            pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, 0, 0.2, 's', 'elastic-inout'),
            pytaVSL.aspi_slide('m_layout', [0, -0.45], [0, 0.52], 0.6),
            pytaVSL.aspi_slide('m_ch2-3', [-0.02, -0.445], [-0.02, 0.53], 0.6),
            mainSampler.aspi_slide(), ### AUDIO
            self.wait(1.2, 's'),
            mainSampler.miraye_out(), ### AUDIO
            pytaVSL.trijc_io('out', 'aspi', 0.4, 'elastic-inout'),
            mainSampler.trijc_io('out', 0.4, 'elastic'), ### AUDIO
            self.wait(0.4, 's'),
            pytaVSL.trijc_io('in', 'tuba', 0.3, 'elastic-inout'),
            mainSampler.trijc_io('in', 0.3, 'elastic'), ### AUDIO
            self.wait(0.3, 's'),
            self.f_ch2_4()
        ])

    @pedalboard_button(103)
    def f_ch2_4(self):
        """
        JC & Enedys dans le bureau
        """
        ### LIGHT
        qlcplus.set_scene('film')

        ### VJ
        self.start_scene('sequence/f_ch2_4', lambda: [
            pytaVSL.movie_in('f_ch2-4', 0.6),
            mainSampler.movie_in(), ### AUDIO
            self.wait(43, 's'), # TODO affiner timing
            pytaVSL.trijc_io('in', tool='compas', duration=1),
            mainSampler.trijc_io('in', 1, 'elastic'), ### AUDIO
            self.wait(1.5, 's'),
            self.p_ch2_5(),
        ])

        self.start_scene('sequence/f_ch2_4_2_m_ch2_6', lambda:[
            self.wait(pytaVSL.get('f_ch2-4', 'video_end') - 2, 's'),
            pytaVSL.set('sub_t_trijc_lustre_allume', 'alpha', 1),
            pytaVSL.trijc_io('in', 'lustre', duration=0.7),
            mainSampler.trijc_io('in', 0.7), ### AUDIO
            self.wait(0.7, 's'),
            pytaVSL.trijc_turn_lights('off', 1),
            mainSampler.trijc_turn_lights(1), ### AUDIO
            pytaVSL.animate('f_ch2-4', 'alpha', None, 0, 1, 's'),
            pytaVSL.animate('f_ara*e_*', 'alpha', None, 0, 1, 's'),
            self.wait(1.1, 's'),
            pytaVSL.set('f_arabesques', 'visible', 0),
            pytaVSL.set('f_ara*e_*', 'alpha', 1),
            pytaVSL.trijc_change_tool('tuba'),
            pytaVSL.set('sub_t_trijc_lustre_allume', 'alpha', 1),
            self.wait(0.2, 's'),

            self.m_ch2_6()
        ])

    @pedalboard_button(104)
    def p_ch2_5(self):
        """
        Spot paillasson publicitaire
        """
        pytaVSL.shaking_tvs(1, 'p_ch2-5')
        pytaVSL.flying_wings('wingsp1')
        pytaVSL.set('tv1', 'position_x', 1)
        pytaVSL.set('tv1', 'position_y', 0)
        pytaVSL.set('tv1', 'scale', 0.6, 0.6)
        pytaVSL.set('tv1', 'visible', 1)

        self.start_scene('sequence/p_2_5_paillasson_buzz', lambda: [
            self.wait(9, 's'),
            mainSampler.send('/instrument/play', 's:paillasson_buzz', 100)
        ])

        self.start_scene('sequence/p_ch2_5', lambda: [
            pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -45, 0.1, 's', 'elastic-inout'),
            self.wait(0.1, 's'),

            ###### TODO : déclencher fish au dernier moment ?

            pytaVSL.animate('t_trijc_compas', 'rotate_z', None, 0, 0.5, 's', 'elastic-inout'),
            mainSampler.trijc_compas(0.5), ### AUDIO
            pytaVSL.animate('f_ilm', 'scale', None, [0.4, 0.4], 0.5, 's'),
            pytaVSL.animate('f_ilm', 'position_x', None, -0.25, 0.5, 's', 'elastic-inout'),
            pytaVSL.animate('f_ilm', 'position_y', None, 0.25, 0.5, 's', 'elastic-inout'),
            pytaVSL.animate('tv1', 'position_x', None, 0.2, 0.5, 's', 'elastic-inout'),
            self.wait(0.5, 's'),
            mainSampler.trijc_compas(pytaVSL.get('p_ch2-5', 'video_end') - 0.5 - 1.2), ### AUDIO
            pytaVSL.animate('tv1', 'scale', None, [0.8, 0.8], pytaVSL.get('p_ch2-5', 'video_end') - 0.5 - 1.2, 's'),
            mainSampler.flying_wings(), ### AUDIO
            pytaVSL.animate('tv1', 'position_x', None, 0.1, pytaVSL.get('p_ch2-5', 'video_end') - 0.5 - 1.2, 's'),
            pytaVSL.animate('t_trijc_compas', 'rotate_z', None, 20, pytaVSL.get('p_ch2-5', 'video_end') - 0.5 - 1.2, 's'),
            self.wait(pytaVSL.get('p_ch2-5', 'video_end') - 1.2, 's'), # TODO affiner timing
            # self.wait((pytaVSL.get('p_ch2-5', 'video_end') - 1) / 2 - 0.5, 's' ),
            pytaVSL.animate('tv1', 'scale', None, [0.6, 0.6], 0.5, 's', 'elastic-inout'),
            pytaVSL.animate('tv1', 'position_x', None, 0.2, 0.5, 's', 'elastic-inout'),
            pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -45, 0.5, 's', 'elastic-inout'),
            mainSampler.trijc_compas(0.5), ### AUDIO
            self.wait(0.5, 's' ),
            pytaVSL.trijc_change_tool('aimant'),
            self.wait(0.1, 's'),
            pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -360, 0.5, 's', 'elastic-inout'),
            mainSampler.trijc_aimant(0.5), ### AUDIO
            pytaVSL.animate('f_ilm', 'scale', None, [0.95, 0.95], 0.5, 's', 'elastic-inout'),
            pytaVSL.animate('f_ilm', 'position_x', None, 0, 0.5, 's', 'elastic-inout'),
            pytaVSL.animate('f_ilm', 'position_y', None, 0, 0.5, 's', 'elastic-inout'),
            pytaVSL.animate('tv1', 'position_x', None, 1, 0.5, 's', 'elastic-inout'),
            self.wait(0.5, 's'),
            pytaVSL.stop_animate('plane_horn_1', 'position_x'),
            pytaVSL.stop_animate('plane_horn_1', 'position_y'),
            pytaVSL.stop_animate('p_ch2-5', 'position_x'),
            pytaVSL.stop_animate('p_ch2-5', 'position_y'),
            pytaVSL.flying_wings('wingsp1', on=False),
            mainSampler.flying_wings('off'), ### AUDIO
            pytaVSL.set('p_ch2-5', 'fish', 0),
            pytaVSL.trijc_io('out', 'aimant', 0.1, easing='elastic-inout'),
            mainSampler.trijc_io('out', 0.1, 'elastic'), ### AUDIO
        ])


    @pedalboard_button(105)
    def m_ch2_6(self):
        """
        Reprise narration Miraye
        """
        ### LIGHT
        qlcplus.set_scene('miraye')

        ### VJ
        pytaVSL.signs_io('in', duration = 0.6),
        pytaVSL.animate('lights*', 'alpha', None, 1, 0.6),
        pytaVSL.animate('bt_main', 'brightness', None, 1, 0.6),
        pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.6),
        pytaVSL.miraye_in('m_ch2-6', 0.5)
        mainSampler.miraye_in(), ### AUDIO
        self.start_scene('sequence/m_ch2_6', lambda: [
            self.wait(6.2, 's'),
            pytaVSL.jc_jingle_in('bottom', 0.1, 'elastic-inout'),
            mainSampler.jc_jingle_in(), ### AUDIO
            self.wait(0.6, 's'),
            pytaVSL.animate('p_jc', 'rgbwave', 0, 1, 0.2, 's'),
            pytaVSL.animate('tv_jc', 'rgbwave', 0, 1, 0.2, 's'),
            self.wait(0.2, 's'),
            pytaVSL.animate('tv_jc', 'position_y', None, -1, 1, 's', 'random'),
            self.wait(1.2, 's'),
            pytaVSL.set('*jc', 'rgbwave', 0),
            pytaVSL.set('tv_jc', 'visible', 0),
            pytaVSL.stop_animate('plane_horn_jc', 'position_x'),
            pytaVSL.stop_animate('plane_horn_jc', 'position_y'),
            pytaVSL.stop_animate('p_jc', 'position_x'),
            pytaVSL.stop_animate('p_jc', 'position_y'),
            pytaVSL.flying_wings('wings_jc', on=False),
            mainSampler.flying_wings('off'), ### AUDIO


        ])
        self.start_scene('sequence/m_ch2_6_2_f_ch2_7', lambda: [
            self.wait(pytaVSL.get('m_ch2-6', 'video_end') - 0.5, 's'),
            # pytaVSL.animate('m_iraye', 'scale', None, [0.25, 0.25], 8, 's'),
            # # pytaVSL.animate('m_iraye', 'position', None, [0.35, 0.15, pytaVSL.get('m_iraye', 'position_z')], 8, 's'),
            # pytaVSL.animate('m_iraye', 'position_x', None, -0.2, 7, 's', 'exponential-inout'),
            # pytaVSL.animate('m_iraye', 'position_y', None, 0.18, 7, 's'),
            # self.wait(9, 's'),
            pytaVSL.animate('m_iraye', 'position_x', None, 1, 1, 's', 'elastic-inout'),
            self.wait(1, 's'),
            mainSampler.miraye_out(), ### AUDIO
            self.f_ch2_7()

        ])

##### DEPRECATED
    #
    # @pedalboard_button(3)
    # def jingle_intempestif_3(self):
    #     pytaVSL.jc_jingle_in('bottom', 0.1, 'elastic-inout')


    @pedalboard_button(106)
    def button_fch2_7(self):
        pytaVSL.set('m_iraye', 'visible', 0)
        self.stop_scene('sequence/*')
        self.f_ch2_7()


    def f_ch2_7(self):
        """
        Enedys se rend au spectacle des Vanupiés
        """
        ### LIGHT
        qlcplus.set_scene('film')


        ### VJ
        self.start_scene('sequence/f_ch2_7', lambda: [
            pytaVSL.movie_in('f_ch2-7', 0.7, warpzoned=True, init=True),
            mainSampler.movie_in(), ### AUDIO
            # self.wait(2, 's'),
            # pytaVSL.animate('t_trijc_tuba', 'rotate_z', None, 7, 0.2, 's', 'elastic-inout'),
            # pytaVSL.animate('m_iraye', 'position_x', None, 1, 1, 's', 'elastic-inout'),
            # self.wait(0.3, 's'),
            # pytaVSL.animate('t_trijc_tuba', 'rotate_z', None, 0, 0.5, 's', 'elastic-inout'),
        ])

        self.dede_doah_chaussure(),
        self.panier_entracte()

    @pedalboard_button(1060)
    def dede_doah_chaussure(self):
        """
        Doah rencontre Dédé qui lui demande d'enlever ses chaussures
        """
        self.dede_doah_chaussures_delay = 90
        self.start_scene('sequence/dede_doah_chaussure', lambda: [
            ### Idées de chaussures de Doah
            self.wait(self.dede_doah_chaussures_delay, 's'),

            # Init
            pytaVSL.set('tv2', 'visible', 1),
            pytaVSL.set('tv3', 'visible', 1),
            pytaVSL.set('plane_tv_2', 'visible', 0),
            pytaVSL.set('plane_tv_3', 'visible', 0),
            pytaVSL.set('plane_bulle_2', 'visible', 1),
            pytaVSL.set('plane_bulle_3', 'visible', 1),
            pytaVSL.set('plane_bulle_fond_2', 'visible', 1),
            pytaVSL.set('plane_bulle_fond_3', 'visible', 1),
            pytaVSL.shaking_tvs(2, 'p_pied'),
            pytaVSL.shaking_tvs(3, 'p_chaussure'),
            pytaVSL.flying_wings('wingsp2'),
            pytaVSL.flying_wings('wingsp3'),


            mainSampler.flying_wings('notv'), ### AUDIO
            pytaVSL.animate('tv2', 'position_x', None, -0, 1, 's'),
            pytaVSL.animate('tv3', 'position_x', None, 0, 1, 's'),
            self.wait(1, 's'),
            pytaVSL.set('p_chaussure', 'mask', 'p_chaussure_mask'),
            self.wait(2, 's'),
            pytaVSL.animate('tv2', 'position_y', None, 1, 0.2, 's', 'elastic-inout'),
            pytaVSL.animate('tv3', 'position_y', None, 1, 0.2, 's', 'elastic-inout'),
            self.wait(0.4, 's'),
            mainSampler.flying_wings('off'), ### AUDIO
            pytaVSL.set('tv2', 'visible', 0),
            pytaVSL.set('tv3', 'visible', 0),
            pytaVSL.stop_animate('plane_horn*', 'position_x'),
            pytaVSL.stop_animate('plane_horn*', 'position_y'),
            pytaVSL.stop_animate('p_pied', 'position_x'),
            pytaVSL.stop_animate('p_pied', 'position_y'),
            pytaVSL.stop_animate('p_chaussure', 'position_x'),
            pytaVSL.stop_animate('p_chaussure', 'position_y'),
            pytaVSL.set('plane_tv_2', 'visible', 1),
            pytaVSL.set('plane_tv_3', 'visible', 1),
            pytaVSL.set('plane_bulle_2', 'visible', 0),
            pytaVSL.set('plane_bulle_3', 'visible', 0),
            pytaVSL.set('plane_bulle_fond_2', 'visible', 0),
            pytaVSL.set('plane_bulle_fond_3', 'visible', 0),
            pytaVSL.set('p_chaussure', 'mask', ''),
            pytaVSL.flying_wings('wingsp2', on=False),
            pytaVSL.flying_wings('wingsp3', on=False),


        ])


    @pedalboard_button(1061)
    def panier_entracte(self):
        def shoot_enedys():
            while pytaVSL.get('bato_h', 'position_x') > 0.53:
                self.wait(0.01, 's')
            self.start_scene('sequence/shoot_enedys', lambda: [
                pytaVSL.animate('bt', 'position_x', None, 0.46, 0.3, 's', 'elastic-out'),
                pytaVSL.animate('bt', 'position_y', None, -0.39, 0.1, 's'),
                pytaVSL.animate('bt_main', 'brightness', None, 1, 0.1, 's'),
                pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.1, 's'),
                pytaVSL.bascule_ep(True),
                mainSampler.bascule_ep(),

            ])

        self.panier_entracte_delay = 153 # 155

        # Init
        pytaVSL.set('bato_h', 'position_y', -0.1),
        pytaVSL.set('bato_h', 'scale', 1.8, 1.8),
        pytaVSL.set('o_bh_embouchure', 'position_x', 0.04),
        pytaVSL.set('o_bh_embouchure', 'position_y', -0.32),

        self.start_scene('sequence/f_ch2_7_panier_entracte', lambda: [

            self.wait(self.panier_entracte_delay, 's'),
            # Init
            pytaVSL.batohacker_shaking(),

            # Arrivée
            pytaVSL.batohacker_hmove(0.385, 'jardin', 0.5),
            mainSampler.batohacker_move(0.5), ### AUDIO
            shoot_enedys(),
            self.wait(0.3, 's'),

######### DEPRECATED, ne marche pas très bien.
            # pytaVSL.animate('bato_h', 'rotate_z', 0, 15, 0.2, 's'),
            # self.wait(0.5, 's'),
            # pytaVSL.animate('bato_h', 'rotate_z', 15, 0, 0.2, 's', 'elastic-out'),

            # Punch
            # self.wait(, 's'),
            pytaVSL.batohacker_punch('f_ch2-7', x_dest = 0),
            mainSampler.batohacker_punch(), ### AUDIO
            pytaVSL.animate('f_ch2-7*', 'brightness', None, 0.3, 0.3, 's'),

            # Sortie Entrailles
            self.wait(0.6, 's'),
            pytaVSL.batohacker_trp_io(),
            mainSampler.batohacker_trp_io(), ### AUDIO

            # Branchement
            self.wait(1, 's'),
            mainSampler.batohacker_plug(0.15), ### AUDIO
            pytaVSL.animate('o_bh_embouchure', 'scale', [0, 0], [0.08, 0.08], 0.15, 's', 'elastic-out'),
            pytaVSL.set('o_bh_embouchure', 'visible', 1),


            # Allumage EP et plateau
            self.wait(1, 's'),
            qlcplus.set_strobe('bt_doah'), ### LIGHT
            qlcplus.animate('d_doah', None, 255, 2, 's'),

#           Déjà dans shoot_enedys
            # pytaVSL.bascule_ep(),
            # mainSampler.bascule_ep(), ### AUDIO
            self.start_scene('sequence/f_ch2-7_ep_reveil', lambda: [
                pytaVSL.face_ep('reveil'),
            ]),
            pytaVSL.allume_ep(on='courrier'),
            mainSampler.allume_ep(on='courrier') ### AUDIO


        ])


    @pedalboard_button(5004, who='orl', after='panier_entracte')
    def f_ch2_6_panier_out_doah_acts(self):
        t_warp_in = 24 / 25
        t_swallow = 3 + 3 / 25
        self.start_scene('sequence/f_ch2-7_panier_out_doah_acts', lambda: [
            # Relâche film
            pytaVSL.animate('f_ch2-7*', 'brightness', None, 1, 0.1, 's'),
            pytaVSL.set('f_ch2-7', 'video_speed', 1),
            pytaVSL.set('f_ch2-7_warpzoned', 'video_speed', 1),
            mainSampler.batohacker_film_stopped(stopped=True), ### AUDIO

            # Prépa mask
            pytaVSL.set('f_ch2-7', 'mask', 'f_ch2-7_mask'),

            # Avalage
            self.wait(t_warp_in, 's'),
            mainSampler.batohacker_warp(warp_in=True), ### AUDIO
            pytaVSL.animate('f_ch2-7_warpzoned', 'rgbwave', None, 0.4, 0.3, 's'),
            pytaVSL.animate('f_ch2-7_warpzoned', 'fish', None, 0.4, 0.3, 's'),

            self.wait(t_swallow - t_warp_in, 's'),
            mainSampler.batohacker_swallow(0.35), ### AUDIO
            pytaVSL.animate('i_bh_entrailles', 'offset_y', 0, 0.01, 0.1, 's', 'random-mirror'),
            self.wait(0.1, 's'),
            pytaVSL.set('o_bh_adam', 'position_x', 0.052), #, 'elastic-out'),
            pytaVSL.set('o_bh_adam', 'visible', 1),
            pytaVSL.animate('o_bh_adam', 'scale', [0.8, 0], [0.8, 0.8], 0.05, 's'),
            pytaVSL.animate('o_bh_adam', 'alpha', 0, 1, 0.2, 's'),
            pytaVSL.animate('o_bh_adam', 'position_y', -0.27, -0.32, 0.2, 's'), #, 'elastic-out'),
            self.wait(0.15, 's'),
            pytaVSL.animate('o_bh_adam', 'alpha', None, 0, 0.05, 's'),
            self.wait(0.1, 's'),
            pytaVSL.set('o_bh_adam', 'visible', 0),

            self.wait(0.5, 's'),
            pytaVSL.animate('f_ch2-7_warpzoned', 'rgbwave', None, 0, 0.3, 's'),
            pytaVSL.animate('f_ch2-7_warpzoned', 'fish', None, 0, 0.3, 's'),
            mainSampler.batohacker_warp(warp_in=False), ### AUDIO


            # Débranchage + rentrage des entrailles
            self.wait(0.6, 's'),
            pytaVSL.animate('o_bh_embouchure', 'scale', None, [0, 0], 0.15, 's', 'elastic-out'),
            mainSampler.batohacker_unplug(0.15), ### AUDIO
            self.wait(0.3, 's'),
            pytaVSL.batohacker_trp_io('in', 0.15),
            mainSampler.batohacker_trp_io(0.15), ### AUDIO


            # Extinction EP + Plateau
            self.wait(0.5, 's'),
            qlcplus.set_strobe('bt_doah', on=False), ### LIGHT


            mainSampler.bascule_ep(), ### AUDIO
            pytaVSL.bascule_ep(False),
            pytaVSL.allume_ep(False),

            # S'en va
            self.wait(0.2, 's'),
            pytaVSL.batohacker_hmove(1, 'cour', duration=0.5),
            mainSampler.batohacker_move(goto='departure'), ### AUDIO


            # EP se rendort
            self.wait(1, 's'),
            self.start_scene('sequence/f_ch2-7_ep_dodo', lambda: [
                pytaVSL.face_ep('dodo')
            ]),
            pytaVSL.animate('bt_main', 'brightness', None, 0.3, 0.2, 's'),
            pytaVSL.animate('*ep_*', 'brightness', None, 0.3, 0.2, 's'),
            pytaVSL.animate('bt', 'position_x', None, 0.525, 1, 's'),
            pytaVSL.animate('bt', 'position_y', None, -0.4, 1, 's', 'random'),

            # Reinit des masques
            pytaVSL.set('f_ch2-7', 'mask', ''),
            pytaVSL.set('o_bh_droit_1', 'scale', 0.25, 0.25),
            pytaVSL.set('o_bh_embouchure', 'scale', 0.1, 0.1),
            pytaVSL.set('bato_h', 'scale', 1.2, 1.2),

        ])

        self.proposition_entracte()

    @pedalboard_button(1062)
    def button_proposition_entracte(self):
        self.stop_scene('sequence/*')
        self.proposition_entracte()

    @pedalboard_button()
    def proposition_entracte(self):
        ### LIGHT
        qlcplus.set_scene('full_stage')

        t_offset = pytaVSL.get('f_ch2-7', 'video_end') - (self.panier_entracte_delay + 1 + 0.5 + 0.3)
        explosion_delay = 4.2
        blackout_duration = 1
        ### VJ
        self.start_scene('sequence/proposition_entracte', lambda: [
            self.wait(t_offset, 's'),
            self.logger.info("proposition entracte"),

            pytaVSL.f_switch_video('f_ch2-7', 'f_ch2-7_down'),

            self.wait(explosion_delay, 's'),
            qlcplus.set_scene('blackout', duration=0.1), ## LIGHT
            self.wait(blackout_duration, 's'),
            qlcplus.set_scene('full_stage', duration = 0.5), ## LIGHT

        #  ME SEMBLE QUE ÇA NE PEUT PAS MARCHER SI C'EST EN DEUX SCÈNES
            self.wait(pytaVSL.get('f_ch2-7_down', 'video_end') - 0.1 - explosion_delay - blackout_duration, 's'), # un certain temps
            pytaVSL.f_switch_video("f_ch2-7_down", "f_ch2-7_waiting", warpzoned=True)
        ])

        ### AUDIO
        self.start_scene('sequence/entracte_audio', lambda: [
            self.wait(t_offset, 's'),
            self.wait(explosion_delay, 's'), # 4" et 5 frames dans fichier Nico
            mainSampler.send('/instrument/play', 's:entracte_tombe', 100)
        ])

    @pedalboard_button(7001, who='adrien', after='proposition_entracte')
    def jmjarret_entracte(self):
        mainSampler.send('/instrument/play', 's:jmjarret_entracte', 100) ### AUDIO

    @pedalboard_button(5005, who='adrien', after='jmjarret_entracte')
    def m_ch2_8(self):
        """
        Miraye - On enchaîne là
        """
        self.start_scene('m_ch2_8', lambda: [
            pytaVSL.trijc_io('in', 'tuba', 0.5),
            mainSampler.trijc_io('in', 0.5), ### AUDIO
            self.wait(0.5, 's'),
            pytaVSL.miraye_in('m_ch2-8'),
            mainSampler.miraye_in(), ### AUDIO
            self.wait(1, 's'),
            pytaVSL.animate('f_ch2-7_waiting', 'rgbwave', None, 0.6, 0.8, 's', 'exponential-out'),
            mainSampler.send('/instrument/stop', 's:jmjarret_entracte'), ### AUDIO
            self.wait(0.2, 's'),
            # pytaVSL.animate('f_ilm_down', 'position_x', None, 0, 0.5, 's', 'elastic-inout'),
            self.wait(pytaVSL.get('m_ch2-8', 'video_end') - 0.2 - 0.5 - 0.8, 's'),
            pytaVSL.trijc_change_tool('aspi'),
            self.wait(0.1, 's'),
            pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, 0, 0.2, 's', 'elastic'),
            pytaVSL.aspi_slide('m_layout', [0, -0.45], [0, 0.52], 0.6),
            pytaVSL.aspi_slide('m_ch2-8', [-0.02, -0.445], [-0.02, 0.53], 0.6),
            mainSampler.aspi_slide(), ### AUDIO
            self.wait(0.6, 's'),
            pytaVSL.trijc_io('out', 'aspi', 0.5),
            mainSampler.miraye_out(), ### AUDIO
            mainSampler.trijc_io('out', 0.5), ### AUDIO
            pytaVSL.animate('lights*', 'alpha', None, 0.3, 1, 's'),
            pytaVSL.animate('bt_main', 'brightness', None, 0.3, 1, 's'),
            pytaVSL.animate('*ep_*', 'brightness', None, 0.3, 1, 's'),
            self.f_ch2_9()
        ])

    @pedalboard_button(107)
    def button_f_ch2_9(self):
        pytaVSL.set('m_iraye', 'visible', 0)
        pytaVSL.set('f_ilm', 'scale', 0.95, 0.95)
        pytaVSL.set('f_ilm', 'position_x', 0)
        pytaVSL.set('f_ilm', 'position_y', 0)
        pytaVSL.set('f_arabesque_1', 'position_y', 3.04)
        pytaVSL.set('f_arabesque_2', 'position_y', -3.04)
        self.stop_scene('sequence/*')
        self.f_ch2_9()

    def f_ch2_9(self):
        """
        Numéro de Dagz
        """
        ### LIGHT
        qlcplus.set_scene('film')

        ### AUDIO
        t0 = 1.4
        t1 = 88 + 27/30
        t2 = 92 + 1/30
        t3 = 94 + 1/30
        self.start_scene('sequence/f_ch2_9_audio', lambda: [
            self.wait(t0, 's'),
            mainSampler.send('/instrument/play', 's:gros_interrupteur', 100),
            self.wait(t1 - t0, 's'),
            mainSampler.send('/instrument/play', 's:f_ch2-9_klaxon', 100),
            self.wait(t2 - t1, 's'),
            mainSampler.send('/instrument/play', 's:f_ch2-9_klaxon', 100),
            self.wait(t3 - t2, 's'),
            mainSampler.send('/instrument/play', 's:f_ch2-9_klaxon', 100),
        ])

        ### VJ
        # pytaVSL.set('f_ch2-9', 'rgbwave', 0.5)
        pytaVSL.f_switch_video('f_ch2-7_waiting', 'f_ch2-9', warpzoned=True),
        # pytaVSL.set('f_ch2-9', 'visible', 1)
        # pytaVSL.set('f_ch2-9_warpzoned', 'visible', 1)
        pytaVSL.animate('f_ch2-7_waiting', 'rgbwave', 0.5, 0, 1, 's')

        pytaVSL.set('tv2', 'scale', 0.2, 0.2),
        pytaVSL.set('tv2', 'position_x', 1),
        pytaVSL.set('tv2', 'position_y', 0.25),
        pytaVSL.set('tv2', 'visible', 1),
        pytaVSL.shaking_tvs(2, 'p_chaussure'),
        pytaVSL.set('plane_tv_2', 'visible', 0),
        pytaVSL.set('plane_bulle_2', 'visible', 1),
        pytaVSL.set('plane_bulle_fond_2', 'visible', 1),
        pytaVSL.flying_wings('wingsp2'),

        self.start_scene('sequence/doah_idee_chaussures2', lambda: [
            self.wait(14.5, 's'),
            mainSampler.flying_wings('notv'), ### AUDIO
            pytaVSL.animate('tv2', 'position_x', -1, -0.2, 0.6, 's'),
            self.wait(1.5, 's'),
            pytaVSL.animate('tv2', 'position_x', None, 1, 0.8, 's', 'elastic-inout'),
            self.wait(0.8, 's'),
            mainSampler.flying_wings('off'), ### AUDIO
            self.wait(0.7, 's'),
            pytaVSL.set('plane_tv_2', 'visible', 1),
            pytaVSL.set('plane_bulle_2', 'visible', 0),
            pytaVSL.set('plane_bulle_fond_2', 'visible', 0),
            pytaVSL.flying_wings('wingsp2', on=False),
            pytaVSL.stop_animate('plane_horn*', 'position_x'),
            pytaVSL.stop_animate('plane_horn*', 'position_y'),
            pytaVSL.stop_animate('p_chaussure', 'position_x'),
            pytaVSL.stop_animate('p_chaussure', 'position_y'),

        ])

        self.start_scene('sequence/f_ch2_9_panier', lambda: [
            #### L'arabesque du haut oscille et laisse tomber la théière

            self.wait(0.5, 's'),

            # Init
            pytaVSL.set('f_arandgrame_1', 'scale', -3, 3),

            # Ouverture arabesque
            pytaVSL.animate('f_ara*e_1', 'rotate_z', None, -70, 1, 's', 'elastic-out'),
            mainSampler.send('/instrument/play', 's:arabesque', 100), ### AUDIO

            # Trajet Panier
            self.wait(1, 's'),
            pytaVSL.set('f_ch2-bs_panier', 'position_x', -0.04),
            pytaVSL.animate('f_ch2-bs_panier', 'scale', None, [0.3, 0.3], 0.15, 's'),
            pytaVSL.animate('f_ch2-bs_panier', 'position_y', 0.32, -0.7, 1, 's'),
            pytaVSL.animate('f_ch2-bs_panier', 'rotate_z', 0, 720, 1, 's'),
            pytaVSL.set("f_ch2-bs_panier", "visible", 1),

            # Remontée Panier
            self.wait(0.3, 's'),
            pytaVSL.animate('f_ara*e_1', 'rotate_z', None, -180, 1, 's', 'elastic-out'),

            # Reinit
            self.wait(1, 's'),
            pytaVSL.set('f_aran*e_1', 'scale', 3, 3),
            pytaVSL.set('f_ch2-bs_panier', 'visible', 0)



        ])

        self.f_ch2_9_veste_out()


    @pedalboard_button(1070)
    def f_ch2_9_veste_out(self):
        """
        Sortie de la veste par le Batohacker
        """
        # pytaVSL.set('f_ch2-9', 'video_time', 71, force_send=True)
        # pytaVSL.set('f_ch2-9_warpzoned', 'video_time', 71, force_send=True)

        def veste_out_batohacker():
            # Init
            pytaVSL.set('bato_h', 'scale', -2, 2),
            pytaVSL.set('bato_h', 'position_x', -1),
            pytaVSL.set('bato_h', 'position_y', 0.05),

            pytaVSL.set('o_bh_embouchure', 'scale', 0.07, 0.07),
            pytaVSL.set('o_bh_embouchure', 'position_x', -0.018),
            pytaVSL.set('o_bh_droit_1', 'scale', 0.18, 0),
            pytaVSL.set('o_bh_droit_1', 'position_x', -0.014),

            self.start_scene('sequence/veste_out_batohacker', lambda: [
                self.wait(0.01, 's'),

                # Init
                pytaVSL.batohacker_shaking(),

                # Arrivée
                pytaVSL.batohacker_hmove(-0.38, 'cour', 0.5),
                mainSampler.batohacker_move(0.5), ### AUDIO

                # Punch
                self.wait(0.5, 's'),
                mainSampler.batohacker_punch(), ### AUDIO
                pytaVSL.batohacker_punch('f_ch2-9', x_dest = 0),
                pytaVSL.animate('f_ch2-9*', 'brightness', None, 0.3, 0.3, 's'),


                # Sortie Entrailles
                self.wait(0.6, 's'),
                pytaVSL.batohacker_trp_io(),
                mainSampler.batohacker_trp_io(), ### AUDIO

                # Branchement
                self.wait(1, 's'),
                mainSampler.batohacker_plug(0.3), ### AUDIO
                pytaVSL.animate('o_bh_embouchure', 'position_y', -0.25, - 0.49, 0.3, 's'),
                pytaVSL.animate('o_bh_droit_1', 'position_y', -0.19, - 0.38, 0.3, 's'),
                pytaVSL.animate('o_bh_droit_1', 'scale', None, [0.18, 0.5], 0.3, 's'),
                pytaVSL.set('o_bh_embouchure', 'visible', 1),
                pytaVSL.set('o_bh_droit_1', 'visible', 1),

                # Allumage EP et plateau
                self.wait(1, 's'),
                qlcplus.set_strobe('bt_sal'), ### LIGHT
                qlcplus.animate('d_sal', None, 255, 2, 's'),
                qlcplus.animate('d_je', None, 255, 2, 's'),

                pytaVSL.bascule_ep(),
                mainSampler.bascule_ep(), ### AUDIO
                pytaVSL.animate('bt_main', 'brightness', None, 1, 0.3, 's'),
                pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.3, 's'),
                self.start_scene('sequence/f_ché-9_ep_reveil', lambda: [
                    pytaVSL.face_ep('reveil'),
                ]),
                pytaVSL.allume_ep(on='courrier'),
                mainSampler.allume_ep(on='courrier'), ### AUDIO
            ])

        def veste_out_ep():
            self.start_scene('sequence/veste_out_ep', lambda: [
                # EP remarque le BatoHacker et file de l'autre côté
                self.wait(0.2, 's'),
                pytaVSL.animate('bt_main', 'brightness', None, 1, 0.3, 's'),
                pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.3, 's'),
                pytaVSL.face_ep('non'),
                pytaVSL.animate('bt', 'position_x', None, 0.6, 0.7, 's', 'elastic-inout'),
                self.wait(0.7, 's'),
                pytaVSL.set('bt', 'scale', -0.2, 0.2),
                pytaVSL.animate('bt', 'position_x', -0.6, -0.415, 0.5, 's', 'elastic-out'),
                self.wait(0.5, 's'),
                # pytaVSL.allume_ep(on='courrier'), # Inutile ?


                # EP reçoit du courrier
                #### voir dans la scène veste_out_batohacker

            ])


        self.veste_delay = 127 + 3 / 25 #128
        self.start_scene('sequence/veste_out', lambda:[
            self.wait(self.veste_delay, 's'),
            veste_out_batohacker(),
            veste_out_ep()
        ])


    @pedalboard_button(5006, who='orl', after='f_ch2_9_veste_out')
    def f_ch2_9_veste_out_sal_acts(self):
        t_warp_in = 0.7 + 1 / 25
        t_swallow = 4 + 1 / 25
        self.start_scene('sequence/f_ch2-9_veste_out_sal_acts', lambda: [
            # Relâche film
            pytaVSL.animate('f_ch2-9*', 'brightness', None, 1, 0.1, 's'),
            pytaVSL.set('f_ch2-9', 'video_speed', 1),
            pytaVSL.set('f_ch2-9_warpzoned', 'video_speed', 1),
            mainSampler.batohacker_film_stopped(stopped=True), ### AUDIO

            # Prépa mask
            pytaVSL.set('f_ch2-9', 'mask', 'f_ch2-9_mask'),

            # Avalage
            self.wait(t_warp_in, 's'),
            mainSampler.batohacker_warp(warp_in=True), ### AUDIO
            pytaVSL.animate('f_ch2-9_warpzoned', 'rgbwave', None, 0.4, 0.3, 's'),
            pytaVSL.animate('f_ch2-9_warpzoned', 'fish', None, 0.4, 0.3, 's'),

            self.wait(t_swallow - t_warp_in, 's'),
            mainSampler.batohacker_swallow(0.5), ### AUDIO
            pytaVSL.animate('i_bh_entrailles', 'offset_y', 0, 0.01, 0.3, 's', 'random-mirror'),
            self.wait(0.2, 's'),
            pytaVSL.animate('o_bh_adam', 'alpha', 0, 1, 0.1, 's'),
            pytaVSL.animate('o_bh_adam', 'scale', [0.7, 0], [0.7, 0.7], 0.1, 's'),
            pytaVSL.animate('o_bh_adam', 'position_x', -0.02, -0.005, 0.3, 's'),
            pytaVSL.animate('o_bh_adam', 'position_y', -0.14, -0.49, 0.3, 's'),
            pytaVSL.set('o_bh_adam', 'visible', 1),
            self.wait(0.2, 's'),
            pytaVSL.animate('o_bh_adam', 'scale', None, [0.7, 0], 0.1, 's'),
            pytaVSL.animate('o_bh_adam', 'alpha', 1, 0, 0.1, 's'),
            self.wait(0.1, 's'),
            pytaVSL.set('o_bh_adam', 'visible', 0),

            self.wait(0.5, 's'),
            pytaVSL.animate('f_ch2-9_warpzoned', 'rgbwave', None, 0, 0.3, 's'),
            pytaVSL.animate('f_ch2-9_warpzoned', 'fish', None, 0, 0.3, 's'),
            mainSampler.batohacker_warp(warp_in=False), ### AUDIO



            # Débranchage
            self.wait(0.6, 's'),
            mainSampler.batohacker_unplug(0.3), ### AUDIO
            pytaVSL.animate('o_bh_embouchure', 'position_y', - 0.49, -0.25, 0.3, 's'),
            pytaVSL.animate('o_bh_droit_1', 'position_y', - 0.38, -0.19, 0.3, 's'),
            pytaVSL.animate('o_bh_droit_1', 'scale', None, [0.18, 0], 0.3, 's'),
            self.wait(0.3, 's'),
            pytaVSL.set('o_bh_embouchure', 'visible', 0),
            pytaVSL.set('o_bh_droit_1', 'visible', 0),


            # Extinction EP + Plateau
            self.wait(0.5, 's'),
            qlcplus.set_strobe('bt_sal', on=False), ### LIGHT

            mainSampler.bascule_ep(), ### AUDIO
            pytaVSL.bascule_ep(False),
            pytaVSL.allume_ep(False),


            # Rentrage des entrailles
            self.wait(1, 's'),
            pytaVSL.batohacker_trp_io('in'),
            mainSampler.batohacker_trp_io(), ### AUDIO

            # Remet le film à sa place - INUTILE ??? TODO
            # self.wait(1, 's'),
            # pytaVSL.batohacker_debevier(),
            # self.wait(0.4, 's'),
            # pytaVSL.animate('f_ilm', 'position_x', None, 0, 1, 's'),

            # S'en va
            self.wait(1, 's'),
            pytaVSL.batohacker_hmove(-1, 'jardin'),
            mainSampler.batohacker_move(goto='departure'), ### AUDIO


            # EP retourne à sa place
            self.wait(0.5, 's'),
            pytaVSL.animate('bt', 'position_x', None, -0.6, 0.8, 's', 'elastic-inout'),
            self.wait(0.8, 's'),
            pytaVSL.set('bt', 'scale', 0.2, 0.2),
            pytaVSL.animate('bt', 'position_x', 0.7, 0.525, 0.5, 's', 'elastic-out'),

            # EP se rendort
            self.wait(1.5, 's'),
            self.start_scene('sequence/f_ch2-9_ep_dodo', lambda: [
                pytaVSL.face_ep('dodo')
            ]),
            pytaVSL.animate('bt_main', 'brightness', None, 0.3, 0.2, 's'),
            pytaVSL.animate('*ep_*', 'brightness', None, 0.3, 0.2, 's'),

            # Reinit des masques
            pytaVSL.set('f_ch2-9', 'mask', ''),
            pytaVSL.set('o_bh_droit_1', 'scale', 0.25, 0.25),
            pytaVSL.set('o_bh_embouchure', 'scale', 0.1, 0.1),
            pytaVSL.set('bato_h', 'scale', 1.2, 1.2),

        ])

        self.start_scene('sequence/f_ch2_9_2_f_ch2_10', lambda: [
            self.wait(pytaVSL.get('f_ch2-9', 'video_end') - self.veste_delay - 1, 's'), ###### TODO : un temps certain
            pytaVSL.f_switch_video('f_ch2-9', 'f_ch2-9_waiting', warpzoned=True)
        ])






    @pedalboard_button(5007, who='orl', after='f_ch2_9_veste_out_sal_acts')
    def f_ch2_10(self):
        """
        Explosion + fin Dédé
        """
        def finito():
            self.fin = _time()
            self.logger.info('Fin @ ' + str(self.fin) + ' secondes')
            duree = self.fin - self.debut
            self.logger.info('Durée : ' + str(round(duree/60)) + ' minutes')


        ### LIGHT
        qlcplus.set_strobe('bt_je'), ### LIGHT

        self.start_scene('sequence/feu_artifice', lambda: [
            self.wait(4, 's'),
            qlcplus.set_strobe('faces', max=100, duration=_rand()/2),
            qlcplus.set_strobe('contres', max=255, duration=_rand()/2),
            qlcplus.set_strobe('d_je', max=255, duration=_rand()/2),
            qlcplus.set_strobe('d_sal', max=255, duration=_rand()/2),
            qlcplus.set_strobe('d_doah', max=255, duration=_rand()/2),
            qlcplus.set_strobe('bulbes', max=255, duration=_rand()/2),
            self.wait(7,'s'),
            qlcplus.set_strobe('faces', on=False),
            qlcplus.set_strobe('contres', on=False),
            qlcplus.set_strobe('d_je', on=False),
            qlcplus.set_strobe('d_sal', on=False),
            qlcplus.set_strobe('d_doah', on=False),
            qlcplus.set_strobe('bulbes', on=False),
            qlcplus.set_scene('film'),
        ])

        ### AUDIO
        t_inter = 1.4
        t_artifice = 4.5 # Dans fichier Nico :3 + 2/30
        t_chute = 113.5
        t_explosion = 114.5 # Nico : 72 + 14/30
        t_baffe = 180
        t_decollage = 180.5
        t_explosion2 = 183.5
        self.start_scene("sequence/feu_artifice_et_chute_audio", lambda: [
            self.wait(t_inter, 's'),
            mainSampler.send('/instrument/play', 's:gros_interrupteur', 100),
            self.wait(t_artifice - t_inter, 's'),
            mainSampler.send('/instrument/play', 's:explosion1', 100),
            mainSampler.send('/instrument/play', 's:f_ch2_feu_artifice', 100),
            self.wait(t_chute - t_artifice, 's'),
            mainSampler.send('/instrument/play', 's:trijc_in_elastic', 100),
            self.wait(t_explosion - t_chute, 's'),
            # mainSampler.send('/instrument/play', 's:explosion1', 100),
            mainSampler.send('/instrument/play', 's:f_ch2-9_crash_dede', 100),
            self.wait(t_baffe - t_explosion, 's'),
            mainSampler.send('/instrument/play', 's:baffe_elan', 100),
            self.wait(t_decollage - t_baffe, 's'),
            mainSampler.send('/instrument/play', 's:f_ch2-9_decollage_enedys', 100),
            self.wait(t_explosion2 - t_decollage, 's'),
            mainSampler.send('/instrument/play', 's:explosion1', 100),
        ])

        ### VJ
        self.start_scene('f_ch2-10', lambda: [
            pytaVSL.f_noisy_switch_video('f_ch2-9', 'f_ch2-10', 0.5),
            mainSampler.noisy_switch(0.5), ### AUDIO

            # Jingle Jack Caesar
            self.wait(77.8, 's'),
            pytaVSL.jc_jingle_io('top', 0.5, 'elastic-inout'),
            mainSampler.jc_jingle_io(0.5), ### AUDIO
            # self.wait(0.5, 's'),
            # pytaVSL.set('f_ch2-10', 'video_speed', 0),


            self.wait(pytaVSL.get('f_ch2-10', 'video_end') - 77.8, 's'),
            engine.set_route('Chapitre 3'),
            finito()
        ])

        self.start_scene('sequence/f_ch2_10_veste', lambda: [
            #### L'arabesque du haut oscille et laisse tomber la théière

            self.wait(0.5, 's'),

            # Init
            pytaVSL.set('f_arandgrame_1', 'scale', 3, 3),

            # Ouverture arabesque
            pytaVSL.animate('f_ara*e_1', 'rotate_z', None, 90, 1, 's', 'elastic-out'),
            mainSampler.send('/instrument/play', 's:arabesque', 100), ### AUDIO
            qlcplus.set_scene('film'), ### LIGHT
            # qlcplus.animate('d_sal', None, 0, 2, 's'), ### LIGHT
            # qlcplus.animate('d_je', None, 0, 2, 's'),


            # Trajet Veste
            self.wait(1, 's'),
            pytaVSL.animate('f_ch2-bs_veste', 'scale', None, [0.3, 0.3], 0.15, 's'),
            pytaVSL.animate('f_ch2-bs_veste', 'position_x', 0.067, 0.07, 0.3, 's'),
            pytaVSL.animate('f_ch2-bs_veste', 'position_y', 0.36, -0.7, 1, 's'),
            pytaVSL.animate('f_ch2-bs_veste', 'rotate_z', 0, 720, 1, 's'),
            pytaVSL.set("f_ch2-bs_veste", "visible", 1),

            # Remontée Arabesque
            self.wait(0.3, 's'),
            qlcplus.set_strobe('bt_je', on=False), ### LIGHT
            pytaVSL.animate('f_ara*e_1', 'rotate_z', None, -180, 1, 's', 'elastic-out'),

            # Reinit
            self.wait(1, 's'),
            pytaVSL.set('f_aran*e_1', 'scale', 3, 3),
            pytaVSL.set('f_ch2-bs_veste', 'visible', 0)



        ])



####### DEPRECATED
    # @pedalboard_button(7)
    # def jingle_jc_enedys(self):
    #     """
    #     Jingle pendant discours Enedys
    #     """
    #     pytaVSL.jc_jingle_io('top', 0.3, 'elastic-inout')
