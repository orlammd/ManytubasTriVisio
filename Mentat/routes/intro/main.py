from ..base import *
from .video import Video
from .light import Light
import time
from random import random as _rand

from modules import *

class Intro(Video, Light, RouteBase):

    def activate(self):
        """
        Called when the engine switches to this route.
        """

        super().activate()

        transport.set_tempo(60)
        transport.set_cycle('4/4', pattern="Xxxx")

        # Setups, banks...
        mainSampler.set_kit("Manytubas")

        self.start_scene('activate_pyta', self.activate_pyta)


    def activate_pyta(self):
        ### Load slides
        pytaVSL.send('/pyta/slide/*/unload')
        pytaVSL.load_slides_from_dir('Common')
        pytaVSL.load_slides_from_dir('Chapitre1')
        pytaVSL.load_slides_from_dir('Chapitre2')
        pytaVSL.load_slides_from_dir('Chapitre3')
        pytaVSL.load_slides_from_dir('Chapitre4')
        pytaVSL.load_slides_from_dir('Chapitre5')
        pytaVSL.sync()

        ### Create clones
        for clone_name in [
            'signs_standright_jack',
            'signs_standright_caesar',
            'signs_standleft_caesar',
            'signs_standright_manytubas',
            'signs_standleft_manytubas',
            'signs_standcenter_manytubas',
            'signs_standright_tri',
            'signs_standleft_tri',
            'signs_standright_visio',
            'signs_standleft_visio']:
            pytaVSL.create_clone('signs_standleft_jack', clone_name)

        for index in range (2,7):
            pytaVSL.create_clone('f_arabesque_1', 'f_arabesque_' + str(index))
            pytaVSL.create_clone('f_arandgrame_1', 'f_arandgrame_' + str(index))

        # pytaVSL.create_clone('plane_horn_1', 'plane_horn_jc')
        pytaVSL.create_clone('plane_tv_1', 'plane_tv_jc')
        for index in range(2,6):
            pytaVSL.create_clone('plane_tv_1', 'plane_tv_' + str(index))
            pytaVSL.create_clone('plane_bulle_1', 'plane_bulle_' + str(index))
            pytaVSL.create_clone('plane_bulle_fond_1', 'plane_bulle_fond_' + str(index))

        for index in range (2,4):
            pytaVSL.create_clone('o_bh_coude_droit_1', 'o_bh_coude_droit_' + str(index))
            pytaVSL.create_clone('o_bh_droit_1', 'o_bh_droit_' + str(index))

        pytaVSL.create_clone('t_nobh_enclume_1', 't_nobh_enclume_2')
        pytaVSL.create_clone('t_nobh_champignon1', 't_nobh_champignon2')
        pytaVSL.create_clone('t_nobh_champignon1_glow', 't_nobh_champignon2_glow')

        pytaVSL.create_clone('bs_theiere', 'f_ch1-bs_theiere')
        pytaVSL.create_clone('bs_panier', 'f_ch2-bs_panier')
        pytaVSL.create_clone('bs_veste', 'f_ch2-bs_veste')
        pytaVSL.create_clone('bs_spatule', 'f_ch3-bs_spatule')
        pytaVSL.create_clone('bs_crepe', 'f_ch3-bs_crepe')
        pytaVSL.create_clone('bs_poele', 'f_ch3-bs_poele')

        for index in range(1,10):

            if index < 7:
                if index < 3:
                    pytaVSL.create_clone('explos_smoke2', 'smoke2-' + str(index))
                pytaVSL.create_clone('explos_smoke1', 'smoke1-' + str(index))
            else:
                pytaVSL.create_clone('explos_smoke1', 'tracto-smoke1-' + str(index-6))



        for index in range(2,4):
            pytaVSL.create_clone('wings1_main', 'wings' + str(index) + '_main')
            pytaVSL.create_clone('bras_wings1_gauche', 'bras_wings' + str(index) + '_gauche')
            pytaVSL.create_clone('bras_wings1_droit', 'bras_wings' + str(index) + '_droit')
            pytaVSL.create_clone('ailes_wings1_hautes', 'ailes_wings' + str(index) + '_hautes')
            pytaVSL.create_clone('ailes_wings1_basses', 'ailes_wings' + str(index) + '_basses')
        for index in range(1,6):
            pytaVSL.create_clone('wings1_main', 'wingsp' + str(index) + '_main')
            pytaVSL.create_clone('bras_wings1_gauche', 'bras_wingsp' + str(index) + '_gauche')
            pytaVSL.create_clone('bras_wings1_droit', 'bras_wingsp' + str(index) + '_droit')
            pytaVSL.create_clone('ailes_wings1_hautes', 'ailes_wingsp' + str(index) + '_hautes')
            pytaVSL.create_clone('ailes_wings1_basses', 'ailes_wingsp' + str(index) + '_basses')

        pytaVSL.create_clone('wings1_main', 'wings_jc_main')
        pytaVSL.create_clone('bras_wings1_gauche', 'bras_wings_jc_gauche')
        pytaVSL.create_clone('bras_wings1_droit', 'bras_wings_jc_droit')
        pytaVSL.create_clone('ailes_wings1_hautes', 'ailes_wings_jc_hautes')
        pytaVSL.create_clone('ailes_wings1_basses', 'ailes_wings_jc_basses')


        pytaVSL.create_clone('tracto-jantus_1', 'tracto-jantus_2')
        for index in range(1,4):
            pytaVSL.create_clone('explosion2', 'tracto-explodus_' + str(index))
        pytaVSL.create_clone('explos_smoke2', 'tracto-explodus_4')

        ### Create groups

        pytaVSL.create_group('talking_trijc', ['talking_trijc_*'])
        pytaVSL.create_group('phone', ['phone_*'])

        pytaVSL.create_group('t_trijc_lustre', ['sub_t_trijc_lustre_*'])

        pytaVSL.create_group('enedys_p', ['ep_*'])
        pytaVSL.create_group('bt_ep', ['enedys_p', 'o_ep_languette'])
        pytaVSL.create_group('bt_sent_obj', ['bs*'])

        pytaVSL.create_group('t_bh_sacvide', ['t_nobh_sacvide*'])

        for index in range(1,4):
            pytaVSL.create_group('wings' + str(index) + '_bras', ['bras_wings' + str(index) + '*'])
            pytaVSL.create_group('wings' + str(index) + '_ailes', ['ailes_wings' + str(index) + '*'])
            pytaVSL.create_group('wings' + str(index), ['wings' + str(index) + '*'])
        for index in range(1,6):
            pytaVSL.create_group('wingsp' + str(index) + '_bras', ['bras_wingsp' + str(index) + '*'])
            pytaVSL.create_group('wingsp' + str(index) + '_ailes', ['ailes_wingsp' + str(index) + '*'])
            pytaVSL.create_group('plane_wings_' + str(index), ['wingsp' + str(index) + '*'])
            pytaVSL.create_group('plane_horn_' + str(index), ['plane_wings_' + str(index), 'plane_tv_' + str(index), 'plane_bulle_' + str(index)])
        pytaVSL.create_group('wings_jc_bras', ['bras_wings_jc*'])
        pytaVSL.create_group('wings_jc_ailes', ['ailes_wings_jc*'])
        pytaVSL.create_group('plane_wings_jc', ['wings_jc*'])
        pytaVSL.create_group('plane_horn_jc', ['plane_wings_jc', 'plane_tv_jc'])


        pytaVSL.create_group('tracto-corps_a', ['tracto-corps*'])
        pytaVSL.create_group('f_ch5-tracto_b', ['tracto-corps_a', 'tracto-bottus', 'tracto-jantus*', 'tracto-corpus', 'tracto-pellus', 'tracto-explodus*', 'tracto-lingus', 'tracto-smoke1*', 'em_1'])
        # pytaVSL.create_group('bato_h_faisceau', ['it_bh_faisceau'])

        for slides_group in [
            'trijc',
            't_trijc',
            'ot_trijc',
            'lights',
            'bh',
            't_bh',
            'o_bh',
            'i_bh',
            'bt',
            'explos',
            ]:
            pytaVSL.create_group(slides_group, [slides_group + '*'])

        pytaVSL.create_group('bato_h', ['bh', 't_bh', 'i_bh', 'o_bh'])

        for slides_group in [
            'jack',
            'caesar',
            'manytubas',
            'tri',
            'visio',
            ]:
            pytaVSL.create_group('w_signs_' + slides_group, ['signs*' + slides_group])


        for index in range(1,6):
            pytaVSL.create_group('tv' + str(index), ['plane_horn_' + str(index), 'p_pub' + str(index)])
        pytaVSL.create_group('tv_jc', ['plane_horn_jc', 'p_jc'])

        pytaVSL.sync(timeout=30)
        pytaVSL.set('m_ch*', 'audio_volume', pytaVSL.get_miraye_volume())

        # Create text slides
        pytaVSL.send('/pyta/create_text', 'jcmtv_status', 'mono')
        # pytaVSL.send('/pyta/create_text', 'soustitre', 'mono')

    @pedalboard_button(1)
    def sleep(self):
        """
        TUTORIEL EN VEILLE
        """
        def main_bug():
            while True:
                self.wait(10 + _rand() * 5, 's')
                self.logger.info('MainBug Loop')
                vib = 1
                if _rand() > 0.6:
                    for slide in ['back', 'lights_stageleft', 'lights_stageright', 'w_signs_jack', 'w_signs_caesar', 'w_signs_manytubas', 'w_signs_tri', 'w_signs_visio']: # self.submodules:
                        if _rand() > 0.7 and vib: # ratio 70%
                            pytaVSL.animate(slide, 'rgbwave', 0, 0.4, 0.5, 's', 'linear-mirror')
                            vib = 0

        self.start_scene('main_bug', main_bug)

        # Pas de panneaux
        pytaVSL.signs_io('out', duration=0.1, easing='random'),

        # Lumières éteintes
        pytaVSL.set("lights*", "saturation", 0)
        pytaVSL.set("lights*", "alpha", 0.7)

        pytaVSL.animate('phone_*', 'alpha', 0.2, 0.4, 2, 's', 'cubic-in-mirror', loop=1)
        pytaVSL.animate('phone', 'offset_y',-0.001, 0.001, 1, 's', 'linear-mirror', loop=1)


    @pedalboard_button(5001, who="adrien", after="sleep")
    def tutoriel_p1(self):
        """
        DÉBUT TUTORIEL -> jusqu'à  "réels ou imaginaires"
        """

        speech_duration = 35.5
        # Sonnerie téléphone
        pytaVSL.animate('phone_comb', 'offset_y', 0, 0.1, 1, 's', 'random-mirror', loop=1)
        pytaVSL.animate('phone_comb', 'rotate_z', -10, 10, 0.3, 's', 'linear-mirror', loop=1)
        pytaVSL.animate('phone', 'offset_x', -0.001, 0.001, 0.3, 's', 'random-mirror', loop=1)
        pytaVSL.animate('phone_*', 'alpha', None, 1, 0.3, 's')
        pytaVSL.animate('phone', 'offset_y',-0.003, 0.003, 0.4, 's', 'linear-mirror', loop=1)

        #### AUDIO
        mainSampler.send('/instrument/play', 's:f_ch5_telephone'),


        self.start_scene('seq_tutoriel_p1', lambda: [
            self.wait(2, 's'),
            # Allumage
            pytaVSL.animate('tv-on', 'scale', None, [1, 1], 0.7, 's', 'elastic-inout'),
            pytaVSL.set('tv-on', 'visible', 1),
            pytaVSL.animate('phone', 'position_y', None, 1.5, 0.7, 's', 'elastic-inout'),


            self.wait(pytaVSL.get('tv-on', 'video_end') - 0.4, 's'),
            # Eveil Trijc
            pytaVSL.animate('tv-on', 'alpha', None, 0, 0.1, 's'),
            pytaVSL.animate('talking_trijc_[h-m]*', 'alpha', 0, 1, 0.3, 's'),
            pytaVSL.set('talking_trijc_back_sleeping', 'visible', 0),
            pytaVSL.set('talking_trijc_back', 'visible', 1),
            # AUDIO
            mainSampler.send('/instrument/stop', 's:f_ch5_telephone'),

            self.wait(0.5, 's'),
            pytaVSL.animate('talking_trijc_*_glow', 'alpha', 0.3, 0.8, 5, 's', 'linear-mirror', loop=1),

            pytaVSL.set('phone', 'visible', 0),
            pytaVSL.set('tv-on', 'visible', 0),


            self.wait(2, 's'),
            # Trijc parle
            pytaVSL.animate('talking_trijc_mutt*', 'offset_y', 0, -0.02, speech_duration, 's', 'random-mirror'),
            #### AUDIO
            mainSampler.send('/instrument/play', 's:tutoriel_p1'),

            self.wait(speech_duration + 1.5, 's'),
            self.tutoriel_p2()
        ])

    @pedalboard_button(102)
    def tutoriel_p2(self):
        """
        TUTORIEL "Ne vous endormez pas" -> "Procédez"
        """
        speech_duration = 15.75

        self.start_scene('seq_tutoriel_p2', lambda: [
            #### AUDIO
            mainSampler.send('/instrument/play', 's:tuba_out'),
            self.wait(0.5, 's'),

            # Trijc parle
            pytaVSL.animate('talking_trijc_mutt*', 'offset_y', 0, -0.02, speech_duration, 's', 'random-mirror'),
            #### AUDIO
            mainSampler.send('/instrument/play', 's:tutoriel_p2'),
        ])

    @pedalboard_button(5002, who='adrien', after='tutoriel_p2')
    def tutoriel_p3(self, sound='f_ch4-14_accordion'):
        """
        TUTORIEL "ERREUR"
        """
        speech_duration = 6.5

        #### AUDIO
        mainSampler.send('/instrument/play', 's:' + sound, 80),
        pytaVSL.animate('talking_trijc_*', 'offset_x', None, 0.05, 0.7, 's', 'random-mirror')
        pytaVSL.animate('talking_trijc_*', 'rgbwave', None, 0.7, 0.7, 's', 'linear-mirror')

        # Trijc parle
        pytaVSL.animate('talking_trijc_mutt*', 'offset_y', 0, -0.02, speech_duration, 's', 'random-mirror'),
        #### AUDIO
        mainSampler.send('/instrument/play', 's:tutoriel_p3'),

    @pedalboard_button(5003, who='adrien', after='tutoriel_p3')
    def tutoriel_p3_bis(self):
        """
        TUTORIEL "ERREUR 2"
        """
        self.tutoriel_p3('f_ch5_meuglement')

    def shaking_tv4(self):
        range_x = (_rand() / 2 + 0.5) * 0.01
        range_y = _rand() * 0.01
        duration = (_rand() / 2 + 0.5) * 10
        pytaVSL.shaking_slide('plane_horn_4', 'position_x', range_x, duration)
        pytaVSL.shaking_slide('p_pub4', 'position_x', range_x, duration)
        pytaVSL.shaking_slide('plane_horn_4', 'position_y', range_y, duration, 'random')
        pytaVSL.shaking_slide('p_pub4', 'position_y', range_y, duration, 'random')

    @pedalboard_button(5004, who='adrien', after='tutoriel_p3_bis')
    def tutoriel_p4(self):
        """
        TUTORIEL "Lancement JCMTV" -> "Pub"
        """
        pytaVSL.send('/pyta/text/jcmtv_status/set', 'position_z', -7)
        speech_duration = 8
        start_duration = 2
        secondstart_duration = 10
        lightson_duration = 10
        bt_duration = 10
        pub_duration = 43

        pytaVSL.send('/pyta/text/jcmtv_status/set', 'visible', 1)
        pytaVSL.send('/pyta/text/jcmtv_status/animate', 'outline', 0, 0.3, 1, 'elastic', 1)
        pytaVSL.send('/pyta/text/jcmtv_status/set', 'position_y', 0.45)


        # def loading_loop(left):
        #     while round(left) > 0:
        #         self.wait(1, 's')
        #         left = left - 1
        #     pytaVSL.animate('tv4', 'position_x', None, 1, 0.7, 's', 'elastic-inout'),
        #     pytaVSL.send('/pyta/text/jcmtv_status/set', 'text', 'le Jack Caesar Manytuba TriVisio est prêt', 0.5),
        #     self.tutoriel_p5()

        def working_wings(left):
            while round(left) > 0:
                self.logger.info('left: ' + str(left))

                d = 5 * _rand()
                coef = _rand()

                for p in range(1,4):


                    x = 2 * _rand() - 1
                    y = 0.8 * (2 * _rand() - 1)

                    if x < 1 and x > 0:
                        x = 1.1
                    elif x > -1 and x < 0:
                        x = -1.1

                    while abs(y) > 1:
                        y = y / 2


                    pytaVSL.animate('wings' + str(p), 'position_x', None, x, d * coef, 's', 'linear')
                    pytaVSL.animate('wings' + str(p), 'offset_x', None, 0.01, d * coef, 's', 'random-mirror')
                    pytaVSL.animate('wings' + str(p), 'position_y', None, y, d * coef / 10, 's', 'cubic')
                    pytaVSL.animate('wings' + str(p), 'offset_y', None, 0.01, d * coef, 's', 'random-mirror')

                self.logger.info('Boucle Wings : ' + str(d * coef) + ' s'),
                self.wait(d * coef, 's')
                left = left - d * coef
            pytaVSL.animate('wings[1-3]' , 'position_x', None, 2, 0.7, 's', 'elastic-inout')
            self.tutoriel_p5()



        self.start_scene('seq_tutoriel_p4', lambda: [
            # Machine se lance
            pytaVSL.send('/pyta/text/jcmtv_status/set', 'text', 'préchauffage', 1.5),
            #### AUDIO
            mainSampler.send('/instrument/play', 's:demarrage'),
            self.wait(start_duration, 's'),
            # Trijc parle
            pytaVSL.animate('talking_trijc_mutt*', 'offset_y', 0, -0.02, speech_duration, 's', 'random-mirror'),
            #### AUDIO
            mainSampler.send('/instrument/play', 's:tutoriel_p4'),


            self.wait(speech_duration, 's'),
            pytaVSL.send('/pyta/text/jcmtv_status/set', 'text', "lancement séquence d'attente gratuitement offerte", 2),
            self.wait(0.5, 's'),
            # Arrivée pub
            pytaVSL.flying_wings('wingsp4'),
            self.shaking_tv4(),


            mainSampler.flying_wings('tv'), ### AUDIO

            pytaVSL.set('tv4', 'visible', 1),
            pytaVSL.animate('tv4', 'position_x', None, 0, 0.7, 's', 'elastic'),

            self.wait(secondstart_duration, 's'),
            # Allumage des Lumières
            pytaVSL.send('/pyta/text/jcmtv_status/set', 'text', 'allumage des lumières', 3),
            pytaVSL.animate('lights*', 'alpha', None, 1, 5, 's'),
            pytaVSL.animate('lights*', 'saturation', None, 1, 5, 's'),
            self.wait(lightson_duration, 's'),

            # Arrivée de la boîte de télépportation
            pytaVSL.send('/pyta/text/jcmtv_status/set', 'text', 'mise en place des boites de téléportation', 3),
            pytaVSL.animate('bt', 'position_x', None, -0.525, bt_duration - 1, 's'),
            pytaVSL.animate('bt', 'offset_y', None, 0.01, bt_duration, 's', 'random-mirror'),
            self.wait(bt_duration - 1, 's'),
            pytaVSL.animate('bt', 'position_x', None, -0.48, 1, 's', 'elastic-inout'),

            # Sortie des wings
            pytaVSL.flying_wings('wings1'),
            pytaVSL.flying_wings('wings2'),
            pytaVSL.flying_wings('wings3'),
            pytaVSL.send('/pyta/text/jcmtv_status/set', 'text', 'sortie et échauffement des wings', 3),
            pytaVSL.set('wings[1-3]', 'visible', 1),
            pytaVSL.animate('wings[1-3]', 'position_y', None, 0.1, 1, 's', 'cubic'),
            self.wait(2, 's'),
            working_wings(pub_duration - bt_duration - lightson_duration),

            # C'est prêt
            # loading_loop(pub_duration - bt_duration - lightson_duration),


        ])

    @pedalboard_button(103)
    def tutoriel_p5(self):
        """
        TUTORIEL partie 5 : JC prêt -> sanf fil
        """
        speech_duration = 2 # TODO : mettre la bonne valeur

        # Pub s'en va
        pytaVSL.animate('tv4', 'position_x', None, 1.2, 0.7, 's', 'elastic-inout')

        # Texte s'en va
        pytaVSL.send('/pyta/text/loading/set', 'visible', 0)

        # Trijc parle
        pytaVSL.animate('talking_trijc_mutt*', 'offset_y', 0, -0.02, speech_duration, 's', 'random-mirror')
        #### AUDIO
        mainSampler.send('/instrument/play', 's:tutoriel_p5')







    # @pedalboard_button(3, who='arriere', after='intro')
    # def aspiration_des_pubs(self):
    #     """
    #     Aspiration des pubs
    #     """
    #     #### On stoppe la séquence de ronde, pour éviter que les valeurs ne changent au milieu de l'aspiration
    #     self.stop_scene('*')
    #     # self.stop_scene('sequence/wait_and_falldown_tv')
    #     start = 1
    #     zoom = 0
    #     for i in range(1,6):
    #         if(pytaVSL.get('tv' + str(i), 'scale')[0] > zoom):
    #             zoom = pytaVSL.get('tv' + str(i), 'scale')[0]
    #             start = i
    #     sorted = []
    #     for i in range(0,5):
    #         sorted.append(start + i if start + i < 6 else start + i - 5)
    #         # ranger dans l'ordre
    #
    #     duration = 1
    #
    #     plane_warp_1 = [0, -0.35]
    #     plane_warp_4 = [0, 0.63]
    #
    #     pub_warp_1 = [-0.0755, -0.11]
    #     pub_warp_4 = [-0.0755, 0.86]
    #
    #     def aspi_pub(index):
    #         pytaVSL.aspi_slide('plane_tv_' + str(index), plane_warp_1, plane_warp_4, duration)
    #         pytaVSL.aspi_slide('p_pub' + str(index), pub_warp_1, pub_warp_4, duration)
    #         mainSampler.aspi_slide() ### AUDIO
    #         self.start_scene('sequence/aspi_pub_' + str(index), lambda: [
    #             pytaVSL.animate('plane_wings_' + str(index), 'offset_y', None, 0.2, 1/4 * duration, 's', 'elastic-out'),
    #             self.wait(1.05 * duration / 4, 's'),
    #             pytaVSL.animate('plane_wings_' + str(index), 'offset_x', None, 1, 3/4 * duration, 's'),
    #             self.wait(3 * duration / 4, 's'),
    #             pytaVSL.set('plane_wings_' + str(index), 'offset_y', 0),
    #             pytaVSL.set('plane_wings_' + str(index), 'visible', 0),
    #             pytaVSL.set('tv' + str(index), 'visible', 0),
    #             pytaVSL.set('plane_wings_' + str(index), 'visible', 1),
    #             pytaVSL.set('plane_tv_' + str(index), 'visible', 1),
    #             pytaVSL.set('plane_wings_' + str(index), 'offset_x', 0),
    #         ])
    #
    #     self.start_scene('sequence/aspi_pubs', lambda: [
    #         ### Aspiration des pubs
    #         pytaVSL.set('t_trijc_aspi', 'rotate_z', -5),
    #         pytaVSL.trijc_io('in', 'aspi', 2, 'linear'),
    #         mainSampler.trijc_io('in', 2), ### AUDIO
    #         pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, -5, 0.1, 's', 'elastic-inout'),
    #         self.wait(2, 's'),
    #         pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, 0, 0.2, 's', 'elastic-inout'),
    #         # self.wait(0.2, 's'),
    #         aspi_pub(sorted[0]),
    #         self.wait(1.1, 's'),
    #         pytaVSL.flying_wings('wingsp' + str(sorted[0]), on=False),
    #         pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, -5, 0.5, 's'),
    #         self.desplats(sorted[4]),
    #         self.wait(1.1, 's'),
    #         pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, 0, 0.2, 's', 'elastic-inout'),
    #         # self.wait(0.2, 's'),
    #         aspi_pub(sorted[4]),
    #         self.wait(1.1, 's'),
    #         pytaVSL.flying_wings('wingsp' + str(sorted[4]), on=False),
    #         pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, -5, 0.5, 's'),
    #         self.desplats(sorted[3]),
    #         self.wait(1.1, 's'),
    #         pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, 0, 0.2, 's', 'elastic-inout'),
    #         # self.wait(0.2, 's'),
    #         aspi_pub(sorted[3]),
    #         self.wait(1.1, 's'),
    #         pytaVSL.flying_wings('wingsp' + str(sorted[3]), on=False),
    #         pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, -5, 0.5, 's'),
    #         self.desplats(sorted[2]),
    #         self.wait(1.1, 's'),
    #         pytaVSL.flying_wings('p' + str(sorted[3]), on=False),
    #         pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, 0, 0.2, 's', 'elastic'),
    #         # self.wait(0.2, 's'),
    #         aspi_pub(sorted[2]),
    #         self.wait(1.1, 's'),
    #         pytaVSL.flying_wings('wingsp' + str(sorted[2]), on=False),
    #         pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, -5, 0.5, 's'),
    #         self.desplats(sorted[1]),
    #         self.wait(1.1, 's'),
    #         pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, 0, 0.2, 's', 'elastic'),
    #         # self.wait(0.2, 's'),
    #         aspi_pub(sorted[1]),
    #         mainSampler.flying_wings('off'), ### AUDIO
    #         self.wait(1.1, 's'),
    #         pytaVSL.flying_wings('wingsp' + str(sorted[1]), on=False),
    #         pytaVSL.trijc_io('out', 'aspi', 1, 'elastic'),
    #         mainSampler.trijc_io('out', 1, easing='elastic'), ### AUDIO
    #         self.wait(1.1, 's'),
    #         engine.set_route('Chapitre 1')
    #         ]
    #     )
