from ..base import *
from .video import Video
from .light import Light
from time import time as _time
from random import random as _rand

from modules import *

class Chapitre3(Video, Light, RouteBase):

    def activate(self):
        """
        Called when the engine switches to this route.
        """

        super().activate()

        transport.set_tempo(60)
        transport.set_cycle('4/4', pattern="Xxxx")

        # Setups, banks...
        # prodSampler.set_kit(self.name)

        self.start_scene('init_chapitre3', lambda: [
            # Overlay
            self.init_chapitre3(),

            # Chapitre 3
            self.m_ch3_1()
        ])

    def init_chapitre3(self):
        self.debut = _time()
        self.logger.info('Début @ ' + str(self.debut) + ' secondes')


        pytaVSL.send('/pyta/slide/*ch2*/remove')
        pytaVSL.send('/pyta/slide/*ch2*/unload')

        chapter = 'ch3'

        ### Création des groupes du chapitre
        pytaVSL.create_group('tv1', ['plane_horn_1','p_' + chapter + '-3']) # Soft & Rains dans le journal

        pytaVSL.create_group('m_iraye', ['m_layout', 'm_' + chapter + '*'])
        pytaVSL.create_group('f_arabesques', ['f_ara*e_1', 'f_ara*e_2'])
        pytaVSL.create_group('f_arabesques_2', ['f_ara*e_3', 'f_ara*e_4'])
        pytaVSL.create_group('f_arabesques_3', ['f_ara*e_5', 'f_ara*e_6'])
        pytaVSL.create_group('f_ilm', ['f_arabesques', 'f_' + chapter + '*'])
        pytaVSL.create_group('f_ilm_2', ['f_arabesques_2', 'f2_' + chapter + '*'])

        pytaVSL.sync()

        pytaVSL.position_overlay('Chapitre3')


    @pedalboard_button(100)
    def m_ch3_1(self):
        """
        Intro Miraye
        """
        ### LIGHT
        qlcplus.set_scene('miraye')


        ### VJ
        if pytaVSL.get('trijc', 'position_x') == 0:
            pytaVSL.trijc_io('in', 'tuba', 1),
            mainSampler.trijc_io('in', 1), ### AUDIO
        self.start_scene('sequence/m_ch3_1', lambda: [

            self.wait(1, 's'),
            pytaVSL.animate('lights*', 'alpha', None, 1, 0.6),
            pytaVSL.animate('bt_main', 'brightness', None, 1, 0.6),
            pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.6),
            pytaVSL.signs_io('in', duration=0.5, easing='elastic-out'),
            pytaVSL.miraye_in('m_ch3-1'),
            mainSampler.miraye_in(), ### AUDIO
            self.wait(pytaVSL.get('m_ch3-1', 'video_end') - 1),
            pytaVSL.trijc_change_tool('aspi'),
            self.wait(0.2, 's'),
            pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, 0, 0.2, 's', 'elastic-inout'),
            self.wait(0.1, 's'),
            pytaVSL.aspi_slide('m_layout', [0, -0.45], [0, 0.52], 0.6),
            pytaVSL.aspi_slide('m_ch3-1', [-0.02, -0.445], [-0.02, 0.53], 0.6),
            mainSampler.aspi_slide(), ### AUDIO
            mainSampler.miraye_out(), ### AUDIO
            self.wait(0.8, 's'),
            self.f_ch3_2()
        ])

    @pedalboard_button(101)
    def f_ch3_2(self):
        """
        Enedys recrute Soft & Rains
        """
        ### LIGHT
        qlcplus.set_scene('film'),

        ### VJ
        self.start_scene('sequence/f_ch3_2', lambda: [
            pytaVSL.trijc_change_tool('tuba'),
            self.wait(0.2, 's'),
            pytaVSL.movie_in('f_ch3-2', 0.5),
            mainSampler.movie_in(), ### AUDIO
            self.wait(28, 's'),
            self.p_ch3_3(),
            self.wait(pytaVSL.get('f_ch3-2', 'video_end') - 28, 's'),
            self.f_ch3_4()
        ])

    @pedalboard_button(102)
    def p_ch3_3(self):
        """
        Journal passe à la télé
        """
        self.start_scene('sequence/pch3_3', lambda: [
            pytaVSL.set('tv1', 'visible', 1),
            pytaVSL.shaking_tvs(1, 'p_ch3-3'),
            pytaVSL.flying_wings('wingsp1'),
            # pytaVSL.set('p_ch3-3', 'video_time', 26), # TODO à enlever
            mainSampler.flying_wings(), ### AUDIO
            pytaVSL.animate('tv1', 'position_x', None, 0.2, 0.5, 's', 'elastic-inout'),
            self.wait(0.5, 's'),
            pytaVSL.animate('tv1', 'position_x', None, -0.1, 3, 's'),
            self.wait(3, 's'),
            pytaVSL.animate('tv1', 'position_x', None, -0.8, 0.5, 's', 'elastic-inout'),
            self.wait(1, 's'),
            pytaVSL.stop_animate('plane_horn_1', 'position_x'),
            pytaVSL.stop_animate('plane_horn_1', 'position_y'),
            pytaVSL.stop_animate('p_ch3-3', 'position_x'),
            pytaVSL.stop_animate('p_ch3-3', 'position_y'),
            pytaVSL.set('p_ch3-3', 'fish', 0),
            pytaVSL.set('tv1', 'visible', 0),
            mainSampler.flying_wings('off'), ### AUDIO
            pytaVSL.stop_animate('plane_horn_1', 'position_x'),
            pytaVSL.stop_animate('plane_horn_1', 'position_y'),
            pytaVSL.stop_animate('p_ch3-3', 'position_x'),
            pytaVSL.stop_animate('p_ch3-3', 'position_y'),            
            pytaVSL.flying_wings('wingsp1', on=False),
        ])

    @pedalboard_button(103)
    def f_ch3_4(self):
        """
        Les Barons déambulent -> Saladin veut jouer du violon
        """
        self.start_scene('sequence/f_ch3_4', lambda: [
            pytaVSL.f_switch_video('f_ch3-2', 'f_ch3-4'),
            self.wait(14, 's'),
            self.m_ch3_5()
        ])

    @pedalboard_button(104)
    def m_ch3_5(self):
        """
        Miraye hurle contre le violon et propose des crêpes
        """
        ### LIGHT
        qlcplus.set_scene('miraye', decoupes=False)

        ### VJ
        self.start_scene('sequence/m_ch3_5', lambda: [
            pytaVSL.trijc_io('in', 'tuba', 0.2, 'random'),
            mainSampler.trijc_io('in', 0.2, 'elastic'), ### AUDIO
            pytaVSL.animate('f_ch3-4', 'rgbwave', None, 0.3, 0.4, 's'),
            self.wait(0.2, 's'),
            pytaVSL.miraye_in('m_ch3-5', 0.3),
            mainSampler.miraye_in(), ### AUDIO
            self.wait(0.5, 's'),
            pytaVSL.set('f_ch3-4', 'video_speed', 0),
            self.wait(0.3, 's'),
            self.wait(pytaVSL.get('m_ch3-5', 'video_end') - 0.3 - 2, 's'),
            pytaVSL.animate('m_iraye', 'position_x', None, -1, 0.8, 's', 'elastic-inout'),
            # self.wait(pytaVSL.get('m_ch3-5', 'video_end') - 0.3 - 1, 's'),
            # pytaVSL.trijc_change_tool('aspi'),
            # self.wait(0.8, 's'),
            # pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, 0, 0.2, 's', 'elastic-inout'),
            # pytaVSL.aspi_slide('m_layout', [0, -0.45], [0, 0.52], 0.3),
            # pytaVSL.aspi_slide('m_ch3-5', [-0.02, -0.445], [-0.02, 0.53], 0.3),
            # pytaVSL.trijc_io('out', 'aspi'),
            pytaVSL.trijc_io('out', 'tuba'),
            mainSampler.trijc_io('out'), ### AUDIO
            self.wait(0.8, 's'),
            mainSampler.miraye_out(), ### AUDIO
            pytaVSL.set('m_ch3-5', 'visible', 0),
            self.f_ch3_6(),

        ])

    @pedalboard_button(105)
    def button_f_ch3_6(self):
        self.stop_scene('sequence/*')
        pytaVSL.set('m_iraye', 'visible', 0)
        pytaVSL.set('f_ilm', 'scale', 0.95, 0.95)
        pytaVSL.set('f_ilm', 'position_x', 0)
        pytaVSL.set('f_ilm', 'position_y', 0)
        pytaVSL.set('f_ilm', 'visible', 1)
        pytaVSL.set('f_ara*e_1', 'position_y', 3.04)
        pytaVSL.set('f_ara*e_2', 'position_y', -3.04)
        pytaVSL.trijc_io('out')

        self.f_ch3_6()

    def f_ch3_6(self):
        """
        Les Barons ont du matos à crêpes
        """
        ### LIGHT
        qlcplus.set_scene('film')

        ### VJ
        self.start_scene('sequence/f_ch3_6', lambda: [
            pytaVSL.set('f_ch3-6', 'rgbwave', 0.3),
            pytaVSL.set('f_ch3-6', 'visible', 1),
            pytaVSL.set('f_ch3-6_warpzoned', 'visible', 1),
            pytaVSL.set('f_ch3-4', 'visible', 0),
            pytaVSL.set('f_ch3-4', 'rgbwave', 0),
            pytaVSL.animate('f_ch3-6', 'rgbwave', None, 0, 1, 's'),
            # pytaVSL.set('f_ch3-6', 'video_speed', 1),
            #### TODO vanupié récupération de la poele et tout

            self.wait(pytaVSL.get('f_ch3-6', 'video_end') - 5 , 's'),
            self.f_ch3_6_crepe_out(),
        ])

    @pedalboard_button(1050)
    def f_ch3_6_crepe_out(self):

        ### VJ
        def crepe_out_batohacker():
            # Init
            pytaVSL.set('bato_h', 'scale', -2, 2),
            pytaVSL.set('bato_h', 'position_x', -0.67),
            pytaVSL.set('bato_h', 'position_y', 0.11),

            pytaVSL.set('o_bh_embouchure', 'scale', 0.08, 0.08),
            pytaVSL.set('o_bh_embouchure', 'position_x', 0.105),
            pytaVSL.set('o_bh_embouchure', 'position_y', -0.56),

            self.start_scene('sequence/crepe_out_batohacker', lambda: [
                self.wait(0.01, 's'),

                # Arrivée
                pytaVSL.batohacker_hmove(-0.67, 'cour', 0.3),
                mainSampler.batohacker_move(0.3), ### AUDIO

                # Punch
                self.wait(0.3, 's'),
                pytaVSL.batohacker_punch('f_ch3-6', x_dest=0.1),
                mainSampler.batohacker_punch(), ### AUDIO
                pytaVSL.animate('f_ch3-6*', 'brightness', None, 0.3, 0.3, 's'),


                # Positionnement fin
                self.wait(0.3, 's'),
                pytaVSL.batohacker_hmove(-0.38, 'cour', 0.3),
                mainSampler.batohacker_move(0.3), ### AUDIO


                # Sortie Entrailles
                self.wait(0.6, 's'),
                pytaVSL.animate('bato_h', 'rotate_z', None, 10, 0.3, 's', 'elastic-out'),
                pytaVSL.batohacker_trp_io('out', 0.3),
                mainSampler.batohacker_trp_io(), ### AUDIO



                # Branchement
                self.wait(1, 's'),
                mainSampler.batohacker_plug(0.05), ### AUDIO
                pytaVSL.set('o_bh_coude_droit_1', 'visible', 1),
                self.wait(0.05, 's'),
                pytaVSL.set('o_bh_coude_droit_2', 'visible', 1),
                pytaVSL.set('o_bh_embouchure', 'visible', 1),


                # Allumage EP et plateau
                self.wait(1, 's'),
                ### LIGHT
                qlcplus.set_strobe('bt_sal'),
                qlcplus.animate('d_sal', None, 255, 2, 's'),
                qlcplus.animate('d_je', None, 255, 2, 's'),


                pytaVSL.bascule_ep(),
                mainSampler.bascule_ep(), ### AUDIO
                pytaVSL.allume_ep(on='courrier'),
                mainSampler.allume_ep(on='courrier') ### AUDIO

            ])


        def crepe_out_ep():
            self.start_scene('sequence/crepe_out_ep', lambda: [
                # EP remarque le BatoHacker et file de l'autre côté
                self.wait(0.2, 's'),
                # pytaVSL.bascule_ep(),
                # mainSampler.bascule_ep(), ### AUDIO
                self.start_scene('sequence/f_ch3-6_ep_reveil', lambda: [
                    pytaVSL.face_ep('reveil'),
                ]),
                self.wait(0.5, 's'),
                pytaVSL.animate('bt_main', 'brightness', None, 1, 0.2, 's'),
                pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.2, 's'),
                pytaVSL.animate('bt', 'position_x', None, 0.6, 0.7, 's', 'elastic-inout'),
                self.wait(0.7, 's'),
                pytaVSL.set('bt', 'scale', -0.2, 0.2),
                pytaVSL.set('bt', 'position_y', -0.42),
                pytaVSL.animate('bt', 'position_x', -0.6, -0.46, 0.5, 's', 'elastic-out'),


                # EP reçoit du courrier
                #### voir dans la scène crepe_out_batohacker


            ])

        self.start_scene('sequence/crepe_out', lambda:[
            # self.wait(71, 's'),
            crepe_out_batohacker(),
            crepe_out_ep()
        ])



    @pedalboard_button(5001, who='orl', after='f_ch3_6_crepe_out')
    def f_ch1_11_batohacker1_doah_acts(self):
        self.start_scene('sequence/f_ch3_6_batohacker1_sal_acts', lambda: [
            # Relâche film
            pytaVSL.animate('f_ch3-6*', 'brightness', None, 1, 0.1, 's'),
            pytaVSL.set('f_ch3-6', 'video_speed', 1),
            pytaVSL.set('f_ch3-6_warpzoned', 'video_speed', 1),
            mainSampler.batohacker_film_stopped(stopped=True), ### AUDIO

            # Prépa mask
            pytaVSL.set('f_ch3-6', 'mask', 'f_ch3-6_mask'),

            # Avalage
            self.wait(1.5, 's'),
            mainSampler.batohacker_warp(warp_in=True), ### AUDIO
            pytaVSL.animate('f_ch3-6_warpzoned', 'rgbwave', None, 0.4, 0.3, 's'),
            pytaVSL.animate('f_ch3-6_warpzoned', 'fish', None, 0.4, 0.3, 's'),
            self.wait(1, 's'),
            mainSampler.batohacker_swallow(0.5), ### AUDIO
            pytaVSL.batohacker_swallow(0.5),
            self.wait(0.5, 's'),
            pytaVSL.animate('f_ch3-6_warpzoned', 'rgbwave', None, 0, 0.3, 's'),
            pytaVSL.animate('f_ch3-6_warpzoned', 'fish', None, 0, 0.3, 's'),
            mainSampler.batohacker_warp(warp_in=False), ### AUDIO


            # Débranchage
            self.wait(0.6, 's'),
            mainSampler.batohacker_unplug(0.1), ### AUDIO
            pytaVSL.set('o_bh_coude_droit_2', 'visible', 0),
            pytaVSL.set('o_bh_embouchure', 'visible', 0),
            self.wait(0.05, 's'),
            pytaVSL.set('o_bh_droit_1', 'visible', 0),
            self.wait(0.05, 's'),
            pytaVSL.set('o_bh_coude_droit_1', 'visible', 0),


            # Extinction EP + Plateau
            self.wait(0.5, 's'),
            qlcplus.set_strobe('bt_sal', on=False),

            pytaVSL.bascule_ep(False),
            mainSampler.bascule_ep(), ### AUDIO
            pytaVSL.allume_ep(False),


            # Rentrage des entrailles
            self.wait(1, 's'),
            pytaVSL.batohacker_trp_io('in'),
            mainSampler.batohacker_trp_io(), ### AUDIO

            # Remet le film à sa place
            self.wait(1, 's'),
            pytaVSL.batohacker_debevier(),
            mainSampler.batohacker_debevier(), ### AUDIO
            self.wait(0.4, 's'),
            pytaVSL.animate('f_ilm', 'position_x', None, 0, 1, 's'),

            # S'en va
            self.wait(1, 's'),
            pytaVSL.batohacker_hmove(-1, 'jardin', 0.5),
            mainSampler.batohacker_move(0.5, goto='departure'), ### AUDIO


            # EP s'en retourne #########
            self.wait(0.5, 's'),
            pytaVSL.animate('bt', 'position_x', None, -0.6, 1, 's', 'elastic-inout'),
            self.wait(0.7, 's'),
            pytaVSL.set('bt', 'scale', 0.2, 0.2),
            pytaVSL.set('bt', 'position_y', -0.4),
            pytaVSL.animate('bt', 'position_x', 0.6, 0.525, 0.7, 's', 'elastic-inout'),
            self.wait(0.7, 's'),


            # EP se rendort
            self.wait(0.5, 's'),
            self.start_scene('sequence/f_ch1-11_ep_dodo', lambda: [
                pytaVSL.face_ep('dodo')
            ]),
            pytaVSL.animate('bt_main', 'brightness', None, 0.3, 0.2, 's'),
            pytaVSL.animate('*ep_*', 'brightness', None, 0.3, 0.2, 's'),

            # Reinit des masques
            pytaVSL.set('f_ch1-11', 'mask', ''),
            pytaVSL.set('o_bh_droit_1', 'scale', 0.25, 0.25),
            pytaVSL.set('o_bh_embouchure', 'scale', 0.1, 0.1),
            pytaVSL.set('bato_h', 'scale', 1.2, 1.2),
            pytaVSL.set('bato_h', 'rotate_z', 0),

        ])

        self.start_scene('sequence/f_ch3_6_2_f_ch3_6_waiting', lambda: [
            self.wait(4.5, 's'),
            pytaVSL.f_switch_video('f_ch3-6', 'f_ch3-6_waiting'),
        ])



    @pedalboard_button(5002, who='adrien', after='f_ch1_11_batohacker1_doah_acts')
    def m_ch3_7(self):
        """
        Miraye demande d'enchaîner
        """
        ### LIGHT
        qlcplus.set_scene('miraye', duration=1, decoupes=False)

        ### VJ
        self.start_scene('sequence/m_ch3-7', lambda: [
            pytaVSL.trijc_io('in', 'tuba', 0.5),
            mainSampler.trijc_io('in', 0.5), ### AUDIO
            self.wait(0.5, 's'),
            pytaVSL.miraye_in('m_ch3-7', 0.3),
            mainSampler.miraye_in(), ### AUDIO
            self.wait(0.5, 's'),
            pytaVSL.trijc_change_tool('aspi'),
            self.wait(pytaVSL.get('m_ch3-7', 'video_end') - 0.7, 's'),
            pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, 0, 0.2, 's', 'elastic-inout'),
            pytaVSL.aspi_slide('m_layout', [0, -0.45], [0, 0.52], 0.3),
            pytaVSL.aspi_slide('m_ch3-7', [-0.02, -0.445], [-0.02, 0.53], 0.3),
            mainSampler.aspi_slide(), ### AUDIO
            mainSampler.miraye_out(), ### AUDIO
            self.wait(0.7, 's'),
            pytaVSL.trijc_change_tool('lustre'),
            pytaVSL.trijc_turn_lights('off', 0.5),
            mainSampler.trijc_turn_lights(0.5), ### AUDIO
            pytaVSL.animate('f_ch3-6_waiting', 'alpha', None, 0, 0.5),
            self.wait(0.5, 's'),
            self.f_ch3_8()
        ])

    @pedalboard_button(107)
    def f_ch3_8(self):
        """
        S&R chez le Fakir
        """
        ### LIGHT
        qlcplus.set_scene('film', decoupes=False),
        qlcplus.animate('d_doah', None, 0, 2, 's'),
        qlcplus.animate('d_sal', None, 0, 2, 's'),
        qlcplus.animate('d_je', None, 0.6 * 255, 2, 's'),

        ### AUDIO
        t_f1 = 3
        t_f2 = 11
        self.start_scene('sequence/f_ch3-8_audio', lambda: [
            self.wait(t_f1, 's'),
            mainSampler.send('/instrument/play', 's:f_ch3_feu_cerceau'),
            self.wait(t_f2 - t_f1, 's'),
            mainSampler.send('/instrument/play', 's:f_ch3_feu_cerceau')
        ])

        ### VJ
        self.start_scene('sequence/f_ch3_8', lambda: [
            pytaVSL.set('f_ch3-8', 'visible', 1),
            pytaVSL.trijc_turn_lights('on', 0.5),
            mainSampler.trijc_turn_lights(0.5), ### AUDIO
            pytaVSL.set('f_ch3-6_waiting', 'visible', 0),
            pytaVSL.animate('f_ch3-8', 'alpha', None, 1, 0.5),
            self.wait(0.5, 's'),
            pytaVSL.trijc_io('out', 'lustre'),
            mainSampler.trijc_io('out'), ### AUDIO
            self.wait(pytaVSL.get('f_ch3-8', 'video_end') - 0.5, 's'),
            self.f_ch3_9()
        ])

    @pedalboard_button(108)
    def f_ch3_9(self):
        """
        Dagz essaie le cerceau
        """
        ### LIGHT
        qlcplus.animate('d_je', None, 255, 1, 's'),

        ### AUDIO
        t_f1 = 1.1
        t_f2 = 6.2
        def audio_flames():
            while True:
                self.wait(t_f1, 's'),
                mainSampler.send('/instrument/play', 's:f_ch3_feu_cerceau'),
                self.wait(t_f2 - t_f1, 's'),
                mainSampler.send('/instrument/play', 's:f_ch3_feu_cerceau'),
                self.wait(pytaVSL.get('f_ch3-9', 'video_end') - t_f2, 's'),

        self.start_scene('sequence/f_ch3_audio_flames', audio_flames)


        ### VJ
        self.start_scene('sequence/f_ch3_9', lambda: [
            pytaVSL.f_switch_video('f_ch3-8', 'f_ch3-9'),
        ])

    @pedalboard_button(5003, who='orl', after='f_ch3_9')
    def f_ch3_10(self):
        """
        La spatule arrive sur le paillasson du fakir
        """
        ### LIGHT
        qlcplus.set_strobe('bt_je'),

        ### AUDIO
        self.stop_scene('sequence/f_ch3_audio_flames')
        t_explosion = 1.5 + 0.9
        self.start_scene('sequence/f_ch3-10_audio', lambda: [
            self.wait(t_explosion, 's'),
            mainSampler.send('/instrument/play', 's:f_ch3_feu_cerceau'),
        ])

        ### VJ
        # Init
        pytaVSL.set('f_ilm_2', 'position_x', 1)
        self.start_scene('sequence/f_ch3_10', lambda: [
            pytaVSL.f_switch_video('f_ch3-9', 'f_ch3-10_up'),
            pytaVSL.set('f2_ch3-10', 'visible', 1),
            self.wait(pytaVSL.get('f2_ch3-10', 'video_end'), 's'),
            self.f_ch3_11()
        ])

        # Init
        pytaVSL.set('f_aran*e_1', 'scale', -3, 3)
        self.duree_trajet_spatule = 1
        self.start_scene('sequence/f_ch3_10_voyage_spatule', lambda: [
            # L'arabesque pivote
            pytaVSL.animate('f_ara*e_1', 'rotate_z', None, 720, 1, 's', 'elastic-out'),
            pytaVSL.animate('f_ara*e_1', 'position_x', None, -2.5, 1, 's', 'elastic-out'),
            mainSampler.send('/instrument/play', 's:arabesque', 100), ### AUDIO

            # L'arabesque crache la bs_spatule
            self.wait(0.9, 's'),
            pytaVSL.animate('f_ch3-bs_spatule*', 'position_x', -0.35, 1.04, self.duree_trajet_spatule, 's'),
            # pytaVSL.animate('f_ch3-bs_spatule', 'position_y', 0.36, 0.11, self.duree_trajet_spatule, 's'),
            pytaVSL.animate('f_ch3-bs_spatule*', 'position_y', 0.36, -0.2, self.duree_trajet_spatule / 3, 's'),
            pytaVSL.animate('f_ch3-bs_spatule*', 'scale', [0.1, 0.1], [0.25, 0.25], self.duree_trajet_spatule, 's'),
            pytaVSL.set('f_ch3-bs_spatule', 'visible', 1),

            # La spatule voyage (en fait, les films )se succèdent
            qlcplus.animate('d_je', None, 0.6 * 255, 1, 's'),
            pytaVSL.set('f2_ch3-10', 'visible', 1),
            pytaVSL.animate('f_ilm', 'position_x', 0, -1, self.duree_trajet_spatule, 's'),
            pytaVSL.animate('f_ilm_2', 'position_x', 1, 0, self.duree_trajet_spatule, 's'),

            # Pendant que l'arabesque reprend son chemin
            qlcplus.set_strobe('bt_je', on=False), ### LIGHT
            self.wait(self.duree_trajet_spatule / 3, 's'),
            pytaVSL.animate('f_ara*e_1', 'rotate_z', 720, -180, 1, 's', 'elastic-out'),
            pytaVSL.animate('f_ara*e_1', 'position_x', -2.5, 0, 1, 's', 'elastic-out'),

            # En y le mouvement de la spatule est plus compliqué
            pytaVSL.animate('f_ch3-bs_spatule*', 'position_y', None, 0.2, self.duree_trajet_spatule / 3, 's'),
            pytaVSL.animate('f_ch3-bs_spatule*', 'rotate_z', None, -360, 2 * self.duree_trajet_spatule / 3, 's'),
            pytaVSL.set('f_ch3-bs_spatule_flamme', 'visible', 1),
            self.wait(self.duree_trajet_spatule / 3, 's'),
            pytaVSL.animate('f_ch3-bs_spatule*', 'position_y', None, 0.11, self.duree_trajet_spatule / 3, 's'),

            # La spatule disaparaît dans le feu
            self.wait(self.duree_trajet_spatule / 3, 's'),
            pytaVSL.animate('f_ch3-bs_spatule*', 'alpha', None, 0, 0.1, 's'),
            self.wait(0.1, 's'),
            pytaVSL.set('f_ch3-bs_spatule*', 'visible', 0),
            pytaVSL.set('f_ch3-bs_spatule*', 'alpha', 1),


            # Reinit
            pytaVSL.set('f_ch3-10_up', 'visible', 0)
        ])

    @pedalboard_button(110)
    def f_ch3_11(self):
        """
        Soft & Rains et la voyante
        """
        self.start_scene('sequence/f_ch3_11', lambda: [
            pytaVSL.f_switch_video('f_ch3-10_up', 'f_ch3-11'),
            pytaVSL.set('f_ilm', 'position_x', 0),
            pytaVSL.set('f_ilm_2', 'position_x', 1),
            self.wait(pytaVSL.get('f_ch3-11', 'video_end') - 0.1, 's'),
            pytaVSL.f_switch_video('f_ch3-11', 'f_ch3-11_waiting'),
            qlcplus.animate('d_je', None, 255, 1, 's'), ### LIGHT
        ])

    @pedalboard_button(5004, who='orl', after='f_ch3_11')
    def f_ch3_12(self):
        """
        La crêpe arrive sur la boule de cristal
        """
        ### LIGHT
        qlcplus.set_strobe('bt_je'),

        ### AUDIO
        t_degagement_crepe = 13
        self.start_scene('sequence/f_ch3-12_audio', lambda: [
            self.wait(t_degagement_crepe, 's'),
            mainSampler.send('/instrument/play', 's:f_ch3_degagement_crepe', 100), ### AUDIO
            self.wait(0.5, 's'),
            mainSampler.send('/instrument/play', 's:f_ch3_degagement_paillasson', 100), ### AUDIO
        ])

        ### VJ
        self.start_scene('sequence/f_ch3_12', lambda: [
            # Les arabesques s'ouvrent pour laisser passer la crêpe
            pytaVSL.animate('f_ara*e_1', 'rotate_z', -180, 70, 0.5, 's'),
            mainSampler.send('/instrument/play', 's:arabesque', 100), ### AUDIO
            self.wait(0.5, 's'),
            pytaVSL.f_switch_video('f_ch3-11_waiting', 'f_ch3-12', f_secu='f_ch3-11'),
            mainSampler.send('/instrument/play', 's:f_ch3_chute_crepe', 100), ### AUDIO
            pytaVSL.animate('f_ara*e_1', 'rotate_z', 70, -180, 0.5, 's'),
            self.wait(0.5, 's'),
            qlcplus.set_strobe('bt_je', on=False), ### LIGHT
            qlcplus.animate('d_je', None, 0.6 * 255, 1, 's'), ### LIGHT
            self.wait(pytaVSL.get('f_ch3-12', 'video_end') - 0.5 - 0.5, 's'),
            self.f_ch3_13()
        ])

    @pedalboard_button(111)
    def f_ch3_13(self):
        """
        Soft & Rains chez Dalida
        """
        self.start_scene('sequence/f_ch3_13', lambda: [
            pytaVSL.f_switch_video('f_ch3-12', 'f_ch3-13'),
            self.wait(pytaVSL.get('f_ch3-13', 'video_end'), 's'),
            pytaVSL.f_switch_video('f_ch3-13', 'f_ch3-13_waiting'),
            qlcplus.animate('d_je', None, 255, 1, 's'), ### LIGHT
        ])

    @pedalboard_button(5005, who='orl', after='f_ch3_13')
    def f_ch3_14(self):
        """
        La poële arrive sur la tête de S&R
        """
        ### LIGHT
        qlcplus.set_strobe('bt_je'),

        ### AUDIO
        t_offset = 0.7 + 0.5
        t_crash = 0.5 + t_offset
        t_degagement_sr = 5.8 + t_offset
        t_explosion = 8 + t_offset
        self.start_scene('sequence/f_ch3-12_audio', lambda: [
            self.wait(t_crash, 's'),
            mainSampler.send('/instrument/play', 's:f_ch3_crash_sr', 100), ### AUDIO
            self.wait(t_degagement_sr - t_crash, 's'),
            mainSampler.send('/instrument/play', 's:f_ch3_degagement_crepe', 100), ### AUDIO
            self.wait(0.5, 's'),
            mainSampler.send('/instrument/play', 's:f_ch3_degagement_paillasson', 100), ### AUDIO
            self.wait(t_explosion  - t_degagement_sr - 0.5, 's'),
            mainSampler.send('/instrument/play', 's:explosion1', 100), ### AUDIO
        ])

        ### VJ
        # Init
        pytaVSL.set('f_aran*e_1', 'scale', 3, 3)
        pytaVSL.set('f_ch3-bs_poele', 'scale', 0.1, 0.1)
        self.start_scene('sequence/f_ch3_14', lambda: [
            # L'arabesque s'ouvre et se referme
            pytaVSL.animate('f_ara*e_1', 'rotate_z', -180, 80, 0.8, 's', 'elastic-out'),
            mainSampler.send('/instrument/play', 's:arabesque', 100), ### AUDIO

            # La poele tombe
            self.wait(0.7, 's'),
            qlcplus.set_scene('film'), ### LIGHT
            pytaVSL.animate('f_ch3-bs_poele', 'position_x', 0.05, 0.06, 1, 's', 'exponential'),
            pytaVSL.animate('f_ch3-bs_poele', 'position_y', 0.375, 0.08, 1, 's', 'exponential'),
            pytaVSL.animate('f_ch3-bs_poele', 'rotate_z', 110, 90, 1, 's', 'exponential'),
            pytaVSL.set('f_ch3-bs_poele', 'visible', 1),

            # On lance le film pour que synchro
            self.wait(0.5, 's'),
            pytaVSL.f_switch_video('f_ch3-13_waiting', 'f_ch3-14'),

            # et se referme
            self.wait(0.2, 's'),
            qlcplus.set_strobe('bt_je', on=False), ### LIGHT
            pytaVSL.animate('f_ara*e_1', 'rotate_z', 80, -180, 0.5, 's', 'elastic-out'),

            # la poele rebondit
            self.wait(0.3, 's'),
            pytaVSL.animate('f_ch3-bs_poele', 'position_x', None, 0.7, 0.5, 's'),
            pytaVSL.animate('f_ch3-bs_poele', 'position_y', None, 0.4, 0.5, 's', 'linear-mirror'),


            # Reinit
            self.wait(2, 's'),
            pytaVSL.set('f_ch3-bs_poele', 'visible', 1),

            # self.wait(pytaVSL.get('f_ch3-12', 'video_end') - 0.2, 's'),
        ])

        def finito():
            self.fin = _time()
            self.logger.info('Fin @ ' + str(self.fin) + ' secondes')
            duree = self.fin - self.debut
            self.logger.info('Durée : ' + str(round(duree/60)) + ' minutes')


        self.start_scene('sequence/f_ch3_14_2_chapter_4', lambda: [
            self.wait(pytaVSL.get('f_ch3-14', 'video_end') - 3, 's'),
            pytaVSL.trijc_io('in', 'lustre', 3),
            mainSampler.trijc_io('in', 3), ### AUDIO
            self.wait(3, 's'),
            pytaVSL.trijc_turn_lights('off', 1),
            mainSampler.trijc_turn_lights(1), ### AUDIO
            pytaVSL.animate('f_*', 'alpha', None, 0, 1, 's'),
            self.wait(1, 's'),
            pytaVSL.set('f_ilm', 'visible', 0),
            pytaVSL.set('f_*', 'alpha', 1),
            engine.set_route('Chapitre 4'),
            finito()
        ])
