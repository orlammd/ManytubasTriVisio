# import Route base class
from mentat import Route

# import engine & modules objects
# so that they can be used in the routing
from modules import *

from inspect import getmembers

method_index = 0
class pedalboard_button():
    """
    Decorator for route methods that can be called directly
    from pedalboard button messages
    """
    def __init__(self, button=None, who=None, after=None):
        self.button = button
        self.human_name = who
        self.human_after_method_name = after

    def __call__(self, method):

        if hasattr(method, 'pedalboard_buttons'):
            # don't redecorate if multiple buttons are assigned
            decorated = method
        else:
            # when called, the method indicates it's been called
            def decorated(_self, *args, **kwargs):
                _self.called_methods.append(method.__name__)
                return method(_self, *args, **kwargs)

        decorated.__name__ = method.__name__

        if self.human_name is not None:
            decorated.human_name = self.human_name

        if self.human_after_method_name is not None:
            decorated.human_after_method_name = self.human_after_method_name

        if self.button is not None:

            # keep track of method order in code
            if not hasattr(decorated, 'index'):
                global method_index
                decorated.index = method_index
                method_index += 1

            # install flag for RouteBase.__init__
            if not hasattr(decorated, 'pedalboard_buttons'):
                decorated.pedalboard_buttons = {}

            decorated.pedalboard_buttons[self.button] = True

        return decorated

class RouteBase(Route):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.direct_routing = {
            '/pedalboard/button': {},
        }

        self.called_methods = []
        self.human_routing = {}

        # init decorated method hooks
        for name, method in getmembers(self):

            if hasattr(method, 'pedalboard_buttons'):
                for button in method.pedalboard_buttons:
                    if button not in self.direct_routing['/pedalboard/button']:
                        self.direct_routing['/pedalboard/button'][button] = []
                    self.direct_routing['/pedalboard/button'][button].insert(0, method)

            if hasattr(method, 'human_name'):
                if method.human_name not in self.human_routing:
                    self.human_routing[method.human_name] = []
                self.human_routing[method.human_name].append(method)

        for name in self.human_routing:
            # reverse sort methods associated with human buttons
            self.human_routing[name].sort(key=lambda method: method.index, reverse=True)


    def call_human_button(self, name):
        if name not in self.human_routing:
            self.logger.warning('no routing found for %s' % name)
            return

        # check all methods associated with that human
        for method in self.human_routing[name]:

            # if method's condition has not been called yet, ignore it
            if method.human_after_method_name is not None:
                if method.human_after_method_name not in self.called_methods:
                    continue

            # if method itself was already called once, ignore it
            if method.__name__ in self.called_methods:
                continue

            # now we should call method
            return method()

        self.logger.warning('no satisfying routing found for %s' % name)

    def activate(self):

        # reset called methods stats
        self.called_methods = []

        super().activate()


    def route(self, protocol, port, address, args):
        """
        Base routing for all routes
        """
        if address in self.direct_routing:
            if len(args) > 0 and args[0] in self.direct_routing[address]:
                for method in self.direct_routing[address][args[0]]:
                    # button debug state arg:
                    # save / load global state associated with the method
                    # if the last arg in the message is "save" or "load"
                    # state must be saved at least once for "load" to work
                    state_cmd = str(args[-1]).lower()
                    if state_cmd == 'save' or state_cmd == 'load':
                        state_name = 'autosave_%s_%s' % (self.name, method.__name__)
                        if state_cmd == 'save':
                            self.engine.save(state_name, omit_defaults=True)
                        else:
                            self.engine.reset()
                            self.engine.load(state_name)

                    method()

        if address == '/set_route':
            engine.set_route(args[0])

        if address == '/load_slides_from_dir':
            if len(args) > 0:
                pytaVSL.load_slides_from_dir(args[0])
            else:
                pytaVSL.load_slides_from_dir()

        if address == '/save_state':
            """
            Save state omitting default values
            args: chapter
            """
            pytaVSL.save_state(args[0])

        if address == '/position_overlay':
            overlay = 'Common'
            if len(args) > 0:
                overlay = args[0]
            pytaVSL.position_overlay(overlay)

        if address == '/stop_scene':
            self.stop_scene('sequence/*')

        if address == '/trijc_io':
            self.logger.info('trijc_io osc trigged')
            pytaVSL.trijc_io(*args)

        if address == '/batohacker_move':
            pytaVSL.batohacker_hmove(*args)
            mainSampler.batohacker_move()

        if address == '/batohacker_trp_io':
            pytaVSL.batohacker_trp_io(*args)

        if address == '/batohacker_punch':
            pytaVSL.batohacker_punch()

        if address == '/batohacker_shake':
            pytaVSL.batohacker_shaking(*args)

        if address == '/batohacker_plug':
            pytaVSL.batohacker_plug(*args)

        if address == '/batohacker_swallow':
            pytaVSL.batohacker_swallow(*args)


        # DEPRACACATED -> utiliser l'API de controle generiqie
        if '/pyta' in address:
            pytaVSL.send(address, *args)
            # transmet les messages à pyta sans que mentat
            # integre les changement de valeur
            self.logger.warning('on fait de la merde !')

        if '/slide/set' in address:
            pytaVSL.set(args[0], args[1], args[2])



    def start_sequence(self, name, sequence, loop=True):
        """
        Start scene with sequence prefix and self.play_sequence() as method

        **Parameters**

        - `name`: name of sequence
        - `sequence`: see Route.play_sequences()
        - `loop`: see Route.play_sequences()
        """
        self.start_scene('sequences/%s' % name, self.play_sequence, sequence, loop)

    def stop_sequence(self, name):
        """
        Stop scene with sequence prefix

        **Parameters**

        - `name`: name of sequence
        """
        self.stop_scene('sequences/%s' % name)

    def resetFX(self):
        """
        Reset effects
        """
        # # BassFX
        # for name in bassFX.meta_parameters:
        #     bassFX.set(name, 'off')
        #
        #
        # for name, mod in engine.modules.items():
        #
        #     # SynthsFX
        #     if 'SynthsFX' in name:
        #
        #         for name in mod.submodules:
        #             # Ins
        #             if name not in mod.name:
        #                 mod.set(name, 'Gain', 'Gain', -70.0)
        #
        #         # Outs
        #         mod.set(mod.name, 'Gain', 'Mute', 1.0)
        #
        #
        #     # SamplesFX
        #     elif 'SamplesFX' in name:
        #         for i in range(1,6):
        #             # Ins
        #             mod.set('Samples' + str(i), 'Gain', 'Gain', -70.0)
        #
        #         # Outs
        #         mod.set(name, 'Gain', 'Mute', 1.0)
        #
        #     # VocalsFX
        #     elif 'VocalsNanoFX' in name or 'VocalsKeschFX' in name:
        #         for name in mod.submodules:
        #             # Ins
        #             if name not in mod.name:
        #                 mod.set(name, 'Gain', 'Gain', -70.0)
        #         # Outs
        #         mod.set(name, 'Gain', 'Mute', 1.0)


        postprocess.set_filter('*', 24000)
        postprocess.set_pitch('*', 1)

    # def resetSamples(self):
    #     """
    #     Reset (mute) all samples
    #     """
    #     for i in range (1,6):
    #         samples.set('Samples' + str(i), 'Gain', 'Mute', 1.0)

    def pause_loops(self):
        """
        Pause loopers and looped scenes (sequences)
        """
        # stop all mentat sequences
        self.stop_sequence('*')

    # def reset(self):
    #     """
    #     Reset samples and effects
    #     """
    #     self.resetFX()
    #     self.resetSamples()
    #
    #     for name in outputs.submodules:
    #         outputs.submodules[name].set('Gain', 'Mute', 0)
