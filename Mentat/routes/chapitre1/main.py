from ..base import *
from .video import Video
from .light import Light
from random import random as _rand
from time import time as _time

from modules import *

class Chapitre1(Video, Light, RouteBase):

    def activate(self):
        """
        Called when the engine switches to this route.
        """

        super().activate()

        transport.set_tempo(60)
        transport.set_cycle('4/4', pattern="Xxxx")

        # Setups, banks...
        # mainSampler.set_kit(self.name)


        self.start_scene('init_chapitre1', lambda: [
            # Overlay
            self.init_chapitre1(),
            # self.wait(0.2, 's'),

            # Chapitre 1
            self.lancement_miraye_1()
        ])


    def init_chapitre1(self):
        self.debut = _time()
        self.logger.info('Début @ ' + str(self.debut) + ' secondes')


        chapter = 'ch1'

        ### LIGHTS
        qlcplus.set_scene('half_stage')


        ### VJ
        ### Création des groupes du chapitre
        pytaVSL.create_group('m_iraye', ['m_layout', 'm_' + chapter + '*'])
        pytaVSL.create_group('f_arabesques', ['f_arabesque_*', 'f_arandgrame_*'])
        pytaVSL.create_group('f_ilm', ['f_arabesques', 'f_ch1-*'])
        for index in range(1,5):
            self.logger.info('Création de groupe')
            pytaVSL.create_group('tv' + str(index), ['plane_horn_' + str(index), ',p_' + chapter + '-' + str(index+2)])

        # pytaVSL.create_group('ti_ch1-1', ['ti_ch1-1_*'])

        pytaVSL.sync()

        pytaVSL.position_overlay('Chapitre1')

        # ])

    @pedalboard_button(99)
    def lancement_miraye_1(self):
        """
        Lancement Miraye Part 1
        """

        if pytaVSL.get('trijc', 'position_x') == 0:
            pytaVSL.trijc_io('in', 'tuba', 1, 'elastic-inout'),
            mainSampler.trijc_io('in', 1, easing='elastic-inout'), ### AUDIO
        self.start_scene('sequence/lancement_miraye_1', lambda: [
            ### Lancement du Film
            self.wait(1.2, 's'),
            pytaVSL.animate('lights*', 'alpha', None, 1, 0.6),
            pytaVSL.animate('bt_main', 'brightness', None, 1, 0.6),
            pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.6),
            pytaVSL.signs_io('in', duration=0.5, easing='elastic-out'),
            pytaVSL.miraye_in('m_ch1-1', 1),
            mainSampler.miraye_in(), ### AUDIO
            ]
        )

    @pedalboard_button(5001, who='adrien', after='lancement_miraye_1')
    def m_ch1_2(self):
        """
        Intro
        """

        ### LIGHTS
        qlcplus.set_scene('miraye')

        switch_duration = 0.5
        ### AUDIO
        self.start_scene('sequence/m_ch1-2_audio', lambda:[
            mainSampler.send('/instrument/play', 's:m_ch1-2_organ', 100),
            mainSampler.noisy_switch(switch_duration),
        ])

        self.start_scene('sequence/m_ch1-2_klap', lambda:[
            self.wait(2.3, 's'),
            mainSampler.send('/instrument/play', 's:baffe_elan', 100), ### AUDIO
        ])

        ### VJ
        self.start_scene('sequence/m_ch1-2', lambda: [
            # pytaVSL.trijc_change_tool('compas'),
            pytaVSL.trijc_change_tool('aimant'),
            pytaVSL.m_noisy_switch_video('m_ch1-1', 'm_ch1-2', switch_duration),
            self.wait(pytaVSL.get('m_ch1-2', 'video_end') - 2.3, 's'),
            self.actes_jc(),
            ]
        )

    @pedalboard_button(100)
    def actes_jc(self):
        """
        Les actes de JC
        """

        # for index in range(1,5):
            # pytaVSL.set('tv' + str(index), 'visible', 1) # fait lagguer

        switch_duration = 5
        ### AUDIO
        self.start_scene('sequence/actes_jc_audio', lambda:[
            mainSampler.noisy_switch(switch_duration),
            # mainSampler.send('/instrument/play', 's:f_ch1_jingle_jc', 100),
        ])

        ### VJ
        self.start_scene('sequence/actes_jc', lambda:[
            pytaVSL.m_noisy_switch_video('m_ch1-2', 'm_ch1-2_waiting', switch_duration),


            pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -40, 0.5, 's', 'elastic-inout'),
            self.wait(0.5, 's'),
            pytaVSL.animate('m_iraye', 'scale', None, [0.26, 0.26], 1.5, 's', 'elastic-out'),
            pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -90, 1.5, 's', 'elastic-out'),
            mainSampler.trijc_aimant(duration=1.5), ### AUDIO
            pytaVSL.animate('m_iraye', 'position_y', None, -0.35, 1.5, 's', 'elastic-out'),
            self.wait(1.5, 's'),
            pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 0, 1, 's'),



            pytaVSL.set('tv1', 'visible', 1.0),
            pytaVSL.set('p_ch1-3', 'video_time', 0),
            pytaVSL.set('p_ch1-3', 'fish', 0.8),
            pytaVSL.flying_wings('wingsp1'),
            mainSampler.flying_wings('tv'), ### AUDIO
            pytaVSL.shaking_tvs(1, 'p_ch1-3'),
            pytaVSL.animate('tv1', 'position_x', None, 0.09, 2, 's'),
            mainSampler.send('/instrument/play', 's:m_ch1_jingle_jc', 100), ### AUDIO

            self.wait(5, 's'),
            pytaVSL.animate('tv1', 'position_x', None, -1, 2, 's'),
            pytaVSL.set('tv2', 'visible', 1.0),
            pytaVSL.set('p_ch1-4', 'video_time', 0),
            pytaVSL.set('p_ch1-4', 'fish', 0.8),
            pytaVSL.flying_wings('wingsp2'),
            pytaVSL.shaking_tvs(2, 'p_ch1-4'),
            pytaVSL.animate('tv2', 'position_x', None, 0.09, 2, 's'),
            mainSampler.send('/instrument/play', 's:m_ch1_jingle_jc', 100), ### AUDIO
            self.wait(2, 's'),
            pytaVSL.set('p_ch1-3', 'fish', 0),
            pytaVSL.set('tv1', 'visible', 0),
            pytaVSL.flying_wings('wingsp1', on=False),
            pytaVSL.stop_animate('plane_horn_1', 'position_x'),
            pytaVSL.stop_animate('plane_horn_1', 'position_y'),
            pytaVSL.stop_animate('p_ch1-3', 'position_x'),
            pytaVSL.stop_animate('p_ch1-3', 'position_y'),

            self.wait(3, 's'),
            pytaVSL.animate('tv2', 'position_x', None, -1, 2, 's'),
            pytaVSL.set('tv3', 'visible', 1.0),
            pytaVSL.set('p_ch1-5', 'video_time', 0),
            pytaVSL.set('p_ch1-5', 'fish', 0.8),
            pytaVSL.flying_wings('wingsp3'),
            pytaVSL.shaking_tvs(3, 'p_ch1-5'),
            pytaVSL.animate('tv3', 'position_x', None, 0.09, 2, 's'),
            mainSampler.send('/instrument/play', 's:m_ch1_jingle_jc', 100), ### AUDIO
            self.wait(2, 's'),
            pytaVSL.set('p_ch1-4', 'fish', 0),
            pytaVSL.set('tv2', 'visible', 0),
            pytaVSL.flying_wings('wingsp2', on=False),
            pytaVSL.stop_animate('plane_horn_2', 'position_x'),
            pytaVSL.stop_animate('plane_horn_2', 'position_y'),
            pytaVSL.stop_animate('p_ch1-4', 'position_x'),
            pytaVSL.stop_animate('p_ch1-4', 'position_y'),


            self.wait(3, 's'),
            pytaVSL.animate('tv3', 'position_x', None, -1, 2, 's'),
            pytaVSL.set('tv4', 'visible', 1.0),
            pytaVSL.set('p_ch1-6', 'video_time', 0),
            pytaVSL.set('p_ch1-6', 'fish', 0.8),
            pytaVSL.flying_wings('wingsp4'),
            pytaVSL.shaking_tvs(4, 'p_ch1-6'),
            pytaVSL.animate('tv4', 'position_x', None, 0.09, 2, 's'),
            mainSampler.send('/instrument/play', 's:m_ch1_jingle_jc', 100), ### AUDIO
            self.wait(2, 's'),
            pytaVSL.set('p_ch1-5', 'fish', 0),
            pytaVSL.set('tv3', 'visible', 0),
            pytaVSL.flying_wings('wingsp3', on=False),
            pytaVSL.stop_animate('plane_horn_3', 'position_x'),
            pytaVSL.stop_animate('plane_horn_3', 'position_y'),
            pytaVSL.stop_animate('p_ch1-5', 'position_x'),
            pytaVSL.stop_animate('p_ch1-5', 'position_y'),

            self.wait(3, 's'),
            pytaVSL.animate('tv4', 'position_x', None, -1, 2, 's'),
            self.wait(2, 's'),
            mainSampler.flying_wings('off'), ### AUDIO
            mainSampler.send('/instrument/off', 's:f_ch1_jingle_jc'),
            pytaVSL.set('tv[2-4]', 'visible', 0),
            pytaVSL.flying_wings('wingsp4', on=False),

            pytaVSL.stop_animate('plane_horn_1', 'position_x'),
            pytaVSL.stop_animate('plane_horn_1', 'position_y'),
            pytaVSL.stop_animate('plane_horn_2', 'position_x'),
            pytaVSL.stop_animate('plane_horn_2', 'position_y'),
            pytaVSL.stop_animate('plane_horn_3', 'position_x'),
            pytaVSL.stop_animate('plane_horn_3', 'position_y'),
            pytaVSL.stop_animate('plane_horn_4', 'position_x'),
            pytaVSL.stop_animate('plane_horn_4', 'position_y'),

            pytaVSL.stop_animate('p_ch1-3', 'position_x'),
            pytaVSL.stop_animate('p_ch1-3', 'position_y'),
            pytaVSL.stop_animate('p_ch1-4', 'position_x'),
            pytaVSL.stop_animate('p_ch1-4', 'position_y'),
            pytaVSL.stop_animate('p_ch1-5', 'position_x'),
            pytaVSL.stop_animate('p_ch1-5', 'position_y'),
            pytaVSL.stop_animate('p_ch1-6', 'position_x'),
            pytaVSL.stop_animate('p_ch1-6', 'position_y'),
            pytaVSL.set('p_ch1-*', 'rgbwave', 0),
            pytaVSL.set('p_ch1-*', 'noise', 0),
            pytaVSL.set('p_ch1-*', 'fish', 0),

            self.m_ch1_7()
            ])

    @pedalboard_button(101)
    def m_ch1_7(self):
        """
        Poursuite de l'intro
        """
        ### LIGHTS
        self.start_scene('sequence/lights_m_ch1-7', lambda: [
            self.wait(53, 's'),
            qlcplus.set_scene('full_stage', duration = 3)
        ])

        switch_duration = 5
        ### AUDIO
        self.start_scene('sequence/m_ch1_7_audio', lambda: [
            mainSampler.noisy_switch(switch_duration),
        ])


        ### VJ
        for index in range(1,6):
            pytaVSL.flying_wings('wingsp' + str(index), on=False)

        self.start_scene('sequence/m_ch1_7-scale', lambda:[
            pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -90, 0.5, 's'),
            self.wait(0.5, 's'),
            pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -40, 1.5, 's', 'elastic-out'),
            mainSampler.trijc_aimant(duration=1.5), ### AUDIO
            pytaVSL.animate('m_iraye', 'position_y', None, 0.016, 1.5, 's', 'elastic-out'),
            pytaVSL.animate('m_iraye', 'scale', None, [0.837, 0.837], 1.5, 's', 'elastic-out'),
            self.wait(1, 's'),
            pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 0, 0.5, 's', 'elastic-out'), # à remplacer par des mvts de ciseaux
        ])


        self.start_scene('sequence/m_ch1_7', lambda: [
            # Lancement
            pytaVSL.m_noisy_switch_video('m_ch1-2_waiting', 'm_ch1-7', switch_duration),

            # Attente de la fin
            self.wait(pytaVSL.get('m_ch1-7', 'video_end') - 2, 's'),

            # Aspiration
            pytaVSL.trijc_change_tool('aspi'),
            self.wait(0.3, 's'),
            pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, -5, 0.5, 's', ),
            self.wait(0.5, 's'),
            pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, 0, 0.2, 's', 'elastic-inout'),
            pytaVSL.aspi_slide('m_layout', [0, -0.45], [0, 0.52], 0.6),
            pytaVSL.aspi_slide('m_ch1-7', [-0.02, -0.445], [-0.02, 0.53], 0.6),
            mainSampler.aspi_slide(), ### AUDIO
            mainSampler.miraye_out(), ### AUDIO
            self.wait(1.2, 's'),
            pytaVSL.trijc_io('out', 'aspi', 0.7, 'elastic-inout'),
            mainSampler.trijc_io('out', 0.7, easing='elastic-inout'), ### AUDIO
        ])


    @pedalboard_button(5002, who='adrien', after='m_ch1_7')
    def f_ch1_9(self):
        """
        Bobine de Fin
        """

        ### LIGHTS
        qlcplus.set_scene('film')

        ### VJ
        self.start_scene('sequence/f_ch1_9', lambda: [
            pytaVSL.trijc_io('in', 'tuba', 0.6, 'elastic-inout'),
            mainSampler.trijc_io('in', 0.6, easing='elastic-inout'), ### AUDIO
            self.wait(0.7, 's'),
            pytaVSL.movie_in('f_ch1-9', 0.6),
            mainSampler.movie_in(), ### AUDIO
            self.wait(pytaVSL.get('f_ch1-9', 'video_end') - 5, 's'),
            self.m_ch1_10()
        ])

    @pedalboard_button(103)
    def m_ch1_10(self):
        """
        Intervention Miraye
        """

        ### LIGHTS
        qlcplus.set_scene('miraye')

        ### VJ
        self.start_scene('sequence/m_ch1_10', lambda: [
            pytaVSL.signs_io('in', duration=0.5, easing='random'),
            pytaVSL.animate('lights*', 'alpha', None, 1, 0.5, 's',  'random'),
            pytaVSL.trijc_io('in', 'tuba', 0.5, 'random'),
            mainSampler.trijc_io('in', 0.5, easing='elastic-inout'), ### AUDIO
            # pytaVSL.shaking_slide('f_ch1-9', 'position_x', (_rand() / 2 + 0.5) * 0.01, 10), # Moche ?
            # pytaVSL.shaking_slide('f_ch1-9', 'position_y', _rand() * 0.01, 5, easing='random'),
            self.wait(0.4, 's'),
            pytaVSL.trijc_change_tool('compas'),
            pytaVSL.miraye_in('m_ch1-10', 0.5),
            mainSampler.miraye_in(), ### AUDIO
            pytaVSL.set('f_ch1-9', 'video_speed', 0),
            pytaVSL.animate('t_trijc_compas', 'rotate_z', None, 10, 0.5, 's'),
            self.wait(0.5, 's'),
            pytaVSL.trijc_change_tool('aimant'),
            pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -45, 0.5, 's'),
            mainSampler.trijc_compas(0.5), ### AUDIO
            pytaVSL.animate('f_ilm', 'scale', None, [0.6, 0.6], 0.2, 's', 'elastic-inout'),
            self.wait(0.2, 's'),
            pytaVSL.animate('f_ilm', 'scale', None, [0.3, 0.3], 1, 's'),
            pytaVSL.animate('f_ilm', 'position_x', None, -0.35, 1, 's'),
            pytaVSL.animate('f_ilm', 'position_y', None, 0.15, 1, 's'),
            self.wait(0.5, 's'),
            pytaVSL.trijc_change_tool('compas'),
            self.wait(0.5, 's'),
            pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -40, 13, 's'),
            mainSampler.trijc_compas(13), ### AUDIO
            pytaVSL.animate('m_iraye', 'scale', None, [0.5, 0.5], 13, 's'),
            pytaVSL.animate('m_iraye', 'position_x', None, 0.1, 13, 's'),
            # self.wait(15, 's'), # TODO Séparer en deux vidéos ?
            # self.wait(10, 's'), # TODO A affiner - moment où elle commence à raconter l'histoire
            # self.f_ch1_11()
        ])

        self.start_scene('sequence/m_ch1-10_titre', lambda: [
                    # self.wait(pytaVSL.get('m_ch1-10', 'video_end') - 15, 's'), # TODO Séparer en deux vidéos ?
                    self.wait(19, 's'),
                    pytaVSL.display_title('ti_ch1-1'),
                    mainSampler.display_title(), ### AUDIO
        ])


        self.start_scene('sequence/m_ch1-10_2_f_ch1_11', lambda: [
                    self.wait(pytaVSL.get('m_ch1-10', 'video_end') - 10, 's'), # TODO A affiner - moment où elle commence à raconter l'histoire
                    self.f_ch1_11()
        ])

    @pedalboard_button(104)
    def button_f_ch1_11(self):
        self.stop_scene('sequence/*')
        pytaVSL.set('f_ilm', 'visible', 1)
        pytaVSL.set('f_ilm', 'scale', 0.95, 0.95)
        pytaVSL.set('f_ilm', 'position_x', 0)
        pytaVSL.set('f_ilm', 'position_y', 0)
        pytaVSL.set('m_ch1-10', 'visible', 1)
        pytaVSL.set('m_ch1-1', 'visible', 0)
        self.f_ch1_11()



    def f_ch1_11(self):
        """
        1ère nuit / 1ère Rencontre avec Sanchouz
        """

        ### LIGHTS
        qlcplus.set_scene('film')

        switch_duration = 1
        ### AUDIO
        self.start_scene('sequence/f_ch1_11_audio', lambda: [
            mainSampler.noisy_switch(switch_duration),
        ])

        ### VJ
        self.start_scene('sequence/f_ch1_11', lambda: [
            pytaVSL.f_noisy_switch_video('f_ch1-9', 'f_ch1-11', switch_duration, warpzoned=True),

            # pytaVSL.animate('ti_ch1-1*', 'alpha', 1, 0, 1, 's'),


            pytaVSL.animate('m_iraye', 'scale', None, [0.3, 0.3], 8, 's'),
            pytaVSL.animate('f_ilm', 'scale', None, [0.95, 0.95], 8, 's'),
            pytaVSL.animate('m_iraye', 'position', None, [0.35, 0.15, pytaVSL.get('m_iraye', 'position_z')], 8, 's'),
            pytaVSL.animate('f_ilm', 'position', None, [0, 0, pytaVSL.get('f_ilm', 'position_z')], 8, 's'),
            pytaVSL.stop_animate('f_ch1-9', 'position_x'), #, None, 0, 8, 's'),
            pytaVSL.stop_animate('f_ch1-9', 'position_y'), #, None, 0, 8, 's', 'random'),
            ## TODO Tout ce qui suit à synchroniser avec le film
            self.wait(8, 's'),

            # On libère le titre
            # pytaVSL.set('ti_ch1-1', 'visible', 0),
            # pytaVSL.set('ti_ch1-1*', 'alpha', 1),

            pytaVSL.trijc_change_tool('lustre'),
            self.wait(0.3, 's'),
            pytaVSL.trijc_turn_lights('off', 1),
            mainSampler.trijc_turn_lights(1), ### AUDIO
            mainSampler.miraye_out(), ### AUDIO
            pytaVSL.animate('m_layout', 'alpha', None, 0, 1, 's'),
            pytaVSL.animate('m_ch1-10', 'alpha', None, 0, 1, 's'),
            pytaVSL.animate('lights*', 'alpha', None, 0.3, 1, 's'),
            pytaVSL.animate('bt_main', 'brightness', None, 0.3, 1, 's'),
            pytaVSL.animate('*ep_*', 'brightness', None, 0.3, 1, 's'),
            self.wait(2, 's'),

            pytaVSL.animate('bt', 'position_x', None, 0.46, 110 - 8 - 0.3 - 2, 's'),
            pytaVSL.trijc_io('out', 'lustre', 2),
            mainSampler.trijc_io('out', 2), ### AUDIO
            self.wait(1, 's'),
            pytaVSL.signs_io('out', duration=1, easing='elastic-inout'),
            self.wait(1.1, 's'),

            # Re-init
            pytaVSL.set('sub_t_trijc_lustre_allume', 'alpha', 1),
            pytaVSL.set('m_iraye', 'visible', 0),
            pytaVSL.set('m_layout', 'alpha', 1),

        ])


        self.f_ch1_11_batohacker1()


    @pedalboard_button(1040)
    def f_ch1_11_batohacker1(self):
        """
        Batohacker - sortie des outils de direction
        """
        self.f_ch1_11_delay1 = 109
        self.start_scene('sequence/f_ch1_11_batohacker1', lambda: [
            self.wait(self.f_ch1_11_delay1, 's'),
            # Init
            pytaVSL.set('bato_h', 'position_y', 0.14),
            pytaVSL.batohacker_shaking(),

            # Arrivée
            pytaVSL.batohacker_hmove(0.47),
            mainSampler.batohacker_move(), ### AUDIO

            # Punch
            self.wait(1, 's'),
            pytaVSL.batohacker_punch('f_ch1-11'),
            mainSampler.batohacker_punch(pitch=0.9), ### AUDIO
            pytaVSL.animate('f_ch1-11*', 'brightness', None, 0.3, 0.3, 's'),

            # Positionnement fin
            self.wait(1, 's'),
            pytaVSL.batohacker_hmove(0.4, duration = 0.5),
            mainSampler.batohacker_move(duration = 0.5), ### AUDIO (???)
            pytaVSL.animate('bato_h', 'rotate_z', None, -10, 1, 's', easing='elastic-inout'),

            # Sortie Entrailles
            self.wait(0.6, 's'),
            pytaVSL.batohacker_trp_io(),
            mainSampler.batohacker_trp_io(), ### AUDIO

            # Branchement
            self.wait(1, 's'),
            pytaVSL.batohacker_plug(),
            mainSampler.batohacker_plug(), ### AUDIO

            # Allumage EP et plateau
            self.wait(1, 's'),
            qlcplus.set_strobe('bt_doah'), ### LIGHT
            qlcplus.animate('d_doah', None, 255, 2, 's'),

            pytaVSL.bascule_ep(),
            mainSampler.bascule_ep(), ### AUDIO
            self.start_scene('sequence/f_ch1-11_ep_reveil', lambda: [
                pytaVSL.face_ep('reveil'),
            ]),
            pytaVSL.allume_ep(on='courrier'),
            mainSampler.allume_ep(on='courrier'), ### AUDIO
        ])

    @pedalboard_button(5004, who='orl', after='f_ch1_11_batohacker1')
    def f_ch1_11_batohacker1_doah_acts(self):
        t_outil1_warp_in = 1 + 6 / 30
        t_outil1_swallow = 2.5 + 20 / 30

        t_outil2_warp_in = 13 + 15 / 30
        t_outil2_swallow = 15.5 + 3 / 30

        t_outil3_warp_in = 29 + 5 / 30
        t_outil3_swallow = 30.5 + 18 / 30
        self.start_scene('sequence/f_ch1_11_batohacker1_doah_acts', lambda: [
            # Relâche film
            pytaVSL.animate('f_ch1-11*', 'brightness', None, 1, 0.1, 's'),
            pytaVSL.set('f_ch1-11', 'video_speed', 1),
            pytaVSL.set('f_ch1-11_warpzoned', 'video_speed', 1),
            mainSampler.batohacker_film_stopped(stopped=True), ### AUDIO

            # Prépa mask
            pytaVSL.set('f_ch1-11', 'mask', 'f_ch1-11_mask'),

            # Outil 1
            self.wait(t_outil1_warp_in, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'rgbwave', None, 0.4, 0.3, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'fish', None, 0.4, 0.3, 's'),
            mainSampler.batohacker_warp(warp_in=True), ### AUDIO
            self.wait(t_outil1_swallow - t_outil1_warp_in, 's'),
            pytaVSL.batohacker_swallow(0.5),
            mainSampler.batohacker_swallow(0.5), ### AUDIO
            self.wait(0.5, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'rgbwave', None, 0, 0.3, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'fish', None, 0, 0.3, 's'),
            mainSampler.batohacker_warp(warp_in=False), ### AUDIO


            # Outil 2
            self.wait(t_outil2_warp_in - t_outil1_swallow - 0.5, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'rgbwave', None, 0.4, 0.3, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'fish', None, 0.4, 0.3, 's'),
            mainSampler.batohacker_warp(warp_in=True), ### AUDIO
            self.wait(t_outil2_swallow - t_outil2_warp_in, 's'),
            pytaVSL.batohacker_swallow(0.5),
            mainSampler.batohacker_swallow(0.5), ### AUDIO
            self.wait(0.5, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'rgbwave', None, 0, 0.3, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'fish', None, 0, 0.3, 's'),
            mainSampler.batohacker_warp(warp_in=False), ### AUDIO


            # Outil 3
            self.wait(t_outil3_warp_in - t_outil2_swallow - 0.5, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'rgbwave', None, 0.4, 0.3, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'fish', None, 0.4, 0.3, 's'),
            mainSampler.batohacker_warp(warp_in=True), ### AUDIO
            self.wait(t_outil3_swallow - t_outil3_warp_in, 's'),
            pytaVSL.batohacker_swallow(0.5),
            mainSampler.batohacker_swallow(0.5), ### AUDIO
            self.wait(0.5, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'rgbwave', None, 0, 0.3, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'fish', None, 0, 0.3, 's'),
            mainSampler.batohacker_warp(warp_in=False), ### AUDIO


            # Débranchage
            self.wait(0.6, 's'),
            pytaVSL.batohacker_plug(False, 0.5),
            mainSampler.batohacker_unplug(0.5), ### AUDIO


            # Extinction EP + Plateau
            self.wait(0.5, 's'),
            pytaVSL.bascule_ep(False),
            mainSampler.bascule_ep(),
            pytaVSL.allume_ep(False),


            # Rentrage des entrailles
            self.wait(1, 's'),
            pytaVSL.batohacker_trp_io('in'),
            mainSampler.batohacker_trp_io(), ### AUDIO

            # Remet le film à sa place
            self.wait(1, 's'),
            pytaVSL.batohacker_debevier(),
            mainSampler.batohacker_debevier(), ### AUDIO
            self.wait(0.4, 's'),
            pytaVSL.animate('f_ilm', 'position_x', None, 0, 1, 's'),

            # S'en va
            self.wait(1, 's'),
            pytaVSL.batohacker_hmove(1, 'cour'),
            mainSampler.batohacker_move(goto='departure'), ### AUDIO (???)


            # EP se rendort
            self.wait(1.5, 's'),
            qlcplus.set_strobe('bt_doah', on=False), ### LIGHT
            qlcplus.animate('d_doah', None, 0, 3, 's'),

            self.start_scene('sequence/f_ch1-11_ep_dodo', lambda: [
                pytaVSL.face_ep('dodo')
            ]),
            pytaVSL.animate('bt_main', 'brightness', None, 0.3, 0.2, 's'),
            pytaVSL.animate('*ep_*', 'brightness', None, 0.3, 0.2, 's'),

            # Reinit des masques
            pytaVSL.set('f_ch1-11', 'mask', ''),

        ])

        self.f_ch1_11_batohacker2()

    @pedalboard_button(1041)
    def f_ch1_11_batohacker2(self):
        """
        Batohacker retour des outils de direction + switch vers film suivant
        """
        self.f_ch1_11_delay2 = 250 + 24 / 30 # 253
        self.start_scene('sequence/f_ch1_11_batohacker2', lambda: [
            self.wait(self.f_ch1_11_delay2 - (self.f_ch1_11_delay1 + 1), 's'),
            # init
            pytaVSL.set('bato_h', 'rotate_z', 0),

            # Arrivée
            pytaVSL.batohacker_hmove(0.47),
            mainSampler.batohacker_move(), ### AUDIO

            # Punch
            self.wait(1, 's'),
            pytaVSL.batohacker_punch('f_ch1-11'),
            mainSampler.batohacker_punch(pitch=0.9), ### AUDIO (???)
            pytaVSL.animate('f_ch1-11*', 'brightness', None, 0.3, 0.3, 's'),

            # Positionnement fin
            self.wait(1, 's'),
            pytaVSL.batohacker_hmove(0.4, duration = 0.5),
            mainSampler.batohacker_move(), ### AUDIO (???)

            # Sortie Entrailles
            self.wait(0.6, 's'),
            pytaVSL.batohacker_trp_io(),
            mainSampler.batohacker_trp_io(), ### AUDIO

            # Branchement
            self.wait(1, 's'),
            mainSampler.batohacker_plug(0.8), ### AUDIO
            pytaVSL.set('o_bh_embouchure', 'position_x', 0.03), #0.026),
            pytaVSL.set('o_bh_embouchure', 'position_y', -0.325), #-0.115),
            pytaVSL.animate('o_bh_embouchure', 'scale', [0, 0], [0.1, 0.1], 0.3, 's', 'elastic-out'),
            pytaVSL.set('o_bh_embouchure', 'visible', 1),
            self.wait(0.3, 's'),
            pytaVSL.animate('bato_h', 'position_y', None, -0.185, 0.5, 's', 'elastic'),
            self.wait(0.5, 's'),
            pytaVSL.animate('bt_main', 'brightness', None, 1, 0.2, 's'),
            pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.2, 's'),

            # Allumage EP et plateau
            self.wait(0.5, 's'),

            qlcplus.set_strobe('bt_doah'), ### LIGHT
            qlcplus.animate('d_doah', None, 255, 2, 's'),

            pytaVSL.bascule_ep(),
            mainSampler.bascule_ep(), ### AUDIO
            self.start_scene('sequence/f_ch1-11_ep_reveil2', lambda: [
                pytaVSL.face_ep('reveil'),
            ]),
            pytaVSL.allume_ep(on='courrier'),
            mainSampler.allume_ep(on='courrier') ### AUDIO
        ])

    @pedalboard_button(5005, who='orl', after='f_ch1_11_batohacker2')
    def f_ch1_11_batohacker2_doah_acts(self):
        t_outil1_warp_in = 20 / 30
        t_outil1_swallow = 1 + 8 / 30

        t_outil2_warp_in = 3 + 13 / 30
        t_outil2_swallow = 4 + 7 / 30

        t_outil3_warp_in = 5 + 22 / 30
        t_outil3_swallow = 6 + 17 / 30
        self.start_scene('sequence/f_ch1_11_batohacker2_doah_acts', lambda: [

            # Init
            pytaVSL.set('o_bh_adam', 'position_x', 0.045),
            pytaVSL.set('o_bh_adam', 'position_y', -0.32),
            pytaVSL.set('f_ch1-11', 'mask', 'f_ch1-11_mask2'),

            # Relâche film
            pytaVSL.animate('f_ch1-11*', 'brightness', None, 1, 0.1, 's'),
            pytaVSL.set('f_ch1-11', 'video_speed', 1),
            pytaVSL.set('f_ch1-11_warpzoned', 'video_speed', 1),
            mainSampler.batohacker_film_stopped(stopped=True), ### AUDIO


            # Objet 1
            self.wait(t_outil1_warp_in, 's'),
            mainSampler.batohacker_warp(warp_in=True), ### AUDIO
            pytaVSL.animate('f_ch1-11_warpzoned', 'rgbwave', None, 0.4, 0.3, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'fish', None, 0.4, 0.3, 's'),
            self.wait(t_outil1_swallow - t_outil1_warp_in, 's'),
            pytaVSL.animate('i_bh_entrailles', 'offset_y', 0, 0.01, 0.2, 's', 'random-mirror'),
            pytaVSL.animate('o_bh_adam', 'scale', [1, 0], [1, 1], 0.2, 's'),
            pytaVSL.animate('o_bh_adam', 'alpha', 0, 1, 0.2, 's'),
            pytaVSL.animate('o_bh_adam', 'position_y', -0.32, -0.25, 0.5, 's'),# , 'elastic-out'),
            pytaVSL.animate('o_bh_adam', 'position_x', 0.045, 0.04, 0.5, 's'), #, 'elastic-out'),
            pytaVSL.set('o_bh_adam', 'visible', 1),
            mainSampler.batohacker_swallow(0.4, avale=False), ### AUDIO
            self.wait(0.3, 's'),
            pytaVSL.animate('o_bh_adam', 'alpha', None, 0, 0.2, 's'),
            self.wait(0.1, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'rgbwave', None, 0, 0.3, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'fish', None, 0, 0.3, 's'),
            mainSampler.batohacker_warp(warp_in=False), ### AUDIO

            # Objet 2
            self.wait(t_outil2_warp_in - t_outil1_swallow - 0.4, 's'),
            mainSampler.batohacker_warp(warp_in=True), ### AUDIO
            pytaVSL.animate('f_ch1-11_warpzoned', 'rgbwave', None, 0.4, 0.3, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'fish', None, 0.4, 0.3, 's'),
            self.wait(t_outil2_swallow - t_outil2_warp_in, 's'),
            pytaVSL.animate('i_bh_entrailles', 'offset_y', 0, 0.01, 0.2, 's', 'random-mirror'),
            pytaVSL.animate('o_bh_adam', 'scale', [1, 0], [1, 1], 0.2, 's'),
            pytaVSL.animate('o_bh_adam', 'alpha', 0, 1, 0.2, 's'),
            pytaVSL.animate('o_bh_adam', 'position_y', -0.32, -0.25, 0.5, 's'),# , 'elastic-out'),
            pytaVSL.animate('o_bh_adam', 'position_x', 0.045, 0.04, 0.5, 's'), #, 'elastic-out'),
            pytaVSL.set('o_bh_adam', 'visible', 1),
            mainSampler.batohacker_swallow(0.4, avale=False), ### AUDIO
            self.wait(0.3, 's'),
            pytaVSL.animate('o_bh_adam', 'alpha', None, 0, 0.2, 's'),
            self.wait(0.1, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'rgbwave', None, 0, 0.3, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'fish', None, 0, 0.3, 's'),
            mainSampler.batohacker_warp(warp_in=False), ### AUDIO


            # Objet 3
            self.wait(t_outil3_warp_in - t_outil2_swallow - 0.4, 's'),
            mainSampler.batohacker_warp(warp_in=True), ### AUDIO
            pytaVSL.animate('f_ch1-11_warpzoned', 'rgbwave', None, 0.4, 0.3, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'fish', None, 0.4, 0.3, 's'),
            self.wait(t_outil3_swallow - t_outil3_warp_in, 's'),
            pytaVSL.animate('i_bh_entrailles', 'offset_y', 0, 0.01, 0.2, 's', 'random-mirror'),
            pytaVSL.animate('o_bh_adam', 'scale', [1, 0], [1, 1], 0.2, 's'),
            pytaVSL.animate('o_bh_adam', 'alpha', 0, 1, 0.2, 's'),
            pytaVSL.animate('o_bh_adam', 'position_y', -0.32, -0.25, 0.5, 's'),# , 'elastic-out'),
            pytaVSL.animate('o_bh_adam', 'position_x', 0.045, 0.04, 0.5, 's'), #, 'elastic-out'),
            pytaVSL.set('o_bh_adam', 'visible', 1),
            mainSampler.batohacker_swallow(0.4, avale=False), ### AUDIO
            self.wait(0.3, 's'),
            pytaVSL.animate('o_bh_adam', 'alpha', None, 0, 0.2, 's'),
            self.wait(0.1, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'rgbwave', None, 0, 0.3, 's'),
            pytaVSL.animate('f_ch1-11_warpzoned', 'fish', None, 0, 0.3, 's'),
            mainSampler.batohacker_warp(warp_in=False), ### AUDIO

            # Débranchage
            pytaVSL.animate('bato_h', 'position_y', None, -0.15, 0.3, 's', 'elastic-out'),
            self.wait(0.3, 's'),
            pytaVSL.animate('o_bh_embouchure', 'scale', None, [0, 0], 0.5, 's', 'elastic-inout'),
            mainSampler.batohacker_unplug(0.8), ### AUDIO

            # Extinction EP + Plateau
            self.wait(0.5, 's'),

            qlcplus.set_strobe('bt_doah', on=False), ### LIGHT
            qlcplus.animate('d_doah', None, 0, 2, 's'),

            pytaVSL.bascule_ep(False),
            mainSampler.bascule_ep(), ### AUDIO
            pytaVSL.allume_ep(False),


            # Rentrage des entrailles
            self.wait(1, 's'),
            pytaVSL.batohacker_trp_io('in'),
            mainSampler.batohacker_trp_io(), ### AUDIO

            # Remet le film à sa place
            self.wait(1, 's'),
            pytaVSL.batohacker_debevier(),
            mainSampler.batohacker_debevier(), ### AUDIO
            self.wait(0.4, 's'),
            pytaVSL.animate('f_ilm', 'position_x', None, 0, 1, 's'),

            # S'en va
            self.wait(1, 's'),
            pytaVSL.batohacker_hmove(1, 'cour'),
            mainSampler.batohacker_move(goto='departure'), ### AUDIO


            # EP se rendort et se repositionne au bon endroit
            self.wait(1.5, 's'),
            self.start_scene('sequence/f_ch1-11_ep_dodo', lambda: [
                pytaVSL.face_ep('dodo')
            ]),
            pytaVSL.animate('bt', 'position_x', None, 0.525, 1, 's'),
            pytaVSL.animate('bt_main', 'brightness', None, 0.3, 0.2, 's'),
            pytaVSL.animate('*ep_*', 'brightness', None, 0.3, 0.2, 's'),


            # Enlève les masques, etc.
            pytaVSL.set('f_ch1-11', 'mask', ''),
            pytaVSL.set('f_ch1-11_warpzoned', 'visible', 0),
            pytaVSL.set('o_bh_embouchure', 'scale', 0.1, 0.1),

        ])



        self.start_scene('sequence/f_ch1-11_2_f_ch1-12', lambda: [
            self.wait(pytaVSL.get('f_ch1-11', 'video_end') - self.f_ch1_11_delay2, 's'),
            self.f_ch1_12()
        ]),


    @pedalboard_button(105)
    def f_ch1_12(self):
        """
        Les BF se promènent
        """
        self.start_scene('sequence/f_ch1_12', lambda: [
            pytaVSL.f_switch_video('f_ch1-11', 'f_ch1-12'),
            self.wait(pytaVSL.get('f_ch1-12', 'video_end'), 's'),
            pytaVSL.f_switch_video('f_ch1-12', 'f_ch1-12_waiting'),

            ### LIGHT
            qlcplus.animate('d_je', None, 255, 3, 's'),
        ])


######### DEPRECATED
    # @pedalboard_button(6)
    # def f_ch1_13(self):
    #     """
    #     Saladin approche / bruit au plateau
    #     """
    #     self.start_scene('f_ch1_13', lambda: [
    #         pytaVSL.f_noisy_switch_video('f_ch1-12_waiting', 'f_ch1-13', 0.2),
    #         self.wait(pytaVSL.get('f_ch1-13', 'video_end'), 's'),
    #         pytaVSL.f_switch_video('f_ch1-13', 'f_ch1-13_waiting'),
    #
    #     ])

    @pedalboard_button(5006, who='orl', after='f_ch1_12')
    def f_ch1_14(self):
        """
        Allumage lumières grâce à la théière
        """
        self.start_scene('sequence/f_ch1_14', lambda: [
            pytaVSL.f_switch_video('f_ch1-12_waiting', 'f_ch1-14', f_secu='f_ch1-12'),
            self.wait(pytaVSL.get('f_ch1-14', 'video_end') - 0.1, 's'),
            self.f_ch1_15()
        ])


        self.start_scene('sequence/f_ch1_14_theiere', lambda: [
            #### L'arabesque du haut oscille et laisse tomber la théière

            ### Light
            qlcplus.set_strobe('bt_je'), ### LIGHT

            #### Clignotage Arabesque
            pytaVSL.set('f_ara*e_1', 'color_strobe', 1),

            self.wait(0.5, 's'),

            # Init
            pytaVSL.set('f_arandgrame_1', 'scale', -3, 3),

            # Ouverture arabesque
            pytaVSL.animate('f_ara*e_1', 'rotate_z', None, -110, 1, 's', 'elastic-out'),
            mainSampler.send('/instrument/play', 's:arabesque', 100), ### AUDIO

            # Trajet Théière
            self.wait(1, 's'),

            ### Light
            qlcplus.animate('d_je', None, 0, 2, 's'),

            ### AUDIO
            mainSampler.send('/instrument/play', 's:gros_interrupteur', 100),

            pytaVSL.animate('f_ch1-bs_theiere', 'scale', None, [0.3, 0.3], 0.15, 's'),
            pytaVSL.animate('f_ch1-bs_theiere', 'position_x', -0.1, -0.12, 0.3, 's'),
            pytaVSL.animate('f_ch1-bs_theiere', 'position_y', 0.415, -0.65, 1, 's'),
            pytaVSL.animate('f_ch1-bs_theiere', 'rotate_z', 50, 770, 1, 's'),
            pytaVSL.set("f_ch1-bs_theiere", "visible", 1),

            # Fin clignotage Arabesque
            pytaVSL.animate('f_ara*e_1', 'offset_x', None, 0.2, 0.15, 's', 'random-mirror'),
            pytaVSL.animate('f_ara*e_1', 'offset_y', None, 0.1, 0.15, 's', 'random-mirror'),
            self.wait(0.3, 's'),
            pytaVSL.set('f_ara*e_1', 'color_strobe', 0),

            # Remontée arabesque
            self.wait(0.3, 's'),
            pytaVSL.animate('f_ara*e_1', 'rotate_z', None, -180, 1, 's', 'elastic-out'),

            # Reinit
            self.wait(1, 's'),
            qlcplus.set_strobe('bt_je', on=False), ### LIGHT

            pytaVSL.set('f_aran*e_1', 'scale', 3, 3)



        ])

    # @pedalboard_button(106)
    # def fake_ch1_15(self):
    #     """
    #     elyspe
    #     """
    #     self.start_scene('sequence/f_ch1_19', lambda: [
    #         pytaVSL.f_switch_video('f_ch1-14', 'f_ch1-19'),
    #         self.wait(pytaVSL.get('f_ch1-19', 'video_end')),
    #     ])


    @pedalboard_button(106)
    def f_ch1_15(self):
        """
        Doah chez la voyante
        """
        ### AUDIO
        t_boite_a_meuh_init = 81 + 7 / 30
        self.start_scene('sequence/f_ch1-15_audio', lambda: [
            self.wait(t_boite_a_meuh_init, 's'),
            mainSampler.send('/instrument/play', 's:f_ch1-15_boite_a_meuh', 100)
        ])


        ### VJ
        self.start_scene('sequence/f_ch1_15', lambda: [
            pytaVSL.f_switch_video('f_ch1-14', 'f_ch1-15'),
            self.wait(pytaVSL.get('f_ch1-15', 'video_end')),
            self.f_ch1_16()
        ])

    @pedalboard_button(107)
    def f_ch1_16(self):
        """
        Musée zarbi de l'étrange
        """
        self.start_scene('sequence/f_ch1_16', lambda: [
            pytaVSL.f_switch_video('f_ch1-15', 'f_ch1-16'),
            self.wait(pytaVSL.get('f_ch1-16', 'video_end')), # TODO à affiner en fonction de la fin du film
            self.f_ch1_19()
        ])

    @pedalboard_button(108)
    def f_ch1_19(self):
        """
        Chez Ravi
        """
        def finito():
            self.fin = _time()
            self.logger.info('Fin @ ' + str(self.fin) + ' secondes')
            duree = self.fin - self.debut
            self.logger.info('Durée : ' + str(round(duree/60)) + ' minutes')


        self.start_scene('sequence/f_ch1_19', lambda: [
            pytaVSL.f_switch_video('f_ch1-16', 'f_ch1-19'),
            self.wait(pytaVSL.get('f_ch1-19', 'video_end') - 5), # TODO à affiner en fonction de la fin du film
            pytaVSL.trijc_io('in', 'lustre', 7),
            mainSampler.trijc_io('in', 7), ### AUDIO
            self.wait(2, 's'),
            pytaVSL.trijc_turn_lights('off', 3),
            mainSampler.trijc_turn_lights(3), ### AUDIo
            pytaVSL.animate('f_*', 'alpha', None, 0, 3),
            self.wait(3.2, 's'),
            pytaVSL.set('f_ilm', 'visible', 0),
            pytaVSL.set('f_*', 'alpha', 1),
            engine.set_route('Chapitre 2'),
            finito()
        ])
