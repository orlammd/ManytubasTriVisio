from ..base import *
from .video import Video
from .light import Light
from random import random as _rand
import math
from time import time as _time

from modules import *

class Chapitre5(Video, Light, RouteBase):

    def activate(self):
        """
        Called when the engine switches to this route.
        """

        super().activate()

        transport.set_tempo(60)
        transport.set_cycle('4/4', pattern="Xxxx")

        # Setups, banks...
        # prodSampler.set_kit(self.name)




        self.start_scene('init_chapitre5', lambda: [
            # Overlay
            self.init_chapitre5(),

            # Chapitre 5
            self.m_ch5_1()
        ])

    def init_chapitre5(self):
        self.debut = _time()
        self.logger.info('Début @ ' + str(self.debut) + ' secondes')


        pytaVSL.send('/pyta/slide/*ch4*/remove')
        pytaVSL.send('/pyta/slide/*ch4*/unload')

        chapter = 'ch5'

        ### Création des groupes du chapitre
        # pytaVSL.create_group('tv1', ['plane_horn_1','p_' + chapter + '-3']) # Soft & Rains dans le journal

        pytaVSL.create_group('m_iraye', ['m_layout', 'm_' + chapter + '*'])
        pytaVSL.create_group('f_arabesques', ['f_ara*e_1', 'f_ara*e_2'])
        pytaVSL.create_group('f_arabesques_2', ['f_ara*e_3', 'f_ara*e_4'])
        pytaVSL.create_group('f_arabesques_3', ['f_ara*e_5', 'f_ara*e_6'])
        pytaVSL.create_group('f_ilm', ['f_arabesques', 'f_' + chapter + '*'])
        pytaVSL.create_group('f_ilm_2', ['f_arabesques_2', 'f2_' + chapter + '*'])
        pytaVSL.create_group('f_ilm_3', ['f_arabesques_3', 'f3_'  + chapter + '*'])

        pytaVSL.create_group('tv1', ['plane_horn_1', 'p_voyante*'])
        pytaVSL.create_group('tv2', ['plane_horn_2', 'p_fakir'])
        pytaVSL.create_group('tv3', ['plane_horn_3', 'p_cockpit*'])

        pytaVSL.sync()

        pytaVSL.position_overlay('Chapitre5')
        pytaVSL.face_ep('reveil')

    def enedys_oscille(self):
        def rot(angle):
            duration = _rand()*2
            if duration > 0.99:
                easing = 'elastic-inout'
            elif duration < 0.2:
                easing = 'random'
            else:
                easing = 'linear'

            pytaVSL.animate('bt_ep', 'rotate_z', None, -angle, duration, 's', easing)
            pytaVSL.animate('enedys_p', 'rotate_z', None, angle, duration, 's', easing)
            self.wait(duration, 's')

        def oscill_alea():
            while True:
                self.wait(_rand()*5, 's')
                rot(60)
                rot(90)

        self.start_scene('sequence/enedys_oscille', oscill_alea)

    def all_vibrating(self):
        for slide in [
            'w_signs_jack',
            'w_signs_caesar',
            'w_signs_manytubas',
            'w_signs_tri',
            'w_signs_visio',
            'back',
            'lights_stageleft',
            'lights_stageright',
            'trijc',
            'bt',
            'f_ilm*',
            'm_ch5-2',
            't_nobh_entovillon'
            ]:
            intensity = _rand() * 0.005
            duration = _rand() + 2
            pytaVSL.animate(slide, 'offset_y', 0, intensity, duration, 's', 'random-mirror', loop=True)



    @pedalboard_button(100)
    def m_ch5_1(self):
        """
        Intro Miraye
        """
        ### LIGHT
        qlcplus.set_scene('miraye')

        ### VJ
        # if pytaVSL.get('trijc', 'position_x') == 0:
        self.start_scene('sequence/m_ch5_1', lambda: [
            pytaVSL.trijc_io('in', 'tuba', 0.5, 'elastic-inout'),
            mainSampler.trijc_io('in', 0.5, 'elastic-inout'), ### AUDIO
            self.wait(0.5, 's'),
            self.enedys_oscille(),
            self.wait(0.1, 's'),
            pytaVSL.trijc_change_tool('tuba'),
            self.wait(0.3, 's'),
            pytaVSL.set('sub_t_trijc_lustre_allume', 'alpha', 1),
            pytaVSL.animate('lights*', 'alpha', None, 1, 0.6),
            pytaVSL.animate('bt_main', 'brightness', None, 1, 0.6),
            pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.6),
            pytaVSL.signs_io('in', duration=0.5, easing='elastic-out'),
            pytaVSL.miraye_in('m_ch5-1', 0.7),
            mainSampler.miraye_in(), ### AUDIO
            pytaVSL.animate('explos_smoke1', 'alpha', None, 1, 26, 's'),

            # Courrier arrive
            self.wait(26, 's'),
            qlcplus.set_strobe('bt_doah'), ### LIGHT
            qlcplus.set_strobe('bt_je'), ### LIGHT
            qlcplus.set_strobe('bt_sal'), ### LIGHT

            self.stop_scene('sequence/enedys_oscille'),
            pytaVSL.bascule_ep(courrier=True),
            mainSampler.bascule_ep(), ### AUDIO
            pytaVSL.allume_ep(on='courrier'),
            mainSampler.allume_ep(on='courrier'), ### AUDIO
            pytaVSL.set('t_bh_megavillon*', 'visible', 0),
            pytaVSL.animate('bt', 'position_x', None, 0.46, 1, 's', 'elastic-inout'),
            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, 0.41, 1, 's', 'elastic-inout'),
            pytaVSL.animate('explos', 'position_x', None, 0.412, 1, 's', 'elastic-inout'),
            mainSampler.send('/instrument/play', 's:f_ch5_bouilloire_stop', 100), ### AUDIO
            mainSampler.send('/instrument/stop', 's:f_ch4_bouilloire_middle'), ### AUDIO

            self.wait(5, 's'),
            pytaVSL.face_ep('non'),
            # self.wait(3, 's'),

            # self.wait(3.5, 's'),  # alignement sur "aveugl'"
            self.wait(3, 's'),
            self.explosion()
        ])

    @pedalboard_button(101)
    def explosion(self):
        """
        Explosion
        """



        ### VJ & Lights
        def explode():
            nb_frames = [47, 18, 8]
            frame_d = [0.070, 0.040, 0.120]
            delay = [0.3, 2, 0.01]
            gif_speed = [1.5, 1, 1]

            # for n in range(0,3):
                # pytaVSL.set('explosion' + str(n+1), 'gif_speed', gif_speed[n])
                # self.start_scene('explosion' + str(n+1), lambda: [
                #     self.logger.info('explosion' + str(n+1)),
                #     self.wait(delay[n], 's'),
                #     self.logger.info(delay[n]),
                #     pytaVSL.set('explosion' + str(n+1), 'visible', 1),
                #     self.wait(frame_d[n] * nb_frames[n] / gif_speed[n] - 0.1, 's'),
                #     pytaVSL.animate('explosion' + str(n+1), 'alpha', None, 0, 0.1, 's'),
                #     pytaVSL.set('explosion' + str(n+1), 'visible', 0),
                # ])

            def allblack(out=True):
                # for slide in ['back', 'm_layout', 'm_ch5-*', '*ep_*', 'bt_main', 'lights*', 'signs_*']:
                for slide in ['back', 'lights*', 'signs_*']:
                    if out:
                        pytaVSL.set(slide, 'brightness', 0.2),
                    else:
                        pytaVSL.animate(slide, 'brightness', None, 1, 0.5, 's'),
                if out:
                    qlcplus.set_scene('blackout', duration=0.5), ### LIGHT

                    pytaVSL.set('m_ch5-1', 'visible', 0)
                else:
                    qlcplus.set_scene('botcave', duration=0.5), ### LIGHT
                    qlcplus.set_strobe('bt_doah'), ### LIGHT
                    qlcplus.set_strobe('bt_sal'), ### LIGHT
                    qlcplus.set_strobe('bt_je'), ### LIGHT
                    qlcplus.set_strobe('bt_centrale'), ### LIGHT
                    #qlcplus.animate('d_je', None, 0.8 * 255, 0.5, 's'), ### LIGHT
                    qlcplus.set_scene('miraye')


                    self.all_vibrating()

            def enfumage():
                for index in range(1,5):
                    pytaVSL.animate('smoke1-' + str(index), 'alpha', None, 1, 0.5, 's')
                    if index < 3:
                        pytaVSL.animate('smoke2-' + str(index), 'alpha', None, 1, 0.5, 's')

            pytaVSL.set('explosion1', 'gif_speed', gif_speed[0])
            self.start_scene('explosion', lambda: [
                # self.logger.info('explosion3'),
                allblack(),
                pytaVSL.animate('m_iraye', 'position_y', None, -0.929, 0.5, 's'),
                pytaVSL.animate('bt', 'position_x', None, 0.7, 0.5, 's'),
                pytaVSL.animate('t_nobh_entovillon', 'position_x', None, 0.65, 0.5, 's'),
                pytaVSL.trijc_io('out', 'tuba'),
                mainSampler.trijc_io('out'), ### AUDIO
                pytaVSL.set('explosion3', 'visible', 1),
                self.wait(8 / 25 - 0.1, 's'),
                # pytaVSL.animate('explosion3', 'alpha', None, 0, 0.1, 's'),
                pytaVSL.set('explosion1', 'visible', 1),
                pytaVSL.set('explosion3', 'visible', 0),
                # self.logger.info('explosion1'),

                self.wait(47 / 25 - 0.1, 's'),
                # pytaVSL.animate('explosion1', 'alpha', None, 0, 0.1, 's'),
                # self.logger.info('explosion2'),
                # pytaVSL.set('explosion2', 'visible', 1),
                allblack(out=False),
                enfumage(),
                pytaVSL.trijc_io('in', 'tuba'),
                mainSampler.trijc_io('in'), ### AUDIO
                pytaVSL.animate('bt', 'position_x', None, 0.46, 1, 's'),
                pytaVSL.animate('t_nobh_entovillon', 'position_x', None, 0.41, 1, 's'),

                pytaVSL.set('explosion1', 'visible', 0),
                # self.wait(18 / 25 - 0.1, 's'),
                # pytaVSL.animate('explosion2', 'alpha', None, 0, 0.1, 's'),
                # pytaVSL.set('explosion2', 'visible', 0),
            ])


        def fracas_leger():
            def sursaut_signs():
                for sign in ['jack', 'caesar', 'manytubas', 'tri', 'visio']:
                    o_set = _rand()*0.1
                    pytaVSL.animate('w_signs*', 'fish', 0, 0.4 + o_set, 0.2, 's', 'linear-mirror'),
                    # pytaVSL.animate('w_signs_' + sign, 'offset_y', 0, o_set, 0.2, 's', 'random-mirror')

            self.start_scene('sequence/sursaut_general', lambda: [
                # SIGNS
                sursaut_signs(),
                pytaVSL.animate('back', 'mask_threshold', 0, 1, 0.2, 's'),
                pytaVSL.animate('ep*', 'mask_threshold', 0, 1, 0.2, 's'),
                pytaVSL.animate('ep_casque', 'mask_threshold', 0, 1, 0.2, 's'),
                pytaVSL.animate('m_layout', 'mask_threshold', 0, 1, 0.2, 's'),
                pytaVSL.animate('t_trijc_*', 'mask_threshold', 0, 1, 0.2, 's'),

                pytaVSL.animate('signs_jack', 'mask_threshold', 0, 1, 0.2, 's'),
                pytaVSL.animate('signs_caesar', 'mask_threshold', 0, 1, 0.2, 's'),
                pytaVSL.animate('signs_manytubas', 'mask_threshold', 0, 1, 0.2, 's'),
                pytaVSL.animate('signs_tri', 'mask_threshold', 0, 1, 0.2, 's'),
                pytaVSL.animate('signs_visio', 'mask_threshold', 0, 1, 0.2, 's'),


                pytaVSL.set('back', 'mask', 'brule_back'),
                pytaVSL.set('ep*', 'mask', 'brule_ep'),
                pytaVSL.set('ep_casque', 'mask', ''),
                pytaVSL.set('m_layout', 'mask', 'brule_layout'),
                pytaVSL.set('t_trijc_*', 'mask', 'brule_t_trijc'),

                pytaVSL.set('signs_jack', 'mask', 'brule_signs_jack'),
                pytaVSL.set('signs_caesar', 'mask', 'brule_signs_caesar'),
                pytaVSL.set('signs_manytubas', 'mask', 'brule_signs_manytubas'),
                pytaVSL.set('signs_tri', 'mask', 'brule_signs_tri'),
                pytaVSL.set('signs_visio', 'mask', 'brule_signs_visio'),

                self.wait(0.6, 's'),
                pytaVSL.animate('signs_jack', 'rotate_z', None, -30, 0.2, 's'),
                pytaVSL.animate('signs_jack', 'position_x', None, -0.18, 0.2, 's'),
                pytaVSL.animate('signs_jack', 'position_y', None, -0.25, 0.2, 's'),

                # MIRAYE
                pytaVSL.animate('m_iraye', 'rotate_z', 0, 5, 0.1, 's'),
                pytaVSL.animate('m_iraye', 'scale', None, [0.7, 0.7], 0.1, 's'),

                pytaVSL.animate('m_ch5-2', 'fish', 0, -0.2, 20, 's', 'random-mirror', loop=True),
                pytaVSL.animate('m_ch5-2', 'rgbwave', 0, 0.6, 20, 's', 'random-mirror', loop=True),
                pytaVSL.animate('m_ch5-2', 'noise', 0, 0.5, 20, 's', 'random-mirror', loop=True),
                pytaVSL.animate('m_ch5-2', 'rotate_z', 0, -3, 0.1, 's'),

            ])


        self.start_scene('sequence/xxxplozzz', lambda: [
            self.wait(0.15, 's'),
            pytaVSL.send('/pyta/slide/{bt,t_nobh_entovillon}/animate', 'offset_y', 0.01, 0, 0.4, 'random'),
            self.wait(0.15, 's'),
            pytaVSL.animate('explos', 'position_y', -0.3, 0.1, 1, 's', 'elastic-inout'),
            self.wait(1, 's'),
            pytaVSL.batohacker_hmove(0.55, 'jardin', 0.5),
            mainSampler.batohacker_move(0.5), ### AUDIO
            pytaVSL.getwhistle_ep(),
            pytaVSL.face_ep('sifflet'),
            pytaVSL.allume_ep(on='alarm'),
            mainSampler.allume_ep(on='alarm'), ### AUDIO

            qlcplus.set_strobe('bt_centrale'), ### LIGHT

            # self.wait(0.5, 's'), # remplacé par getwhistle
            pytaVSL.batohacker_punch(x_dest=0),
            mainSampler.send('/instrument/play', 's:batohacker_punch', 100), ### AUDIO

            pytaVSL.set('explos_smoke2', 'visible', 1),
            pytaVSL.set('explos_smoke1', 'visible', 0),
            pytaVSL.animate('explos', 'position_x', None, 0, 1, 's', 'elastic-out'),
            self.wait(0.7, 's'),
            pytaVSL.batohacker_hmove(1),
            mainSampler.batohacker_move(goto='departure'), ### AUDIO
            self.wait(0.5, 's'),
            mainSampler.send('/instrument/play', 's:f_ch5_explosion_grenade', 100), ### AUDIO
            explode(),
            fracas_leger(),
            pytaVSL.set('explos_grenade', 'visible', 0),
            pytaVSL.set('explos_smoke2', 'visible', 0),
            self.m_ch5_2()
        ])

    @pedalboard_button(102)
    def button_m_ch5_2(self):
        self.stop_scene('sequence/*')
        self.m_ch5_2()

    def m_ch5_2(self):
        """
        Miraye reprend la main
        """

        ### AUDIO
        t_offset = 0.5 + 2.5 + 1.4 + 0.8 + 0.4
        t_symphonie = 39 + t_offset
        transport.set_tempo(105)
        self.start_scene('sequence/m_ch5-2_audio', lambda: [
            self.wait(t_symphonie, 's'),
            mainSampler.send('/instrument/stop', 's:alarm_ep'),
            mainSampler.send('/instrument/play', 's:symphonie_x', 127),
            self.wait(5, 'beat'),
            mainSampler.send('/instrument/play', 's:symphonie_a', 127),
            self.wait(11, 'beat'),
            # mainSampler.send('/instrument/play', 's:symphonie_x', 127),
            self.wait(5, 'beat'),
            mainSampler.send('/instrument/play', 's:symphonie_a', 127),
            self.wait(11, 'beat'),
            mainSampler.send('/instrument/play', 's:symphonie_x', 127),
        ])

        ### VJ
        pytaVSL.flying_wings('wings1', True)
        pytaVSL.flying_wings('wings2', True)
        self.start_scene('sequence/m_ch5_2', lambda: [
            self.wait(0.5, 's'), # dû à alignement son pour "aveugl'"
            self.wait(2.5, 's'),
            mainSampler.flying_wings('notv'), ### AUDIO
            pytaVSL.animate('wings1', 'position_x', None, -0.22, 1, 's'), # -0.23
            pytaVSL.animate('wings2', 'position_x', None, 0.3, 0.8, 's'),
            self.wait(1.4, 's'),
            pytaVSL.animate('wings1', 'position_y', None, -0.613, 0.6, 's', 'elastic-inout'),
            pytaVSL.animate('wings2', 'position_y', None, -0.71, 0.8, 's', 'elastic-inout'),
            self.wait(0.8, 's'),
            pytaVSL.f_noisy_switch_video('m_ch5-1', 'm_ch5-2', 0.8),
            mainSampler.noisy_switch(0.8), ### AUDIO
            self.wait(0.4, 's'),
            pytaVSL.set('wings1', 'rotate_z', 30),
            pytaVSL.animate('m_iraye', 'position_y', None, 0.016, 1, 's'),
            pytaVSL.animate('smoke1-2', 'position_y', None, 0.46, 1, 's'),
            pytaVSL.animate('wings1', 'position_y', None, 0.358, 1, 's'), # 0.345
            pytaVSL.animate('wings2', 'position_y', None, 0.455, 1, 's'),
            pytaVSL.animate('wings1', 'offset_y', 0, 0.01, 2, 's', 'random-mirror', loop=True),
            pytaVSL.animate('wings2', 'offset_y', 0, 0.01, 2, 's', 'random-mirror', loop=True),
            pytaVSL.animate('m_iraye', 'offset_y', 0, 0.01, 2, 's', 'random-mirror', loop=True),

            pytaVSL.animate('wings2_main', 'color', None, [0.7, 0.5, 0.5], 55, 's'),
            pytaVSL.animate('bras_wings2_droit', 'color', None, [1, 0.5, 0.5], 55, 's'),
            pytaVSL.animate('bras_wings2_gauche', 'color', None, [0.8, 0.5, 0.5], 55, 's'),
            pytaVSL.animate('wings2_main', 'rgbwave', None, 0.1, 55, 's'),
            pytaVSL.animate('bras_wings2_droit', 'rgbwave', None, 0.1, 55, 's'),
            pytaVSL.animate('bras_wings2_gauche', 'rgbwave', None, 0.1, 55, 's'),

            self.wait(2, 's'),
            pytaVSL.animate('m_ch5-2', 'rgbwave', 0, 0.4, 20, 's', 'random-mirror', loop=True),
            pytaVSL.animate('m_ch5-2', 'noise', 0, 0.5, 20, 's', 'random-mirror', loop=True),
            pytaVSL.set('explosion2', 'scale', 0.1, 0.1),
            pytaVSL.set('explosion2', 'position_x', 0.495),
            pytaVSL.set('explosion2', 'position_y', 0.47),
            pytaVSL.animate('explosion2', 'alpha', 0, 0.3, 2, 's', 'linear-mirror', loop=True),
            pytaVSL.set('explosion2', 'visible', 1),

            self.wait(56, 's'), # (appel Jack Caesar)
            pytaVSL.animate('wings2', 'offset_y', 0, 0.03, 4, 's', 'random'),
            self.wait(2.3, 's'),
            pytaVSL.animate('bras_wings2_droit', 'rotate_z', None, -10, 0.2, 's'), # Le support se casse
            pytaVSL.animate('bras_wings2_gauche', 'rotate_z', None, 10, 0.2, 's'), # Le support se casse
            self.wait(0.2, 's'),

            pytaVSL.animate('wings2', 'position_y', None, 0.7, 1, 's', 'elastic-inout'), # Le support se casse
            pytaVSL.animate('smoke1-2', 'alpha', None, 0, 0.2, 's'),
            pytaVSL.animate('explosion2', 'alpha', None, 0, 0.2, 's'),

            self.f_ch5_3()

        ])

    @pedalboard_button(103)
    def button_f_ch5_3(self):
        self.stop_scene('sequence/*')
        self.f_ch5_3()

    @pedalboard_button()
    def f_ch5_3(self):
        """
        Arrivée Jack Caesar
        """
        self.start_scene('sequence/f_ch5_3', lambda: [
            self.wait(0.4, 's'),
            pytaVSL.set('m_ch5-2', 'video_speed', 0),

            pytaVSL.animate('m_iraye', 'rotate_z', None, -80, 0.8, 's', 'elastic-out'),
            pytaVSL.animate('m_iraye', 'position_x', None, -0.3, 0.8, 's', 'elastic-out'),
            pytaVSL.animate('m_iraye', 'position_y', None, -0.26, 0.8, 's', 'elastic-out'),
            pytaVSL.animate('wings1', 'rotate_z', None, -55, 0.8, 's', 'elastic-out'),
            pytaVSL.animate('wings1', 'position_x', None, -0.14, 0.8, 's', 'elastic-out'),
            pytaVSL.animate('wings1', 'position_y', None, 0.34, 0.8, 's', 'elastic-out'),

            self.wait(0.5, 's'),
            pytaVSL.set('smoke1-2', 'visible', 0),
            pytaVSL.set('explosion2', 'visible', 0),
            pytaVSL.set('wings1', 'position_z', -11),

            self.wait(0.3, 's'),
            pytaVSL.animate('m_iraye', 'rotate_z', -79, -82, 2, 's', 'random-mirror', loop=True),
            pytaVSL.animate('m_iraye', 'position_x', None, 0.3, 2, 's', 'exponential-inout'),
            pytaVSL.animate('m_iraye', 'position_y', None, 0.24, 2, 's', 'exponential-inout'),
            pytaVSL.animate('wings1', 'position_x', None, 0.448, 2, 's', 'exponential-inout'),
            pytaVSL.animate('wings1', 'position_y', None, 0.78, 2, 's', 'exponential-inout'),
            pytaVSL.animate('wings1', 'offset_y', 0, 0.01, 2, 's', 'random-mirror', loop=True),
            pytaVSL.animate('wings2', 'offset_y', 0, 0.01, 2, 's', 'random-mirror', loop=True),
            pytaVSL.animate('m_iraye', 'offset_y', 0, 0.01, 2, 's', 'random-mirror', loop=True),

            self.wait(1, 's'),
            pytaVSL.set('m_ch5-2', 'video_speed', 1),
            pytaVSL.movie_in('f_ch5-3_jack', 0.2, zoom=0.5, x=-0.2, y=-0.15, extinction = False),
            mainSampler.movie_in(), ### AUDIO
            pytaVSL.animate('smoke*', 'alpha', 1, 0, 0.2, 's'),
            self.wait(4.5, 's'),
            pytaVSL.animate('m_iraye', 'position_y', None, 1.5, 0.8, 's', 'elastic-inout'),
            pytaVSL.animate('wings1', 'position_y', None, 1.5, 0.8, 's', 'elastic-inout'),
            pytaVSL.animate('m_iraye', 'position_x', None, 0.3, 0.8, 's', 'elastic-inout'),
            pytaVSL.animate('wings1', 'position_x', None, 0.3, 0.8, 's', 'elastic-inout'),
            mainSampler.miraye_out(), ### AUDIO
            mainSampler.flying_wings('off'), ### AUDIO

            pytaVSL.animate('f_ilm_2', 'position_x', 1, 0.3, 1, 's', 'elastic-inout'),
            pytaVSL.set('f2_ch5-3_enedys', 'visible', 1),
            pytaVSL.set('f_ilm_2', 'visible', 1),

            self.wait(2, 's'),
            pytaVSL.set('m_iraye', 'visible', 0),
            pytaVSL.stop_animate('m_iraye', 'offset_y'),
            pytaVSL.stop_animate('m_ch5-2', 'rgbwave'),
            pytaVSL.stop_animate('m_ch5-2', 'noise'),
            pytaVSL.stop_animate('m_ch5-2', 'fish'),
            pytaVSL.stop_animate('m_ch5-2', 'offset_y'),
            pytaVSL.set('m_ch5-2', 'noise', 0),
            pytaVSL.set('m_ch5-2', 'rgbwave', 0),
            pytaVSL.set('m_ch5-2', 'fish', 0),

            pytaVSL.animate('f_ilm_3', 'scale', None, [0.95, 0.95], 1, 's', 'elastic-inout'),
            pytaVSL.set('f_ilm_3', 'visible', 1),
            pytaVSL.set('f3_ch5-3_panneau', 'visible', 1),

            # On rentre le BT
            pytaVSL.animate('bt', 'position_x', None, 0.525, 0.8, 's', 'elastic-out'),
            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, 0.475, 0.8, 's', 'elastic-out'),
            # pytaVSL.animate('bt_main', 'brightness', None, 0.3, 0.3, 's'),
            # pytaVSL.animate('*ep_*', 'brightness', None, 0.3, 0.3, 's'),
            # pytaVSL.animate('t_nobh_entovillon', 'brightness', None, 0.3, 0.3, 's'),

            # self.wait(1.2, 's'),
            self.wait(pytaVSL.get('f3_ch5-3_panneau', 'video_end'), 's'),
            pytaVSL.set('f_ilm', 'visible', 0),
            pytaVSL.set('f_ilm_2', 'visible', 0),
            self.f_ch5_4()
        ])

    # @pedalboard_button(5001, who='orl', after='f_ch5_3')
    @pedalboard_button()
    def f_ch5_4(self):
        """
        Vers le Tracto-Botte
        """
        ### AUDIO
        # transport.set_tempo(105)
        # self.start_scene('sequence/m_ch5-4_audio', lambda: [
        #     mainSampler.send('/instrument/play', 's:symphonie_x', 100),
        #     self.wait(5, 'beat'),
        #     mainSampler.send('/instrument/play', 's:symphonie_b', 100),
        #     self.wait(3, 'beat'),
        #     mainSampler.send('/instrument/play', 's:symphonie_x', 100),
        #     mainSampler.send('/instrument/stop', 's:symphonie_b'),
        # ])

        ### VJ
        self.start_scene('sequence/f_ch5_4', lambda: [
            pytaVSL.set('f_ilm', 'scale', 0.95, 0.95),
            pytaVSL.set('f_ilm', 'position_x', 0),
            pytaVSL.set('f_ilm', 'position_y', 0),
            pytaVSL.set('f_ch5-3_jack', 'visible', 0),
            pytaVSL.set('f_ch5-4', 'visible', 1),
            pytaVSL.set('f_ilm', 'visible', 1),
            pytaVSL.set('f_ilm_3', 'visible', 0),
            pytaVSL.set('f3_ch5-3_panneau', 'visible', 0),
            self.wait(pytaVSL.get('f_ch5-4', 'video_end'), 's'),
            self.f_ch5_5()
        ])


    @pedalboard_button(104)
    def f_ch5_5(self):
        """
        Vers le Tracto-Botte
        """
        self.start_scene('sequence/f_ch5_5', lambda: [
            pytaVSL.stop_animate('m_iraye', 'offset_y'),
            pytaVSL.stop_animate('wings1', 'offset_y'),
            pytaVSL.stop_animate('wings2', 'offset_y'),

            pytaVSL.f_switch_video('f_ch5-4', 'f_ch5-5'),
            # TODO : voir si ça peut être utile : pytaVSL.set('f_ch5-5', 'video_speed', 0),
            pytaVSL.set('f_ilm_2', 'scale', 0.8, 0.8),
            pytaVSL.set('f_ilm_2', 'position_y', 0),
            pytaVSL.set('f_ilm_2', 'position_x', 1),
            pytaVSL.set('f_ilm_3', 'scale', 0.8, 0.8),
            pytaVSL.set('f_ilm_3', 'position_y', 0),
            pytaVSL.set('f_ilm_3', 'position_x', 1),
            pytaVSL.set('f2_ch5-entree_vanupies', 'visible', 1),
            pytaVSL.set('t2_ch5-entree_vanupies_front', 'position_x', 1),
            pytaVSL.set('t2_ch5-entree_vanupies_front', 'visible', 1),
            pytaVSL.set('f2_ch5-3_enedys', 'visible', 0),
            self.wait(0.2, 's'),
            pytaVSL.set('f_ilm_2', 'visible', 1),
            pytaVSL.set('f_ilm_3', 'visible', 1),

            # self.wait(pytaVSL.get('f_ch5-5', 'video_end') - 0.2),
            # self.f_ch5_6()
        ])

    @pedalboard_button(5002, who='orl', after='f_ch5_5')
    def f_ch5_6(self):
        """
        Vers le Tracto-Botte
        """
        pytaVSL.f_switch_video('f_ch5-5', 'f_ch5-6'), ### VJ
        transport.set_tempo(105)
        self.start_scene('sequence/m_ch5-6_audio', lambda: [
            # mainSampler.send('/instrument/play', 's:symphonie_x', 100),
            # self.wait(5, 'beat'),
            mainSampler.send('/instrument/play', 's:symphonie_b', 127),
            self.wait(3, 'beat'),
            mainSampler.send('/instrument/play', 's:symphonie_x', 127),
        ])

    @pedalboard_button(5003, who='orl', after='f_ch5_6')
    def f_ch5_7(self):
        """
        Vers le Tracto-Botte
        """
        pytaVSL.f_switch_video('f_ch5-6', 'f_ch5-7')
        mainSampler.send('/instrument/stop', 's:symphonie_b'),
        # self.start_scene('sequence/f_ch5_7_2_f_ch5_8', lambda:[
        #     self.wait(pytaVSL.get('f_ch5-7', 'video_end'), 's'),
        #     # self.f_ch5_8()
        # ])


    # @pedalboard_button()
    @pedalboard_button(5004, who='orl', after='f_ch5_7')
    def f_ch5_8(self):
        """
        Vers le Tracto-Botte
        """
        transport.set_tempo(105)
        self.start_scene('sequence/m_ch5-6_audio', lambda: [
            # mainSampler.send('/instrument/play', 's:symphonie_x', 100),
            # self.wait(5, 'beat'),
            pytaVSL.f_switch_video('f_ch5-7', 'f_ch5-8'), ### VJ
            mainSampler.send('/instrument/play', 's:symphonie_c', 127),
            self.wait(6, 'beat'),
            mainSampler.send('/instrument/play', 's:symphonie_x', 127),
        ])


    @pedalboard_button(5005, who='orl', after='f_ch5_8')
    def f_ch5_9(self):
        """
        Vers le Tracto-Botte
        """

        ### AUDIO
        # transport.set_tempo(105)
        # self.start_scene('sequence/m_ch5-9_audio', lambda: [
        #     mainSampler.send('/instrument/play', 's:symphonie_x', 100),
        # ])
        mainSampler.send('/instrument/stop', 's:symphonie_c'),

        ### VJ
        self.start_scene('sequence/f_ch5_9', lambda: [
            pytaVSL.f_switch_video('f_ch5-8', 'f_ch5-9'),
            self.wait(pytaVSL.get('f_ch5-9', 'video_end') - 0.01, 's'),
            self.f_ch5_tracto()
        ])


    @pedalboard_button(105)
    def button_f_ch5_tracto(self):
        self.f_ch5_tracto()

    def f_ch5_tracto(self):
        """
        Dans la botcave
        """
        dz_t = 2

        ### AUDIO

        def symphonie_on():
            transport.set_tempo(125)
            self.start_scene('sequence/m_ch5-tracto', lambda: [
                mainSampler.send('/instrument/play', 's:symphonie_intro', 127),
                self.wait(5, 'beat'),
                transport.start(),
                self.start_sequence('sequence/symphonie_full_1', [
                    { # bar 1
                        1: lambda: [mainSampler.send('/instrument/play', 's:symphonie_full', 127), self.logger.info('s:symphonie_full - symphonie_full_1 - symphonie_on f_ch5_tracto')]
                    },
                    {}, {}, {}, {}, {}, {}, {}# bars 2 - 8
                ], loop=True)
            ])

        ### VJ

        self.start_scene('sequence/f_ch5_tracto', lambda: [
            # Init
            pytaVSL.animate('tracto-corps_a', 'sequence_index', 0, 30, 4, 's', loop=True),
            pytaVSL.animate('f_ch5-tracto_b', 'offset_y', 0, 0.01, 1, 's' 'random-mirror'),
            pytaVSL.animate('tracto-lingus', 'position_x', None, 0.1, 0.2, 's'),
            pytaVSL.animate('tracto-lingus', 'offset_x', 0, 0.02, 1, 's', 'random-mirror'),
            pytaVSL.animate('tracto-lingus', 'offset_y', 0, 0.02, 1, 's', 'random-mirror'),

            pytaVSL.set('f_ch5-tracto_b', 'scale', 2.5, 2.5),
            pytaVSL.set('f_ch5-tracto_b', 'position_x', 0.25),
            pytaVSL.set('f_ch5-tracto_b', 'position_y', -0.1),
            pytaVSL.set('f_ch5-botcave*', 'scale', 8.33, 8.33),



            # Affichage
            pytaVSL.set('f_ch5-botcave*', 'visible', 1),
            # pytaVSL.set('f_ch5-wings*', 'visible', 1),
            pytaVSL.set('f_ch5-tracto_b', 'visible', 1),
            pytaVSL.set('f_ch5-9', 'visible', 0),
            pytaVSL.set('diaphragme', 'visible', 1),
            mainSampler.send('/instrument/play', 's:f_ch5_demarrage_tractobotte', 100), ### AUDIO


            self.wait(1, 's'),
            # Dezoom
            mainSampler.send('/instrument/play', 's:f_ch5_moteur_tractobotte', 100), ### AUDIO
            pytaVSL.animate('f_ch5-tracto_b', 'scale', None, [0.3, 0.3], dz_t, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, 0, dz_t, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, 0, dz_t, 's'),
            pytaVSL.animate('f_ch5-botcave*', 'scale', None, [1, 1], dz_t, 's'),
            # pytaVSL.animate('f_ch5-wings*', 'scale', None, [0.07, 0.07], dz_t, 's'), # Tentative
            pytaVSL.animate('diaphragme', 'scale', None, [3, 3], dz_t, 's'),
            self.wait(dz_t / 2, 's'),
            pytaVSL.animate('diaphragme', 'alpha', None, 0, dz_t, 's'),

            self.wait(dz_t / 2, 's'),




            # Démarrage
            self.wait(0.7, 's'),
            mainSampler.send('/instrument/play', 's:f_ch5_acceleration_tractobotte', 100), ### AUDIO
            pytaVSL.flying_wings('wingsp3'),
            mainSampler.flying_wings(), ### AUDIO
            pytaVSL.animate('tv3', 'position_y', None, 0.38, 0.7, 's', 'elastic-out'),
            pytaVSL.animate('f_ch5-tracto_b', 'offset_y', 0, 0.01, 1, 's' 'random-mirror'),
            pytaVSL.set('tracto-explodus*', 'alpha', 1),
            pytaVSL.animate('tracto-lingus', 'position_x', None, -0.05, 0.8, 's', 'elastic-inout'),
            pytaVSL.animate('tracto-corps_a', 'sequence_index', 0, 30, 4, 's', loop=True),
            pytaVSL.animate('tracto-jantus*', 'rotate_z', 0, -1080, 4, 's', loop=True),

            self.wait(18/25 - 0.01, 's'),
            pytaVSL.set('tracto-smoke1-*', 'alpha', 1),
            pytaVSL.set('tracto-explodus*', 'alpha', 0),

            self.wait(0.2, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, 0.295, 1, 's', 'elastic-in'),
            mainSampler.send('/instrument/play', 's:symphonie_x', 127), ### AUDIO


            self.wait(0.3, 's'),
            # pytaVSL.flying_wings('6', on=True, duration=4/25),
            # pytaVSL.flying_wings('7', on=True, duration=4/25),
            # pytaVSL.flying_wings('8', on=True, duration=4/25),
            # pytaVSL.flying_wings('9', on=True, duration=4/25),

            # pytaVSL.animate('f_ch5-em_2', 'rotate_z', None, 150, 0.5, 's'),
            # pytaVSL.animate('f_ch5-em_2', 'position_y', None, -0.1, 0.5, 's'),
            # pytaVSL.set('f_ch5-em_2', 'visible', 1),
            # pytaVSL.set('f_ch5-em_1', 'visible', 0),
            #
            # pytaVSL.animate('f_ch5-wings1', 'rotate_z', None, 30, 0.6, 's'),
            # pytaVSL.animate('f_ch5-wings1', 'position_x', None, -0.163, 1, 's'),
            # pytaVSL.animate('f_ch5-wings1', 'position_y', None, 0.161, 1, 's'),
            #
            # pytaVSL.animate('f_ch5-wings2', 'rotate_z', None, -10, 0.7, 's'),
            # pytaVSL.animate('f_ch5-wings2', 'position_x', None, -0.172, 3, 's'),
            # pytaVSL.animate('f_ch5-wings2', 'position_y', None, 0.25, 3, 's'),
            # pytaVSL.animate('f_ch5-wings2', 'scale', None, [0.01, 0.01], 3, 's'),
            #
            # pytaVSL.animate('f_ch5-wings3', 'rotate_z', None, 5, 0.7, 's'),
            # pytaVSL.animate('f_ch5-wings3', 'position_x', None, -0.174, 3, 's'),
            # pytaVSL.animate('f_ch5-wings3', 'position_y', None, 0.27, 3, 's'),
            # pytaVSL.animate('f_ch5-wings3', 'scale', None, [0.01, 0.01], 3, 's'),
            #
            # pytaVSL.animate('f_ch5-wings4', 'rotate_z', None, 5, 0.7, 's'),
            # pytaVSL.animate('f_ch5-wings4', 'position_x', None, -0.184, 3, 's'),
            # pytaVSL.animate('f_ch5-wings4', 'position_y', None, 0.28, 3, 's'),
            # pytaVSL.animate('f_ch5-wings4', 'scale', None, [0.01, 0.01], 3, 's'),
            #
            # self.wait(0.5, 's'),
            # pytaVSL.animate('f_ch5-em_3', 'rotate_z', None, 210, 0.5, 's'),
            # pytaVSL.animate('f_ch5-em_3', 'position_x', None, -0.14, 0.5, 's'),
            # pytaVSL.animate('f_ch5-em_3', 'position_y', None, 0.1, 0.5, 's'),
            #
            #
            # pytaVSL.set('f_ch5-em_3', 'visible', 1),
            # pytaVSL.set('f_ch5-em_2', 'visible', 0),

            # self.wait(0.5, 's'), Pose pb pour calage tracto_b
            self.wait(0.21, 's'),


            pytaVSL.stop_animate('tracto-corps_a', 'sequence_index'),
            pytaVSL.stop_animate('tracto-jantus*', 'rotate_z'),
            pytaVSL.set('tracto-*us*', 'saturation', 1),
            pytaVSL.set('f_ch5-botcave*', 'saturation', 1),
            # pytaVSL.set('*ailes*', 'saturation', 1),
            # pytaVSL.set('*bras*', 'saturation', 1),
            # pytaVSL.set('wings*_main', 'saturation', 1),
            pytaVSL.animate('f_ch5-botcave*', 'offset_x', 0, 0.01, 0.7, 's', 'elastic-mirror'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, 0.1, 0.5, 's', 'elastic-out'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, 0.1, 0.5, 's', 'linear-mirror'),
            mainSampler.send('/instrument/stop', 's:symphonie_x'),### AUDIO
            mainSampler.send('/instrument/play', 's:f_ch3_crash_sr', 100), ### AUDIO

            # Enedys se fait éjecter en choc frontal
            pytaVSL.animate('em_1', 'position_x', None, 1, 0.5, 's', 'elastic-out'),
            pytaVSL.animate('em_1', 'rotate_z', None, -360, 0.5, 's', 'elastic-out'),

            self.wait(0.3, 's'), # Pour rattraper le manque des lignes d'avant
            # pytaVSL.animate('f_ch5-wings1', 'position_x', None, -0.17, 2, 's'),
            # pytaVSL.animate('f_ch5-wings1', 'position_y', None, 0.23, 2, 's'),
            # pytaVSL.animate('f_ch5-wings1', 'scale', None, [0.01, 0.01], 2, 's'),
            #
            # pytaVSL.animate('f_ch5-em_3', 'position_x', None, -0.168, 2, 's'),
            # pytaVSL.animate('f_ch5-em_3', 'position_y', None, 0.22, 2, 's'),
            # pytaVSL.animate('f_ch5-em_3', 'scale', None, [0.01, 0.01], 2, 's'),



            self.wait(2, 's'),
            pytaVSL.set('em_1', 'visible', 0),
            # pytaVSL.set('f_ch5-wings*', 'visible', 0),
            # pytaVSL.set('f_ch5-em*', 'visible', 0),
            # pytaVSL.flying_wings('6', on=False),
            # pytaVSL.flying_wings('7', on=False),
            # pytaVSL.flying_wings('8', on=False),
            # pytaVSL.flying_wings('9', on=False),


            # Lève la Botte
            pytaVSL.animate('tracto-bottus', 'rotate_z', None, 80, 0.8, 's'),

            self.wait(1, 's'),
            # Démarrage
            mainSampler.send('/instrument/play', 's:f_ch5_acceleration_tractobotte', 100), ### AUDIO
            pytaVSL.animate('f_ch5-tracto_b', 'offset_y', 0, 0.01, 1, 's' 'random-mirror'),
            pytaVSL.animate('tracto-lingus', 'position_x', None, 0.1, 0.2, 's'),
            pytaVSL.animate('tracto-lingus', 'offset_x', 0, 0.02, 1, 's', 'random-mirror'),
            pytaVSL.animate('tracto-lingus', 'offset_y', 0, 0.02, 1, 's', 'random-mirror'),


            self.wait(0.7, 's'),
            symphonie_on(),
            pytaVSL.animate('f_ch5-tracto_b', 'offset_y', 0, 0.01, 1, 's' 'random-mirror'),
            pytaVSL.set('tracto-explodus*', 'alpha', 1),
            pytaVSL.animate('tracto-lingus', 'position_x', None, -0.05, 0.8, 's', 'elastic-inout'),
            pytaVSL.animate('tracto-corps_a', 'sequence_index', 0, 30, 4, 's', loop=True),
            pytaVSL.animate('tracto-jantus*', 'rotate_z', 0, -1080, 4, 's', loop=True),

            self.wait(18/25 - 0.01, 's'),
            pytaVSL.set('tracto-smoke1-*', 'alpha', 1),
            pytaVSL.set('tracto-explodus*', 'alpha', 0),

            self.wait(0.2, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, 1, 1, 's', 'elastic-in'),
            pytaVSL.animate('f_ch5-tracto_b', 'offset_y', 0, 0.01, 1, 's' 'random-mirror', loop=True),

            #### Démarrer séquence moteur et fumée ici
            pytaVSL.tracto_exhaustive(),
            mainSampler.tracto_exhaustive(), ### AUDIO

            self.wait(0.7, 's'),
            self.t_ch5_cassage_p1()
        ])

    @pedalboard_button(106)
    def button_t_ch5_cassage_p1(self):
        self.stop_scene('sequence/*')
        self.t_ch5_cassage_p1()
    def t_ch5_cassage_p1(self):
        self.start_scene('sequence/t_ch5_cassage_p1', lambda: [
            # cassage du cadre  - vers écran suivant
            pytaVSL.animate('f_ilm', 'fish', 0, 0.8, 0.2, 's', 'linear-mirror'),
            # self.wait(0.2, 's'),
            # pytaVSL.animate('f_ilm', 'fish', 0, -0.8, 0.2, 's', 'linear-mirror'),

            self.wait(0.7, 's'),
            # Déplacement films
            pytaVSL.set('f_ch5-tracto_b', 'scale', 0.8, 0.8),
            pytaVSL.set('f_ch5-tracto_b', 'position_y', -0.05),
            pytaVSL.animate('f_ilm', 'position_x', 0, -1, 0.3, 's'),
            pytaVSL.animate('f_ilm_2', 'position_x', 1, 0.12, 0.3, 's'),
            pytaVSL.animate('t2_ch5-entree_vanupies_front', 'position_x', 1, 0.12, 0.3, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_x', 1, 0.8, 0.3, 's'),

            pytaVSL.set('f_ch5-botcave*', 'visible', 0),

            self.wait(0.7),
            self.t_ch5_cassage_p2()
        ])

    @pedalboard_button(107)
    def button_t_ch5_cassage_p2(self):
        self.stop_scene('sequence/*')
        pytaVSL.set('f_ilm*', 'visible', 1)
        pytaVSL.set('f_ilm', 'scale', 0.95, 0.95)
        pytaVSL.set('f_ch5-tracto_b', 'visible', 1)
        pytaVSL.set('f_ch5-tracto_b', 'scale', 0.8, 0.8)
        pytaVSL.set('f_ch5-tracto_b', 'position_y', -0.05)
        pytaVSL.set('tracto*', 'saturation', 1)
        pytaVSL.set('m_iraye', 'visible', 0)
        pytaVSL.trijc_io('out', 'tuba')
        self.t_ch5_cassage_p2()
    def t_ch5_cassage_p2(self):
        """
        Entrée chez les vanupiés
        """
        self.start_scene('sequence/t_ch5_cassage_p2', lambda: [
            pytaVSL.animate('tracto-bottus', 'rotate_z', None, 0, 1, 's', 'elastic-inout'),

            self.wait(1.3, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'offset_y', 0, 0.01, 1, 's' 'random-mirror'),
            pytaVSL.set('tracto-explodus*', 'alpha', 1),
            pytaVSL.animate('tracto-lingus', 'position_x', None, -0.05, 0.8, 's', 'elastic-inout'),
            pytaVSL.animate('tracto-corps_a', 'sequence_index', 0, 30, 4, 's', loop=True),
            pytaVSL.animate('tracto-jantus*', 'rotate_z', 0, -1080, 4, 's', loop=True),

            # Vers écran suivant
            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, 2, 1, 's', 'elastic-in'),
            pytaVSL.animate('f_ch5-tracto_b', 'offset_y', 0, 0.01, 1, 's' 'random-mirror', loop=True),

            self.wait(0.7, 's'),
            # Déplacement films
            pytaVSL.set('f_arabesques', 'visible', 0),
            pytaVSL.animate('f_ilm', 'position_x', 1, 0, 0.3, 's'),

            pytaVSL.animate('f_ilm_2', 'position_x', None, -1, 0.3, 's'),
            pytaVSL.animate('t2_ch5-entree_vanupies_front', 'position_x', None, -1, 0.3, 's'),
            pytaVSL.set('f_ch5-tracto_b', 'position_x', -0.3),
            self.wait(0.4, "s"),
            pytaVSL.set('t2_ch5-entree_vanupies_front', 'visible', 0),

            self.t_ch5_cassage_p3()
        ])

    def t_ch5_cassage_pn(self, n, f_ch, o_f_ch,
        y_ref = 0,
        duration_attente = 2,
        duration_arrivee = 2,
        duration_ecrase = 1,
        duration_montee = 1,
        duration_mid = 0.3,
        bifurcation=''):
        """
        Cassage des bâtiments et films
        """
        pytaVSL.set(f_ch, 'visible', 1)
        pytaVSL.set(o_f_ch, 'visible', 0)
        pytaVSL.set('f_ilm_' + str(n), 'position_x', 1)
        pytaVSL.set('f_ilm_' + str(n), 'position_y', y_ref - 0.28)
        pytaVSL.set('f_ilm_' + str(n), 'scale', 0.3, 0.3)
        x_dest = _rand() * 0.3 - 0.1
        if y_ref:
            tv3_ypos = -0.3
        else:
            tv3_ypos = 0.38
        if bifurcation == '':
            duration_run = duration_arrivee * 0.6 /2
            self.wait(duration_attente, 's'),
            pytaVSL.animate('f_ilm_' + str(n), 'position_x', None, x_dest, duration_arrivee, 's'),
            self.wait(duration_run * 1.5, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, y_ref - 0.05, duration_run / 2, 's', 'elastic-out'),
            pytaVSL.animate('tv3', 'position_y', None, tv3_ypos, 0.5, 's', 'elastic-inout'),
            self.wait(duration_run * 0.5, 's'),

            pytaVSL.tracto_defile(on=False),
            pytaVSL.animate('tracto-corps_a', 'sequence_index', 0, 30, duration_run, 's'),
            pytaVSL.animate('tracto-jantus*', 'rotate_z', 0, -1080, duration_run, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'offset_x', None, x_dest, duration_run, 's', 'elastic-inout'),
            self.wait(duration_run, 's'),

            pytaVSL.tracto_ecrase(f_ch, n, y_ref, duration_ecrase, duration_montee, duration_mid),
            mainSampler.tracto_ecrase(duration_ecrase, duration_montee, duration_mid), ### AUDIO

            self.wait(duration_ecrase + duration_montee + duration_mid, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'offset_x', None, 0.1, 0.4, 's', 'elastic-in'),
            self.wait(duration_ecrase/4, 's')
        elif bifurcation == 'ravi':
            duration_run = duration_arrivee * 0.6 /2
            self.wait(duration_attente, 's'),


            pytaVSL.animate('f_ilm_' + str(n), 'position_x', None, 0.1, duration_arrivee, 's'), # avant : x_dest



            self.wait(duration_arrivee * 1 / 2, 's'),

            intermed_run = 0.4

            pytaVSL.animate('tv2', 'position_y', None, 0.15, intermed_run, 's'),
            pytaVSL.animate('tv2', 'position_x', None, -0.2, intermed_run, 's'),
            pytaVSL.animate('tv2', 'rotate_z', -5, 5, intermed_run / 4, 's', 'linear-mirror', loop=True),
            pytaVSL.animate('tv2', 'scale', None, [0.35, 0.35], intermed_run, 's'),
            pytaVSL.animate('p_fakir', 'color', None, [1, 0.5, 0.5], intermed_run, 's', 'random-mirror', loop=True),
            pytaVSL.animate('p_fakir', 'offset_x', None, 0.01, intermed_run, 's', 'random-mirror', loop=True),
            pytaVSL.animate('p_fakir', 'offset_y', None, 0.02, intermed_run, 's', 'random-mirror', loop=True),


            self.wait(intermed_run + 0.2, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, y_ref - 0.05, duration_run / 2, 's', 'elastic-out'),
            pytaVSL.animate('tv3', 'position_y', None, tv3_ypos, 0.5, 's', 'elastic-inout'),
            self.wait(duration_run * 0.5, 's'),

            pytaVSL.animate('f_ilm_' + str(n), 'position_x', None, 0.2, intermed_run, 's', 'elastic-out'),
            pytaVSL.tracto_defile(on=False),
            pytaVSL.animate('tracto-corps_a', 'sequence_index', 0, 30, intermed_run, 's'),
            pytaVSL.animate('tracto-jantus*', 'rotate_z', 0, -1080, intermed_run, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'offset_x', None, x_dest - 0.1, intermed_run, 's', 'elastic-inout'),

            pytaVSL.animate('tracto-pellus', 'rotate_z', None, 30, intermed_run / 2, 's'),
            pytaVSL.animate('tracto-bottus', 'rotate_z', None, 70, intermed_run / 2, 's'),
            pytaVSL.animate('tracto-bottus', 'position_y', None, -0.2, intermed_run / 2, 's'),
            # self.wait(0.6, 's'),

            pytaVSL.animate('tracto-bottus', 'offset_x', 0, 0.02, intermed_run, 's', 'elastic-out'),




            self.wait(intermed_run, 's'),
            pytaVSL.animate('tracto-bottus', 'offset_x', 0.02, 0, 0.3 , 's', 'elastic-out'),

            self.wait(0.5, 's'),
            pytaVSL.animate('tv2', 'rotate_z', None, 0, 0.7, 's'),
            pytaVSL.animate('tv2', 'position_y', None, 0.36, 0.7, 's', 'elastic-inout'),
            pytaVSL.animate('tv3', 'position_y', None, -0.3, 0.7, 's', 'elastic-inout'),
            pytaVSL.animate('tv2', 'position_x', None, 0, 0.7, 's', 'linear-mirror'),
            pytaVSL.animate('tv2', 'scale', None, [0.24, 0.24], 0.7, 's', 'elastic-inout'),
            pytaVSL.animate('p_fakir', 'color', None, [0.5, 0.5, 0.5], 0.7, 's', 'elastic-inout'),
            pytaVSL.animate('p_fakir', 'offset_x', None, 0, 0.7, 's', 'elastic-inout'),
            pytaVSL.animate('p_fakir', 'offset_y', None, 0, 0.7, 's', 'elastic-inout'),
            pytaVSL.set('p_cockpit_surpris', 'visible', 1),

            self.wait(1, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, -0.6, 0.4, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'offset_x', None, 0, 0.4, 's', 'elastic-inout'),
            pytaVSL.animate('tracto-bottus', 'rotate_z', None, 0, 0.4, 's', 'elastic-inout'),
            pytaVSL.animate('tracto-bottus', 'position_y', None, 0, 0.4, 's'),
            pytaVSL.animate('tracto-pellus', 'rotate_z', None, 10, 0.4, 's'),
            self.wait(0.4, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, -0.42, 0.2, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, 0.25, 0.2, 's'),
            pytaVSL.animate('tv3', 'position_y', None, -0.3, 0.2, 's', 'elastic-inout'),
            pytaVSL.set('p_cockpit_success1', 'visible', 1),
            pytaVSL.set('p_cockpit_surpris', 'visible', 0),

            self.wait(0.2, 's'),

            # pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, 0.1, 0.8, 'elastic-inout'),
            # pytaVSL.animate('f_ch5-tracto_b', 'offset_x', None, 0, 0.8, 'elastic-inout'),
            # self.wait(0.9, 's'),
            pytaVSL.tracto_kick(tv='tv', n=2, duration=0.5, y_ref=0)

            # self.t_ch5_poursuite_batohacker()

            self.t_ch5_poursuite_batohacker()

            self.wait(0.1, 's')
            mainSampler.send('/instrument/play', 's:baffe_elan', 100), ### AUDIO
            self.wait(0.4, 's'),
            mainSampler.send('/instrument/play', 's:f_ch3_degagement_crepe', 100), ### AUDIO


        # self.send('/pedalboard/button', next_button)
        # ])


    @pedalboard_button(108)
    def button_t_ch5_cassage_p3(self):
        self.stop_scene('sequence/*')
        self.t_ch5_cassage_p3()
    def t_ch5_cassage_p3(self):
        """
        Destructions de l'entrée de la voyante
        """

        ### VJ
        self.start_scene('sequence/t_ch5_cassage_p3', lambda:[
            pytaVSL.tracto_defile(),
            self.t_ch5_cassage_pn(2, 'f2_ch5-entree_voyante', 'f2_ch5-entree_vanupies',
                y_ref = 0,
                duration_attente = 2,
                duration_arrivee = 2,
                duration_ecrase = 1),
            self.t_ch5_cassage_p4()
        ])


    @pedalboard_button(109)
    def button_t_ch5_cassage_p4(self):
        self.t_ch5_cassage_p4()
    def t_ch5_cassage_p4(self):
        """
        Destructions de l'entrée de Ravi
        """

        self.start_scene('sequence/t_ch5_cassage_p4', lambda:[
            pytaVSL.tracto_defile(),
            self.t_ch5_cassage_pn(2, 'f2_ch5-entree_fakir', 'f2_ch5-entree_voyante',
                y_ref = 0.3,
                duration_attente = 1.5,
                duration_arrivee = 2,
                duration_ecrase = 1),
            self.f_ch5_arrivee_voyante()
        ])

    @pedalboard_button(110)
    def button_f_ch5_arrivee_voyante(self):
        self.stop_scene('sequence/*')
        self.f_ch5_arrivee_voyante()
    def f_ch5_arrivee_voyante(self):
        """
        Arrivée de la voyante
        """
        ### LIGHT
        qlcplus.animate('faces', None, 180, 5, 's')

        transport.set_tempo(125)
        self.start_scene('sequence/t_ch5_arrivee_voyante_audio', lambda: [
            self.wait_next_cycle(),
            arrivee_v(),
            self.stop_scene('sequence/symphonie*'),
            self.start_sequence('sequence/symphonie_violons_cb_bottes', [
                { # bar 1
                    1: lambda: [
                        # mainSampler.send('/instrument/stop', 's:symphonie_full'),
                        mainSampler.stop_symphonies(),
                        self.logger.info('s:symphonie_full stopped  - symphonie_violons_cb_bottes - f_ch5_arrivee_voyante'),
                        mainSampler.send('/instrument/play', 's:symphonie_violons_cb_bottes', 127),
                        self.logger.info('s:symphonie_violons_cb_bottes'),
                    ]
                },
                {}, {}, {}, {}, {}, {}, {}# bars 2 - 8
            ], loop=True)
        ])


        pytaVSL.tracto_defile(),
        def arrivee_v():
            ### VJ
            self.start_scene('sequence/f_ch5_arrivee_voyante', lambda: [
                pytaVSL.flying_wings('wingsp1'),
                pytaVSL.set('p_voyante', 'visible', 1),

                pytaVSL.animate('tv1', 'offset_y', 0, 0.001, 3, 's', 'random-mirror', loop = True),
                pytaVSL.animate('tv1', 'position_x', 1, 0.36, 1, 's', 'elastic-inout'),
                pytaVSL.set('tv1', 'visible', 1),
                self.wait(2, 's'),
                pytaVSL.animate('tv1', 'scale', None, [1, 1], 0.7, 's', 'elastic-out'),
                pytaVSL.animate('tv1', 'offset_y', 0, 0.01, 3, 's', 'random-mirror', loop = True),
                pytaVSL.animate('tv1', 'position_x', None, 0.2, 0.7, 's', 'elastic-out'),
                pytaVSL.animate('tv1', 'position_y', None, 0, 0.7, 's', 'elastic-out'),
                self.wait(0.7, 's'),
                pytaVSL.animate('tv1', 'position_x', None, 0.1, 20, 's', 'linear-mirror'),
                self.wait(2, 's'),
                pytaVSL.tracto_ecrase('f2_ch5-entree_voyante', 2, 0, 2, 1, 1),
                mainSampler.tracto_ecrase(2, 1, 1), ### AUDIO
                self.wait(6, 's'),
                pytaVSL.tracto_ecrase('f2_ch5-entree_voyante', 2, 0, 1, 1, 1),
                mainSampler.tracto_ecrase(1, 1, 1), ### AUDIO
                self.wait(3, 's'),
                pytaVSL.tracto_ecrase('f2_ch5-entree_voyante', 2, 0, 1, 1, 2),
                mainSampler.tracto_ecrase(1, 1, 2), ### AUDIO
                self.wait(3, 's'),
                pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, -0.05, 0.5, 's', 'elastic-inout'),
                pytaVSL.animate('tv3', 'position_y', None, 0.38, 0.5, 's', 'elastic-inout'),
                self.wait(1, 's'),
                pytaVSL.tracto_ecrase('f2_ch5-entree_voyante', 2, 0, 2, 2, 1),
                mainSampler.tracto_ecrase(2, 2, 1), ### AUDIO
                self.wait(5, 's'),
                pytaVSL.animate('tv1', 'position_y', None, 0.38, 0.8, 's', 'elastic-inout'),
                pytaVSL.animate('tv1', 'position_x', None, 0.3, 0.8, 's', 'elastic-inout'),
                pytaVSL.animate('tv1', 'scale', None, [0.25, 0.25], 0.8, 's', 'elastic-inout'),
                pytaVSL.animate('tv1', 'offset_y', 0, 0.001, 3, 's', 'random-mirror', loop = True),
                self.wait(1, 's'),
                pytaVSL.set('p_voyante2', 'visible', 1),
                pytaVSL.set('p_voyante', 'visible', 0),
                self.f_ch5_arrivee_ravi(),
                self.t_ch5_cassage_p5()
            ])

    @pedalboard_button(111)
    def button_f_ch5_arrivee_ravi(self):
        self.stop_scene('sequence/*')
        self.f_ch5_arrivee_ravi()
    def f_ch5_arrivee_ravi(self):
        """
        Arrivée de Ravi
        """
        ### AUDIO
        transport.set_tempo(125)
        self.start_scene('sequence/t_ch5_arrivee_ravi_audio', lambda: [
            self.wait_next_cycle(),
            self.stop_scene('sequence/symphonie*'),
            self.start_sequence('sequence/symphonie_full_2', [
                { # bar 1
                    1: lambda: [
                        mainSampler.stop_symphonies(),
                        mainSampler.send('/instrument/play', 's:symphonie_full', 127),
                        self.logger.info('s:symphonie_full -  - symphonie_full_2 - f_ch5_arrivee_ravi'),
                        # mainSampler.send('/instrument/stop', 's:symphonie_violons_cb_bottes'),

                        self.logger.info('s:symphonie_violons_cb_bottes stopped'),
                    ]
                },
                {}, {}, {}, {}, {}, {}, {}# bars 2 - 8
            ], loop=True)
        ])




        ### VJ
        self.start_scene('sequence/f_ch5_arrivee_ravi', lambda: [
            pytaVSL.flying_wings('wingsp2'),
            pytaVSL.animate('tv2', 'offset_y', 0, 0.001, 3, 's', 'random-mirror', loop = True),
            pytaVSL.animate('tv2', 'offset_x', 0, 0.001, 7, 's', 'linear-mirror', loop = True),
            pytaVSL.animate('tv2', 'position_y', 1, 0.36, 1, 's', 'elastic-inout'),
            pytaVSL.set('tv2', 'visible', 1),
        ])

    @pedalboard_button(112)
    def t_ch5_cassage_p5(self):
        """
        Destructions de l'entrée du cabaret
        """

        self.start_scene('sequence/t_ch5_cassage_p5', lambda:[
            pytaVSL.tracto_defile(),
            pytaVSL.get('f_ilm_3', 'scale', 23456),
            self.t_ch5_cassage_pn(3, 'f3_ch5-entree_cabaret', 'f2_ch5-entree_fakir',
                y_ref = 0,
                duration_attente = 0.1,
                duration_arrivee = 1,
                duration_ecrase = 1),
            self.t_ch5_cassage_p6()
        ])

    @pedalboard_button(113)
    def t_ch5_cassage_p6(self):
        """
        Destructions de l'intérieur de Ravi
        """

        self.start_scene('sequence/t_ch5_cassage_p6', lambda:[
            pytaVSL.tracto_defile(),
            self.t_ch5_cassage_pn(2, 'f2_ch5-interieur_ravi', 'f3_ch5-entree_cabaret',
                y_ref = 0,
                duration_attente = 1,
                duration_arrivee = 2,
                duration_ecrase = 1,
                bifurcation = 'ravi'),
            #self.t_ch5_cassage_p4()
        ])

    @pedalboard_button(114)
    def button_t_ch5_poursuite_batohacker(self):
        self.stop_scene('sequence/*')
        pytaVSL.set('m_iraye', 'visible', 0)
        pytaVSL.set('f_ilm*', 'visible', 1)
        pytaVSL.set('f_ilm_3', 'position_x', 1)
        pytaVSL.set('f_ilm', 'position_x', 0)
        pytaVSL.set('f_ilm', 'position_y', 0)
        pytaVSL.set('f_ilm*', 'scale', 0.95, 0.95)

        pytaVSL.set('f_ilm_*', 'visible', 1)
        pytaVSL.set('f_ilm_2', 'scale', 0.3, 0.3)
        pytaVSL.set('f_ilm_2', 'position_x', 0.2)
        pytaVSL.set('f_ilm_2', 'position_y', -0.28)
        pytaVSL.set('f2_ch5-interieur_ravi', 'visible', 1)

        pytaVSL.set('f_ch5-tracto_b', 'visible', 1)
        pytaVSL.set('f_ch5-tracto_b', 'scale', 0.8, 0.8)
        pytaVSL.set('f_ch5-tracto_b', 'position_x', -0.42)
        pytaVSL.set('f_ch5-tracto_b', 'position_y', 0.25)

        pytaVSL.set('bt', 'position_x', 0.525)
        pytaVSL.set('bt', 'position_y', -0.4)

        pytaVSL.set('t_nobh_entovillon', 'position_x', 0.41)
        pytaVSL.set('t_nobh_entovillon', 'position_y', -0.221)
        pytaVSL.tracto_kick(tv='tv', n=2, duration=0.5, y_ref=0)

        pytaVSL.set('tv1', 'visible', 1)
        pytaVSL.animate('tv1', 'position_y', None, 0.38, 0.8, 's', 'elastic-inout'),
        pytaVSL.animate('tv1', 'position_x', None, 0.3, 0.8, 's', 'elastic-inout'),
        pytaVSL.animate('tv1', 'scale', None, [0.25, 0.25], 0.8, 's', 'elastic-inout'),
        pytaVSL.animate('tv1', 'offset_y', 0, 0.001, 3, 's', 'random-mirror', loop = True),
        pytaVSL.set('p_voyante2', 'visible', 1),


        self.t_ch5_poursuite_batohacker(),



    def t_ch5_poursuite_batohacker(self):
        """
        Lancement course poursuite batohacker / tracto botte
        """
        ### AUDIO
        transport.set_tempo(125)
        self.start_scene('sequence/t_ch5_poursuite_batohacker_audio', lambda: [
            self.wait_next_cycle(),
            self.stop_scene('sequence/symphonie*'),
            self.start_sequence('sequence/symphonie_soufflants_bottes', [
                { # bar 1
                    1: lambda: [
                        # mainSampler.send('/instrument/stop', 's:symphonie_full'),
                        mainSampler.stop_symphonies(),
                        self.logger.info('s:symphonie_full stopped - symphonie_soufflants_bottes - t_ch5_poursuite_batohacker'),
                        mainSampler.send('/instrument/play', 's:symphonie_soufflants_bottes', 127),
                        self.logger.info('s:symphonie_soufflants_bottes'),
                    ]
                },
                {}, {}, {}, {}, {}, {}, {}# bars 2 - 8
            ], loop=True)
        ])




        ### VJ
        self.start_scene('sequence/f_ch5_poursuite_batohacker', lambda:[
            pytaVSL.batohacker_hmove(0.193, 'jardin', 0.5),
            self.wait(0.5, 's'),

            # On dégage Ravi des calculs
            pytaVSL.flying_wings('wingsp2', on=False),
            pytaVSL.stop_animate('plane_horn_2', 'position_x'),
            pytaVSL.stop_animate('plane_horn_2', 'position_y'),

            pytaVSL.set('p_cockpit_surpris', 'visible', 1),
            pytaVSL.set('p_cockpit_success1', 'visible', 0),
            pytaVSL.animate('t_bh_grappin_*', 'position_y', 0, -0.123, 0.4, 's'),
            self.wait(0.1, 's'),
            pytaVSL.animate('t_bh_grappin_*', 'scale', None, [0.1, 0.1], 0.1, 's'),
            self.wait(0.4, 's'),
            pytaVSL.set('t_bh_grappin_ferme', 'visible', 1),
            pytaVSL.set('t_bh_grappin_ouvert', 'visible', 0),

            self.wait(0.2, 's'),
            pytaVSL.animate('t_bh_grappin_*', 'position_y', None, -0.115, 0.2, 's'),
            pytaVSL.animate('f_ilm_2', 'position_y', None, -0.27, 0.2, 's'),

            self.wait(0.2, 's'),
            pytaVSL.batohacker_hmove(1, 'cour', 1.5),
            pytaVSL.set('f_ilm_2', 'scale', -pytaVSL.get('f_ilm_2', 'scale')[0] , pytaVSL.get('f_ilm_2', 'scale')[0]),
            pytaVSL.animate('f_ilm_2', 'position_x', None, 1 , 1.5, 's', 'elastic-inout'),

            self.wait(0.3, 's'),
            pytaVSL.animate('tracto-corps_a', 'sequence_index', 0, 30, 4, 's', 'linear-mirror', loop=True),
            pytaVSL.animate('tracto-jantus*', 'rotate_z', 0, -1080, 4, 's', 'linear-mirror', loop=True),
            self.wait(0.5, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, 0.15, 0.2, 's'), #, 'elastic-out'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, -0.15, 0.2, 's'), #, 'elastic-out'),
            self.wait(0.2, 's'),
            pytaVSL.stop_animate('tracto-corps_a', 'sequence_index'),
            pytaVSL.stop_animate('tracto-jantus_1', 'rotate_z'),
            pytaVSL.stop_animate('tracto-jantus_2', 'rotate_z'),

            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, 0, 0.2, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'offset_x', None, 0.01, 0.2, 's', 'random-mirror'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_y', -0.15, -0.1, 0.2, 's', 'linear-mirror'),
            pytaVSL.set('p_cockpit_surpris', 'visible', 0),

            pytaVSL.animate('bt', 'scale', None, [0, 0.2], 0.1, 's', 'linear-mirror'),
            pytaVSL.animate('t_nobh_entovillon', 'scale', None, [0, 0.3], 0.1, 's', 'linear-mirror'),
            self.wait(0.1, 's'),

            pytaVSL.animate('bt', 'position_x', None, -0.1, 1, 's', 'elastic-out'),
            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, 0, 1, 's', 'elastic-out'),
            pytaVSL.animate('bt', 'position_y', None, 0.4, 1, 's', 'elastic-out'),
            pytaVSL.animate('t_nobh_entovillon', 'position_y', None, 0.31, 1, 's', 'elastic-out'),
            pytaVSL.animate('bt', 'rotate_z', None, 450, 0.4, 's', 'elastic-out'),
            pytaVSL.animate('t_nobh_entovillon', 'rotate_z', None, 450, 1, 's', 'elastic-out'),
            pytaVSL.animate('bt', 'scale', None, [0.2, -0.2], 0.1, 's'),
            pytaVSL.animate('t_nobh_entovillon', 'scale', None, [0.3, -0.3], 0.1, 's'),
            pytaVSL.animate('tv3', 'position_y', None, -1, 0.4, 's', 'elastic-out'),


            self.wait(1.5, 's'),
            # On dégage Cockpit des calculs
            pytaVSL.flying_wings('wingsp3', on=False),
            pytaVSL.stop_animate('plane_horn_3', 'position_x'),
            pytaVSL.stop_animate('plane_horn_3', 'position_y'),


            # pytaVSL.animate('tv3', 'position_x', None, 1, 1, 's', 'elastic-inout'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, 1, 1, 's', 'elastic-inout'),
            pytaVSL.animate('tracto-corps_a', 'sequence_index', 0, 30, 4, 's', 'linear-mirror', loop=True),
            pytaVSL.animate('tracto-jantus*', 'rotate_z', 0, -1080, 4, 's', 'linear-mirror', loop=True),
            # pytaVSL.animate('tv3', 'position_y', None, 0.38, 0.7, 's', 'elastic-out'),

            self.wait(1.5, 's'),
            self.t_ch5_course_poursuite_suite()

        ])

    @pedalboard_button(115)
    def button_t_ch5_course_poursuite_suite(self):
        self.stop_scene('sequence/*')
        self.t_ch5_course_poursuite_suite()

    @pedalboard_button()
    def t_ch5_course_poursuite_suite(self):
        """
        Course Poursuite Batohacker / Tracto Botte
        """
        def tracto_bato():
            pytaVSL.set('bato_h', 'scale', 0.8, 0.8)
            pytaVSL.set('f_ch5-tracto_b', 'scale', 0.4, 0.4)
            t_coef = 1
            index = 0
            while True:
                if _rand() > 0.35:
                    if pytaVSL.get('bato_h', 'position_x') > 0:
                        coef = -1
                        proue = 'cour'
                    else:
                        coef = 1
                        proue = 'jardin'

                    duration = t_coef * 1.1 * (_rand() + 1)
                    y = _rand() * 0.2 - 0.3
                    delay = t_coef * duration / ((_rand() + 1) * 2)
                    pytaVSL.set('bato_h', 'position_y', y)
                    # pytaVSL.batohacker_hmove(0, proue, delay)
                    pytaVSL.animate('bato_h', 'position_x', None, coef, duration)
                    mainSampler.batohacker_move(duration, goto='midtime'), ### AUDIO
                    pytaVSL.set('bato_h', 'scale', coef * pytaVSL.get('bato_h', 'scale')[1], pytaVSL.get('bato_h', 'scale')[1])
                    self.wait(delay, 's')
                    # pytaVSL.batohacker_hmove(coef, proue, duration - delay)
                    pytaVSL.set('f_ch5-tracto_b', 'position_y', y)
                    pytaVSL.set('f_ch5-tracto_b', 'scale', coef * pytaVSL.get('f_ch5-tracto_b', 'scale')[1], pytaVSL.get('f_ch5-tracto_b', 'scale')[1])
                    pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, coef, duration, 's')
                    self.wait(duration + delay, 's')
                else:
                    self.wait(0.5, 's')

                t_coef = t_coef - index / 20

        self.start_scene('sequence/tracto_bato', tracto_bato)
        pytaVSL.animate('tracto-bottus', 'rotate_z', None, 0, 0.5, 's')

        def trijc_clash(index=0):
            while pytaVSL.get('f_ch5-tracto_b', 'position_x') < 0.9:
                self.wait(0.1, 's')
            if index == 0:
                pytaVSL.trijc_io('in', 'tuba', 0.5)
                mainSampler.trijc_io('in', 0.5), ### AUDIO
            elif index == 1:
                pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, -0.22, 0.6, 's', 'linear-mirror', loop=True)
            elif index == 2:
                while pytaVSL.get('f_ch5-tracto_b', 'position_x') > 0.9:
                    self.wait(0.01, 's')
                self.stop_scene('sequence/tracto_bato')
                pytaVSL.stop_animate('f_ch5-tracto_b', 'position_x')
                pytaVSL.stop_animate('f_ch5-tracto_b', 'position_y')
                pytaVSL.animate('bato_h', 'position_x', None, -1, 1, 's'),
                mainSampler.batohacker_move(1, goto='midtime'), ### AUDIO
                pos = pytaVSL.get('f_ch5-tracto_b', 'position_x')
                arf=0
                while pos > -1:
                    if arf == 1:
                        ### AUDIO
                        transport.set_tempo(125 * 1.1)
                        self.start_scene('sequence/t_ch5_arf_audio', lambda: [
                            self.wait_next_cycle(),
                            self.stop_scene('sequence/symphonie*'),
                            self.start_sequence('sequence/symphonie_flutes_didj_percs_bottes_arf', [
                                { # bar 1
                                    1: lambda: [
                                        mainSampler.stop_symphonies(),
                                        mainSampler.send('/instrument/play', 's:symphonie_flutes_didj_percs_bottes', 127, 1.1),
                                        self.logger.info('s:symphonie_flutes_didj_percs_bottes  - symphonie_fflutes_didj_percs_bottes_arf - trijc_clash'),
                                        # mainSampler.send('/instrument/stop', 's:symphonie_soufflants_bottes'),
                                        self.logger.info('s:symphonie_soufflants_bottes stopped'),
                                    ]
                                },
                                {}, {}, {}, {}, {}, {}, {}# bars 2 - 8
                            ], loop=True)
                        ])


                    pos = pos - 0.3
                    self.start_scene('sequence/tracto_saut_' + str(arf), lambda: [
                        pytaVSL.animate('f_ch5-tracto_b', 'offset_y', 0, 0.1, 1, 's', 'elastic-mirror'),
                        self.wait(0.3, 's'),
                        pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, 0.2, 0.4, 's', 'linear-mirror'),
                        pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, pos, 0.4, 's'),
                        self.wait(0.5, 's'),
                        mainSampler.send('/instrument/play', 's:f_ch5_saut_tractobotte', 100), ### AUDIO
                        pytaVSL.animate('back', 'position_y', None, -arf/10, 0.5, 's', 'elastic-out'),
                        pytaVSL.animate('ciel_etoile_melies', 'position_y', None, 1 - arf/10, 0.5, 's', 'elastic-out'),
                        pytaVSL.animate('lights_*', 'offset_y', 0.01, 0, 0.5, 's', 'elastic-out'),
                    ])
                    if arf == 2:
                        self.start_scene('sequence/falling_left', lambda: [
                            pytaVSL.animate('w_signs_*', 'position_y', None, -1, 0.7, 's', 'elastic-inout'),
                            pytaVSL.animate('trijc', 'position_y', None, -1, 0.7, 's', 'elastic-inout'),
                            pytaVSL.animate('lights_stageright', 'position_y', None, -1, 0.7, 's', 'elastic-inout'),
                            self.wait(0.8, 's'),
                            pytaVSL.set('w_signs_*', 'visible', 0)
                        ])
                    self.wait(1.5, 's')

                    arf = arf + 1

                music_change = 1
                while pos < 1 and pytaVSL.get('ciel_etoile_melies', 'position_y') > 0:
                    if music_change:
                        ### AUDIO
                        transport.set_tempo(125*1.2)
                        self.start_scene('sequence/t_ch5_arf' + str(arf), lambda: [
                            self.wait_next_cycle(),
                            self.stop_scene('sequence/symphonie*'),
                            self.start_sequence('sequence/symphonie_soufflants_moinsdidj_bottes', [
                                { # bar 1
                                    1: lambda: [
                                        # mainSampler.send('/instrument/stop', 's:symphonie_flutes_didj_percs_bottes'),
                                        mainSampler.stop_symphonies(),
                                        self.logger.info('s:symphonie_flutes_didj_percs_bottes stopped'),
                                        mainSampler.send('/instrument/play', 's:symphonie_soufflants_moinsdidj_bottes', 127, 1.2),
                                        self.logger.info('s:symphonie_flutes_soufflants__moinsdidj_bottes'),
                                    ]
                                },
                                {}, {}, {}, {}, {}, {}, {}# bars 2 - 8
                            ], loop=True)
                        ])
                        music_change = 0

                    # TODO if pytaVSL.get('ciel_etoile_melies', 'position_y') > 0:
                    pytaVSL.set('f_ch5-tracto_b', 'scale', 0.5, 0.5)
                    pos = pos + 0.3
                    self.start_scene('sequence/tracto_saut_' + str(arf), lambda: [
                        pytaVSL.animate('f_ch5-tracto_b', 'offset_y', 0, 0.1, 1, 's', 'elastic-mirror'),
                        self.wait(0.3, 's'),
                        pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, 0.2, 0.4, 's', 'linear-mirror'),
                        pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, pos, 0.4, 's'),
                        self.wait(0.5, 's'),
                        mainSampler.send('/instrument/play', 's:f_ch5_saut_tractobotte', 100), ### AUDIO
                        pytaVSL.animate('back', 'position_y', None, -arf/10, 0.5, 's', 'elastic-out'),
                        pytaVSL.animate('lights_*', 'offset_y', 0.01, 0, 0.5, 's', 'elastic-out'),
                        pytaVSL.animate('ciel_etoile_melies', 'position_y', None, 1 - arf/10, 0.5, 's', 'elastic-out'),
                        pytaVSL.animate('lights_*', 'alpha', None, 1 -arf/10, 0.5, 's', 'elastic-out'),
                    ])
                    self.wait(1.5, 's')
                    # TODO: else: faire défiler la texture
                    arf = arf + 1

                coco = 1
                while True:
                    if pos > 1:
                        coco = -1
                    elif pos < -1:
                        coco =1
                    pos = pos + 0.3 * coco
                    pytaVSL.set('f_ch5-tracto_b', 'scale', coco*0.5, 0.5)
                    self.start_scene('sequence/tracto_saut_' + str(arf), lambda: [
                        pytaVSL.animate('f_ch5-tracto_b', 'offset_y', 0, 0.1, 1, 's', 'elastic-mirror'),
                        self.wait(0.3, 's'),
                        pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, 0.2, 0.4, 's', 'linear-mirror'),
                        pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, pos, 0.4, 's'),
                        self.wait(0.5, 's'),
                        mainSampler.send('/instrument/play', 's:f_ch5_saut_tractobotte', 100), ### AUDIO
                        pytaVSL.animate('ciel_etoile_melies', 'texture_offset', None, [0, 1 - arf/10], 0.5, 's', 'elastic-out'),
                    ])
                    self.wait(1.5, 's')
                    arf = arf + 1


            while pytaVSL.get('f_ch5-tracto_b', 'position_x') > - 0.15:
                self.wait(0.01, 's')
            if index == 0:
                mainSampler.send('/instrument/play', 's:f_ch3_degagement_crepe', 100), ### AUDIO
                pytaVSL.animate('trijc_head*', 'position_x', None, -0.5, 0.4, 's')
                pytaVSL.animate('trijc_head*', 'position_y', None, 1, 0.4, 's')
                self.wait(0.2, 's')
                pytaVSL.set('trijc_head*', 'visible', 0)
                pytaVSL.animate('*trijc', 'position_y', None, -0.1, 1, 's', 'elastic-inout')
                pytaVSL.animate('*trijc', 'rotate_z', None, -5, 1, 's', 'elastic-inout')
                trijc_clash(index=1)
            elif index == 1:
                mainSampler.send('/instrument/play', 's:f_ch3_degagement_crepe', 100), ### AUDIO
                pytaVSL.animate('trijc_tarte', 'position_x', None, -0.5, 0.4, 's')
                pytaVSL.animate('trijc_tarte', 'position_y', None, 1, 0.4, 's')
                self.wait(0.2, 's')
                pytaVSL.set('trijc_tarte', 'visible', 0)
                pytaVSL.animate('*trijc', 'position_y', None, -1, 1, 's', 'elastic-inout')
                self.wait(1, 's')
                pytaVSL.set('trijc', 'visible', 0)
                trijc_clash(index=2)



        self.start_scene('sequence/tracto_botto_bato', lambda:[
            self.wait(5, 's'),
            self.start_sequence('sequence/botto', [
                {
                'signature': '1/8',
                1: lambda: pytaVSL.tracto_tape(0.3, 0.2, 0.15)
                }
            ], loop = True
            ),
            self.wait(10, 's'),
            self.stop_scene('sequence/botto'),
            self.start_sequence('sequence/kicko', [
                {
                'signature': '1/8',
                1: lambda: pytaVSL.tracto_kickiteasy(0.5)
                }
            ], loop = True
            ),
            trijc_clash()
        ])


    @pedalboard_button(5006, who='adrien', after='t_ch5_course_poursuite_suite')
    def f_ch5_voyante_plein_la_tronche(self):
        """
        Le  EP jette les choses par les fenêtres (ORL active)
        """
        def ep_jet(to_destroy, future_angle):
            yzones_v= {
            70:  [-0.18, 0.28],# [-0.15, 0.25],
            80:  [0, 0.39],# [0.03, 0.36],
            90:  [0.15, 0.51],# [0.18 , 0.48],
            100: [0.32, 0.64],# [0.35, 0.61],
            110: [0.37, 0.83],# [0.4, 0.8]
            }

            pos_coef = pytaVSL.get('tv1', 'scale')[0] / 0.25 # coefficient de scale par rapport au début de scène
            duration = _rand() * 0.1 + 0.6
            cur_pos = pytaVSL.get('bt', 'position_x')
            cur_angle = round(pytaVSL.get('bt', 'rotate_z')/10)*10
            if cur_angle == 450:
                cur_angle = 90

            pytaVSL.animate('bt', 'position_x', None, cur_pos - 0.01, 0.5, 's', 'elastic-inout')
            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, cur_pos + 0.1 - 0.01, 0.5, 's', 'elastic-inout')
            self.wait(0.1, 's')
            mainSampler.send('/instrument/play', 's:f_ch4_pop', 100), ### AUDIO
            pytaVSL.animate('bt_sent_obj', 'position_y', 0, 8 - cur_pos, duration, 's')
            pytaVSL.set('bt_sent_obj', 'visible', 1),
            pytaVSL.animate('bt_sent_obj', 'scale', None, [0.55, 0.55], duration / 5, 's') # [0.3, 0.3]


            # c = cur_pos
            # for ys in yzones_v[cur_angle]:
            #     if abs(ys - cur_pos) < c:
            #         c = ys

            if cur_angle < 90:
                ys = yzones_v[cur_angle][1]
            elif cur_angle == 90:
                ys = yzones_v[cur_angle][round(_rand())]
            else:
                ys = yzones_v[cur_angle][0]

            future_zone = (yzones_v[future_angle][1] - yzones_v[future_angle][0])/2 + yzones_v[future_angle][0]

            # self.logger.info(
            #     'cur_angle: ' + str(cur_angle) +
            #     ' / ys: ' + str(ys) +
            #     ' / future_angle: ' + str(future_angle) +
            #     ' / future_zone: ' + str(future_zone)
            # )

            self.start_scene('sequence/evitement_balle_' + str(round(future_zone/100)*100), lambda: [
                self.wait(duration / 4, 's'),
                pytaVSL.animate('tv1', 'position_y', None, ys * pos_coef, duration / 2, 's', 'elastic-out'),
                self.wait(2.5 * duration / 4, 's'),
                pytaVSL.animate('tv1', 'position_y', None, (yzones_v[future_angle][1] - yzones_v[future_angle][0]) / 2 + yzones_v[future_angle][0], 0.5 * duration / 4, 's')
            ])

            if to_destroy == 'lights_stageright':
                """
                On défonce les lumières à cour
                """
                self.start_scene('defonce_' + to_destroy, lambda: [
                    self.wait(duration * 2 / 3, 's'),
                    mainSampler.send('/instrument/play', 's:f_ch5_smashed_tractobotte', 100), ### AUDIO
                    pytaVSL.animate('lights_stageright', 'fish', 0, 1, 0.2, 's'),
                    pytaVSL.animate('lights_stageright', 'offset_x', 0, 1, 0.2, 's'),
                    pytaVSL.animate('lights_stageright', 'offset_y', 0, 0.2, 0.2, 's'),
                    self.wait(0.2, 's'),
                    pytaVSL.set('lights_stageright', 'visible', 0),
                    pytaVSL.set('lights_stageright', 'fish', 0),
                ])

            elif to_destroy == 'lights_stageleft':
                """
                On défonce les lumières à jardin
                """
                self.start_scene('defonce_' + to_destroy, lambda: [
                    self.wait(duration * 2 / 3, 's'),
                    mainSampler.send('/instrument/play', 's:f_ch5_smashed_tractobotte', 100),### AUDIO
                    pytaVSL.animate('lights_stageleft', 'fish', 0, 1, 0.2, 's'),
                    pytaVSL.animate('lights_stageleft', 'offset_x', 0, -1, 0.2, 's'),
                    pytaVSL.animate('lights_stageleft', 'offset_y', 0, -0.2, 0.2, 's'),
                    self.wait(0.2, 's'),
                    pytaVSL.set('lights_stageleft', 'visible', 0),
                    pytaVSL.set('lights_stageleft', 'fish', 0),
                ])

            elif to_destroy in ['jack', 'caesar', 'manytubas', 'tri', 'visio']:
                """
                On défonce les panneaux
                """
                self.start_scene('defonce_' + to_destroy, lambda: [
                    self.wait(duration * 2 / 3, 's'),
                    mainSampler.send('/instrument/play', 's:f_ch5_smashed_tractobotte', 100), ### AUDIO
                    pytaVSL.animate('w_signs_' + str(to_destroy), 'fish', 0, .3, 0.5, 's', 'linear-mirror'),
                    pytaVSL.animate('w_signs_' + str(to_destroy), 'offset_y', 0, 0.01, 0.5, 's'),
                    self.wait(0.5, 's'),
                    pytaVSL.animate('w_signs_' + str(to_destroy), 'offset_y', 0, -1, 0.2, 's'),
                    self.wait(0.2, 's'),
                    pytaVSL.set('w_signs_' + str(to_destroy), 'visible', 0),
                    pytaVSL.set('w_signs_' + str(to_destroy), 'fish', 0),
                ])

            self.wait(duration + 0.2, 's')

        def ep_jet_fontaine():
            index = 0
            val = {
                70: [-0.023, -0.055],
                80: [-0.01, -0.03],
                90: [-0, 0],
                100: [0.008, 0.03],
                110: [0.012, 0.066]
            }
            detruits = []
            zones = {
                'lights_stageleft': {

                },
                'jack': {

                },
                'caesar': {
                    110: [-0.41, -0.4]
                },
                'manytubas': {
                    100: [-0.41, -0.36],
                    110: [-0.35, -0.15]
                },
                'tri': {
                    100: [-0.3, -0.27],
                    110: [-0.1, 0]
                },
                'visio': {
                    100: [-0.23, -0.151]
                },
                'lights_stageright': {
                    100: [-0.15, 0]
                },
            }
            to_destroy = ''
            while True:

                cur_pos = pytaVSL.get('bt', 'position_x')
                cur_angle = pytaVSL.get('bt', 'rotate_z')
                cia = 10 * round(cur_angle / 10)
                ia = cia
                while ia == cia:
                    angle = 40 * _rand() + 70
                    ia = 10 * round(angle / 10)
                    angle = ia + (angle - ia) / 5
                # self.logger.info(angle)

                # On avance d'un cran dans les objets
                pytaVSL.set('bt_sent_obj', 'sequence_index', index)

                ep_jet(to_destroy, ia)

                if cur_pos < -0.4: # or index == 2 - 1:
                    o_set = _rand() * 0.1
                    # pytaVSL.animate('bt', 'position_x', None, -0.1 - o_set, 0.2, 's', 'elastic-out')
                    pytaVSL.animate('bt', 'position_x', None, -0.4, 0.2, 's', 'elastic-out')
                    pytaVSL.animate('t_nobh_entovillon', 'position_x', None, -0.3, 0.2, 's', 'elastic-out')

                elif not angle < 90:
                    for zone in zones:
                        if ia in zones[zone]:
                            if cur_pos > zones[zone][ia][0] and cur_pos < zones[zone][ia][1] and zone not in detruits:
                                to_destroy = zone
                                detruits.append(zone)

                else:
                    to_destroy = ''



                #On change l'angle
                pytaVSL.animate('bt', 'rotate_z', None, angle, 0.1, 's', 'elastic-inout')
                pytaVSL.animate('t_nobh_entovillon', 'rotate_z', None, angle, 0.1, 's', 'elastic-inout')
                pytaVSL.animate('t_nobh_entovillon', 'offset_x', 0, val[ia][0], 0.1, 's', 'elastic-inout')
                pytaVSL.animate('t_nobh_entovillon', 'offset_y', 0, val[ia][1], 0.1, 's', 'elastic-inout')


                self.wait(0.2, 's')
                index = index + 1


        pytaVSL.set('t_bh_grappin*', 'visible', 0)

        self.start_scene('sequence/enedys_mitraille', lambda: [
            pytaVSL.allume_ep(on='courrier'),
            mainSampler.allume_ep(on='courrier'), ### AUDIO
            pytaVSL.bascule_ep(),
            mainSampler.bascule_ep(), ### AUDIO
            pytaVSL.face_ep('sifflet'),
            mainSampler.allume_ep(on='alarm'), ### AUDIO
            pytaVSL.allume_ep(on='alarm'),
            pytaVSL.getwhistle_ep(),
        ])

        self.start_scene('sequence/voyante_tronche_main', lambda: [
            self.wait(1, 's'),
            pytaVSL.set('p_voyante3', 'visible', 1),
            pytaVSL.set('p_voyante2', 'visible', 0),
            pytaVSL.animate('tv1', 'scale', None, [0.35, 0.35], 30, 's'),
            self.start_scene('sequence/f_ch5_voyante_tronche', ep_jet_fontaine)
        ])



    @pedalboard_button(5007, who='adrien', after='f_ch5_voyante_plein_la_tronche')
    def f_ch5_voyante_stop_jet(self):
        """
        La voyante arrête de s'en prendre (active par ORL)
        """
        ### LIGHT
        qlcplus.animate('d_je', None, 255, 1, 's'),

        ### AUDIO
        transport.set_tempo(125*1)
        self.start_scene('sequence/t_ch5_arf_pause', lambda: [
            # self.wait_next_cycle(),
            self.stop_scene('sequence/symphonie*'),
            # self.stop_scene('sequence/symphonie_flutes_soufflants__moinsdidj_bottes'),
            # self.stop_scene('sequence/symphonie_flutes_didj_percs_bottes_arf'),
            self.start_sequence('sequence/symphonie_violons_seuls', [
                { # bar 1
                    1: lambda: [
                        mainSampler.stop_symphonies(),
                        mainSampler.send('/instrument/play', 's:symphonie_violons', 127, 1),
                        self.logger.info('s:symphonie_violons  - symphonie_violons_seuls - f_ch5_voyante_stop_jet'),
                        # mainSampler.send('/instrument/stop', 's:symphonie_soufflants_moinsdidj_bottes'),
                        # mainSampler.send('/instrument/stop', 's:symphonie_flutes_soufflants__moinsdidj_bottes'),
                        # mainSampler.send('/instrument/stop', 's:symphonie_flutes_didj_percs_bottes'),
                        # mainSampler.send('/instrument/stop', 's:symphonie_soufflants_bottes'),
                        # self.logger.info('s:symphonie_soufflants__moinsdidj_bottes stopped'),
                        # self.logger.info('s:symphonie_flutes_soufflants__moinsdidj_bottes stopped'),
                        # self.logger.info('s:symphonie_flutes_didj_percs_bottes stopped'),
                        # self.logger.info('s:symphonie_soufflants_bottes stopped'),
                    ]
                },
                {}, {}, {}, {}, {}, {}, {}# bars 2 - 8
            ], loop=True)
        ])


        ### VJ
        self.stop_scene('sequence/f_ch5_voyante_tronche')

        pytaVSL.set('bt_sent_obj', 'visible', 0)
        pytaVSL.set('p_voyante4', 'visible', 1)
        pytaVSL.set('p_voyante3', 'visible', 0)

        self.start_scene('sequence/voyante_focused', lambda: [
            self.wait(1.5, 's'),
            pytaVSL.animate('tv1', 'scale', None, [0.5, 0.5], 0.7, 's', 'elastic-out'),
            pytaVSL.animate('tv1', 'position_y', None, 0.195, 0.7, 's', 'elastic-out'),
            pytaVSL.animate('tv1', 'position_x', None, 0.3, 0.7, 's', 'elastic-out')
        ])

    @pedalboard_button(5008, who='adrien', after='f_ch5_voyante_stop_jet')
    def f_ch5_voyante_boite_a_meuh(self):
        """
        La voyante prend la boîte à meuh dans la tronche (active ORL)
        """

        self.stop_scene('*')

        # self.stop_scene('sequence/*o')
        # self.stop_scene('sequence/tracto_botto_bato')
        # self.stop_scene('sequence/tracto_botto')
        # self.stop_scene('sequence/kicko')
        # self.stop_scene('sequence/botto')

        pytaVSL.set('explos_*', 'visible', 0)
        pytaVSL.set('explos', 'position', -0.37, -0.2, -25)
        pytaVSL.set('explos_smoke1', 'position_x', -0.07)
        pytaVSL.set('explos_smoke1', 'position_y', -0.03)
        pytaVSL.set('explos_smoke1', 'rotate_z', 30)
        pytaVSL.set('explos_smoke2', 'position_x', 0.22)
        pytaVSL.set('explos_smoke2', 'position_y', 0.15)
        pytaVSL.set('explos_smoke2', 'rotate_z', 40)



        def explocow():
            pytaVSL.set('explosion1', 'gif_frame', 0),
            pytaVSL.set('explosion1', 'visible', 1),
            mainSampler.send('/instrument/play', 's:explosion1', 100), ### AUDIO
            ### AUDIO
            self.stop_scene('sequence/symphonie_violons_seuls'),
            mainSampler.stop_scene('sequence/gas_audio'),
            mainSampler.stop_symphonies(),
            # mainSampler.send('/instrument/stop', 's:symphonie_full'),
            self.logger.info('s:symphonie_full'),
            mainSampler.send('/instrument/stop', 's:f_ch5_moteur_tractobotte'),



            self.start_scene('sequence/stop_explocow', lambda: [
                pytaVSL.animate('explos_vache', 'scale', [0, 0], [0.7, 0.7], 0.3, 's'),
                pytaVSL.animate('explos_vache', 'alpha', 0, 1, 47/25, 's', 'linear-mirror'),
                pytaVSL.set('explos_vache', 'visible', 1),
                self.wait(47 / 25 - 0.1, 's'),
                pytaVSL.animate('explos_vache', 'alpha', None, 0, 0.1, 's'),
                pytaVSL.animate('explosion1', 'alpha', None, 0, 0.1, 's'),
                self.wait(0.1, 's'),
                pytaVSL.set('explos_vache', 'visible', 0),
                pytaVSL.set('explosion1', 'visible', 0)
            ])

        def meuh():
            def moo():
                t = 1.5
                while True:
                    mainSampler.send('/instrument/play', 's:boite_a_meuh', 1 / t *  100)
                    self.wait(t, 's')
                    t = t - _rand() / 2
            self.start_scene('sequence/meuhmeuh', moo)


        self.start_scene('sequence/tracto_saut_final' , lambda: [
            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, -1, 1, 's','elastic-inout'),
            pytaVSL.stop_scene('sequence/tracto_ecrase_vite'),
            self.wait(1, 's'),
            pytaVSL.set('tracto-bottus', 'rotate_z', 45),
            pytaVSL.set('f_ch5-tracto_b', 'position_y', 0.2),
            pytaVSL.set('f_ch5-tracto_b', 'scale', 0.65, 0.65),
            self.wait(2, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'offset_y', 0, 0.1, 1, 's', 'elastic-mirror'),
            self.wait(0.3, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, 0.4, 0.4, 's', 'linear-mirror'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, -0.22, 0.4, 's'),
            self.wait(0.5, 's'),
            mainSampler.send('/instrument/play', 's:f_ch5_sautfinal_tractobotte', 100), ### AUDIO
            ### AUDIO
            transport.set_tempo(125),
            self.stop_scene('sequence/symphonie_violons_seuls'),
            mainSampler.stop_symphonies(),
            # mainSampler.send('/instrument/stop', 's:symphonie_violons'),
            self.logger.info('s:symphonie_violons stopped'),
            mainSampler.send('/instrument/play', 's:symphonie_full', 127),
            self.logger.info('s:symphonie_full'),

            pytaVSL.animate('ciel_etoile_melies', 'position_y', 0.1, 0, 0.5, 's', 'elastic-out'),
            pytaVSL.animate('ciel_etoile_melies', 'texture_offset', None, [0, 0], 0.5, 's', 'elastic-out'),
            pytaVSL.animate('w_signs_*', 'position_y', None, -1, 0.5, 's', 'elastic-inout'),
            pytaVSL.animate('trijc', 'position_y', None, -1, 0.5, 's', 'elastic-inout'),
            pytaVSL.animate('lights_stageright', 'position_y', None, -1, 0.5, 's', 'elastic-inout'),
            pytaVSL.animate('lights_stageleft', 'position_y', None, -1, 0.5, 's', 'elastic-inout'),

            pytaVSL.animate('bt', 'rotate_z', None, 150, 0.5, 's', 'elastic-out'),
            pytaVSL.animate('bt', 'position_x', None, -0.42, 0.5, 's', 'elastic-out'),
            pytaVSL.animate('bt', 'position_y', None, -0.35, 0.5, 's', 'elastic-out'),
            pytaVSL.animate('t_nobh_entovillon', 'rotate_z', None, 150, 0.5, 's', 'elastic-out'),
            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, -0.33, 0.5, 's', 'elastic-out'),
            pytaVSL.animate('t_nobh_entovillon', 'position_y', None, -0.238, 0.5, 's', 'elastic-out'),
            pytaVSL.animate('t_nobh_entovillon', 'offset_x', None, 0, 0.5, 's', 'elastic-out'),
            pytaVSL.animate('t_nobh_entovillon', 'offset_y', None, 0, 0.5, 's', 'elastic-out'),

            self.wait(0.5, 's'),
            pytaVSL.animate('t_nobh_entovillon', 'offset_y', None, 0.01, 0.5, 's', 'elastic-mirror', loop=True),
            pytaVSL.animate('*ep_*', 'color', None, [1, 0.5, 0.5], 10, 's'), # , 'exponential-out')
            pytaVSL.animate('*ep_*', 'rgbwave', None, 0.4, 10, 's'), # , 'exponential-out')

            pytaVSL.animate('bt_main', 'color', None, [1, 0.1, 0.1], 5, 's'), # , 'exponential-out')
            pytaVSL.animate('bt_main', 'rgbwave', None, 0.4, 5, 's'), # , 'exponential-out')
            pytaVSL.animate('bt_main', 'fish', 0, 0.3, 5, 's'),

            self.wait(5, 's'),

            # Levée de pelle
            pytaVSL.animate('tracto-bottus', 'position_x', None, 0.34, 3, 's'),
            pytaVSL.animate('tracto-bottus', 'position_y', None, 0.3, 3, 's'),
            pytaVSL.animate('tracto-bottus', 'scale', None, [1, 1], 3, 's'),

            pytaVSL.animate('tracto-pellus', 'rotate_z', None, 45, 3, 's'),
            pytaVSL.animate('tracto-pellus', 'scale', None, [0.7, 0.7], 3, 's'),


            # gonflage d'entovillon
            mainSampler.send('/instrument/play', 's:f_ch5_bouilloire_stop', 100), ### AUDIO
            meuh(),
            pytaVSL.animate('explos_smoke*', 'alpha', 0, 1, 3, 's'),
            pytaVSL.animate('explos_smoke*', 'gif_speed', None, 3, 6, 's'),
            pytaVSL.set('explos_smoke*', 'visible', 1),
            pytaVSL.animate('bt', 'scale', None, [0.3, -0.3], 3, 's'),
            pytaVSL.animate('t_nobh_entovillon', 'scale', None, [0.4, -0.4], 3, 's'),
            pytaVSL.animate('bt', 'position_x', None, -0.445, 3, 's'),
            pytaVSL.animate('bt', 'position_y', None, -0.345, 3, 's'),
            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, -0.31, 3, 's'),
            pytaVSL.animate('t_nobh_entovillon', 'position_y', None, -0.2, 3, 's'),
            pytaVSL.animate('ep_halo', 'scale', None, [1.5, 1.5], 3, 's'),
            pytaVSL.animate('ep_halo', 'mask_threshold', None, 0, 3, 's'),
            pytaVSL.animate('bt_main', 'color', None, [0.5, 0.5, 0.5], 0.5, 's', 'elastic-out'), # , 'exponential-out')
            pytaVSL.animate('bt_main', 'rgbwave', None, 0, 0.5, 's', 'elastic-out'), # , 'exponential-out')
            pytaVSL.animate('bt_main', 'fish', None, 0, 0.5, 's', 'elastic-out'),


            pytaVSL.animate('t_nobh_entovillon', 'fish', None, 0.3, 3, 's'), # , 'exponential-out')
            pytaVSL.animate('t_nobh_entovillon', 'color', None, [1, 0.5, 0.5], 3, 's'), # , 'exponential-out')
            pytaVSL.animate('t_nobh_entovillon', 'rgbwave', None, 0.4, 3, 's'), # , 'exponential-out')
            self.wait(2.5, 's'),
            pytaVSL.animate('explos_smoke*', 'gif_speed', None, 8, 0.5, 's'),


            # tremblement pelle et entovillon
            pytaVSL.animate('t_nobh_entovillon', 'offset_x', 0, 0.04, 0.5, 's', 'random-mirror'), # , 'exponential-out')
            pytaVSL.animate('tracto-bottus', 'offset_x', 0, 0.02, 0.5, 's', 'random'),
            pytaVSL.animate('tracto-bottus', 'offset_y', 0, 0.02, 0.5, 's', 'random'),
            ###### METTRE LE SON DE VACHE ICI
            self.wait(0.5, 's'),

            self.stop_scene('sequence/meuhmeuh'),
            mainSampler.send('/instrument/play', 's:f_ch4_pop', 100), ### AUDIO
            mainSampler.send('/instrument/play', 's:f_ch5_meuglement', 100), ### AUDIO
            mainSampler.stop_scene('sequence/gas_audio'),
            mainSampler.allume_ep(on='False'),
            # tir + décollage
            ### Explosion
            explocow(),
            pytaVSL.set('explos_smoke*', 'visible', 0),


            pytaVSL.animate('bt', 'scale', None, [0.2, -0.2], 0.3, 's', 'elastic-out'),
            pytaVSL.animate('t_nobh_entovillon', 'scale', None, [0.2, -0.3], 0.3, 's', 'elastic-out'),
            pytaVSL.animate('bt', 'position_x', None, -0.42, 0.3, 's', 'elastic-out'),
            pytaVSL.animate('bt', 'position_y', None, -0.35, 0.3, 's', 'elastic-out'),
            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, -0.33, 0.3, 's', 'elastic-out'),
            pytaVSL.animate('t_nobh_entovillon', 'position_y', None, -0.238, 0.3, 's', 'elastic-out'),
            pytaVSL.animate('ep_halo', 'scale', None, [1, 1], 0.3, 's', 'elastic-out'),
            pytaVSL.animate('t_nobh_entovillon', 'fish', None, 0, 0.3, 's', 'elastic-out'), # , 'exponential-out')
            pytaVSL.animate('t_nobh_entovillon', 'color', None, [0.5, 0.5, 0.5], 2, 's', 'elastic-out'), # , 'exponential-out')
            pytaVSL.animate('t_nobh_entovillon', 'rgbwave', None, 0, 2, 's', 'elastic-out'), # , 'exponential-out')


            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, -0.28, 0.3, 's'),
            pytaVSL.animate('t_nobh_entovillon', 'position_y', None, -0.1, 0.3, 's'),
            pytaVSL.animate('f_ch5-bs_boiteameuh', 'position_x', None, -0.346, 0.3, 's'),
            pytaVSL.animate('f_ch5-bs_boiteameuh', 'position_y', None, -0.252, 0.3, 's'),
            pytaVSL.set('f_ch5-bs_boiteameuh', 'visible', 1),

            self.wait(0.2, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, -0.2, 0.1, 's'),
            pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, 0.25, 0.1, 's'),

            self.wait(0.05, 's'),
            pytaVSL.animate('ciel_etoile_melies', 'texture_offset', None, [0, -1], 0.7, 's', loop=True),
            pytaVSL.animate('bt', 'position_y', None, -1.5, 0.1, 's'),
            pytaVSL.animate('tv1', 'position_y', None, -1, 0.1, 's'),
            mainSampler.flying_wings('off'), ### AUDIO

            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, 0.08, 12, 's'), # 0.4
            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, 0.07, 12, 's'), # 0.32
            pytaVSL.animate('f_ch5-bs_boiteameuh', 'position_x', None, 0.07, 12, 's'), # 0.32
            pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, 0.33, 12, 's'),
            pytaVSL.animate('t_nobh_entovillon', 'position_y', None, 0.29, 12, 's'), #-0.02
            pytaVSL.animate('f_ch5-bs_boiteameuh', 'position_y', None, 0.0295, 12, 's'), # 0.32

            pytaVSL.animate('f_ch5-tracto_b', 'scale', None, [0.05, 0.05], 12, 's'),
            pytaVSL.animate('t_nobh_entovillon', 'scale', None, [0.05 * 0.3 / 0.8, -0.05 * 0.3 / 0.8], 12, 's'),
            pytaVSL.animate('f_ch5-bs_boiteameuh', 'scale', None, [0.05 * 0.08 / 0.8, 0.05 * 0.08 / 0.8], 12, 's'),

            self.wait(5, 's'),
            pytaVSL.flying_wings('wingsp1', on=False),
            pytaVSL.stop_animate('plane_horn_1', 'position_x'),
            pytaVSL.stop_animate('plane_horn_1', 'position_y'),
            melies_2_lune()
            # send_osc 4000 /pyta/slide/tv1/set scale 0.5 0.5 && send_osc 4000 /pyta/slide/tracto-bottus/set scale 0.6 0.6 &&send_osc 4000 /pyta/slide/tv1/animate position_y 0.195 0.19 1 && send_osc 4000 /pyta/slide/tracto-pellus/animate rotate_z 10 30 1 && send_osc 4000 /pyta/slide/tracto-bottus/animate position_y 0 0.075 1 && send_osc 4000 /pyta/slide/tracto-bottus/animate position_x 0.263 0.25 1 && sleep 1 &&  send_osc 4000 /pyta/slide/tracto-bottus/animate scale 0.6 0.6 1 1 0.6 elastic && send_osc 4000 /pyta/slide/tv1/animate scale 0.5 0.5 0.4 0.4 1 elastic



        ])

        def melies_2_lune():
            while pytaVSL.get('ciel_etoile_melies', 'texture_offset')[1] > 0.1:
                self.wait(0.01, 's')
            # pytaVSL.animate('ciel_etoile_melies', 'texture_offset', None, [0, -1], 1, 's'),
            # self.wait(1, 's'),
            # pytaVSL.animate('ciel_etoile_melies', 'texture_offset', None, [0, -1], 2, 's'),
            # self.wait(2, 's')
            # pytaVSL.animate('ciel_etoile_melies', 'texture_offset', None, [0, -1], 5, 's', loop=True),

            # retrig pour impact quand position_y = 0.2 pour lune
            pytaVSL.animate('f_ch5-tracto_b', 'position_x', None, 0.08, 4, 's'), # 0.4
            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, 0.07, 4, 's'), # 0.32
            pytaVSL.animate('f_ch5-bs_boiteameuh', 'position_x', None, 0.07, 4, 's'), # 0.32
            pytaVSL.animate('f_ch5-tracto_b', 'position_y', None, 0.33, 4, 's'),
            pytaVSL.animate('t_nobh_entovillon', 'position_y', None, 0.29, 4, 's'), #-0.02
            pytaVSL.animate('f_ch5-bs_boiteameuh', 'position_y', None, 0.0295, 4, 's'), # 0.32
            pytaVSL.animate('f_ch5-tracto_b', 'scale', None, [0.05, 0.05], 4, 's'),
            pytaVSL.animate('t_nobh_entovillon', 'scale', None, [0.05 * 0.3 / 0.8, -0.05 * 0.3 / 0.8], 4, 's'),
            pytaVSL.animate('f_ch5-bs_boiteameuh', 'scale', None, [0.05 * 0.08 / 0.8, 0.05 * 0.08 / 0.8], 4, 's'),


            pytaVSL.animate('lune', 'position_y', None, 0, 5, 's'),
            pytaVSL.animate('lune', 'alpha', None, 1, 4, 's'),
            pytaVSL.set('lune', 'visible', 1),
            self.wait(4, 's'),
            mainSampler.send('/instrument/play', 's:f_ch5_explosion_lune', 100), ### AUDIO
            pytaVSL.set('f_ch5-tracto_b', 'visible', 0)
            pytaVSL.set('t_nobh_entovillon', 'visible', 0)
            pytaVSL.stop_animate('ciel_etoile_melies', 'texture_offset'),
            pytaVSL.set('ciel_etoile_melies', 'texture_offset', 0, 0),
            self.wait(2, 's'),
            mainSampler.send('/instrument/play', 's:symphonie_pianos', 127),

            self.wait(pytaVSL.get('lune', 'video_end') - 4 - 30 - 2, 's') ##### TODO pb de timing, c'est trop long
            # pytaVSL.set('lune_waiting', 'visible', 1)
            # pytaVSL.set('lune', 'visible', 0)
            self.back_from_the_moon()

    @pedalboard_button(116)
    def button_back_from_the_moon(self):
        self.stop_scene('sequence/*')
        self.back_from_the_moon()

    @pedalboard_button()
    def back_from_the_moon(self):
        delay = 0
        t = 2
        self.start_scene('sequence/back_from_the_moon', lambda: [
            # la boîte retombe
            pytaVSL.animate('lune', 'position_y', None, 1, t, 's'),
            pytaVSL.animate('lune', 'alpha', None, 0, t, 's'),
            pytaVSL.animate('ciel_etoile_melies', 'texture_offset', None, [0, 1], 0.7, 's', loop=True),
            pytaVSL.animate('f_ch5-bs_boiteameuh', 'position_x', None, 0, t, 's'), # 0.32
            pytaVSL.animate('f_ch5-bs_boiteameuh', 'position_y', None, 0, t, 's'), #-0.02
            pytaVSL.animate('f_ch5-bs_boiteameuh', 'scale', None, [0.4, 0.4], 2 * t / 3, 's'),

            pytaVSL.animate('f_ch5-bs_boiteameuh', 'rotate_z', None, 1080, t, 's'),
            self.wait(2 * t / 3, 's'),
            pytaVSL.animate('f_ch5-bs_boiteameuh', 'scale', None, [1, 1], t /3, 's'),

            # la boîte casse l'écran
            self.wait(t - 2 * t / 3, 's'),
            pytaVSL.set('crashfinal1', 'visible', 1),
            mainSampler.send('/instrument/stop', 's:symphonie_pianos'),
            mainSampler.send('/instrument/play', 's:f_ch5_crashfinal', 100), ### AUDIO

            pytaVSL.animate('f_ch5-bs_boiteameuh', 'alpha', None, 0, 0.2, 's'),
            self.wait(pytaVSL.get('crashfinal1', 'video_end') - 0.1, 's'),
            pytaVSL.set('crashfinal1_waiting', 'visible', 1),
            pytaVSL.set('crashfinal1', 'visible', 0),
            pytaVSL.set('ciel_etoile_melies', 'visible', 0),

            ### Light
            self.wait(1, 's'),
            qlcplus.set_scene('full_stage', duration=5),

            ### AUDIO
            mainSampler.send('/instrument/play', 's:f_ch5_telephone', 100), ### AUDIO
        ])

    @pedalboard_button(5009, who='adrien', after='back_from_the_moon')
    def casse_casse(self):
        pytaVSL.set('crashfinal2', 'visible', 1)
        mainSampler.send('/instrument/play', 's:f_ch5_crashfinal2', 100), ### AUDIO

    @pedalboard_button(5010, who='adrien', after='casse_casse')
    def miraye_call(self):

        pytaVSL.set('m_iraye', 'visible', 0)
        pytaVSL.set('m_iraye', 'rotate_z', 0)
        pytaVSL.set('m_iraye', 'position_x', 1)
        pytaVSL.set('m_iraye', 'position_y', 1)
        # pytaVSL.set('m_iraye', 'visible', 1)
        # pytaVSL.set('m_ch5-call', 'visible', 1)
        mainSampler.send('/instrument/stop', 's:f_ch5_telephone'), ### AUDIO
        self.start_scene('sequence/miraye_telephone', lambda: [
            self.wait(2, 's'),
            mainSampler.send('/instrument/play', 's:f_ch5_miraye_telephone', 100)
        ])

    @pedalboard_button(5011, who='adrien', after='miraye_call')
    def miraye_call_ending(self):
        qlcplus.set_scene('miraye')
        qlcplus.set_strobe('bt_doah', on=False), ### LIGHT
        qlcplus.set_strobe('bt_je', on=False), ### LIGHT
        qlcplus.set_strobe('bt_sal', on=False), ### LIGHT        
        qlcplus.set_strobe('bt_centrale', on=False), ### LIGHT        

    @pedalboard_button(5012, who='orl', after='miraye_call_ending')
    def terminus(self):
        def finito():
            self.fin = _time()
            self.logger.info('Fin @ ' + str(self.fin) + ' secondes')
            duree = self.fin - self.debut
            self.logger.info('Durée : ' + str(round(duree/60)) + ' minutes')
        finito()
