from ..base import *
from .video import Video
from .light import Light
from random import random as _rand
from time import time as _time

from modules import *

class Chapitre4(Video, Light, RouteBase):


    def activate(self):
        """
        Called when the engine switches to this route.
        """

        super().activate()

        transport.set_tempo(60)
        transport.set_cycle('4/4', pattern="Xxxx")

        # Setups, banks...
        # prodSampler.set_kit(self.name)


        self.f_ch4_22_waiting_lock = 1


        self.start_scene('init_chapitre4', lambda: [
            # Overlay
            self.init_chapitre4(),

            # Chapitre 3
            self.m_ch4_1()
        ])

    def init_chapitre4(self):
        self.debut = _time()
        self.logger.info('Début @ ' + str(self.debut) + ' secondes')


        pytaVSL.send('/pyta/slide/*ch3*/remove')
        pytaVSL.send('/pyta/slide/*ch3*/unload')

        chapter = 'ch4'

        ### Création des groupes du chapitre
        # pytaVSL.create_group('tv1', ['plane_horn_1','p_' + chapter + '-3']) # Soft & Rains dans le journal

        pytaVSL.create_group('m_iraye', ['m_layout', 'm_' + chapter + '*'])
        pytaVSL.create_group('f_arabesques', ['f_ara*e_1', 'f_ara*e_2'])
        pytaVSL.create_group('f_arabesques_2', ['f_ara*e_3', 'f_ara*e_4'])
        pytaVSL.create_group('f_arabesques_3', ['f_ara*e_5', 'f_ara*e_6'])
        pytaVSL.create_group('f_ilm', ['f_arabesques', 'f_' + chapter + '*'])

        pytaVSL.create_group('doah_b', ['doah_b*'])
        pytaVSL.create_group('saladin_b', ['saladin_b*'])

        pytaVSL.sync()

        pytaVSL.position_overlay('Chapitre4')

    @pedalboard_button(100)
    def m_ch4_1(self):
        """
        Intro Miraye
        """
        ### LIGHT
        qlcplus.set_scene('miraye')

        if pytaVSL.get('trijc', 'position_x') == 0:
            pytaVSL.trijc_io('in', 'lustre', 0.1, 'elastic-inout'),
            mainSampler.trijc_io('in', 0.1, 'elastic'), ### AUDIO


        ### VJ
        self.start_scene('sequence/m_ch4_1', lambda: [
            ### Lancement du Film
            self.wait(0.1, 's'),
            pytaVSL.trijc_change_tool('tuba'),
            self.wait(0.3, 's'),
            pytaVSL.set('sub_t_trijc_lustre_allume', 'alpha', 1),
            pytaVSL.signs_io('in', duration=0.5, easing='elastic-out'),
            pytaVSL.animate('lights*', 'alpha', None, 1, 0.6),
            pytaVSL.animate('bt_main', 'brightness', None, 1, 0.6),
            pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.6),
            pytaVSL.miraye_in('m_ch4-1', 0.7),
            mainSampler.miraye_in(), ### AUDIO
            self.wait(pytaVSL.get('m_ch4-1', 'video_end') - 2, 's'),
            pytaVSL.trijc_change_tool('aspi'),
            self.wait(0.2, 's'),
            self.f_ch4_2()
        ])

    @pedalboard_button(101)
    def button_f_ch4_2(self):
        """
        Enedys fait les cent pas en bas de la tour
        """
        self.stop_scene('sequence/*')
        self.f_ch4_2()

    def f_ch4_2(self):
        """
        Enedys fait les cent pas en bas de la tour
        """
        ### LIGHT
        qlcplus.set_scene('film')

        ### AUDIO
        t_offset = 0.8 + 0.3
        t_tvsound1 = 3.7 + t_offset
        t_tvsound1_end = 3.4 + t_offset
        t_tvsound2 = 15 + t_offset
        self.start_scene('sequence/f_ch4-2_audio', lambda: [
            self.wait(t_tvsound1, 's'),
            mainSampler.send('/instrument/play', 's:paillasson_buzz', 100),
            self.wait(t_tvsound1_end - t_tvsound1, 's'),
            mainSampler.send('/instrument/stop', 's:paillasson_buzz'),
            self.wait(t_tvsound2 - t_tvsound1_end, 's'),
            mainSampler.send('/instrument/play', 's:paillasson_buzz', 100),
        ])

        ### VJ
        self.start_scene('sequence/f_ch4_2', lambda: [
            pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, 0, 0.2, 's', 'elastic-inout'),
            pytaVSL.aspi_slide('m_layout', [0, -0.45], [0, 0.52], 0.4),
            pytaVSL.aspi_slide('m_ch4-1', [-0.02, -0.445], [-0.02, 0.53], 0.4),
            mainSampler.miraye_out(), ### AUDIO
            mainSampler.aspi_slide(), ### AUDIO
            self.wait(0.8, 's'),
            pytaVSL.set('m_ch4-1', 'visible', 0),
            pytaVSL.trijc_change_tool('tuba'),
            self.wait(0.3, 's'),
            pytaVSL.movie_in('f_ch4-2', 0.8),
            mainSampler.movie_in(), ### AUDIO
            self.wait(pytaVSL.get('f_ch4-2', 'video_end') - 3 , 's'),
            pytaVSL.trijc_io('in', 'lustre', 0.8),
            mainSampler.trijc_io('in', 0.1, 'elastic'), ### AUDIO
            self.wait(3, 's'),
            pytaVSL.trijc_turn_lights('off', 1),
            mainSampler.trijc_turn_lights(1), ### AUDIO
            pytaVSL.animate('f_*', 'alpha', None, 0, 1),
            self.wait(1.2, 's'),
            pytaVSL.set('f_ilm', 'visible', 0),
            pytaVSL.set('f_ch4-2', 'visible', 0),
            pytaVSL.set('f_*', 'alpha', 1),
            self.m_ch4_3()
        ])

    @pedalboard_button(102)
    def button_m_ch4_3(self):
        """
        Miraye reprend la narration + titres gloriole
        """
        self.stop_scene('sequence/*')
        self.m_ch4_3()

    def m_ch4_3(self):
        """
        Miraye reprend la narration + titres gloriole
        """
        ### LIGHT
        qlcplus.set_scene('miraye')

        ### VJ
        self.start_scene('sequence/m_ch4_3', lambda: [
            pytaVSL.trijc_change_tool('tuba'),
            self.wait(0.1, 's'),
            pytaVSL.miraye_in('m_ch4-3', 0.6),
            mainSampler.miraye_in(), ### AUDIO
            pytaVSL.signs_io('in', duration = 0.6),
            pytaVSL.animate('lights*', 'alpha', None, 1, 0.6),
            pytaVSL.animate('bt_main', 'brightness', None, 1, 0.6),
            pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.6),
            self.wait(22.5, 's'), # 25.2 avec titre Gloriole et arrêt avant "Livre"
            pytaVSL.trijc_change_tool('aspi'),
            self.wait(0.2, 's'),
            # self.wait(0.3, 's'),
            # pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -40, 0.2, 's', 'elastic-inout'),
            # mainSampler.trijc_compas(0.2), ### AUDIO
            # pytaVSL.animate('m_iraye', 'scale', None, [0.5, 0.5], 0.2, 's', 'exponential-inout'),
            # pytaVSL.animate('m_iraye', 'position_x', None, 0.1, 0.2, 's', 'elastic-inout'),
            # self.wait(0.2, 's'),
            # pytaVSL.animate('t_trijc_compas', 'rotate_z', None, 0, 1, 's', 'elastic-inout'),
            # pytaVSL.animate('m_iraye', 'position_x', None, 0.3, 12.5, 's', 'exponential-inout'), # avant : 20 secondes
            # pytaVSL.animate('m_iraye', 'position_y', None, 0.2, 12.5, 's', 'exponential-inout'), # avant : 20 secondes
            # pytaVSL.animate('m_iraye', 'scale', None, [0.4, 0.4], 12.5, 's', 'exponential-inout'), # avant : 20 secondes
            # self.wait(pytaVSL.get('m_ch4-3', 'video_end') - 37.5, 's'),
            # self.wait(pytaVSL.get('m_ch4-3', 'video_end') - 30, 's'),
            pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, 0, 0.2, 's', 'elastic-inout'),
            pytaVSL.aspi_slide('m_layout', [0, -0.45], [0, 0.52], 0.4),
            pytaVSL.aspi_slide('m_ch4-3', [-0.02, -0.445], [-0.02, 0.53], 0.4),
            mainSampler.miraye_out(), ### AUDIO
            mainSampler.aspi_slide(), ### AUDIO
            self.wait(0.8, 's'),
            pytaVSL.set('m_ch4-3', 'visible', 0),
            pytaVSL.trijc_change_tool('tuba'),
            # pytaVSL.animate('m_iraye', 'position_x', None, -1, 0.7, 's', 'elastic-inout'),
            # self.wait(0.7, 's'),
            # mainSampler.miraye_out(), ### AUDIO
            # pytaVSL.trijc_change_tool('tuba'),

            self.f_ch4_4()
        ])

    @pedalboard_button(103)
    def button_f_ch4_4(self):
        """
        Gloriole Yüla partie 1 -> fin apprentissage Assistant
        """
        self.stop_scene('sequence/*')
        self.f_ch4_4()

    def f_ch4_4(self):
        """
        Gloriole Yüla partie 1 -> fin apprentissage Assistant
        """
        ### LIGHT
        qlcplus.set_scene('film', duration=5)

        ### VJ
        in_prop = 5/6
        self.start_scene('sequence/f_ch4_4', lambda: [
            pytaVSL.movie_in('f_ch4-4', 0.8, init=True),
            mainSampler.movie_in(), ### AUDIO
            # self.wait(4, 's'),
            # pytaVSL.animate('m_iraye', 'position_x', None, -1, 0.7, 's', 'elastic-inout'),
            # self.wait(0.7, 's'),
            # mainSampler.miraye_out(), ### AUDIO
            pytaVSL.set('m_ch4-3', 'visible', 0),
            self.wait(pytaVSL.get('f_ch4-4', 'video_end') - 4.7, 's'),
            self.f_ch4_6()
######### DEPRECATED
            # self.wait(pytaVSL.get('f_ch4-4', 'video_end') - 4 - 0.7 - in_prop * pytaVSL.get('m_ch4-5', 'video_end'), 's'),
            # self.m_ch4_5()
        ])

################### DEPRECATED
    # @pedalboard_button(104)
    # def button_m_ch4_5(self):
    #     """
    #     Miraye annonce Gloriole de Dalida
    #     """
    #     self.stop_scene('sequence/*')
    #     self.m_ch4_5()
    #
    # def m_ch4_5(self):
    #     """
    #     Miraye annonce Gloriole de Dalida
    #     """
    #     ###### TODO : est-ce qu'on stoppe sur Yüla ?
    #     in_prop = 5/6
    #     mn = 5
    #     d_over_miraye = in_prop * pytaVSL.get('m_ch4-' + str(mn), 'video_end') - 0.5 - 0.7 - 0.2 - 0.2 - 1 - 0.2
    #     self.start_scene('sequence/m_ch4_' + str(mn), lambda: [
    #         pytaVSL.trijc_io('in', 'aimant', 0.2, 'elastic-inout'),
    #         pytaVSL.set('m_ch4-' + str(mn), 'visible', 1),
    #         pytaVSL.set('m_iraye', 'scale', 0.4, 0.4),
    #         pytaVSL.set('m_iraye', 'position_y', 0.17),
    #         pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 20, 0.4, 's'),
    #         self.wait(0.5, 's'),
    #         pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -25, 0.7, 's', 'elastic-inout'),
    #         pytaVSL.animate('m_iraye', 'position_x', None, -0.3, 0.7, 's', 'elastic-inout'),
    #         self.wait(0.7, 's'),
    #         pytaVSL.trijc_change_tool('compas'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -90, 0.1, 's'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -30, d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'scale', None, [0.75, 0.75], d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'position_x', None, 0.12, d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'position_y', None, 0.05, d_over_miraye - 1 - 0.2, 's'),
    #         self.wait(d_over_miraye, 's'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, 10, 1, 's', 'elastic-inout'),
    #         self.wait(1, 's'),
    #         self.f_ch4_6()
    #     ])

    @pedalboard_button(105)
    def button_f_ch4_6(self):
        self.stop_scene('sequence/*')
        self.f_ch4_6()

    def f_ch4_6(self):
        """
        Le pâle éclat de Dalida
        """
        past_out_prop = 1/6
        fn = 6
        future_in_prop = 5/6
        inherit_wait = past_out_prop * pytaVSL.get('m_ch4-' + str(fn -1), 'video_end') - 0.2 - 1
        switch_duration = 0.5
        self.start_scene('sequence/f_ch4_' + str(fn), lambda: [
            pytaVSL.f_noisy_switch_video('f_ch4-' + str(fn - 2), 'f_ch4-' + str(fn), switch_duration),
            mainSampler.noisy_switch(switch_duration), ### AUDIO
            self.wait(pytaVSL.get('f_ch4-6', 'video_end'), 's'),
            self.f_ch4_8()
##### DEPRECATED
            # self.wait(inherit_wait, 's'),
            # pytaVSL.trijc_change_tool('aimant'),
            # self.wait(0.2, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -25, 0.1, 's', 'elastic-inout'),
            # self.wait(0.1, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 30, 0.7, 's', 'elastic-inout'),
            # pytaVSL.animate('m_iraye', 'position_x', None, -1, 0.7, 's', 'elastic-inout'),
            # self.wait(0.7, 's'),
            # pytaVSL.set('m_ch4-' + str(fn - 1), 'visible', 0),
            # self.wait(0.15, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 0, 0.5, 's', 'elastic-inout'),
            # pytaVSL.animate('f_ilm', 'scale', None, [0.95, 0.95], 0.3, 's'),
            # pytaVSL.animate('f_ilm', 'position_x', None, 0, 0.3, 's'),
            # pytaVSL.animate('f_ilm', 'position_y', None, 0, 0.3, 's'),
            # self.wait(0.5, 's'),
            #
            # pytaVSL.trijc_io('out', 'aimant'),
            # self.wait(pytaVSL.get('f_ch4-' + str(fn), 'video_end') - inherit_wait - 0.2 - 0.1 - 0.7 - 0.15 - 0.5 - future_in_prop * pytaVSL.get('m_ch4-' + str(fn + 1), 'video_end'), 's'),
            # self.m_ch4_7()

        ])

######## DEPRECATED
    # @pedalboard_button(106)
    # def button_m_ch4_7(self):
    #     """
    #     Miraye ré-annonce gloriole Yüla
    #     """
    #     self.stop_scene('sequence/*')
    #     self.m_ch4_7()
    #
    # def m_ch4_7(self):
    #     """
    #     Miraye réannonce gloriole Yüla
    #     """
    #     ###### TODO : est-ce qu'on stoppe sur Yüla ?
    #     in_prop = 5/6
    #     mn = 7
    #     d_over_miraye = in_prop * pytaVSL.get('m_ch4-' + str(mn), 'video_end') - 0.5 - 0.7 - 0.2 - 0.2 - 1 - 0.2
    #     self.start_scene('sequence/m_ch4_' + str(mn), lambda: [
    #         pytaVSL.trijc_io('in', 'aimant', 0.2, 'elastic-inout'),
    #         pytaVSL.set('m_ch4-' + str(mn), 'visible', 1),
    #         pytaVSL.set('m_iraye', 'scale', 0.4, 0.4),
    #         pytaVSL.set('m_iraye', 'position_y', 0.17),
    #         pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 20, 0.4, 's'),
    #         self.wait(0.5, 's'),
    #         pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -25, 0.7, 's', 'elastic-inout'),
    #         pytaVSL.animate('m_iraye', 'position_x', None, -0.3, 0.7, 's', 'elastic-inout'),
    #         self.wait(0.7, 's'),
    #         pytaVSL.trijc_change_tool('compas'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -90, 0.1, 's'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -30, d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'scale', None, [0.75, 0.75], d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'position_x', None, 0.12, d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'position_y', None, 0.05, d_over_miraye - 1 - 0.2, 's'),
    #         self.wait(d_over_miraye, 's'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, 10, 1, 's', 'elastic-inout'),
    #         self.wait(1, 's'),
    #         self.f_ch4_8()
    #     ])

    @pedalboard_button(107)
    def button_f_ch4_8(self):
        self.stop_scene('sequence/*')
        self.f_ch4_8()

    def f_ch4_8(self):
        """
        Gloriole de Yüla 2
        """
        past_out_prop = 1/6
        fn = 8
        future_in_prop = 5/6
        inherit_wait = past_out_prop * pytaVSL.get('m_ch4-' + str(fn -1), 'video_end') - 0.2 - 1
        switch_duration = 0.5
        self.start_scene('sequence/f_ch4_' + str(fn), lambda: [
            pytaVSL.f_noisy_switch_video('f_ch4-' + str(fn - 2), 'f_ch4-' + str(fn), switch_duration),
            mainSampler.noisy_switch(switch_duration), ### AUDIO
            self.wait(pytaVSL.get('f_ch4-8', 'video_end'), 's'),
            self.f_ch4_10()

####### DEPRECATED
            # self.wait(inherit_wait, 's'),
            # pytaVSL.trijc_change_tool('aimant'),
            # self.wait(0.2, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -25, 0.1, 's', 'elastic-inout'),
            # self.wait(0.1, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 30, 0.7, 's', 'elastic-inout'),
            # pytaVSL.animate('m_iraye', 'position_x', None, -1, 0.7, 's', 'elastic-inout'),
            # self.wait(0.7, 's'),
            # pytaVSL.set('m_ch4-' + str(fn - 1), 'visible', 0),
            # self.wait(0.15, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 0, 0.5, 's', 'elastic-inout'),
            # pytaVSL.animate('f_ilm', 'scale', None, [0.95, 0.95], 0.3, 's'),
            # pytaVSL.animate('f_ilm', 'position_x', None, 0, 0.3, 's'),
            # pytaVSL.animate('f_ilm', 'position_y', None, 0, 0.3, 's'),
            # self.wait(0.5, 's'),
            #
            # pytaVSL.trijc_io('out', 'aimant'),
            # self.wait(pytaVSL.get('f_ch4-' + str(fn), 'video_end') - inherit_wait - 0.2 - 0.1 - 0.7 - 0.15 - 0.5 - future_in_prop * pytaVSL.get('m_ch4-' + str(fn + 1), 'video_end'), 's'),
            # self.m_ch4_9()
        ])


#################### DEPRECATED
    #
    # @pedalboard_button(108)
    # def button_m_ch4_9(self):
    #     """
    #     Miraye annonce gloriole Véronique
    #     """
    #     self.stop_scene('sequence/*')
    #     self.m_ch4_9()
    #
    #
    # def m_ch4_9(self):
    #     """
    #     Miraye annonce gloriole Véronique
    #     """
    #     ###### TODO : est-ce qu'on stoppe sur Yüla ?
    #     in_prop = 5/6
    #     mn = 9
    #     d_over_miraye = in_prop * pytaVSL.get('m_ch4-' + str(mn), 'video_end') - 0.5 - 0.7 - 0.2 - 0.2 - 1 - 0.2
    #     self.start_scene('sequence/m_ch4_' + str(mn), lambda: [
    #         pytaVSL.trijc_io('in', 'aimant', 0.2, 'elastic-inout'),
    #         pytaVSL.set('m_ch4-' + str(mn), 'visible', 1),
    #         pytaVSL.set('m_iraye', 'scale', 0.4, 0.4),
    #         pytaVSL.set('m_iraye', 'position_y', 0.17),
    #         pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 20, 0.4, 's'),
    #         self.wait(0.5, 's'),
    #         pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -25, 0.7, 's', 'elastic-inout'),
    #         pytaVSL.animate('m_iraye', 'position_x', None, -0.3, 0.7, 's', 'elastic-inout'),
    #         self.wait(0.7, 's'),
    #         pytaVSL.trijc_change_tool('compas'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -90, 0.1, 's'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -30, d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'scale', None, [0.75, 0.75], d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'position_x', None, 0.12, d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'position_y', None, 0.05, d_over_miraye - 1 - 0.2, 's'),
    #         self.wait(d_over_miraye, 's'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, 10, 1, 's', 'elastic-inout'),
    #         self.wait(1, 's'),
    #         self.f_ch4_10()
    #     ])

    @pedalboard_button(109)
    def button_f_ch4_10(self):
        self.stop_scene('sequence/*')
        self.f_ch4_10()

    def f_ch4_10(self):
        """
        Gloriole de Véronique
        """
        past_out_prop = 1/6
        fn = 10
        future_in_prop = 5/6
        inherit_wait = past_out_prop * pytaVSL.get('m_ch4-' + str(fn -1), 'video_end') - 0.2 - 1
        switch_duration = 0.5
        self.start_scene('sequence/f_ch4_' + str(fn), lambda: [
            pytaVSL.f_noisy_switch_video('f_ch4-' + str(fn - 2), 'f_ch4-' + str(fn), switch_duration),
            mainSampler.noisy_switch(switch_duration), ### AUDIO
            self.wait(pytaVSL.get('f_ch4-10', 'video_end'), 's'),
            self.f_ch4_12()

####### TODO split final en 4 cadres...


################# DEPRECATED
            # self.wait(inherit_wait, 's'),
            # pytaVSL.trijc_change_tool('aimant'),
            # self.wait(0.2, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -25, 0.1, 's', 'elastic-inout'),
            # self.wait(0.1, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 30, 0.7, 's', 'elastic-inout'),
            # pytaVSL.animate('m_iraye', 'position_x', None, -1, 0.7, 's', 'elastic-inout'),
            # self.wait(0.7, 's'),
            # pytaVSL.set('m_ch4-' + str(fn - 1), 'visible', 0),
            # self.wait(0.15, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 0, 0.5, 's', 'elastic-inout'),
            # pytaVSL.animate('f_ilm', 'scale', None, [0.95, 0.95], 0.3, 's'),
            # pytaVSL.animate('f_ilm', 'position_x', None, 0, 0.3, 's'),
            # pytaVSL.animate('f_ilm', 'position_y', None, 0, 0.3, 's'),
            # self.wait(0.5, 's'),
            # pytaVSL.trijc_io('out', 'aimant'),
            # self.wait(pytaVSL.get('f_ch4-' + str(fn), 'video_end') - inherit_wait - 0.2 - 0.1 - 0.7 - 0.15 - 0.5 - future_in_prop * pytaVSL.get('m_ch4-' + str(fn + 1), 'video_end'), 's'),
            # self.m_ch4_11()
        ])

############ DEPRECATED
    # @pedalboard_button(110)
    # def button_m_ch4_11(self):
    #     """
    #     Miraye ré-annonce gloriole Yüla
    #     """
    #     self.stop_scene('sequence/*')
    #     self.m_ch4_11()
    #
    # def m_ch4_11(self):
    #     """
    #     Miraye réannonce gloriole Yüla
    #     """
    #     ###### TODO : est-ce qu'on stoppe sur Yüla ?
    #     in_prop = 5/6
    #     mn = 11
    #     d_over_miraye = in_prop * pytaVSL.get('m_ch4-' + str(mn), 'video_end') - 0.5 - 0.7 - 0.2 - 0.2 - 1 - 0.2
    #     self.start_scene('sequence/m_ch4_' + str(mn), lambda: [
    #         pytaVSL.trijc_io('in', 'aimant', 0.2, 'elastic-inout'),
    #         pytaVSL.set('m_ch4-' + str(mn), 'visible', 1),
    #         pytaVSL.set('m_iraye', 'scale', 0.4, 0.4),
    #         pytaVSL.set('m_iraye', 'position_y', 0.17),
    #         pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 20, 0.4, 's'),
    #         self.wait(0.5, 's'),
    #         pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -25, 0.7, 's', 'elastic-inout'),
    #         pytaVSL.animate('m_iraye', 'position_x', None, -0.3, 0.7, 's', 'elastic-inout'),
    #         self.wait(0.7, 's'),
    #         pytaVSL.trijc_change_tool('compas'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -90, 0.1, 's'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -30, d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'scale', None, [0.75, 0.75], d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'position_x', None, 0.12, d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'position_y', None, 0.05, d_over_miraye - 1 - 0.2, 's'),
    #         self.wait(d_over_miraye, 's'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, 10, 1, 's', 'elastic-inout'),
    #         self.wait(1, 's'),
    #         self.f_ch4_12()
    #     ])

    @pedalboard_button(111)
    def button_f_ch4_12(self):
        self.stop_scene('sequence/*')
        self.f_ch4_12()

    def f_ch4_12(self):
        """
        Gloriole de Yüla 3
        """
        past_out_prop = 1/6
        fn = 12
        future_in_prop = 5/6
        inherit_wait = past_out_prop * pytaVSL.get('m_ch4-' + str(fn -1), 'video_end') - 0.2 - 1
        switch_duration = 0.5
        self.start_scene('sequence/f_ch4_' + str(fn), lambda: [
            pytaVSL.f_noisy_switch_video('f_ch4-' + str(fn - 2), 'f_ch4-' + str(fn), switch_duration),
            mainSampler.noisy_switch(switch_duration), ### AUDIO
            self.wait(pytaVSL.get('f_ch4-12', 'video_end'), 's'),
            self.f_ch4_14()

#############" DEPRECATED"
            # self.wait(inherit_wait, 's'),
            # pytaVSL.trijc_change_tool('aimant'),
            # self.wait(0.2, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -25, 0.1, 's', 'elastic-inout'),
            # self.wait(0.1, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 30, 0.7, 's', 'elastic-inout'),
            # pytaVSL.animate('m_iraye', 'position_x', None, -1, 0.7, 's', 'elastic-inout'),
            # self.wait(0.7, 's'),
            # pytaVSL.set('m_ch4-' + str(fn - 1), 'visible', 0),
            # self.wait(0.15, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 0, 0.5, 's', 'elastic-inout'),
            # pytaVSL.animate('f_ilm', 'scale', None, [0.95, 0.95], 0.3, 's'),
            # pytaVSL.animate('f_ilm', 'position_x', None, 0, 0.3, 's'),
            # pytaVSL.animate('f_ilm', 'position_y', None, 0, 0.3, 's'),
            # self.wait(0.5, 's'),
            #
            # pytaVSL.trijc_io('out', 'aimant'),
            # self.wait(pytaVSL.get('f_ch4-' + str(fn), 'video_end') - inherit_wait - 0.2 - 0.1 - 0.7 - 0.15 - 0.5 - future_in_prop * pytaVSL.get('m_ch4-' + str(fn + 1), 'video_end'), 's'),
            # self.m_ch4_13()
        ])

############# DEPRECTAED
    # @pedalboard_button(112)
    # def button_m_ch4_13(self):
    #     """
    #     Miraye annonce Ravi
    #     """
    #     self.stop_scene('sequence/*')
    #     self.m_ch4_13()
    #
    # def m_ch4_13(self):
    #     """
    #     Miraye annonce Ravi
    #     """
    #     ###### TODO : est-ce qu'on stoppe sur Yüla ?
    #     in_prop = 5/6
    #     mn = 13
    #     d_over_miraye = in_prop * pytaVSL.get('m_ch4-' + str(mn), 'video_end') - 0.5 - 0.7 - 0.2 - 0.2 - 1 - 0.2
    #     self.start_scene('sequence/m_ch4_' + str(mn), lambda: [
    #         pytaVSL.trijc_io('in', 'aimant', 0.2, 'elastic-inout'),
    #         pytaVSL.set('m_ch4-' + str(mn), 'visible', 1),
    #         pytaVSL.set('m_iraye', 'scale', 0.4, 0.4),
    #         pytaVSL.set('m_iraye', 'position_y', 0.17),
    #         pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 20, 0.4, 's'),
    #         self.wait(0.5, 's'),
    #         pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -25, 0.7, 's', 'elastic-inout'),
    #         pytaVSL.animate('m_iraye', 'position_x', None, -0.3, 0.7, 's', 'elastic-inout'),
    #         self.wait(0.7, 's'),
    #         pytaVSL.trijc_change_tool('compas'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -90, 0.1, 's'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -30, d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'scale', None, [0.75, 0.75], d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'position_x', None, 0.12, d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'position_y', None, 0.05, d_over_miraye - 1 - 0.2, 's'),
    #         self.wait(d_over_miraye, 's'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, 10, 1, 's', 'elastic-inout'),
    #         self.wait(1, 's'),
    #         self.f_ch4_14()
    #     ])

    @pedalboard_button(113)
    def button_f_ch4_14(self):
        self.stop_scene('sequence/*')
        self.f_ch4_14()

    def f_ch4_14(self):
        """
        Gloriole de Ravi
        """

        ### AUDIO
        t_mega = 41 + 17/30 # + 13/30
        t_acc = 57
        t_xrays_switch = 68.6
        t_xrays = 69.6
        t_xrays_end = 70.6
        self.start_scene('f_ch4-14_audio', lambda: [
            self.wait(t_mega, 's'),
            mainSampler.send('/instrument/play', 's:f_ch4-14_megaphone', 100),
            self.wait(t_acc - t_mega, 's'),
            mainSampler.send('/instrument/play', 's:f_ch4-14_accordion', 100),
            self.wait(t_xrays_switch - t_acc, 's'),
            mainSampler.send('/instrument/play', 's:f_ch4-14_xrays_switch', 100),
            self.wait(t_xrays - t_xrays_switch, 's'),
            mainSampler.send('/instrument/play', 's:f_ch4-14_xrays', 100),
            self.wait(t_xrays_end - t_xrays, 's'),
            mainSampler.send('/instrument/stop', 's:f_ch4-14_xrays'),
        ])

        ### VJ
        past_out_prop = 1/6
        fn = 14
        future_in_prop = 5/6
        inherit_wait = past_out_prop * pytaVSL.get('m_ch4-' + str(fn -1), 'video_end') - 0.2 - 1
        switch_duration = 0.5
        self.start_scene('sequence/f_ch4_' + str(fn), lambda: [
            pytaVSL.f_noisy_switch_video('f_ch4-' + str(fn - 2), 'f_ch4-' + str(fn), switch_duration),
            mainSampler.noisy_switch(switch_duration), ### AUDIO
            self.wait(pytaVSL.get('f_ch4-14', 'video_end'), 's'),
            self.f_ch4_16()

            # self.wait(inherit_wait, 's'),
            # pytaVSL.trijc_change_tool('aimant'),
            # self.wait(0.2, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -25, 0.1, 's', 'elastic-inout'),
            # self.wait(0.1, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 30, 0.7, 's', 'elastic-inout'),
            # pytaVSL.animate('m_iraye', 'position_x', None, -1, 0.7, 's', 'elastic-inout'),
            # self.wait(0.7, 's'),
            # pytaVSL.set('m_ch4-' + str(fn - 1), 'visible', 0),
            # self.wait(0.15, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 0, 0.5, 's', 'elastic-inout'),
            # pytaVSL.animate('f_ilm', 'scale', None, [0.95, 0.95], 0.3, 's'),
            # pytaVSL.animate('f_ilm', 'position_x', None, 0, 0.3, 's'),
            # pytaVSL.animate('f_ilm', 'position_y', None, 0, 0.3, 's'),
            # self.wait(0.5, 's'),
            #
            # pytaVSL.trijc_io('out', 'aimant'),
            # self.wait(pytaVSL.get('f_ch4-' + str(fn), 'video_end') - inherit_wait - 0.2 - 0.1 - 0.7 - 0.15 - 0.5 - future_in_prop * pytaVSL.get('m_ch4-' + str(fn + 1), 'video_end'), 's'),
            # self.m_ch4_15()
        ])

####### DEPRECATED
    # @pedalboard_button(114)
    # def button_m_ch4_15(self):
    #     """
    #     Miraye ré-annonce Yüla
    #     """
    #     self.stop_scene('sequence/*')
    #     self.m_ch4_15()
    #
    #
    # def m_ch4_15(self):
    #     """
    #     Miraye ré-annonce Yüla
    #     """
    #     ###### TODO : est-ce qu'on stoppe sur Yüla ?
    #     in_prop = 5/6
    #     mn = 15
    #     d_over_miraye = in_prop * pytaVSL.get('m_ch4-' + str(mn), 'video_end') - 0.5 - 0.7 - 0.2 - 0.2 - 1 - 0.2
    #     self.start_scene('sequence/m_ch4_' + str(mn), lambda: [
    #         pytaVSL.trijc_io('in', 'aimant', 0.2, 'elastic-inout'),
    #         pytaVSL.set('m_ch4-' + str(mn), 'visible', 1),
    #         pytaVSL.set('m_iraye', 'scale', 0.4, 0.4),
    #         pytaVSL.set('m_iraye', 'position_y', 0.17),
    #         pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 20, 0.4, 's'),
    #         self.wait(0.5, 's'),
    #         pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -25, 0.7, 's', 'elastic-inout'),
    #         pytaVSL.animate('m_iraye', 'position_x', None, -0.3, 0.7, 's', 'elastic-inout'),
    #         self.wait(0.7, 's'),
    #         pytaVSL.trijc_change_tool('compas'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -90, 0.1, 's'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, -30, d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'scale', None, [0.75, 0.75], d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'position_x', None, 0.12, d_over_miraye - 1 - 0.2, 's'),
    #         pytaVSL.animate('f_ilm', 'position_y', None, 0.05, d_over_miraye - 1 - 0.2, 's'),
    #         self.wait(d_over_miraye, 's'),
    #         self.wait(0.2, 's'),
    #         pytaVSL.animate('t_trijc_compas', 'rotate_z', None, 10, 1, 's', 'elastic-inout'),
    #         self.wait(1, 's'),
    #         self.f_ch4_16()
    #     ])

    @pedalboard_button(115)
    def button_f_ch4_16(self):
        self.stop_scene('sequence/*')
        self.f_ch4_16()

    def f_ch4_16(self):
        """
        Gloriole de Yüla 4 (including Pano)
        """
        past_out_prop = 1/6
        fn = 16
        future_in_prop = 5/6
        inherit_wait = past_out_prop * pytaVSL.get('m_ch4-' + str(fn -1), 'video_end') - 0.2 - 1
        switch_duration = 0.5
        self.t_arrivee_pano = 4
        self.start_scene('sequence/f_ch4_' + str(fn), lambda: [
            pytaVSL.f_noisy_switch_video('f_ch4-' + str(fn - 2), 'f_ch4-' + str(fn), switch_duration, warpzoned=True),
            mainSampler.noisy_switch(switch_duration), ### AUDIO

            # self.wait(inherit_wait, 's'),
            # pytaVSL.trijc_change_tool('aimant'),
            # self.wait(0.2, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, -25, 0.1, 's', 'elastic-inout'),
            # self.wait(0.1, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 30, 0.7, 's', 'elastic-inout'),
            # pytaVSL.animate('m_iraye', 'position_x', None, -1, 0.7, 's', 'elastic-inout'),
            # self.wait(0.7, 's'),
            # pytaVSL.set('m_ch4-' + str(fn - 1), 'visible', 0),
            # self.wait(0.15, 's'),
            # pytaVSL.animate('t_trijc_aimant', 'rotate_z', None, 0, 0.5, 's', 'elastic-inout'),
            # pytaVSL.animate('f_ilm', 'scale', None, [0.95, 0.95], 0.3, 's'),
            # pytaVSL.animate('f_ilm', 'position_x', None, 0, 0.3, 's'),
            # pytaVSL.animate('f_ilm', 'position_y', None, 0, 0.3, 's'),

            self.wait(self.t_arrivee_pano, 's'),
            self.f_ch4_16_bato(),

            # pytaVSL.trijc_io('out', 'aimant'),

##### A mettre dans bouton redémarrage du bateau
            # self.wait(pytaVSL.get('f_ch4-' + str(fn), 'video_end') - inherit_wait - 0.2 - 0.1 - 0.7 - 0.15 - 0.5),
            # self.f_ch4_16_waiting()
        ])

    @pedalboard_button()
    def f_ch4_16_bato(self):
        # Init
        pytaVSL.batohacker_shaking(),
        pytaVSL.set('bato_h', 'position_x', 1),
        pytaVSL.set('bato_h', 'scale', 2, 2),
        pytaVSL.set('bato_h', 'rotate_z', 0),
        pytaVSL.set('bato_h', 'position_y', 0.02),

        pytaVSL.set('o_bh_embouchure', 'scale', 0.08, 0.08)
        pytaVSL.set('o_bh_embouchure', 'position_x', -0.015)
        pytaVSL.set('o_bh_embouchure', 'position_y', -0.445)



        pytaVSL.set('o_bh_droit_1', 'position_x', -0.012)
        pytaVSL.set('o_bh_droit_1', 'position_y', -0.34)


        # pytaVSL.set('o_bh_coude_roit_1', 'position_x', -0.005)
        # pytaVSL.set('o_bh_coude_droit_1', 'position_y', -0.295)
        #
        # pytaVSL.set('o_bh_coude_droit_2', 'position_x', 0.06)
        # pytaVSL.set('o_bh_coude_droit_2', 'position_y', -0.483)

        self.start_scene('sequence/f_ch4_16_bato', lambda: [
            self.wait(0.01, 's'),
            # Arrivée
            pytaVSL.batohacker_hmove(0.67, 0.5),
            mainSampler.batohacker_move(0.5), ### AUDIO,
            self.wait(0.5, 's'),

            # Punch
            pytaVSL.batohacker_punch('f_ch4-16'),
            mainSampler.batohacker_punch(), ### AUDIO
            pytaVSL.animate('f_ch4-16*', 'brightness', None, 0.3, 0.3, 's'),
            pytaVSL.animate('bt', 'position_x', None, 0.447, 0.5, 's', 'elastic-out'),


            # Positionnement fin
            self.wait(1, 's'),
            pytaVSL.batohacker_hmove(0.41, duration = 0.5),
            mainSampler.batohacker_move(0.5), ### AUDIO

            # Sortie Entrailles
            self.wait(0.6, 's'),
            pytaVSL.batohacker_trp_io(),
            mainSampler.batohacker_trp_io(), ### AUDIO


            # Branchement
            self.wait(1, 's'),
            # pytaVSL.set('o_bh_coude_droit_1', 'visible', 1),
            pytaVSL.animate('o_bh_droit_1', 'scale', [0.2, 0], [0.2, 0.39], 0.3, 's'),
            pytaVSL.animate('o_bh_droit_1', 'position_y', -0.17, -0.34, 0.3, 's'),
            pytaVSL.set('o_bh_droit_1', 'visible', 1),
            self.wait(0.3, 's'),
            # self.wait(0.3, 's'),
            # pytaVSL.set('o_bh_coude_droit_2', 'visible', 1),
            # self.wait(0.3, 's'),
            pytaVSL.set('o_bh_embouchure', 'visible', 1),
            mainSampler.batohacker_plug(0.3), ### AUDIO


            # Allumage EP et plateau
            self.wait(1, 's'),
            qlcplus.set_strobe('bt_doah'), ### LIGHT
            qlcplus.animate('d_doah', None, 255, 2, 's'), ### LIGHT
            qlcplus.animate('d_sal', None, 0.6 * 255, 2, 's'), ### LIGHT
            qlcplus.animate('d_je', None, 0.6 * 255, 2, 's'), ### LIGHT

            pytaVSL.animate('bt_main', 'brightness', None, 1, 1, 's'),
            pytaVSL.animate('*ep_*', 'brightness', None, 1, 1, 's'),
            pytaVSL.bascule_ep(),
            mainSampler.bascule_ep(), ### AUDIO
            self.start_scene('sequence/f_ch4-16_ep_reveil', lambda: [
                pytaVSL.face_ep('reveil'),
            ]),
            pytaVSL.allume_ep(on='courrier'),
            mainSampler.allume_ep(on='courrier'), ### AUDIO


        ])


    @pedalboard_button(5001, who='orl', after='f_ch4_16_bato')
    def f_ch4_16_bato_doah_acts(self):
        self.f_ch4_16_delay = 0.6 + self.t_arrivee_pano
        t_warpzoned_debut = 8
        t_avalage = 10.5
        duree_avalage = 0.5
        t_warpzoned_fin = 11.2
        self.start_scene('sequence/f_ch4_16_batohacker1_doah_acts', lambda: [
            # Relâche film
            pytaVSL.animate('f_ch4-16*', 'brightness', None, 1, 0.1, 's'),
            pytaVSL.set('f_ch4-16', 'video_speed', 1),
            pytaVSL.set('f_ch4-16_warpzoned', 'video_speed', 1),
            mainSampler.batohacker_film_stopped(stopped=True), ### AUDIO

            # Prépa mask
            pytaVSL.set('f_ch4-16', 'mask', 'f_ch4-16_mask'),

            # Avalage
            self.wait(t_warpzoned_debut - self.f_ch4_16_delay, 's'),
            mainSampler.batohacker_warp(warp_in=True), ### AUDIO
            pytaVSL.animate('f_ch4-16_warpzoned', 'rgbwave', None, 0.4, 0.3, 's'),
            pytaVSL.animate('f_ch4-16_warpzoned', 'fish', None, 0.4, 0.3, 's'),
            self.wait(t_avalage - t_warpzoned_debut, 's'),
            #### TODO à revoir
            mainSampler.batohacker_swallow(0.5), ### AUDIO
            # pytaVSL.batohacker_swallow(0.5),
            pytaVSL.animate('i_bh_entrailles', 'offset_y', 0, 0.005, 0.3, 's', 'random-mirror'),
            pytaVSL.animate('o_bh_adam', 'alpha', 0.3, 1, 0.2, 's'),
            pytaVSL.animate('o_bh_adam', 'scale', [1, 0], [1, 1], 0.2, 's'),
            pytaVSL.set('o_bh_adam', 'visible', 1),
            pytaVSL.animate('o_bh_adam', 'position_x', -0.02, 0, 0.5, 's'),
            pytaVSL.animate('o_bh_adam', 'position_y', -0.08, -0.435, 0.5, 's'),
            self.wait(duree_avalage - 0.1, 's'),
            pytaVSL.animate('o_bh_adam', 'alpha', 1, 0, 0.1, 's'),
            pytaVSL.animate('o_bh_adam', 'scale', [1, 1], [1, 0], 0.1, 's'),
            self.wait(0.1, 's'),
            pytaVSL.set('o_bh_adam', 'visible', 0),
            #### TODO
            self.wait(t_warpzoned_fin - t_avalage - duree_avalage, 's'),
            pytaVSL.animate('f_ch4-16_warpzoned', 'rgbwave', None, 0, 0.3, 's'),
            pytaVSL.animate('f_ch4-16_warpzoned', 'fish', None, 0, 0.3, 's'),
            mainSampler.batohacker_warp(warp_in=False), ### AUDIO


            # Débranchage
            self.wait(0.6, 's'),
            mainSampler.batohacker_plug(0.6), ### AUDIO
            pytaVSL.set('o_bh_embouchure', 'visible', 0),
            self.wait(0.3, 's'),
            # pytaVSL.set('o_bh_coude_droit_2', 'visible', 0),
            # self.wait(0.3, 's'),
            # pytaVSL.set('o_bh_coude_droit_1', 'visible', 0),
            pytaVSL.animate('o_bh_droit_1', 'scale', None, [0.2, 0], 0.3, 's'),
            pytaVSL.animate('o_bh_droit_1', 'position_y', None, -0.17, 0.3, 's'),
            pytaVSL.set('o_bh_droit_1', 'visible', 1),
            self.wait(0.3, 's'),


            # Extinction EP + Plateau
            self.wait(0.5, 's'),
            qlcplus.set_strobe('bt_doah', on=False), ### LIGHT

            pytaVSL.animate('bt', 'position_x', None, 0.525, 1, 's'),
            mainSampler.bascule_ep(), ### AUDIO
            pytaVSL.bascule_ep(False),
            pytaVSL.allume_ep(False),


            # Rentrage des entrailles
            self.wait(1, 's'),
            pytaVSL.batohacker_trp_io('in'),
            mainSampler.batohacker_trp_io(), ### AUDIO

            # Remet le film à sa place
            self.wait(1, 's'),

            pytaVSL.batohacker_debevier(),
            mainSampler.batohacker_debevier(), ### AUDIO
            self.wait(0.4, 's'),
            pytaVSL.animate('f_ilm', 'position_x', None, 0, 1, 's'),

            # S'en va
            self.wait(1, 's'),
            pytaVSL.batohacker_hmove(1, 'cour'),
            mainSampler.batohacker_move(goto='departure'), ### AUDIO


            # EP se rendort
            self.wait(1.5, 's'),
            self.start_scene('sequence/f_ch4-16_ep_dodo', lambda: [
                pytaVSL.face_ep('dodo')
            ]),
            pytaVSL.animate('bt_main', 'brightness', None, 0.3, 0.2, 's'),
            pytaVSL.animate('*ep_*', 'brightness', None, 0.3, 0.2, 's'),

            # Reinit des masques
            pytaVSL.set('f_ch4-16', 'mask', ''),
            pytaVSL.set('f_ch4-16_warpzoned', 'visible', 0),

        ])

        self.start_scene('sequence/f_ch4_16_2_f_ch4_16_waiting', lambda: [
            self.wait(pytaVSL.get('f_ch4-16', 'video_end') - self.f_ch4_16_delay, 's'),
            self.f_ch4_16_waiting()
        ])


################"" DEPRECATED
    # def f_ch4_16_enedys(self):
    #     self.start_scene('sequence/f_ch4_16_enedys', lambda: [
    #         # pytaVSL.animate('bt', 'position_x', None, 0.5, 0.2, 's', 'elastic-inout'),
    #         pytaVSL.animate('bt_main', 'brightness', None, 1, 0.1, 's', 'random'),
    #         pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.1, 's', 'random'),
    #         pytaVSL.animate('bt', 'position_x', None, 0.48, 0.1, 's', 'elastic'),
    #         pytaVSL.animate('bt_ep', 'rotate_z', -93, -87, 0.5, 's', 'random-mirror'),
    #         pytaVSL.face_ep('reveil'),
    #         self.wait(0.2, 's'),
    #         self.wait(0.8, 's'),
    #         pytaVSL.face_ep('non'),
    #         self.wait(0.5, 's'),
    #         pytaVSL.animate('bt', 'position_x', None, 0.41, 0.8, 's', 'elastic-mirror', loop=True),
    #         pytaVSL.getwhistle_ep(0.2),
    #         self.wait(0.2, 's'),
    #         pytaVSL.face_ep('sifflet'),
    #         self.wait(0.5, 's'),
    #         pytaVSL.allume_ep()
    #     ])

    @pedalboard_button()
    def f_ch4_16_waiting(self):
        """
        Zombie attend de lire
        """
        pytaVSL.f_switch_video('f_ch4-16', 'f_ch4-16_waiting')

    @pedalboard_button(5002, who='orl', after='f_ch4_16_waiting')
    def f_ch4_17(self):
        """
        Fin Gloriole de Yüla 4 (including Pano)
        """
        ### LIGHT
        self.start_scene('sequence/light_f_ch4-17', lambda: [
            self.wait(3, 's'),
            qlcplus.animate('d_doah', None, 0, 2, 's'),
            qlcplus.animate('d_je', None, 0, 2, 's'),
            qlcplus.animate('d_sal', None, 0, 2, 's'),
        ])

        ### VJ
        self.start_scene('sequence/f_ch4_17', lambda: [
            pytaVSL.f_noisy_switch_video('f_ch4-16_waiting', 'f_ch4-17', 0.5),
            mainSampler.noisy_switch(0.5), ### AUDIO
            self.wait(pytaVSL.get('f_ch4-17', 'video_end') - 1, 's'),
            pytaVSL.trijc_io('in', 'lustre'),
            mainSampler.trijc_io('in'), ### AUDIO
            self.wait(1, 's'),
            pytaVSL.trijc_turn_lights('off'),
            mainSampler.trijc_turn_lights(), ### AUDIO
            pytaVSL.animate('f_ch4-17', 'alpha', None, 0, 1, 's'),
            pytaVSL.animate('f_arabesque*', 'alpha', None, 0, 1, 's'),
            self.wait(1.1, 's'),
            pytaVSL.set('f_ilm', 'visible', 0),
            pytaVSL.set('f_ch4-17', 'alpha', 1),
            pytaVSL.set('f_ch4-17', 'visible', 0),
            pytaVSL.set('f_arabesque*', 'alpha', 1),
            self.m_ch4_18()
        ])



    @pedalboard_button(116)
    def button_m_ch4_18(self):
        """
        Miraye raconte
        """
        self.stop_scene('sequence/*')
        self.m_ch4_18()

    @pedalboard_button()
    def m_ch4_18(self):
        """
        Miraye raconte "Pathétique n'est-ce pas ?" -> "Jean-Eudes, on enchaîne"
        """
        ### LIGHT
        qlcplus.set_scene('miraye')

        ### VJ
        self.start_scene('sequence/m_ch4-18', lambda: [
            pytaVSL.trijc_change_tool('tuba'),
            pytaVSL.signs_io('in', duration=0.5, easing='elastic-out'),
            pytaVSL.animate('lights*', 'alpha', None, 1, 0.5, 's'),
            pytaVSL.animate('bt_main', 'brightness', None, 1, 0.5, 's'),
            pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.5, 's'),
            pytaVSL.miraye_in('m_ch4-18'),
            mainSampler.miraye_in(), ### AUDIO

            self.wait(pytaVSL.get('m_ch4-18', 'video_end'), 's'),
            self.f_ch4_19()
        ])


    @pedalboard_button()
    def f_ch4_19(self):
        """
        Jour de spectacle
        """
        ### LIGHT
        qlcplus.set_scene('film')

        ### AUDIO
        t_offset = 1.2 + 0.8 + 0.7 + 0.6
        t_xrays_switch = 283.9
        t_xrays = 285
        t_xrays_end = 287
        self.start_scene('f_ch4-14_audio', lambda: [
            self.wait(t_offset, 's'),
            self.wait(t_xrays_switch, 's'),
            mainSampler.send('/instrument/play', 's:f_ch4-14_xrays_switch', 100),
            self.wait(t_xrays - t_xrays_switch, 's'),
            mainSampler.send('/instrument/play', 's:f_ch4-14_xrays', 100),
            self.wait(t_xrays_end - t_xrays, 's'),
            mainSampler.send('/instrument/stop', 's:f_ch4-14_xrays'),
        ])

        ### VJ
        self.start_scene('sequence/f_ch4_19', lambda: [
            pytaVSL.trijc_io('in', 'aspi', 0.2),
            mainSampler.trijc_io('in', 0.2), ### AUDIO
            pytaVSL.animate('t_trijc_aspi', 'rotate_z', None, 0, 0.2, 's', 'elastic-inout'),
            pytaVSL.aspi_slide('m_layout', [0, -0.45], [0, 0.52], 0.6),
            pytaVSL.aspi_slide('m_ch4-18', [-0.02, -0.445], [-0.02, 0.53], 0.6),
            mainSampler.miraye_out(), ### AUDIO
            mainSampler.aspi_slide(), ### AUDIO
            self.wait(1.2, 's'),
            pytaVSL.trijc_io('out', 'aspi', 0.7, 'elastic-inout'),
            mainSampler.trijc_io('out', 0.7, 'elastic-inout'), ### AUDIO
            self.wait(0.8, 's'),
            pytaVSL.trijc_io('in', 'tuba', 0.6, 'elastic-inout'),
            mainSampler.trijc_io('in', 0.6, 'elastic-inout'), ### AUDIO
            self.wait(0.7, 's'),
            pytaVSL.movie_in('f_ch4-19', 0.6, init=True),
            mainSampler.movie_in(), ### AUDIO
            self.wait(pytaVSL.get('f_ch4-19', 'video_end') + 1, 's'), # - 0.25 - 1.2 - 0.8 - 0.7, 's'),
            self.wait(1, 's'), # pour passage vers scène Houdini / Barons
            self.f_ch4_20()
        ])

    @pedalboard_button(117)
    def button_f_ch4_20(self):
        """
        Barons et Houdiniwi
        """
        self.stop_scene('sequence/*')
        self.f_ch4_20()

    def f_ch4_20(self):
        """
        Barons et Houdiniwi
        """
        self.start_scene('sequence/f_ch4_20', lambda: [
            self.wait(0.5, 's'),
            # pytaVSL.animate('f_ch4-20', 'alpha', 0.2, 1, 0.8, 's'),
            pytaVSL.f_switch_video('f_ch4-19', 'f_ch4-20'),
            self.wait(pytaVSL.get('f_ch4-20', 'video_end') - 0.25, 's'),
            self.f_ch4_20_waiting()
        ])

    @pedalboard_button(1170)
    def f_ch4_20_waiting(self):
        """
        Barons et Houdiniwi waiting
        """
        ### LIGHT
        qlcplus.animate('d_doah', None, 255, 2, 's')
        qlcplus.animate('d_sal', None, 255, 2, 's')
        qlcplus.animate('d_je', None, 255, 2, 's')

        ### VJ
        pytaVSL.f_switch_video('f_ch4-20', 'f_ch4-20_waiting')


    @pedalboard_button(5004, who='adrien', after='f_ch4_20_waiting')
    def f_ch4_21(self):
        """
        Barons et Houdiniwi pause
        """
        pytaVSL.f_switch_video('f_ch4-20_waiting', 'f_ch4-21')
        mainSampler.send('/instrument/play', 's:f_ch4-21_pause', 100),
        self.start_scene('sequence/f_ch4_21_audio', lambda: [
            self.wait(0.3, 's'),
            mainSampler.send('/instrument/play', 's:jmjarret_entracte', 100),
        ])

    @pedalboard_button(6001, who='arriere', after='f_ch4_21')
    def f_ch4_21_light(self):
        """
        Allumage boîte téléportation
        """
        qlcplus.set_strobe('bt_centrale')

    @pedalboard_button(5005, who='arriere', after='f_ch4_21_light')
    def f_ch4_22(self):
        """
        Barons envoyés dans le f_ilm
        """
        ### LIGHT
        qlcplus.animate('d_doah', None, 0.6 * 255, 2, 's') # Utile ?
        qlcplus.animate('d_sal', None, 0.6 * 255, 2, 's') # Utile ?
        qlcplus.animate('d_je', None, 0.6 * 255, 2, 's') # Utile ?


        ### VJ
        def arrivees_barons():
            r_period = 0.8
            y_dest = 0.1
            def doah():
                pytaVSL.animate('t_nobh_entovillon', 'offset_y', 0, 0.01, 0.7, 's', 'elastic-mirror'),
                self.wait(0.25, 's'),
                pytaVSL.animate('doah_b', 'position_y', None, y_dest, 0.8, 's', 'elastic-inout'),
                self.wait(0.2, 's'),
                mainSampler.send('/instrument/play', 's:f_ch4_pop', 100), ### AUDIO
                self.wait(0.3, 's'),

                pytaVSL.animate('doah_b', 'rotate_z', 20, 379, r_period, 's', 'linear', loop=True),

            def saladin():
                pytaVSL.animate('t_nobh_entovillon', 'offset_y', 0, 0.01, 0.7, 's', 'elastic-mirror'),
                self.wait(0.25, 's'),
                pytaVSL.animate('saladin_b', 'position_y', None, y_dest, 0.8, 's', 'elastic-inout'),
                self.wait(0.2, 's'),
                mainSampler.send('/instrument/play', 's:f_ch4_pop', 100), ### AUDIO
                self.wait(0.3, 's'),

                pytaVSL.animate('saladin_b', 'rotate_z', 0, 359, r_period, 's', 'linear', loop=True),

            def baron_fall(baron):
                if baron == 'doah_b':
                    y_inter = 0.07

                    final_run_d = 0.7
                    final_run_x = 0.1 #0.38 - 0.2
                    final_run_y = -0.42 + 0.3
                    final_run_rot = 20
                    n_enclume = 1
                    bz = -0.1

                    x1 = 0.38 - 0.035
                    y1 = -0.42 + 0.045

                    x2 = 0.38
                    y2 = -0.42

                    x3 = 0.23
                    y3 = -0.42 +  0.135

                    x4 = 0.33
                    y4 = -0.42

                    x5 = final_run_x
                    y5 = -0.42 +  0.3

                else:
                    y_inter = 0.065
                    final_run_d = 0.7
                    final_run_x = 0 #-0.3
                    final_run_y = 0
                    final_run_rot = 0
                    n_enclume = 2
                    bz = 0

                    x1 = 0.3
                    y1 = -0.35

                    x2 = 0.25
                    y2 = -0.415

                    x3 = 0.12
                    y3 = -0.23

                    x4 = 0.17
                    y4 = -0.415

                    x5 = final_run_x
                    y5 = final_run_y

                def commeapres():
                    if baron == 'doah_b':
                        pytaVSL.animate(baron, 'position_x', None, 0.28, r_period + 2.5, 's')
                        self.wait(r_period + 2.5, 's')
                    pytaVSL.set(baron, 'sequence_mode', 0)


                self.start_scene('sequence/fall_' + baron, lambda: [
                    pytaVSL.set('t_nobh_enclume_' + str(n_enclume), 'visible', 1),
                    pytaVSL.animate('t_nobh_enclume_' + str(n_enclume), 'position_y', 0.35, 0.07, 2 * r_period, 's'),
                    pytaVSL.animate('t_nobh_enclume_' + str(n_enclume), 'offset_x', 0, 0.05, 2 * r_period / 4, 's'),
                    self.wait(2 * r_period / 4, 's'),
                    pytaVSL.animate('t_nobh_enclume_' + str(n_enclume), 'offset_x', None, -0.05, r_period, 's', 'linear-mirror', loop='True'),
                    self.wait(2 * 2 * r_period / 4, 's'),

                    pytaVSL.stop_animate('t_nobh_enclume_' + str(n_enclume), 'offset_x'),
                    pytaVSL.animate('t_nobh_enclume_' + str(n_enclume), 'offset_x', None, 0, 2 * r_period / 4, 's'),

                    self.wait(2 * r_period / 4, 's'),
                    pytaVSL.stop_animate(baron, 'rotate_z'),
                    pytaVSL.set(baron, 'rotate_z', final_run_rot),
                    self.wait(0.5, 's'),
                    pytaVSL.set(baron, 'position_z', -26 + bz),
                    pytaVSL.animate(baron, 'position_y', None, y2, 0.2, 's'),
                    pytaVSL.animate('t_nobh_enclume_' + str(n_enclume), 'position_y', None, -0.55, 0.2, 's'),
                    mainSampler.send('/instrument/play', 's:f_ch4_enclume', 100), ### AUDIO
                    self.wait(0.2, 's'),

                    commeapres()

                ])

            def mush_fall(mush, duration):
                mushroom = 't_nobh_champignon' + str(mush)
                self.start_scene('sequence/mushroom_falls_' + str(mush), lambda: [
                    pytaVSL.set(mushroom + '*', 'visible', 1),
                    pytaVSL.animate(mushroom + '*', 'position_y', None, -0.4, duration, 's'),
                    self.wait(duration - 0.2, 's'),
                    pytaVSL.animate(mushroom, 'noise', 0, -0.7, 0.2, 's'),
                    self.wait(0.1, 's'),
                    pytaVSL.animate(mushroom, 'alpha', 1, 0, 0.1, 's'),
                    self.wait(0.11, 's'),
                    pytaVSL.set(mushroom + '*', 'visible', 0),
                    pytaVSL.set(mushroom + '*', 'alpha', 1),
                    pytaVSL.set(mushroom + '*', 'position_y', 0.35),
                ])

            def baron_grow(baron):
                if baron == 'doah_b':
                    x1 = 0.25
                    y1 = -0.42 + 0.045

                    x2 = 0.28
                    y2 = -0.42

                    x3 = 0.19
                    y3 = -0.42 +  0.135

                    x4 = 0.28
                    y4 = -0.42

                    x5 = 0.06
                    y5 = -0.15

                else:
                    x1 = 0.34
                    y1 = -0.35

                    x2 = 0.38
                    y2 = -0.415

                    x3 = 0.28
                    y3 = -0.23

                    x4 = 0.17
                    y4 = -0.415

                    x5 = 0.15
                    y5 = 0

                self.start_scene('sequence/grow_' + baron, lambda: [
                    mainSampler.send('/instrument/play', 's:f_ch4_champi', 100), ### AUDIO
                    pytaVSL.set(baron, 'color_strobe', 1),
                    pytaVSL.animate(baron, 'scale', None, [0.25, 0.25], 0.05, 's', 'exponential-inout'),
                    pytaVSL.animate(baron, 'position_x', None, x1, 0.05, 's', 'exponential-inout'),
                    pytaVSL.animate(baron, 'position_y', None, y1, 0.05, 's', 'exponential-inout'),
                    self.wait(0.1, 's'),
                    pytaVSL.animate(baron, 'scale', None, [0.12, 0.12], 0.05, 's', 'exponential-inout'),
                    pytaVSL.animate(baron, 'position_x', None, x2, 0.05, 's', 'exponential-inout'),
                    pytaVSL.animate(baron, 'position_y', None, y2, 0.05, 's', 'exponential-inout'),
                    self.wait(0.1, 's'),
                    pytaVSL.animate(baron, 'scale', None, [0.5, 0.5], 0.05, 's', 'exponential-inout'),
                    pytaVSL.animate(baron, 'position_x', None, x3, 0.05, 's', 'exponential-inout'),
                    pytaVSL.animate(baron, 'position_y', None, y3, 0.05, 's', 'exponential-inout'),
                    self.wait(0.1, 's'),
                    pytaVSL.animate(baron, 'scale', None, [0.12, 0.12], 0.05, 's', 'exponential-inout'),
                    pytaVSL.animate(baron, 'position_x', None, x4, 0.05, 's', 'exponential-inout'),
                    pytaVSL.animate(baron, 'position_y', None, y4, 0.05, 's', 'exponential-inout'),
                    self.wait(0.1, 's'),
                    pytaVSL.animate(baron, 'scale', None, [0.95, 0.95], 0.05, 's', 'exponential-inout'),
                    pytaVSL.animate(baron, 'position_x', None, x5, 0.05, 's', 'exponential-inout'),
                    pytaVSL.animate(baron, 'position_y', None, y5, 0.05, 's', 'exponential-inout'),
                    self.wait(0.1, 's'),
                    pytaVSL.set(baron, 'color_strobe', 0),
                ])

            def baron_inside(baron):
                if baron == 'doah_b':

                    # pytaVSL.set('f_ch4-22_warpzoned', 'rgbwave', 0.4)
                    # pytaVSL.set('f_ch4-22_warpzoned', 'fish', 0.2)
                    # pytaVSL.set('f_ch4-22_warpzoned', 'mask', 'f_ch4-23_mask')
                    #
                    # pytaVSL.set('f_ch4-22_warpzoned', 'visible', 1),
                    pytaVSL.set('f_ch4-22', 'visible', 1),
                    ### AUDIO
                    mainSampler.batohacker_film_stopped(stopped=True),
                    self.wait(0.2, 's'),


                    rot = 0
                    x = -0.1
                    y = 0
                    d = 0.2

                    # pytaVSL.set(baron, 'sequence_mode', 1)

                    # pytaVSL.animate('f_ch4-22_warpzoned', 'scale', [1, 0], [1, 1], d, 's', 'elastic-inout')

                else:
                    rot = 0
                    x = 0
                    y = 0
                    d = 0.5


                pytaVSL.set(baron, 'sequence_mode', 1)
                pytaVSL.animate(baron, 'rotate_z', None, rot, d, 's'),
                pytaVSL.animate(baron, 'position_y', None, y, d, 's'),
                pytaVSL.animate(baron + '*', 'rgbwave', None, 0, d, 's'),
                pytaVSL.animate(baron + '*', 'noise', None, 0, d, 's'),
                pytaVSL.animate(baron, 'position_x', None, x, d,  's'),
                pytaVSL.animate(baron + '*', 'alpha', None, 0, d, 's'),
                pytaVSL.animate(baron, 'offset_x', None, 0, d, 's'),
                pytaVSL.animate(baron, 'offset_y', None, 0, d, 's'),
                self.wait(d, 's'),
                pytaVSL.set(baron, 'visible', 0)
                pytaVSL.set(baron, 'sequence_mode', 0)
                pytaVSL.set(baron + '*', 'alpha', 1)



            ### Init
            pytaVSL.animate('*_blanc', 'offset_y', 0, 0.01, 1, 's', 'random-mirror'),
            pytaVSL.animate('*_blanc', 'offset_x', 0, 0.01, 1, 's', 'random-mirror'),
            self.start_sequence('sequence/animate_legs_doah', [
                {
                    'signature' : '1/16',
                    1: lambda: pytaVSL.set('doah_b', 'sequence_index', 0),
                    1 + 1/16: lambda: pytaVSL.set('doah_b', 'sequence_index', 1),
                    1 + 2/16: lambda: pytaVSL.set('doah_b', 'sequence_index', 0),
                    1 + 3/16: lambda: pytaVSL.set('doah_b', 'sequence_index', 1),
                    }
            ], loop=True),
            self.start_sequence('sequence/animate_legs_saladin', [
                {
                    'signature' : '1/16',
                    1: lambda: pytaVSL.set('saladin_b', 'sequence_index', 0),
                    1 + 1/16: lambda: pytaVSL.set('saladin_b', 'sequence_index', 1),
                    1 + 2/16: lambda: pytaVSL.set('saladin_b', 'sequence_index', 0),
                    1 + 3/16: lambda: pytaVSL.set('saladin_b', 'sequence_index', 1),
                    }
            ], loop=True),
            pytaVSL.set('*_blanc*', 'rgbwave', 0.4),
            pytaVSL.set('*_blanc*', 'noise', -0.3),
            pytaVSL.set('doah_b*', 'visible', 1),
            pytaVSL.set('saladin_b*', 'visible', 1),

            ### Doah
            doah()
            self.wait(r_period / 2, 's'),

            ### Saladin
            saladin()

            self.wait(r_period * 0.5, 's'),

            pytaVSL.set('bato_h', 'scale', 1, 1)
            pytaVSL.set('bato_h', 'position_y', 0.35)
            pytaVSL.batohacker_hmove(0.4, duration = r_period/2)
            mainSampler.batohacker_move(r_period/2), ### AUDIO
            self.wait(r_period/2, 's')
            baron_fall('doah_b')

            self.wait(2 *r_period, 's')

            ### Chute des Barons
            baron_fall('saladin_b')
            self.wait(2 * r_period + 0.5 + 0.2, 's')

            ### Champignons
            pytaVSL.animate('t_nobh_champignon*_glow', 'alpha', 0.3, 1, 1, 's', 'random-mirror', loop=True),
            mush_fall(1, 1.5)
            self.wait(0.2, 's')
            pytaVSL.batohacker_hmove(0.3, duration=0.5)
            mainSampler.batohacker_move(0.5), ### AUDIO
            self.wait(0.5, 's')
            mush_fall(2, 1)

            self.wait(0.5, 's')
            pytaVSL.batohacker_hmove(1, proue='cour', duration=0.5)
            mainSampler.batohacker_move(0.5), ### AUDIO
            self.wait(0.5, 's')
            baron_grow('doah_b')
            baron_grow('saladin_b')
            pytaVSL.stop_animate('bt', 'position_x')
            pytaVSL.stop_animate('t_nobh_entovillon', 'position_x')
            pytaVSL.stop_animate('ep_halo', 'alpha')
            self.wait(0.7, 's')
            pytaVSL.animate('bt', 'position_x', 0.5, 0.54, 0.8, 's', 'elastic-mirror', loop=True),
            pytaVSL.animate('t_nobh_entovillon', 'position_x', 0.45, 0.49, 0.8, 's', 'elastic-mirror', loop=True),
            pytaVSL.animate('ep_halo', 'alpha', 0.3, 1, 1.5, 's', 'linear-mirror', loop=True),
            baron_inside('doah_b')
            pytaVSL.batohacker_hmove(0.38, duration = 0.4)
            mainSampler.batohacker_move(0.4), ### AUDIO
            self.wait(0.4, 's')
            pytaVSL.batohacker_debevier()
            mainSampler.batohacker_debevier(), ### AUDIO
            self.wait(0.4, 's')
            pytaVSL.animate('bato_h', 'position_x', None, 0.48, 1)
            pytaVSL.animate('f_ilm', 'position_x', None, 0, 1)
            pytaVSL.set('saladin_b', 'sequence_mode', 1) # Tentative
            self.wait(0.6, 's')
            baron_inside('saladin_b')
            self.wait(0.5, 's')
            pytaVSL.batohacker_hmove(1, proue='cour', duration = 0.4)
            mainSampler.batohacker_move(0.4, goto='departure'), ### AUDIO
            pytaVSL.animate('f_ch4-22_warpzoned', 'scale', [1, 1], [1, 0], 0.5, 's', 'elastic-inout')
            pytaVSL.animate('f_ch4-22_warpzoned', 'rgbwave', None, 0, 0.5, 's')
            pytaVSL.animate('f_ch4-22_warpzoned', 'fish', None, 0, 0.5, 's')


        def f_ch4_22_ep():
            self.start_scene('sequence/f_ch4_22_ep', lambda: [
                pytaVSL.animate('bt_main', 'brightness', None, 1, 0.2, 's'),
                pytaVSL.animate('*ep_*', 'brightness', None, 1, 0.2, 's'),
                pytaVSL.animate('bt', 'position_x', None, 0.46, 0.5, 's', 'elastic-out'),
                self.wait(0.3, 's'),
                pytaVSL.bascule_ep(),
                mainSampler.bascule_ep(), ### AUDIO
                pytaVSL.animate('enedys_p', 'rotate_z', None, 0, 0.5, 's'),
                self.wait(0.5, 's'),
                pytaVSL.face_ep('reveil'),

                self.wait(3, 's'),
                pytaVSL.face_ep('non'),

                self.wait(0.3, 's'),
                pytaVSL.getwhistle_ep(duration = 0.1),

                self.wait(0.8, 's'),
                pytaVSL.face_ep('sifflet'),
                pytaVSL.allume_ep(),
                mainSampler.allume_ep(on='alarm'), ### AUDIO
                pytaVSL.animate('bt', 'position_x', None, 0.41, 0.8, 's', 'elastic-mirror', loop=True),
                pytaVSL.animate('t_nobh_entovillon', 'position_x', None, 0.36, 0.8, 's', 'elastic-mirror', loop=True),
            ])



        self.start_scene('sequence/f_ch4_22', lambda: [
            f_ch4_22_ep(),

            ### Batohacker drop the entovillon
            pytaVSL.batohacker_shaking(),
            pytaVSL.set('bato_h', 'position_y', 0.2),
            pytaVSL.batohacker_hmove(0.47, duration=0.5),
            mainSampler.batohacker_move(0.5), ### AUDIO
            self.wait(0.6, 's'),
            pytaVSL.batohacker_punch(f_ch='f_ch4-21', x_dest = -0.15),
            mainSampler.batohacker_punch(), ### AUDIO
            self.wait(0.4,'s'),
            pytaVSL.batohacker_hmove(0.4, duration=0.2),
            mainSampler.batohacker_move(0.2), ### AUDIO
            self.wait(0.2, 's'),
            pytaVSL.animate('t_nobh_entovillon', 'position_y', 0.1, -0.221, 0.3, 's'),
            pytaVSL.set('t_nobh_entovillon', 'visible', 1),
            self.wait(0.1, 's'),
            pytaVSL.animate('t_nobh_entovillon', 'scale', None, [0.3, 0.3], 0.2, 's', 'elastic-inout'),
            pytaVSL.batohacker_hmove(1, proue='cour'),
            mainSampler.batohacker_move(goto='departure'), ### AUDIO
            self.wait(0.2, 's'),
            # pytaVSL.face_ep('non'),
            self.wait(0.3, 's'),

            ### Barons jump Inside
            arrivees_barons(),

            ### Tir à l'arc (début de décompte)
            self.wait(7, 's'),
            pytaVSL.set('f_ch4-22', 'video_speed', 0),
            pytaVSL.set('f_ch4-22_letterwaiting', 'visible', 1),
        ])

    @pedalboard_button(5006, who='adrien', after='f_ch4_22')
    def f_ch4_22_fin(self):
        """
        Flêche
        """
        def panique_ep():
            pytaVSL.animate('bt', 'position_x', None, 1, 1, 's', 'elastic-inout'),
            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, 0.95, 1, 's', 'elastic-inout'),


            def pan(duration, coef):

                ### AUDIO
                mainSampler.send('/instrument/stop', 's:alarm_ep'),


                y = 0.8*(_rand() - 0.5)
                x = coef * (0.06 * _rand() + 0.41)

                n = round(_rand()*2) + 2

                pytaVSL.set('bt', 'scale', coef * abs(pytaVSL.get('bt', 'scale')[0]), pytaVSL.get('bt', 'scale')[1], force_send=True),
                pytaVSL.set('t_nobh_entovillon', 'scale', coef * pytaVSL.get('t_nobh_entovillon', 'scale')[0], pytaVSL.get('t_nobh_entovillon', 'scale')[1], force_send=True),

                pytaVSL.set('bt', 'position_y', y),
                pytaVSL.set('t_nobh_entovillon', 'position_y', y + 0.179),
                pytaVSL.animate('bt', 'position_x', coef * 0.5, x, 0.7, 's', 'elastic-mirror', loop = True),
                pytaVSL.animate('t_nobh_entovillon', 'position_x', coef * 0.45, x - coef * 0.05, 0.7, 's', 'elastic-mirror', loop = True),

                ### AUDIO
                self.wait(0.1, 's'),
                mainSampler.send('/instrument/play', 's:alarm_ep'),

            def panic():
                while True:
                    if _rand() > 0.5:
                        coef = 1
                    else:
                        coef = -1

                    duration = 2 * _rand() + 2
                    self.wait(duration / 4, 's')
                    pan(duration, coef)
                    self.wait(duration, 's')


                    pytaVSL.animate('bt', 'position_x', None, coef*1, 1, 's', 'elastic-inout'),
                    pytaVSL.animate('t_nobh_entovillon', 'position_x', None, coef*0.95, 1, 's', 'elastic-inout'),
                    self.wait(1, 's')
                    ### AUDIO
                    mainSampler.send('/instrument/stop', 's:alarm_ep'),


            self.start_scene('sequence/panique_ep', panic)

        t_fleche_son = 11.85 #12
        t_fleche_img = 11.8
        self.start_scene('sequence/f_ch4_22_fin', lambda: [

            pytaVSL.set('f_ch4-22', 'video_speed', 1),
            pytaVSL.set('f_ch4-22_letterwaiting', 'visible', 0),
            ### Tir à l'arc
            self.wait(t_fleche_img, 's'),

            # self.wait(19, 's'),
            self.stop_sequence('sequence/animate_legs*'),
            pytaVSL.set('f_ch4-21', 'visible', 0, force_send=True),
            # pytaVSL.face_ep('non'), ### Est-ce que c'est ça qui enlève le casque ?
            # self.wait(1, 's'),
            # pytaVSL.set('ep_sifflet_*', 'visible', 0),
            pytaVSL.animate('f_ch4-22', 'fish', 0, 0.8, 1, 's', 'elastic-mirror'), # avant, c'était sur f_ilm
            #### Si ça buggue parfois, passer fish sur f_ch4-21
            self.wait(0.5, 's'),
            panique_ep(),
            self.wait(6, 's'),
            self.f_ch4_22_waiting()

        ])

        ### AUDIO
        self.start_scene('sequence/f_ch4_22_fin_audio', lambda: [
            self.wait(t_fleche_son, 's'),
            # self.wait(1, 's'),
            mainSampler.send('/instrument/play', 's:f_ch4-22_fleche', 100),
            mainSampler.send('/instrument/stop', 's:jmjarret_entracte'),
            self.wait(8, 's'),
            mainSampler.send('/instrument/play', 's:f_ch4-22_snares', 100),
        ])


    @pedalboard_button(118)
    def button_f_ch4_22_waiting(self):
        self.f_ch4_22_waiting()

    @pedalboard_button()
    def f_ch4_22_waiting(self):
        """
        En attendant la grenade
        """
        pytaVSL.set('f_ch4-22_waiting', 'visible', 1)
        pytaVSL.set('f_ch4-22', 'visible', 0),

        def midtime_highlight():
            ve = pytaVSL.get('f_ch4-22_waiting', 'video_end')
            index = 0
            while True:
                self.wait(ve / 2 - 0.2, 's')
                self.f_ch4_22_waiting_lock = 0
                self.wait(0.4, 's')
                self.f_ch4_22_waiting_lock = 1
                self.wait(ve / 2 - 0.2, 's')
                # Tentative de recalage régulier pour éviter la dérive temporelle
                index = index + 1
                if index > 4:
                    pytaVSL.set('f_ch4-22_waiting', 'video_time', 0)
                    index = 0


        self.start_scene('sequence/midtime_highlight', midtime_highlight)

    @pedalboard_button(5007, who='adrien', after='f_ch4_22_waiting')
    def f_ch4_23(self):
        """
        Arrivée de la grenade
        """
        mainSampler.send('/instrument/play', 's:f_ch4-22_snares_out', 100),
        mainSampler.send('/instrument/stop', 's:f_ch4-22_snares'),
        def attente_regard_and_go():
            while True:
                if self.f_ch4_22_waiting_lock:
                    self.wait(0.1, 's')
                else:
                    pytaVSL.set('f_ch4-23', 'visible', 1)
                    pytaVSL.set('f_ch4-23_warpzoned', 'visible', 1)
                    pytaVSL.set('f_ch4-23_warpzoned', 'audio_volume', 0)
                    pytaVSL.set('f_ch4-22_waiting', 'visible', 0)
                    ### AUDIO
                    self.start_scene('sequence/grenade_coming', lambda: [
                        mainSampler.send('/instrument/play', 's:f_ch4_bouilloire_start', 100),
                        self.wait(1 + 11 / 30),
                        mainSampler.send('/instrument/play', 's:f_ch4_bouilloire_middle', 100),
                        mainSampler.send('/instrument/stop', 's:f_ch4_bouilloire_start'),
                    ])
                    break

            self.stop_scene('sequence/panique_ep')
            if abs(pytaVSL.get('bt', 'position_x')) < 0.8:
                coef = pytaVSL.get('bt', 'position_x')/abs(pytaVSL.get('bt', 'position_x'))
                pytaVSL.animate('bt', 'position_x', None, coef*1, 0.8, 's', 'elastic-inout'),
                pytaVSL.animate('t_nobh_entovillon', 'position_x', None, coef*0.95, 0.8, 's', 'elastic-inout'),
                self.wait(0.8, 's')

            # Init nouvelle place
            pytaVSL.set('bt', 'visible', 0)
            pytaVSL.set('t_nobh_entovillon', 'visible', 0)
            pytaVSL.set('bt', 'scale', -0.2, 0.2)
            pytaVSL.set('t_nobh_entovillon', 'scale', 0.3, 0.3)
            pytaVSL.set('bt', 'rotate_z', -90)
            pytaVSL.set('t_nobh_entovillon', 'rotate_z', -90)
            pytaVSL.set('bt', 'position_y', 0.35)
            pytaVSL.set('t_nobh_entovillon', 'position_y', 0.27)

            # Entrée
            pytaVSL.animate('bt', 'position_x', -0.7, -0.46, 4, 's')
            pytaVSL.animate('t_nobh_entovillon', 'position_x', -0.6, -0.36, 4, 's')
            pytaVSL.animate('bt', 'position_y', 0.25, 0.35, 4, 's')
            pytaVSL.animate('t_nobh_entovillon', 'position_y', 0.17, 0.27, 4, 's')
            pytaVSL.animate('bt', 'offset_y', 0.01, 0, 4, 's', 'random')
            pytaVSL.animate('t_nobh_entovillon', 'offset_y', 0.01, 0, 4, 's', 'random')
            pytaVSL.set('bt', 'visible', 1)
            pytaVSL.set('t_nobh_entovillon', 'visible', 1)

            # Arrivée grenade
            self.wait(5, 's')
            mainSampler.send('/instrument/play', 's:tuba_out', 100), ### AUDIO
            pytaVSL.animate('bt', 'offset_x', 0, -0.005, 0.2, 's', 'random-mirror')
            pytaVSL.animate('t_nobh_entovillon', 'offset_x', 0, -0.005, 0.2, 's', 'random-mirror')

            # Sortie EP
            # self.wait(0.5, 's')
            self.wait(8, 's')
            pytaVSL.animate('bt', 'position_x', None, -0.47, 0.2, 's', 'elastic-inout')
            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, -0.37, 0.1, 's'), #, 'elastic-inout')
            self.wait(3, 's')

            pytaVSL.animate('bt', 'position_x', None, -0.7, .2, 's', 'elastic-inout')
            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, -0.6, 0.2, 's', 'elastic-inout')
            self.wait(1, 's')

            # Reinit
            pytaVSL.set('bt', 'visible', 0)
            pytaVSL.set('t_nobh_entovillon', 'visible', 0)
            pytaVSL.set('bt', 'scale', 0.2, 0.2)
            pytaVSL.set('t_nobh_entovillon', 'scale', 0.3, 0.3)
            pytaVSL.set('bt', 'rotate_z', 0)
            pytaVSL.set('t_nobh_entovillon', 'rotate_z', 0)
            pytaVSL.set('bt', 'position_y', -0.4)
            pytaVSL.set('t_nobh_entovillon', 'position_y', -0.221)
            mainSampler.send('/instrument/stop', 's:alarm_ep')

            # Traversée & init bato_h
            self.wait(4, 's')
            # self.wait(14, 's')
            pytaVSL.set('bato_h', 'scale', 0.3, 0.3)
            pytaVSL.set('bato_h', 'position_y', -0.25)
            pytaVSL.set('t_bh_megavillon*', 'visible', 1)
            pytaVSL.set('t_bh_sacvide', 'visible', 1)
            self.start_sequence('sequence/flapsac', [
            {
                'signature': '1/16',
                1: lambda: pytaVSL.set('t_bh_sacvide', 'sequence_index', 0),
                1 + 1/32: lambda: pytaVSL.set('t_bh_sacvide', 'sequence_index', 1),
                1 + 2/32: lambda: pytaVSL.set('t_bh_sacvide', 'sequence_index', 2),
                1 + 3/32: lambda: pytaVSL.set('t_bh_sacvide', 'sequence_index', 1),
            }
            ], loop=True)
            pytaVSL.animate('bato_h', 'position_x', 1, -1.2, 2, 's')
            self.wait(0.7, 's')
            pytaVSL.animate('bt', 'position_x', None, 0.5, 0.8, 's', 'elastic-inout')
            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, 0.45, 0.8, 's', 'elastic-inout')
            self.wait(0.8, 's')
            pytaVSL.animate('bt', 'position_x', 0.5, 0.54, 0.8, 's', 'elastic-mirror', loop=True),
            mainSampler.send('/instrument/play', 's:alarm_ep'),
            pytaVSL.animate('t_nobh_entovillon', 'position_x', 0.45, 0.49, 0.8, 's', 'elastic-mirror', loop=True),
            pytaVSL.set('bt', 'visible', 1)
            pytaVSL.set('t_nobh_entovillon', 'visible', 1)

            self.wait(6.3, 's')
            pytaVSL.stop_scene('sequence/flapsac')
            pytaVSL.set('t_bh_sacvide', 'visible', 0)
            pytaVSL.set('t_nobh_sacplein*', 'visible', 1)
            pytaVSL.set('bato_h', 'scale', -1.2, 1.2)
            pytaVSL.set('bato_h', 'position_y', 0.285)
            pytaVSL.animate('bato_h', 'position_x', None, -0.97, 1, 's', 'elastic-inout')
            pytaVSL.animate('bato_h', 'offset_y', 0, 0.001, 1, 's', 'random-mirror')
            pytaVSL.animate('t_nobh_sacplein*', 'position_x', None, -0.956, 1, 's', 'elastic-inout')
            pytaVSL.animate('t_nobh_sacplein*', 'offset_y', 0, 0.001, 1, 's', 'random-mirror')
            pytaVSL.set('f_ch4-23_warpzoned', 'rgbwave', 0.4)
            pytaVSL.set('f_ch4-23_warpzoned', 'fish', 0.2)
            pytaVSL.set('f_ch4-23_warpzoned', 'mask', 'f_ch4-23_mask')
            self.wait(0.5, 's')
            pytaVSL.animate('f_ch4-23_warpzoned', 'scale', [1, 0], [1, 1], 0.5, 's', 'elastic-inout')


            # On emmène les BF
            self.wait(3.5, 's')

            pytaVSL.batohacker_hmove(-0.4, 'cour', sacplein=True)
            mainSampler.batohacker_move(), ### AUDIO

            self.wait(0.4, 's')
            pytaVSL.animate('t_bh_megavillon', 'rotate_z', None, -80, 0.6, 's', 'elastic-inout')
            pytaVSL.animate('t_nobh_sacplein*', 'rotate_z', None, 80, 0.6, 's', 'elastic-inout')
            pytaVSL.animate('t_nobh_sacplein*', 'position_x', None, -0.4 + 0.2, 0.6, 's', 'elastic-inout')
            pytaVSL.animate('t_nobh_sacplein*', 'position_y', None, -0.1, 0.6, 's', 'elastic-inout')
            pytaVSL.animate('f_ch4-23_warpzoned', 'scale', [1, 1], [1, 0], 0.5, 's', 'elastic-inout')
            mainSampler.send('/instrument/play', 's:f_ch3_degagement_paillasson', 100), ### AUDIO

            pytaVSL.animate('t_nobh_sacplein*', 'offset_y', 0, -0.01, 2, 's', 'random-mirror', loop=True)
            pytaVSL.animate('t_nobh_sacplein*', 'scale', [-0.5, 0.5], [-0.51, 0.51], 4, 's', 'random-mirror', loop=True)
            pytaVSL.animate('bato_h', 'offset_y', 0, -0.02, 1, 's', 'random-mirror', loop=True)

            self.wait(1, 's')
            pytaVSL.set('f_ch4-23_warpzoned', 'rgbwave', 0)
            pytaVSL.set('f_ch4-23_warpzoned', 'fish', 0)
            pytaVSL.set('f_ch4-23_warpzoned', 'mask', '')

            pytaVSL.animate('bato_h', 'position_x', None, -0.5, 1, 's')
            pytaVSL.animate('t_nobh_sacplein*', 'position_x', None, -0.5 + 0.2, 1, 's')
            pytaVSL.animate('bh_jambe_gauche', 'offset_y', 0, 0.04, 1, 's', loop=True, easing='random-mirror'),
            pytaVSL.animate('bh_jambe_droite', 'offset_y', 0, 0.04, 1, 's', loop=True, easing='random-mirror'),
            pytaVSL.animate('bh_jambe_gauche', 'offset_x', 0, -0.02, 1, 's',  loop=True, easing='random-mirror'),
            pytaVSL.animate('bh_jambe_droite', 'offset_x', 0, 0.02, 1, 's',  loop=True, easing='random-mirror'),

            # Déplacement
            self.wait(1, 's')
            mainSampler.send('/instrument/play', 's:f_ch4_truck_beep', 100), ### AUDIO
            pytaVSL.animate('bato_h', 'position_x', None, 0.35, 3, 's')
            pytaVSL.animate('t_nobh_sacplein*', 'position_x', None, 0.45, 3, 's')
            pytaVSL.animate('bato_h', 'position_y', None, 0.3, 3, 's')
            pytaVSL.animate('t_nobh_sacplein*', 'position_y', None, 0.105, 3, 's')
            pytaVSL.animate('bato_h', 'scale', None, [-0.6, 0.6], 3, 's')
            pytaVSL.animate('t_nobh_sacplein*', 'scale', None, [-0.25, 0.25], 3, 's')
            pytaVSL.send('/pyta/slide/{bato_h,t_nobh_sacplein*}/animate', 'offset_y', 0, 0.01, 3, 'random')

            # Largage
            self.wait(3.2, 's')
            mainSampler.send('/instrument/stop', 's:f_ch4_truck_beep'), ### AUDIO
            mainSampler.send('/instrument/play', 's:f_ch4_truck_open', 100), ### AUDIO
            pytaVSL.animate('{t_bh_megavillon,t_nobh_sacplein*}', 'offset_y', 0, 0.01, 0.3, 's', 'random-mirror')
            self.wait(0.2, 's')
            mainSampler.send('/instrument/play', 's:f_ch4_truck_empty', 100), ### AUDIO
            pytaVSL.animate('t_nobh_sacplein*', 'position_y', None, -0.09, 0.2, 's')
            self.wait(0.05, 's')
            pytaVSL.set('t_nobh_sacplein_front', 'visible', 0)
            self.wait(0.15, 's')
            pytaVSL.animate('t_nobh_sacplein*', 'position_y', None, -0.12, 0.08, 's')
            pytaVSL.animate('t_nobh_sacplein*', 'scale', None, [0, 0.25], 0.05, 's')
            pytaVSL.batohacker_hmove(1, 'cour')
            mainSampler.batohacker_move(goto='departure'), ### AUDIO


            # Extinction de EP + Chapitre suivant
            qlcplus.set_strobe('bt_centrale', on=False)

            pytaVSL.animate('bt', 'position_x', None, 0.525, 1, 's')
            pytaVSL.animate('t_nobh_entovillon', 'position_x', None, 0.475, 1, 's')
            pytaVSL.allume_ep(on=False)
            mainSampler.allume_ep(on=False), ### AUDIO
            pytaVSL.face_ep('reveil')
            pytaVSL.bascule_ep(courrier=False)
            mainSampler.bascule_ep(), ### AUDIO


            pytaVSL.trijc_io('in', 'lustre')
            mainSampler.trijc_io('in'), ### AUDIO
            self.wait(1, 's'),
            pytaVSL.trijc_turn_lights('off', 1),
            mainSampler.trijc_turn_lights(1), ### AUDIO
            pytaVSL.animate('f_*', 'alpha', None, 0, 1),
            self.wait(1.2, 's'),
            pytaVSL.set('f_ilm', 'visible', 0),
            pytaVSL.set('f_ch4-23', 'visible', 0),
            pytaVSL.set('f_*', 'alpha', 1),
            pytaVSL.trijc_io('out', 'lustre')



        self.start_scene('sequence/attente_regard_and_go', attente_regard_and_go)

    @pedalboard_button(5008, who='orl', after='f_ch4_23')
    def final(self):
        ##### TODO En fait, devrait être déclenché au pied ?
        def finito():
            self.fin = _time()
            self.logger.info('Fin @ ' + str(self.fin) + ' secondes')
            duree = self.fin - self.debut
            self.logger.info('Durée : ' + str(round(duree/60)) + ' minutes')



        # pytaVSL.trijc_turn_lights()
        pytaVSL.trijc_turn_lights('off', 1),
        mainSampler.trijc_turn_lights(), ### AUDIO
        pytaVSL.animate('f_*', 'alpha', None, 0, 1),
        self.wait(1.2, 's'),
        pytaVSL.set('f_ilm', 'visible', 0),
        pytaVSL.set('f_ch4-23', 'visible', 0),
        pytaVSL.set('f_*', 'alpha', 1),
        engine.set_route('Chapitre 5'),
        finito()
