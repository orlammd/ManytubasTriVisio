#!/bin/bash
killall -9 jackd ray-daemon raysession pulseaudio pipewire* jack_wait python3
# jack_control stop

if [ "$1" = "debug" ]
then
    echo "Debug"
    ray-daemon --debug -r /home/jeaneudes/OrageOTournage/ManytubasTriVisio/RaySessions -s Manytubas -p 2000 > /home/jeaneudes/OrageOTournage/log/startManytubas-raydaemon.log 2>&1 &
else
    ray-daemon -r /home/jeaneudes/OrageOTournage/ManytubasTriVisio/RaySessions -s Manytubas -p 2000 > /home/jeaneudes/OrageOTournage/log/startManytubas-raydaemon.log 2>&1 &
fi

raysession -r /home/jeaneudes/OrageOTournage/ManytubasTriVisio/RaySessions/ -p 2000 &
